module.exports = {
    purge: {
        enabled: true,
        content: [
            './resources/views/**/*.blade.php',
            './vendor/uncgits/ccps-core/src/views/**/*.blade.php',
            './vendor/uncgits/ccps-user-feed/src/views/**/*.blade.php',
            './vendor/uncgits/ccps-core/src/View/Components/*.php'
        ],
        options: {
            whitelistPatterns: [
                /^text-[a-z0-9-]+$/,
                /^bg-[a-z0-9-]+$/,
                /^border-[a-z0-9-]+$/,
                /^opacity-[0-9-]+$/,
                /^disabled:opacity-[0-9-]+$/,

            ]
        }

    },
    theme: {
        extend: {},
    },
    variants: {
        opacity: ['responsive', 'hover', 'focus', 'active', 'group-hover', 'disabled'],
        cursor: ['responsive', 'hover', 'focus', 'active', 'group-hover', 'disabled']
    },
    plugins: [],
}
