<?php

namespace App\Seeders;

use App\CcpsCore\Permission;
use Illuminate\Support\Facades\App;
use Uncgits\Ccps\Exceptions\InvalidSeedDataException;

use Uncgits\Ccps\Seeders\CcpsValidatedSeeder;

class LmsPermissionsTableSeeder extends CcpsValidatedSeeder
{
    public $permissions = [
        // fill in permissions here
        [
           "name"         => "canvasfeed.view",
           "display_name" => "Canvas Feed - View",
           "description"  => "View information on the Canvas Feed",
        ],
        [
           "name"         => "canvasfeed.edit",
           "display_name" => "Canvas Feed - Edit",
           "description"  => "Edit and configure the Canvas Feed",
        ],

    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $writeConsoleOutput = App::runningInConsole();

        if ($writeConsoleOutput) {
            // get console output
            $output = $this->command->getOutput();
        }

        // validate
        try {
            $this->validateSeedData($this->permissions, $this->permissionArrayConstruction);
            $this->checkForExistingSeedData($this->permissions, Permission::all());

            $mergeData = [
                'source_package' => 'app',
                'created_at'     => date("Y-m-d H:i:s", time()),
                'updated_at'     => date("Y-m-d H:i:s", time()),
                'editable'       => 0
            ];

            $this->commitSeedData($this->permissions, 'ccps_permissions', $mergeData);
        } catch (InvalidSeedDataException $e) {
            if ($writeConsoleOutput) {
                $output->error($e->getMessage());
                return;
            }
        }
    }
}
