<?php

use App\CcpsCore\CronjobMeta;
use Illuminate\Database\Migrations\Migration;

class RemoveSisReportCronjobs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $metasToRemove = [
            'App\Cronjobs\Sis\StartSisExportReport', 'App\Cronjobs\Sis\GetSisExportReportStatus', 'App\Cronjobs\Sis\DownloadSisExportReport', 'App\Cronjobs\Sis\ExtractSisExportReportData', 'App\Cronjobs\Sis\ImportSisExportReportDataToDatabase'
        ];

        CronjobMeta::whereIn('class', $metasToRemove)->delete();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::transaction(function () {
            $metasToAdd = [
                'App\Cronjobs\Sis\StartSisExportReport', 'App\Cronjobs\Sis\GetSisExportReportStatus', 'App\Cronjobs\Sis\DownloadSisExportReport', 'App\Cronjobs\Sis\ExtractSisExportReportData', 'App\Cronjobs\Sis\ImportSisExportReportDataToDatabase'
            ];

            foreach ($metasToAdd as $className) {
                CronjobMeta::create([
                    'class'  => $className,
                    'status' => 'disabled'
                ]);
            }
        });
    }
}
