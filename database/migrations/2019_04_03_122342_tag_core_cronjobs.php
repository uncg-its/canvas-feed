<?php

use App\CcpsCore\CronjobMeta;
use Illuminate\Database\Migrations\Migration;

class TagCoreCronjobs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $ccpsUserFeedCrons = [
            'Uncgits\\Ccps\\UserFeed\\Cronjobs\\FetchNewUserDataBatch',
            'Uncgits\\Ccps\\UserFeed\\Cronjobs\\ExpandUserDataFromBatch',
            'Uncgits\\Ccps\\UserFeed\\Cronjobs\\CreateUserDataDiff',
            'Uncgits\\Ccps\\UserFeed\\Cronjobs\\ExpandUserDataChangesFromDiff',
            'Uncgits\\Ccps\\UserFeed\\Cronjobs\\PruneOldDiffsAndBatches',
        ];

        CronjobMeta::whereIn('class', $ccpsUserFeedCrons)->update(['tags' => 'ccps-user-feed,core']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
