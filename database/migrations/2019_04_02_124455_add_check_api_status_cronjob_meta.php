<?php

use App\CcpsCore\CronjobMeta;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddCheckApiStatusCronjobMeta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
            CronjobMeta::create([
                'class'  => 'App\Cronjobs\CheckApiStatus',
                'status' => 'disabled'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $meta = CronjobMeta::where('class', 'App\Cronjobs\CheckApiStatus')->delete();
    }
}
