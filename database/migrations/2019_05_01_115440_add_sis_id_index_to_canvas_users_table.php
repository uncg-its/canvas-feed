<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSisIdIndexToCanvasUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('canvas_users', function (Blueprint $table) {
            $table->string('sis_id')->change();
            $table->index('sis_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('canvas_users', function (Blueprint $table) {
            $table->text('sis_id')->change();
            $table->dropIndex('sis_id');
        });
    }
}
