<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSisDiffsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sis_diffs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('old_zip_id');
            $table->unsignedBigInteger('new_zip_id');
            $table->string('filename');
            $table->unsignedTinyInteger('zip_csv_count')->nullable();
            $table->string('status');

            $table->unsignedMediumInteger('accounts')->default(0);
            $table->unsignedMediumInteger('courses')->default(0);
            $table->unsignedMediumInteger('enrollments')->default(0);
            $table->unsignedMediumInteger('groups')->default(0);
            $table->unsignedMediumInteger('groups_membership')->default(0);
            $table->unsignedMediumInteger('sections')->default(0);
            $table->unsignedMediumInteger('terms')->default(0);
            $table->unsignedMediumInteger('users')->default(0);
            $table->unsignedMediumInteger('xlists')->default(0);
            $table->unsignedMediumInteger('total')->default(0);

            $table->unsignedBigInteger('canvas_id')->nullable();

            $table->timestamp('created_at')->nullable();
            $table->timestamp('uploaded_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sis_diffs');
    }
}
