<?php

use Uncgits\Ccps\UserFeed\Alarm;
use Illuminate\Database\Migrations\Migration;

class DefineDefaultAlarms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::beginTransaction();

        Alarm::create([
            'name'          => 'Batch: contains min expected records',
            'class'         => 'BatchContainsExpectedNumberOfSourceRecords',
            'cronjob_class' => 'FetchNewUserDataBatch',
            'active'        => '1',
            'log_channel'   => 'aux-feed',
            'log_level'     => 'warning',
        ]);

        Alarm::create([
            'name'          => 'Diff: does not exceed max allowed changes',
            'class'         => 'DiffChangesFewerThanMaxAllowed',
            'cronjob_class' => 'CreateUserDataDiff',
            'active'        => '1',
            'log_channel'   => 'aux-feed',
            'log_level'     => 'warning',
        ]);

        \DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Alarm::whereIn('class', ['BatchContainsExpectedNumberOfSourceRecords', 'DiffChangesFewerThanMaxAllowed'])
            ->delete();
    }
}
