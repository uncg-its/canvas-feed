<?php

use App\CcpsCore\NotificationEvent;
use Illuminate\Database\Migrations\Migration;

class AddAlarmCheckFailedNotificationEvent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::transaction(function () {
            NotificationEvent::create([
                'display_name'       => 'AlarmCheckFailed',
                'description'        => 'AlarmCheckFailed',
                'event_class'        => 'App\\Events\\Aux\\UserFeedAlarmCheckFailed',
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $event = NotificationEvent::where('event_class', 'App\\Events\\Aux\\UserFeedAlarmCheckFailed')->delete();
    }
}
