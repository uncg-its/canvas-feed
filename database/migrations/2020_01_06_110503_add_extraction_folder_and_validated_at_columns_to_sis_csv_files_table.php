<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddExtractionFolderAndValidatedAtColumnsToSisCsvFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // the original migration was edited at one point instead of adding another one. thus these checks are necessary.
        if (! Schema::hasColumn('sis_csv_files', 'extraction_folder')) {
            Schema::table('sis_csv_files', function (Blueprint $table) {
                $table->rename('temp_path', 'extraction_folder');
            });
        }

        if (! Schema::hasColumn('sis_csv_files', 'validated_at')) {
            Schema::table('sis_csv_files', function (Blueprint $table) {
                $table->timestamp('validated_at')->nullable()->after('imported_at');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // let's not do this.

        // Schema::table('sis_csv_files', function (Blueprint $table) {
        //     $table->dropColumn('extraction_folder');
        //     $table->dropColumn('validated_at');
        // });
    }
}
