<?php

use App\CcpsCore\CronjobMeta;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddInitiateNewRunCronjobMeta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
            CronjobMeta::create([
                'class'  => 'App\Cronjobs\Aux\InitiateNewRun',
                'tags'   => 'aux-feed,core',
                'status' => 'disabled'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $meta = CronjobMeta::where('class', 'App\Cronjobs\Aux\InitiateNewRun')->delete();
    }
}
