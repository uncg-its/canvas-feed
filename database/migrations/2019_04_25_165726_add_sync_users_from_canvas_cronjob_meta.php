<?php

use App\CcpsCore\CronjobMeta;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddSyncUsersFromCanvasCronjobMeta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
            CronjobMeta::create([
                'class'  => 'App\Cronjobs\Aux\SyncUsersFromCanvas',
                'status' => 'disabled'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $meta = CronjobMeta::where('class', 'App\Cronjobs\Aux\SyncUsersFromCanvas')->delete();
    }
}
