<?php

use App\CcpsCore\NotificationEvent;
use Illuminate\Database\Migrations\Migration;

class AddInvalidDiffDetectedNotificationEvent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::transaction(function () {
            NotificationEvent::create([
                'display_name'       => 'InvalidDiffDetected',
                'description'        => 'InvalidDiffDetected',
                'event_class'        => 'App\\Events\\Aux\\DiffFailedValidation',
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $event = NotificationEvent::where('class', 'App\\Events\\Aux\\DiffFailedValidation')->delete();
    }
}
