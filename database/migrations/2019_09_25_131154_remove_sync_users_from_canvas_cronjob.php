<?php

use App\CcpsCore\CronjobMeta;
use Illuminate\Database\Migrations\Migration;

class RemoveSyncUsersFromCanvasCronjob extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        CronjobMeta::where('class', 'App\Cronjobs\Aux\SyncUsersFromCanvas')->delete();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::transaction(function () {
            CronjobMeta::create([
                'class'  => 'App\Cronjobs\Aux\SyncUsersFromCanvas',
                'status' => 'disabled'
            ]);
        });
    }
}
