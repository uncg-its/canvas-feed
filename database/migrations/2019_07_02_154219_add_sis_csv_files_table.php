<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSisCsvFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::create('sis_csv_files', function (Blueprint $table) {
            $table->bigincrements('id');
            $table->unsignedBigInteger('sis_zip_id');
            $table->string('filename');
            $table->string('format');
            $table->string('extraction_folder');
            $table->unsignedBigInteger('row_count');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('imported_at')->nullable();
            $table->timestamp('validated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::dropIfExists('sis_csv_files');
    }
}
