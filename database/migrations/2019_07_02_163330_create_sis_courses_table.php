<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSisCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::create('sis_courses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('checksum');
            $table->unsignedBigInteger('sis_upload_id');
            $table->unsignedBigInteger('sis_zip_id');
            $table->unsignedBigInteger('sis_csv_file_id');

            // here's where we hard-define the fields for search, etc., purposes...
            $table->string('course_id', 255)->nullable();
            $table->string('short_name', 255)->nullable();
            $table->string('long_name', 255)->nullable();
            $table->string('account_id', 255)->nullable();
            $table->string('term_id', 255)->nullable();
            $table->string('status', 255)->nullable();
            $table->string('start_date', 255)->nullable();
            $table->string('end_date', 255)->nullable();

            $table->timestamp('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::dropIfExists('sis_courses');
    }
}
