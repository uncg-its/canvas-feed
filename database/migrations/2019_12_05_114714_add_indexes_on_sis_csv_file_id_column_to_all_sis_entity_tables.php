<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexesOnSisCsvFileIdColumnToAllSisEntityTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::transaction(function () {
            Schema::table('sis_courses', function (Blueprint $table) {
                $table->index('sis_csv_file_id');
            });
            Schema::table('sis_sections', function (Blueprint $table) {
                $table->index('sis_csv_file_id');
            });
            Schema::table('sis_users', function (Blueprint $table) {
                $table->index('sis_csv_file_id');
            });
            Schema::table('sis_enrollments', function (Blueprint $table) {
                $table->index('sis_csv_file_id');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::transaction(function () {
            Schema::table('sis_courses', function (Blueprint $table) {
                $table->dropIndex(['sis_csv_file_id']);
            });
            Schema::table('sis_sections', function (Blueprint $table) {
                $table->dropIndex(['sis_csv_file_id']);
            });
            Schema::table('sis_users', function (Blueprint $table) {
                $table->dropIndex(['sis_csv_file_id']);
            });
            Schema::table('sis_enrollments', function (Blueprint $table) {
                $table->dropIndex(['sis_csv_file_id']);
            });
        });
    }
}
