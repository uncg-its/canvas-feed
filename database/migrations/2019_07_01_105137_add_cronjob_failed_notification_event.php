<?php

use App\CcpsCore\NotificationEvent;
use Illuminate\Database\Migrations\Migration;

class AddCronjobFailedNotificationEvent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::transaction(function () {
            NotificationEvent::create([
                'display_name'       => 'CronjobFailed',
                'description'        => 'CronjobFailed',
                'event_class'        => 'App\\Events\\CcpsCore\\CronjobFailed',
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $event = NotificationEvent::where('class', 'App\\Events\\CcpsCore\\CronjobFailed')->delete();
    }
}
