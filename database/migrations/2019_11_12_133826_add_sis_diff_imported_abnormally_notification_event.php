<?php

use App\CcpsCore\NotificationEvent;
use Illuminate\Database\Migrations\Migration;

class AddSisDiffImportedAbnormallyNotificationEvent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::transaction(function () {
            NotificationEvent::create([
                'display_name'       => 'SisDiffImportedAbnormally',
                'description'        => 'SisDiffImportedAbnormally',
                'event_class'        => 'App\\Events\\Sis\\AbnormalSisDiffImport',
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $event = NotificationEvent::where('event_class', 'App\\Events\\Sis\\AbnormalSisDiffImport')->delete();
    }
}
