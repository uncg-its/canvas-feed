<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSisEnrollmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sis_enrollments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('checksum');

            $table->unsignedBigInteger('sis_upload_id');
            $table->unsignedBigInteger('sis_zip_id');
            $table->unsignedBigInteger('sis_csv_file_id');

            // here's where we hard-define the fields for search, etc., purposes...
            $table->string('course_id', 255)->nullable();
            $table->string('root_account', 255)->nullable();
            $table->string('user_id', 255)->nullable();
            $table->string('role', 255)->nullable();
            $table->string('section_id', 255)->nullable();
            $table->string('status', 255)->nullable();
            $table->string('associated_user_id', 255)->nullable();

            $table->timestamp('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sis_enrollments');
    }
}
