<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSisZipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sis_zips', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('filename');
            $table->json('csv_contents');
            $table->string('type')->default('auto');
            $table->string('status')->default('new');
            $table->string('feed_status')->default('new');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('in_canvas_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sis_zips');
    }
}
