<?php

use App\CcpsCore\NotificationEvent;
use Illuminate\Database\Migrations\Migration;

class AddSisDiffThresholdBreachedNotificationEvent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::transaction(function () {
            NotificationEvent::create([
                'display_name'       => 'SisDiffThresholdBreached',
                'description'        => 'SisDiffThresholdBreached',
                'event_class'        => 'App\\Events\\Sis\\SisDiffThresholdBreached',
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $event = NotificationEvent::where('event_class', 'App\\Events\\Sis\\SisDiffThresholdBreached')->delete();
    }
}
