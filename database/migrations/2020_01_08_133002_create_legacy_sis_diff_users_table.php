<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLegacySisDiffUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('legacy_sis_diff_users', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();
            $table->unsignedBigInteger('timestamp');
            $table->unsignedBigInteger('diffZipId');
            $table->string('user_id', 255)->nullable();
            $table->string('login_id', 255)->nullable();
            $table->string('password', 255)->nullable();
            $table->string('first_name', 255)->nullable();
            $table->string('last_name', 255)->nullable();
            $table->string('full_name', 255)->nullable();
            $table->string('sortable_name', 255)->nullable();
            $table->string('short_name', 255)->nullable();
            $table->string('email', 255)->nullable();
            $table->string('status', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('legacy_sis_diff_users');
    }
}
