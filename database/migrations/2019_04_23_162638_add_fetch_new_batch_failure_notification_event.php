<?php

use Illuminate\Support\Facades\DB;
use App\CcpsCore\NotificationEvent;
use Illuminate\Database\Migrations\Migration;

class AddFetchNewBatchFailureNotificationEvent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
            NotificationEvent::create([
                'display_name'       => 'FetchNewBatchFailure',
                'description'        => 'FetchNewBatchFailure',
                'event_class'        => 'App\\Events\\Aux\\FailedToFetchNewBatch',
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $event = NotificationEvent::where('event_class', 'App\\Events\\Aux\\FailedToFetchNewBatch')->delete();
    }
}
