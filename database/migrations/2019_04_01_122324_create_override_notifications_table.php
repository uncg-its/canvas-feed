<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOverrideNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('override_notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('override_id');
            $table->unsignedSmallInteger('interval_days');
            $table->boolean('manual')->default(false);
            $table->timestamp('reset_at')->nullable();
            $table->timestamp('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('override_notifications');
    }
}
