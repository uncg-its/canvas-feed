<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLegacySisDiffsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('legacy_sis_diffs', function (Blueprint $table) {
            $table->unsignedBigInteger('diffZipId')->primary();
            $table->unsignedInteger('diffOldZipId')->nullable();
            $table->unsignedInteger('diffNewZipId')->nullable();
            $table->unsignedBigInteger('diffZipCreateTime')->nullable();
            $table->unsignedBigInteger('diffZipCanvasTimestamp')->nullable();
            $table->string('diffZipFileName', 255)->nullable();
            $table->string('diffZipContents', 255)->nullable();
            $table->string('diffZipStatus', 255)->nullable();
            $table->string('diffZipCstStatus', 255)->nullable();
            $table->string('diffZipAccounts', 255)->nullable();
            $table->string('diffZipCourses', 255)->nullable();
            $table->string('diffZipEnrollments', 255)->nullable();
            $table->string('diffZipGroups', 255)->nullable();
            $table->string('diffZipGroupsmembership', 255)->nullable();
            $table->string('diffZipSections', 255)->nullable();
            $table->string('diffZipTerms', 255)->nullable();
            $table->string('diffZipUsers', 255)->nullable();
            $table->string('diffZipXlists', 255)->nullable();
            $table->unsignedBigInteger('diffZipTotal')->nullable();
            $table->unsignedBigInteger('diffZipCanvasSisId')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('legacy_sis_diffs');
    }
}
