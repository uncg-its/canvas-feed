<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLastEnrollmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('last_enrollments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('canvas_user_id');
            $table->unsignedBigInteger('action_id');
            $table->unsignedMediumInteger('enrollment_id');
            $table->unsignedMediumInteger('course_id');
            $table->string('type');
            $table->unsignedMediumInteger('course_section_id');
            $table->unsignedMediumInteger('root_account_id');
            $table->string('enrollment_state');
            $table->unsignedInteger('role_id');
            $table->boolean('limit_privileges_to_course_section');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('restored_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('last_enrollments');
    }
}
