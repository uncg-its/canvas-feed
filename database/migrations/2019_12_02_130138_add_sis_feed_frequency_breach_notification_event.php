<?php

use App\CcpsCore\NotificationEvent;
use Illuminate\Database\Migrations\Migration;

class AddSisFeedFrequencyBreachNotificationEvent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::transaction(function () {
            NotificationEvent::create([
                'display_name'       => 'SisFeedFrequencyBreach',
                'description'        => 'SisFeedFrequencyBreach',
                'event_class'        => 'App\\Events\\Sis\\SisFeedFrequencyBreach',
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $event = NotificationEvent::where('event_class', 'App\\Events\\Sis\\SisFeedFrequencyBreach')->delete();
    }
}
