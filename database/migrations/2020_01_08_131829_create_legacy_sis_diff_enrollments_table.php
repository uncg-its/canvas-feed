<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLegacySisDiffEnrollmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('legacy_sis_diff_enrollments', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();
            $table->unsignedBigInteger('timestamp');
            $table->unsignedBigInteger('diffZipId');
            $table->string('course_id', 255)->nullable();
            $table->string('root_account', 255)->nullable();
            $table->string('user_id', 255)->nullable();
            $table->string('role', 255)->nullable();
            $table->string('section_id', 255)->nullable();
            $table->string('status', 255)->nullable();
            $table->string('associated_user_id', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('legacy_sis_diff_enrollments');
    }
}
