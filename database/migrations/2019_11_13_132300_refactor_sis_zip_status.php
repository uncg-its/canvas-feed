<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RefactorSisZipStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sis_zips', function (Blueprint $table) {
            $table->dropColumn('feed_status');
            $table->dropColumn('in_canvas_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sis_zips', function (Blueprint $table) {
            $table->string('feed_status')->default('new')->after('status');
            $table->timestamp('in_canvas_at')->nullable()->after('created_at');
        });
    }
}
