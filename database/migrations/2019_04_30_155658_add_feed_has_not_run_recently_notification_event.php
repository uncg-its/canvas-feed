<?php

use Illuminate\Support\Facades\DB;
use App\CcpsCore\NotificationEvent;
use Illuminate\Database\Migrations\Migration;

class AddFeedHasNotRunRecentlyNotificationEvent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
            NotificationEvent::create([
                'display_name'       => 'FeedHasNotRunRecently',
                'description'        => 'FeedHasNotRunRecently',
                'event_class'        => 'App\\Events\\Aux\\FeedRunFrequencyBreach',
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $event = NotificationEvent::where('event_class', 'App\\Events\\Aux\\FeedRunFrequencyBreach')->delete();
    }
}
