<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInvalidationFieldsToUserDataChangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::table('ccps_userfeed_user_data_changes', function (Blueprint $table) {
            $table->boolean('invalid')->default(false)->after('encrypted');
            $table->timestamp('ignored_at')->nullable()->after('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::table('ccps_userfeed_user_data_changes', function (Blueprint $table) {
            $table->dropColumn('invalid');
            $table->dropColumn('ignored_at');
        });
    }
}
