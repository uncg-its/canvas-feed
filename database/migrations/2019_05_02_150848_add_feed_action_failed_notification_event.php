<?php

use Illuminate\Support\Facades\DB;
use App\CcpsCore\NotificationEvent;
use Illuminate\Database\Migrations\Migration;

class AddFeedActionFailedNotificationEvent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
            NotificationEvent::create([
                'display_name'       => 'FeedActionFailed',
                'description'        => 'A Feed Action (add/update/delete) has failed',
                'event_class'        => 'App\\Events\\Aux\\FeedActionFailed',
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $event = NotificationEvent::where('event_class', 'App\\Events\\Aux\\FeedActionFailed')->delete();
    }
}
