<?php

use App\CcpsCore\CronjobMeta;
use Illuminate\Database\Migrations\Migration;

class AddGetUnpublishedCoursesReportCronjobMeta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::transaction(function () {
            CronjobMeta::create([
                'class'  => 'App\Cronjobs\Sis\GetUnpublishedCoursesReport',
                'status' => 'disabled'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $meta = CronjobMeta::where('class', 'App\Cronjobs\Sis\GetUnpublishedCoursesReport')->delete();
    }
}
