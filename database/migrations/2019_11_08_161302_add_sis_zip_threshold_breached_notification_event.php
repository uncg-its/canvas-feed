<?php

use App\CcpsCore\NotificationEvent;
use Illuminate\Database\Migrations\Migration;

class AddSisZipThresholdBreachedNotificationEvent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::transaction(function () {
            NotificationEvent::create([
                'display_name'       => 'SisZipThresholdBreached',
                'description'        => 'SisZipThresholdBreached',
                'event_class'        => 'App\\Events\\Sis\\SisZipThresholdBreached',
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $event = NotificationEvent::where('event_class', 'App\\Events\\Sis\\SisZipThresholdBreached')->delete();
    }
}
