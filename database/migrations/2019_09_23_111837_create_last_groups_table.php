<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLastGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('last_groups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('canvas_user_id');
            $table->unsignedBigInteger('action_id');
            $table->unsignedMediumInteger('group_id');
            $table->string('join_level');
            $table->unsignedMediumInteger('group_category_id');
            $table->unsignedMediumInteger('course_id');
            $table->boolean('concluded');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('restored_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('last_groups');
    }
}
