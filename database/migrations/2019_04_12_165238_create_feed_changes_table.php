<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedChangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feed_changes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('run_id');
            $table->string('canvas_user_id');
            $table->enum('type', ['add', 'update', 'delete'])->default('add');
            $table->uuid('user_data_change_id')->nullable();
            $table->text('user_template');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feed_changes');
    }
}
