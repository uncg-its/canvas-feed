<?php

use App\CcpsCore\NotificationEvent;
use Illuminate\Database\Migrations\Migration;

class AddSisZipMoveExtractFailureNotificationEvent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::transaction(function () {
            NotificationEvent::create([
                'display_name'       => 'SIS Zip Move/Extract Failure',
                'description'        => 'An exception during the SIS - Step 1 - Move cronjob',
                'event_class'        => 'App\\Events\\Sis\\Base\\SisZipMoveExtractException',
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $event = NotificationEvent::where('class', 'App\\Events\\Sis\\Base\\SisZipMoveExtractException')->delete();
    }
}
