<?php

use App\CcpsCore\NotificationEvent;
use Illuminate\Database\Migrations\Migration;

class AddRestorationFailedNotificationEvent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::transaction(function () {
            NotificationEvent::create([
                'display_name'       => 'RestorationFailed',
                'description'        => 'RestorationFailed',
                'event_class'        => 'App\\Events\\Aux\\RestorationFailed',
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $event = NotificationEvent::where('event_class', 'App\\Events\\Aux\\RestorationFailed')->delete();
    }
}
