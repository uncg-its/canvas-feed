<?php

use App\Constants\SuccessStatus;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('canvas_user_id');
            $table->unsignedInteger('actionable_id');
            $table->string('actionable_type');
            $table->unsignedInteger('status_id')->default(SuccessStatus::PENDING);
            $table->enum('type', ['add', 'update', 'delete']);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('completed_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actions');
    }
}
