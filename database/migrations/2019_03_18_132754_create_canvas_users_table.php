<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCanvasUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('canvas_users', function (Blueprint $table) {
            $table->string('primary_id');
            $table->unsignedMediumInteger('sis_id');
            $table->longText('data');
            $table->timestamps();
            $table->softDeletes();

            $table->primary('primary_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('canvas_users');
    }
}
