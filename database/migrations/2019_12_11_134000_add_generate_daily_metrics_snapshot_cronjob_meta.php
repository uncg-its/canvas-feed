<?php

use App\CcpsCore\CronjobMeta;
use Illuminate\Database\Migrations\Migration;

class AddGenerateDailyMetricsSnapshotCronjobMeta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::transaction(function () {
            CronjobMeta::create([
                'class'  => 'App\Cronjobs\GenerateDailyMetricsSnapshot',
                'status' => 'disabled'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $meta = CronjobMeta::where('class', 'App\Cronjobs\GenerateDailyMetricsSnapshot')->delete();
    }
}
