<?php

use App\Models\Aux\CanvasUser;
use Illuminate\Database\Migrations\Migration;

class DecryptCanvasidFieldOnCanvasUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        CanvasUser::each(function ($user) {
            $user->canvasId = decrypt($user->canvasId);
            $user->save();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        CanvasUser::each(function ($user) {
            $user->canvasId = encrypt($user->canvasId);
            $user->save();
        });
    }
}
