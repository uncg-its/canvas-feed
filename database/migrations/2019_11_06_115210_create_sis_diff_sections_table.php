<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSisDiffSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sis_diff_sections', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('diff_id');
            $table->string('section_id');
            $table->string('course_id');
            $table->text('name');
            $table->string('status');
            $table->string('start_date')->nullable();
            $table->string('end_date')->nullable();
            $table->timestamp('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sis_diff_sections');
    }
}
