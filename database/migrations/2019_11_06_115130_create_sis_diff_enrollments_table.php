<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSisDiffEnrollmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sis_diff_enrollments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('diff_id');
            $table->string('course_id')->nullable();
            $table->string('root_account')->nullable();
            $table->string('user_id');
            $table->string('role');
            $table->string('section_id')->nullable();
            $table->string('status');
            $table->string('associated_user_id')->nullable();
            $table->timestamp('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sis_diff_enrollments');
    }
}
