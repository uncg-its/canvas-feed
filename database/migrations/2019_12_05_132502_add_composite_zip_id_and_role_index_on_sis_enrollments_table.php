<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCompositeZipIdAndRoleIndexOnSisEnrollmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::transaction(function () {
            Schema::table('sis_enrollments', function (Blueprint $table) {
                $table->index(['sis_zip_id', 'role']);
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::transaction(function () {
            Schema::table('sis_enrollments', function (Blueprint $table) {
                $table->dropIndex(['sis_zip_id', 'role']);
            });
        });
    }
}
