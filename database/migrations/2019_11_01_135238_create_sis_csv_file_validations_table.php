<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSisCsvFileValidationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sis_csv_file_validations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('sis_csv_file_id');
            $table->longText('messages')->nullable();
            $table->boolean('success')->default(true);
            $table->timestamp('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sis_csv_file_validations');
    }
}
