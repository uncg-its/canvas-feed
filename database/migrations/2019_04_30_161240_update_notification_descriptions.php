<?php

use App\CcpsCore\NotificationEvent;
use App\Events\FailedToFetchNewBatch;
use Illuminate\Database\Migrations\Migration;
use App\Events\FeedRunFrequencyBreach;

class UpdateNotificationDescriptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        NotificationEvent::where('event_class', FailedToFetchNewBatch::class)->update([
            'description' => 'A new User Data Batch could not be fetched because of an error.'
        ]);

        NotificationEvent::where('event_class', FeedRunFrequencyBreach::class)->update([
            'description' => 'The last Canvas Aux Feed run was too long ago.'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
