<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSisUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sis_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('checksum');

            $table->unsignedBigInteger('sis_upload_id');
            $table->unsignedBigInteger('sis_zip_id');
            $table->unsignedBigInteger('sis_csv_file_id');

            // here's where we hard-define the fields for search, etc., purposes...
            $table->string('user_id', 255)->nullable();
            $table->string('login_id', 255)->nullable();
            $table->string('password', 255)->nullable();
            $table->string('first_name', 255)->nullable();
            $table->string('last_name', 255)->nullable();
            $table->string('full_name', 255)->nullable();
            $table->string('sortable_name', 255)->nullable();
            $table->string('short_name', 255)->nullable();
            $table->string('email', 255)->nullable();
            $table->string('status', 255)->nullable();

            $table->timestamp('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sis_users');
    }
}
