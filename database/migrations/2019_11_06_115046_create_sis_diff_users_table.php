<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSisDiffUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sis_diff_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedSmallInteger('diff_id');
            $table->string('user_id');
            $table->string('login_id');
            $table->string('password')->nullable();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('full_name')->nullable();
            $table->string('sortable_name')->nullable();
            $table->string('short_name')->nullable();
            $table->string('email');
            $table->string('status');
            $table->timestamp('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sis_diff_users');
    }
}
