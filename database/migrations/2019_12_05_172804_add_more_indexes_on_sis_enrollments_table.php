<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMoreIndexesOnSisEnrollmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::transaction(function () {
            Schema::table('sis_enrollments', function (Blueprint $table) {
                $table->index('user_id');
                $table->index('role');
                $table->index('section_id');
                $table->index('status');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::transaction(function () {
            Schema::table('sis_enrollments', function (Blueprint $table) {
                $table->dropIndex(['user_id']);
                $table->dropIndex(['role']);
                $table->dropIndex(['section_id']);
                $table->dropIndex(['status']);
            });
        });
    }
}
