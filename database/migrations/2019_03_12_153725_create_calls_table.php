<?php

use App\Constants\SuccessStatus;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCallsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calls', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('action_id');
            $table->unsignedInteger('status_id')->default(SuccessStatus::PENDING);
            $table->string('endpoint')->nullable();
            $table->string('method')->nullable();
            $table->json('request_parameters')->nullable();
            $table->unsignedSmallInteger('response_code')->nullable();
            $table->text('message')->nullable();
            $table->json('result')->nullable();
            $table->timestamp('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calls');
    }
}
