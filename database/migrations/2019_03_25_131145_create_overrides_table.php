<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOverridesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('overrides', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('submitter_id');
            $table->string('canvas_user_id');
            $table->text('user_template');
            $table->enum('type', ['add', 'delete'])->default('add');
            $table->string('contact_email');
            $table->text('reason');
            $table->unsignedInteger('canceled_by_id')->nullable();
            $table->timestamp('canceled_at')->nullable();
            $table->timestamp('expires_at')->nullable();
            $table->timestamp('resolved_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('overrides');
    }
}
