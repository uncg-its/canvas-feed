<?php

use App\CcpsCore\NotificationEvent;
use Illuminate\Database\Migrations\Migration;

class AddCanvasApiEventsToNotificationEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::transaction(function () {
            NotificationEvent::create([
                'display_name'       => 'CanvasApiUp',
                'description'        => 'Canvas API is up (has recovered from an outage)',
                'event_class'        => 'App\\Events\\CanvasApiIsUp',
            ]);

            NotificationEvent::create([
                'display_name'       => 'CanvasApiDown',
                'description'        => 'Canvas API is down (new outage)',
                'event_class'        => 'App\\Events\\CanvasApiIsDown',
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $event = NotificationEvent::where('event_class', 'App\\Events\\CanvasApiIsUp')->delete();
        $event = NotificationEvent::where('event_class', 'App\\Events\\CanvasApiIsDown')->delete();
    }
}
