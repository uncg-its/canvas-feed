<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUnpublishedCoursesReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unpublished_courses_reports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedMediumInteger('canvas_id');
            $table->string('term_id');
            $table->unsignedMediumInteger('unpublished_courses_count')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('completed_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unpublished_courses_reports');
    }
}
