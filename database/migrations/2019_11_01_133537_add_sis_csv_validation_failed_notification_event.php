<?php

use App\CcpsCore\NotificationEvent;
use Illuminate\Database\Migrations\Migration;

class AddSisCsvValidationFailedNotificationEvent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::transaction(function () {
            NotificationEvent::create([
                'display_name'       => 'SisCsvValidationFailed',
                'description'        => 'SisCsvValidationFailed',
                'event_class'        => 'App\\Events\\Sis\\SisCsvValidationErrorsFound',
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $event = NotificationEvent::where('event_class', 'App\\Events\\Sis\\SisCsvValidationErrorsFound')->delete();
    }
}
