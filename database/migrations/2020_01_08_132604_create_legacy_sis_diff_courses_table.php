<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLegacySisDiffCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('legacy_sis_diff_courses', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();
            $table->unsignedBigInteger('timestamp');
            $table->unsignedBigInteger('diffZipId');
            $table->string('course_id', 255)->nullable();
            $table->string('short_name', 255)->nullable();
            $table->string('long_name', 255)->nullable();
            $table->string('account_id', 255)->nullable();
            $table->string('term_id', 255)->nullable();
            $table->string('status', 255)->nullable();
            $table->string('start_date', 255)->nullable();
            $table->string('end_date', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('legacy_sis_diff_courses');
    }
}
