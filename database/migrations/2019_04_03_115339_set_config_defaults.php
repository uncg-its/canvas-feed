<?php

use App\CcpsCore\DbConfig;
use Illuminate\Database\Migrations\Migration;

class SetConfigDefaults extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DbConfig::updateOrCreate(['key' => 'notification_intervals'], ['value' => '30,14,7,3,1']);
        DbConfig::updateOrCreate(['key' => 'api_check_threshold'], ['value' => '3']);
        DbConfig::updateOrCreate(['key' => 'expected_source_min_count'], ['value' => '100']);
        DbConfig::updateOrCreate(['key' => 'expected_change_max_count'], ['value' => '100']);
        DbConfig::updateOrCreate(['key' => 'max_hours_since_last_feed_run'], ['value' => '48']);
        DbConfig::updateOrCreate(['key' => 'aux_restore_data'], ['value' => 1]);
        DbConfig::updateOrCreate(['key' => 'aux_restoration_cleanup_days'], ['value' => 7]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
