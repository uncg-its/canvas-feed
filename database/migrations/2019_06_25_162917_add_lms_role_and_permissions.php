<?php

use Illuminate\Database\Migrations\Migration;

use App\CcpsCore\Permission;
use App\CcpsCore\Role;

class AddLmsRoleAndPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Artisan::call('db:seed', [
            '--class' => 'App\\Seeders\\LmsRolesTableSeeder',
            '--force' => true
        ]);

        \Artisan::call('db:seed', [
            '--class' => 'App\\Seeders\\LmsPermissionsTableSeeder',
            '--force' => true
        ]);

        \Artisan::call('db:seed', [
            '--class' => 'App\\Seeders\\LmsPermissionRoleTableSeeder',
            '--force' => true
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $permissionsToFind = [
            // fill in permission keys here for each permission to remove

            //'mything.view',
            //'mything.edit'
        ];

        $rolesToFind = [
            // fill in role keys here for each role from which to detach permissions.
            // role will also be deleted UNLESS it is not editable (e.g. admin)

            //'myrole',
            //'admin'
        ];

        $permissions = Permission::whereIn('name', $permissionsToFind)->get();
        $roles = Role::whereIn('name', $rolesToFind)->get();

        foreach ($roles as $role) {
            $role->detachPermissions($permissions);
            if ($role->editable) {
                $role->delete();
            }
        }

        foreach ($permissions as $permission) {
            $permission->delete();
        }
    }
}
