<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Splunk Rest Client - HTTP Collector Variables
    |--------------------------------------------------------------------------
    |
    */

    'host' => env('SPLUNK_HTTP_COLLECTOR_HOST', false), // must include http:// or https://

    'port' => env('SPLUNK_HTTP_COLLECTOR_PORT', '8088'), // 8088 is default port

    'token' => env('SPLUNK_HTTP_COLLECTOR_TOKEN', ''), // default token

    'source_type' => env('SPLUNK_HTTP_COLLECTOR_SOURCE_TYPE', ''), // source type as agreed upon with the Splunk Admin

    'use_proxy'  => env('SPLUNK_HTTP_COLLECTOR_USE_PROXY', false), // token as agreed upon with the Splunk Admin
    'proxy_host' => env('SPLUNK_HTTP_COLLECTOR_PROXY_HOST', false), // proxy host if using
    'proxy_port' => env('SPLUNK_HTTP_COLLECTOR_PROXY_PORT', false), // proxy port if using

    'debug_mode' => env('SPLUNK_HTTP_COLLECTOR_DEBUG_MODE', false), // Debug mode on or off

    'cache_active' => env('SPLUNK_HTTP_COLLECTOR_CACHING', 'on') == 'on', // set to 'on' or 'off' in .env file

    'cache_minutes' => env('SPLUNK_HTTP_COLLECTOR_CACHE_MINUTES', 10),

    'notification_mode' => env('SPLUNK_HTTP_COLLECTOR_NOTIFICATION_MODE', ''), // comma separated list: flash, log, or empty

    'pagination_limit' => env('SPLUNK_HTTP_COLLECTOR_PAGINATION_LIMIT', 500), // comma separated list: flash, log, or empty

];
