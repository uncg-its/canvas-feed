<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application variables
    |--------------------------------------------------------------------------
    |
    */

    'search' => env('APP_SEARCH', false), // should we show the search?

    'login_methods' => explode(',', env('APP_LOGIN_METHODS', 'local')),

    'redirect_to_login' => env('APP_LOGIN_REDIRECT', 'true') == 'true',

    'allow_signups' => env('APP_ALLOW_SIGNUPS', 'false') == 'true',

    'paginator_per_page' => env('PAGINATOR_PER_PAGE', 25),

    'queue_failures_allowed' => [
        'default' => env('APP_QUEUE_FAILURES_ALLOWED_DEFAULT', 0),
        'email'   => env('APP_QUEUE_FAILURES_ALLOWED_EMAIL', 3),
        'api'     => env('APP_QUEUE_FAILURES_ALLOWED_API', 5),
    ],

    'http_proxy' => [
        'enabled' => env('HTTP_REQUEST_USE_PROXY', false) == 'true',
        'host'    => env('HTTP_REQUEST_PROXY_HOST', null),
        'port'    => env('HTTP_REQUEST_PROXY_PORT', null),
    ],

    'log-viewer' => [
        'per_page_options' => [25, 50, 100, 250],
        'storage_path'     => env('LOG_VIEWER_STORAGE_PATH', storage_path('logs')),
    ],

    'encryption' => env('MODEL_ENCRYPTION', 'true') == 'true', // for models using Encryptable trait

    'notifications' => [
        'channel_verification_code_ttl_minutes' => env('NOTIFICATION_CHANNEL_VERIFICATION_CODE_TTL_MINUTES', 15),
    ],

    'socialite' => [
        'allow_only_domain' => env('SOCIALITE_ALLOW_ONLY_DOMAIN')
    ],

    'app_snapshots' => [
        'log_channel' => env('APPLICATION_SNAPSHOT_LOGGING_CHANNEL', 'application-snapshots')
    ],

    'role_mapping' => [
        'method' => env('ROLE_MAPPING_METHOD', 'none'), // none, login, or cronjob
    ],

    'exceptions'           => [
        'should_log'  => env('EXCEPTIONS_SHOULD_LOG', true),
        'log_channel' => env('EXCEPTIONS_LOG_CHANNEL', 'exceptions'),
    ],

    /*
    |--------------------------------------------------------------------------
    | Google Chat Notification Channels
    |--------------------------------------------------------------------------
    |
    */
    'google_chat_channels' => [
        'default' => env('GOOGLE_CHAT_DEFAULT_CHANNEL', null),
    ],

    /*
    |--------------------------------------------------------------------------
    | CCPS Modules
    |--------------------------------------------------------------------------
    |
    | Configuration for CCPS modules. Core CCPS modules are set up upon
    | installation of 'ccps-core'. Others will need to be added as packages are
    | added.
    |
    | Default configuration can be changed.
    | Icons and menu items will be displayed in the order in which they appear in the array below.
    |
    |
    */

    'modules' => [

        'user-feed' => [
            'package'              => 'uncgits/ccps-user-feed',
            'icon'                 => 'fas fa-users',
            'title'                => 'User Feed',
            'index'                => 'user-feed.index',
            'parent'               => '',
            'required_permissions' => 'user-feed.*',
            'use_custom_routes'    => false,
            'custom_view_path'     => 'ccps-user-feed::',
            'order'                => 10,
            'helpText'             => 'Information and settings for Phase 1 of the feed - retrieving latest user changes from Grouper'
        ],

        'users' => [
            'package'              => 'uncgits/ccps-core',
            'icon'                 => 'fas fa-users',
            'title'                => 'Users',
            'index'                => 'users',
            'parent'               => 'admin',
            'required_permissions' => 'users.*',
            'use_custom_routes'    => false,
            'custom_view_path'     => false,
        ],

        'acl' => [
            'package'              => 'uncgits/ccps-core',
            'icon'                 => 'fas fa-list',
            'title'                => 'ACL',
            'index'                => 'acl',
            'parent'               => 'admin',
            'required_permissions' => 'acl.*',
            'use_custom_routes'    => false,
            'custom_view_path'     => false,
        ],

        'email' => [
            'package'              => 'uncgits/ccps-core',
            'icon'                 => 'fas fa-envelope',
            'title'                => 'App Email',
            'index'                => 'email',
            'parent'               => '',
            'required_permissions' => 'email.*',
            'use_custom_routes'    => false,
            'custom_view_path'     => false,
            'helpText'             => 'View emails sent by this application'
        ],

        'queues' => [
            'package'              => 'uncgits/ccps-core',
            'icon'                 => 'fas fa-server',
            'title'                => env('QUEUE_DRIVER') == 'redis' ? ' Queues (Horizon)' : 'Queues & Jobs',
            'index'                => env('QUEUE_DRIVER') == 'redis' ? 'horizon.index' : 'queues',
            'parent'               => 'admin',
            'required_permissions' => 'queues.*',
            'use_custom_routes'    => env('QUEUE_DRIVER') == 'redis',
            'custom_view_path'     => false,
        ],

        'cronjobs' => [
            'package'              => 'uncgits/ccps-core',
            'icon'                 => 'fas fa-hourglass-half',
            'title'                => 'Cron Jobs',
            'index'                => 'cronjobs',
            'parent'               => '',
            'required_permissions' => 'cronjobs.*',
            'use_custom_routes'    => false,
            'custom_view_path'     => false,
            'helpText'             => 'View / Edit the application\'s scheduled tasks'
        ],

        'log-viewer' => [
            'package'              => 'uncgits/ccps-core',
            'icon'                 => 'fas fa-table',
            'title'                => 'Log Viewer',
            'index'                => 'log-viewer',
            'parent'               => 'admin',
            'required_permissions' => 'logs.*',
            'use_custom_routes'    => false,
            'custom_view_path'     => false,
        ],

        'cache' => [
            'package'              => 'uncgits/ccps-core',
            'icon'                 => 'fas fa-database',
            'title'                => 'Cache',
            'index'                => 'cache',
            'parent'               => '',
            'required_permissions' => 'cache.clear',
            'use_custom_routes'    => false,
            'custom_view_path'     => false,
            'helpText'             => 'View / clear the application\'s internal cache',
        ],

        'notifications-log' => [
            'package'              => 'uncgits/ccps-core',
            'icon'                 => 'fas fa-bell',
            'title'                => 'Notifications Log',
            'index'                => 'notifications-log.index',
            'parent'               => 'admin',
            'required_permissions' => 'logs.*',
            'use_custom_routes'    => false,
            'custom_view_path'     => false,
        ],

        'config' => [
            'package'              => 'uncgits/ccps-core',
            'icon'                 => 'fas fa-cog',
            'title'                => 'Config',
            'index'                => 'config',
            'parent'               => '',
            'required_permissions' => 'config.*',
            'use_custom_routes'    => false,
            'custom_view_path'     => false,
            'helpText'             => 'Configure application options (covers SIS and Aux feeds)'
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Bootstrap Skins
    |--------------------------------------------------------------------------
    |
    | Collection of Bootstrap 4 skins that can be applied to the base app theme
    | May be added or removed as the app developer sees fit.
    |
    | Files must be accessible from the public/ folder.
    |
    */

    'bootstrap_skins' => [
        'default' => [
            'asset_path'   => 'vendor/uncgits/ccps-core/css/skins/default.min.css',
            'display_name' => 'Default'
        ],

        'cerulean'  => [
            'asset_path'   => 'vendor/uncgits/ccps-core/css/skins/cerulean.min.css',
            'display_name' => 'Cerulean'
        ],
        'cosmo'     => [
            'asset_path'   => 'vendor/uncgits/ccps-core/css/skins/cosmo.min.css',
            'display_name' => 'Cosmo'
        ],
        'darkly'    => [
            'asset_path'   => 'vendor/uncgits/ccps-core/css/skins/darkly.min.css',
            'display_name' => 'Darkly'
        ],
        'flatly'    => [
            'asset_path'   => 'vendor/uncgits/ccps-core/css/skins/flatly.min.css',
            'display_name' => 'Flatly'
        ],
        'journal'   => [
            'asset_path'   => 'vendor/uncgits/ccps-core/css/skins/journal.min.css',
            'display_name' => 'Journal'
        ],
        'litera'    => [
            'asset_path'   => 'vendor/uncgits/ccps-core/css/skins/litera.min.css',
            'display_name' => 'Litera'
        ],
        'lumen'     => [
            'asset_path'   => 'vendor/uncgits/ccps-core/css/skins/lumen.min.css',
            'display_name' => 'Lumen'
        ],
        'lux'       => [
            'asset_path'   => 'vendor/uncgits/ccps-core/css/skins/lux.min.css',
            'display_name' => 'Lux'
        ],
        'materia'   => [
            'asset_path'   => 'vendor/uncgits/ccps-core/css/skins/materia.min.css',
            'display_name' => 'Materia'
        ],
        'minty'     => [
            'asset_path'   => 'vendor/uncgits/ccps-core/css/skins/minty.min.css',
            'display_name' => 'Minty'
        ],
        'pulse'     => [
            'asset_path'   => 'vendor/uncgits/ccps-core/css/skins/pulse.min.css',
            'display_name' => 'Pulse'
        ],
        'sandstone' => [
            'asset_path'   => 'vendor/uncgits/ccps-core/css/skins/sandstone.min.css',
            'display_name' => 'Sandstone'
        ],
        'simplex'   => [
            'asset_path'   => 'vendor/uncgits/ccps-core/css/skins/simplex.min.css',
            'display_name' => 'Simplex'
        ],
        'slate'     => [
            'asset_path'   => 'vendor/uncgits/ccps-core/css/skins/slate.min.css',
            'display_name' => 'Slate'
        ],
        'solar'     => [
            'asset_path'   => 'vendor/uncgits/ccps-core/css/skins/solar.min.css',
            'display_name' => 'Solar'
        ],
        'superhero' => [
            'asset_path'   => 'vendor/uncgits/ccps-core/css/skins/superhero.min.css',
            'display_name' => 'Superhero'
        ],
        'united'    => [
            'asset_path'   => 'vendor/uncgits/ccps-core/css/skins/united.min.css',
            'display_name' => 'United'
        ],
        'yeti'      => [
            'asset_path'   => 'vendor/uncgits/ccps-core/css/skins/yeti.min.css',
            'display_name' => 'Yeti'
        ],

    ],

];
