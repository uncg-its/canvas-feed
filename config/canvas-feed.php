<?php

return [
    'sis' => [
        'original_path'       => env('SIS_ORIGINAL_PATH', ''),

        'zip_move_limit' => env('SIS_ZIP_MOVE_LIMIT', 5),

        'acceptable_csv_fields' => [
            'users' => [
                'user_id','integration_id','authentication_provider_id','login_id', 'password', 'first_name', 'last_name', 'full_name', 'sortable_name', 'short_name', 'email', 'status'
            ],
            'accounts' => [
                'account_id', 'parent_account_id', 'name', 'status'
            ],
            'terms' => [
                'term_id', 'name', 'status', 'start_date', 'end_date'
            ],
            'courses' => [
                'course_id', 'integration_id','short_name', 'long_name', 'account_id', 'term_id', 'status', 'start_date', 'end_date', 'course_format','blueprint_course_id'
            ],
            'sections' => [
                'section_id', 'course_id', 'integration_id', 'name', 'status', 'start_date', 'end_date'
            ],
            'enrollments' => [
                'course_id', 'root_account', 'user_id', 'role', 'role_id', 'section_id', 'status', 'associated_user_id', 'limit_section_privileges'
            ],
            'group_categories' => [
                'group_category_id', 'account_id', 'course_id', 'category_name', 'status',
            ],
            'groups' => [
                'group_id', 'group_category_id', 'account_id', 'course_id', 'name', 'status'
            ],
            'groups_membership' => [
                'group_id', 'user_id', 'status'
            ],
            'xlists' => [
                'xlist_course_id', 'section_id', 'status'
            ],
            'user_observers' => [
                'observer_id', 'student_id', 'status'
            ],
            'admins' => [
                'user_id', 'account_id', 'role', 'role_id', 'status', 'root_account'
            ],
            'logins' => [
                'user_id', 'integration_id', 'login_id', 'password', 'ssha_password', 'authentication_provider_id', 'existing_user_id', 'existing_integration_id', 'existing_canvas_user_id', 'root_account', 'email',
            ],
            'change_sis_id' => [
                'old_id', 'new_id', 'old_integration_id', 'new_integration_id', 'type'
            ]
        ],

        'required_csv_fields' => [
            'users' => [
                'user_id', 'login_id', 'first_name', 'last_name', 'short_name', 'email', 'status'
            ],
            'accounts' => [
                'account_id', 'parent_account_id', 'name', 'status'
            ],
            'terms' => [
                'term_id', 'name', 'status', 'start_date', 'end_date'
            ],
            'courses' => [
                'course_id', 'short_name', 'long_name', 'account_id', 'term_id', 'status', 'start_date', 'end_date'
            ],
            'sections' => [
                'section_id', 'course_id', 'name', 'status', 'start_date', 'end_date'
            ],
            'enrollments' => [
                'user_id', 'role', 'section_id', 'status'
            ],
            'groups' => [
                'group_id', 'account_id', 'name', 'status'
            ],
            'groups_membership' => [
                'group_id', 'user_id', 'status'
            ],
            'xlists' => [
                'xlist_course_id', 'section_id', 'status'
            ],
        ],

        'validation' => [
            'chunk_size' => env('SIS_VALIDATION_CHUNK_SIZE', 10000),
        ],

        'root_account_id' => env('SIS_ROOT_ACCOUNT_ID', 1),

        'expected_roles' => explode(',', env('SIS_EXPECTED_ROLES', 'student,teacher,observer,designer,ta')),

        'formats_to_diff' => explode(',', env('SIS_FORMATS_TO_DIFF', 'users,courses,sections,enrollments')),

        'term_semester_map' => [
            '01' => 'SP',
            '05' => 'SU',
            '08' => 'FA',
        ],

        'upload_config' => [
            'import_type'             => env('SIS_UPLOAD_CONFIG_IMPORT_TYPE', 'instructure_csv'),
            'extension'               => env('SIS_UPLOAD_CONFIG_EXTENSION', 'zip'),
            'batch_mode'              => env('SIS_UPLOAD_CONFIG_BATCH_MODE', 'false'),
            'batch_mode_term_id'      => env('SIS_UPLOAD_CONFIG_BATCH_MODE_TERM_ID', ''),
            'override_sis_stickiness' => env('SIS_UPLOAD_CONFIG_OVERRIDE_SIS_STICKINESS', 'false'),
            'add_sis_stickiness'      => env('SIS_UPLOAD_CONFIG_ADD_SIS_STICKINESS', 'false'),
            'clear_sis_stickiness'    => env('SIS_UPLOAD_CONFIG_CLEAR_SIS_STICKINESS', 'false'),
        ],

        'purge_padding' => [
            'zips'  => env('SIS_PURGE_PADDING_ZIPS', 5),
            'diffs' => env('SIS_PURGE_PADDING_DIFFS', 5),
        ],

    ],
    'aux' => [
        'canvas'         => [
        // mapping fields for the addUser() api call to user feed fields
            'user_field_map' => [
                'firstname'            => env('CANVAS_FIRST_NAME_FIELD', 'uncgpreferredgivenname'),
                'lastname'             => env('CANVAS_LAST_NAME_FIELD', 'uncgpreferredsurname'),
                'email'                => env('CANVAS_EMAIL_FIELD', 'uncgemail'),
                'username'             => env('CANVAS_USERNAME_FIELD', 'cn'),
                'sis_user_id'          => env('CANVAS_SIS_USER_ID_FIELD', 'uidnumber'),
            ],

            'primary_id' => env('CANVAS_PRIMARY_ID_FIELD', 'cn'),

        ],
        'grouper' => [
            'allow_group' => env('GROUPER_ALLOW_GROUP', null),
            'deny_group'  => env('GROUPER_DENY_GROUP', null),
        ],

        'overrides' => [
            // min is 24 hours no matter what
            'min_length_hours' => env('OVERRIDE_MIN_LENGTH_HOURS', 24) < 24 ? 24 : env('OVERRIDE_MIN_LENGTH_HOURS', 24),
        ],

        'run_pagination'              => env('RUN_PAGINATION', 10),

        'queue' => [
            'aux-feed' => [
                'throttle_job_limit'        => env('AUX_FEED_QUEUE_THROTTLE_JOB_LIMIT', 30),
                'throttle_timespan_seconds' => env('AUX_FEED_QUEUE_THROTTLE_TIMESPAN_SECONDS', 60),
                'throttle_release_seconds'  => env('AUX_FEED_QUEUE_THROTTLE_RELEASE_SECONDS', 30),
                'retry_delay'               => env('AUX_FEED_QUEUE_RETRY_DELAY', 60),
            ]
        ]
    ],
    'canvas' => [
        'root_account_id' => env('CANVAS_ROOT_ACCOUNT_ID', 1),
    ],

    'exception_reporting_channel' => env('EXCEPTION_REPORTING_CHANNEL', null),
    'exception_throttle_seconds'  => env('EXCEPTION_THROTTLE_SECONDS', 120),

    'metrics' => [
        'service_id'            => env('METRICS_SERVICE_ID', env('APP_NAME')),
        'enrollment_thresholds' => explode(',', env('METRICS_ENROLLMENT_THRESHOLDS', '')),
        'generation_gap_days'   => env('METRICS_GENERATION_GAP_DAYS', 3),
        'backfill_gap_days'     => env('METRICS_BACKFILL_GAP_DAYS', 7),
        'push_destination'      => env('METRICS_PUSH_DESTINATION'),
        'prefix'                => env('METRICS_PREFIX', 'canvas'),
        'host'                  => env('METRICS_HOST', env('APP_URL')),

        'splunk' => [
            'index'  => env('METRICS_SPLUNK_INDEX', 'canvas'),
            'source' => env('METRICS_SPLUNK_SOURCE', env('METRICS_SERVICE_ID', '')),
        ]

    ],
];
