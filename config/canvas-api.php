<?php

return [

    // configurations

    'defaults' => [
        'config'  => env('CANVAS_API_DEFAULT_CONFIG_KEY', 'uncg.test'), // config key where 'class' is set
        'adapter' => \Uncgits\CanvasApi\Adapters\Guzzle::class, // adapter class
    ],

    'configs' => [

        'uncg' => [
            'production' => [
                'class' => \App\CanvasApiConfigs\Production::class,
                'host'  => env('CANVAS_API_UNCG_PRODUCTION_HOST'),
                'token' => env('CANVAS_API_UNCG_PRODUCTION_TOKEN'),
                'proxy' => [
                    'use'  => env('CANVAS_API_UNCG_PRODUCTION_USE_PROXY', 'false') == 'true',
                    'host' => env('CANVAS_API_UNCG_PRODUCTION_PROXY_HOST'),
                    'port' => env('CANVAS_API_UNCG_PRODUCTION_PROXY_PORT'),
                ],
            ],
            'beta' => [
                'class' => \App\CanvasApiConfigs\Beta::class,
                'host'  => env('CANVAS_API_UNCG_BETA_HOST'),
                'token' => env('CANVAS_API_UNCG_BETA_TOKEN'),
                'proxy' => [
                    'use'  => env('CANVAS_API_UNCG_BETA_USE_PROXY', 'false') == 'true',
                    'host' => env('CANVAS_API_UNCG_BETA_PROXY_HOST'),
                    'port' => env('CANVAS_API_UNCG_BETA_PROXY_PORT'),
                ],
            ],
            'test' => [
                'class' => \App\CanvasApiConfigs\Test::class,
                'host'  => env('CANVAS_API_UNCG_TEST_HOST'),
                'token' => env('CANVAS_API_UNCG_TEST_TOKEN'),
                'proxy' => [
                    'use'  => env('CANVAS_API_UNCG_TEST_USE_PROXY', 'false') == 'true',
                    'host' => env('CANVAS_API_UNCG_TEST_PROXY_HOST'),
                    'port' => env('CANVAS_API_UNCG_TEST_PROXY_PORT'),
                ],
            ],
        ]
    ],

    // caching

    'cache_active'    => env('CANVAS_API_CACHING', 'on') == 'on', // set to 'on' or 'off' in .env file
    'cache_minutes'   => env('CANVAS_API_CACHE_MINUTES', 10),

    // cache these specific GET requests by client class. use * to cache all
    'cacheable_calls' => [
        'Uncgits\CanvasApi\Clients\Accounts' => [
            'getSubaccounts',
        ],
        // 'Uncgits\CanvasApi\Clients\Users' => ['*']
    ],

];
