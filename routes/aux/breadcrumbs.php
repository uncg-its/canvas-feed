<?php

// AUX FEED
Breadcrumbs::register('aux-feed', function ($breadcrumbs) {
    $breadcrumbs->push('Canvas Aux Feed', route('aux.index'));
});
Breadcrumbs::register('aux-feed.runs', function ($breadcrumbs) {
    $breadcrumbs->parent('aux-feed');
    $breadcrumbs->push('Runs', route('aux.runs.index'));
});
Breadcrumbs::register('aux-feed.runs.show', function ($breadcrumbs, $run) {
    $breadcrumbs->parent('aux-feed.runs');
    $breadcrumbs->push('Run ' . $run->id, route('aux.runs.show', ['run' => $run]));
});

Breadcrumbs::register('aux-feed.overrides', function ($breadcrumbs) {
    $breadcrumbs->parent('aux-feed');
    $breadcrumbs->push('Overrides', route('aux.overrides.index'));
});
Breadcrumbs::register('aux-feed.overrides.create', function ($breadcrumbs) {
    $breadcrumbs->parent('aux-feed.overrides');
    $breadcrumbs->push('Create New', route('aux.overrides.create'));
});
Breadcrumbs::register('aux-feed.overrides.show', function ($breadcrumbs, $override) {
    $breadcrumbs->parent('aux-feed.overrides');
    $breadcrumbs->push('Override ' . $override->id, route('aux.overrides.show', ['override' => $override]));
});

Breadcrumbs::register('aux-feed.calls', function ($breadcrumbs, $action) {
    $breadcrumbs->parent('aux-feed.' . $action->event . 's.show', $action->actionable);
    $breadcrumbs->push('Action ' . $action->id . ' API Calls', route('aux.calls.index', ['action' => $action]));
});
Breadcrumbs::register('aux-feed.calls.show', function ($breadcrumbs, $call) {
    $breadcrumbs->parent('aux-feed.calls', $call->action);
    $breadcrumbs->push('Call ' . $call->id, route('aux.calls.show', ['call' => $call]));
});

Breadcrumbs::register('aux-feed.users.index', function ($breadcrumbs) {
    $breadcrumbs->parent('aux-feed');
    $breadcrumbs->push('Canvas Users', route('aux.users.index'));
});
Breadcrumbs::register('aux-feed.users.show', function ($breadcrumbs, $canvasUser) {
    $breadcrumbs->parent('aux-feed.users.index');
    $breadcrumbs->push('Canvas User: ' . $canvasUser->email, route('aux.users.show', ['id' => $canvasUser->id]));
});
Breadcrumbs::register('aux-feed.users.history', function ($breadcrumbs, $canvasUser) {
    $breadcrumbs->parent('aux-feed.users.show', $canvasUser);
    $breadcrumbs->push('Event History', route('aux.users.history', ['id' => $canvasUser->id]));
});

Breadcrumbs::register('aux-feed.feed-changes.show', function ($breadcrumbs, $feedChange) {
    $breadcrumbs->parent('aux-feed');
    $breadcrumbs->push('Feed Change: ' . $feedChange->id, route('aux.feed-changes.show', ['feedChange' => $feedChange]));
});

Breadcrumbs::register('aux-feed.user-imports.index', function ($breadcrumbs) {
    $breadcrumbs->parent('aux-feed');
    $breadcrumbs->push('User Imports', route('aux.user-imports.index'));
});
