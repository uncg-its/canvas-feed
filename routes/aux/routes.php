<?php

use App\Http\Controllers\Aux\AuxController;
use App\Http\Controllers\Aux\RunController;
use App\Http\Controllers\Aux\CallController;
use App\Http\Controllers\Aux\ActionController;
use App\Http\Controllers\Aux\OverrideController;
use App\Http\Controllers\Aux\CanvasUserController;
use App\Http\Controllers\Aux\FeedChangeController;
use App\Http\Controllers\Aux\UserImportController;
use App\Http\Controllers\Aux\InvalidChangeController;

// Canvas Auxiliary Feed routes
Route::prefix('aux')->name('aux.')->middleware(['permission:user-feed.*'])->group(function () {
    Route::get('/', [AuxController::class, 'index'])->name('index');
    Route::get('restart-crons', [AuxController::class, 'restartCrons'])->name('restart-crons');

    Route::prefix('runs')->name('runs.')->group(function () {
        Route::get('/', [RunController::class, 'index'])->name('index');
        Route::get('{run}', [RunController::class, 'show'])->name('show');
    });
    Route::prefix('actions')->name('actions.')->group(function () {
        Route::get('{run}/actions', [ActionController::class, 'index'])->name('index');
        Route::get('{action}', [ActionController::class, 'show'])->name('show');
        Route::post('{action}/retry', [ActionController::class, 'retry'])->name('retry');
        Route::post('{run}/retry-all', [ActionController::class, 'retryAll'])->name('retry-all');
        Route::post('{action}/ignore', [ActionController::class, 'ignore'])->name('ignore');
    });
    Route::name('calls.')->group(function () {
        Route::get('action/{action}/calls', [CallController::class, 'index'])->name('index');
        Route::get('calls/{call}', [CallController::class, 'show'])->name('show');
    });

    Route::prefix('overrides')->name('overrides.')->group(function () {
        Route::get('/', [OverrideController::class, 'index'])->name('index');
        Route::get('/create', [OverrideController::class, 'create'])->name('create');
        Route::post('/create', [OverrideController::class, 'store'])->name('store');
        Route::get('/{override}', [OverrideController::class, 'show'])->name('show');
        Route::get('/{override}/change-time', [OverrideController::class, 'changeTime'])->name('change-time');
        Route::patch('/{override}/change-time', [OverrideController::class, 'changeTimeUpdate'])->name('change-time-update');
        Route::post('/{override}/cancel', [OverrideController::class, 'cancel'])->name('cancel');
        Route::post('/{override}/notify', [OverrideController::class, 'notify'])->name('notify');
    });

    Route::prefix('feed-changes')->name('feed-changes.')->group(function () {
        Route::get('/{feedChange}', [FeedChangeController::class, 'show'])->name('show');
    });

    Route::prefix('invalid-changes')->name('invalid-changes.')->group(function () {
        Route::get('/', [InvalidChangeController::class, 'index'])->name('index');
        Route::post('/{change}/ignore', [InvalidChangeController::class, 'ignore'])->name('ignore');
    });

    Route::prefix('users')->name('users.')->group(function () {
        Route::get('/', [CanvasUserController::class, 'index'])->name('index');
        Route::get('{id}', [CanvasUserController::class, 'show'])->name('show');
        Route::get('{id}/history', [CanvasUserController::class, 'history'])->name('history');
    });

    Route::prefix('user-imports')->name('user-imports.')->group(function () {
        Route::get('/', [UserImportController::class, 'index'])->name('index');
        Route::post('/', [UserImportController::class, 'store'])->name('store');
    });
});
