<?php

use App\Http\Controllers\Sis\SisController;
use App\Http\Controllers\Sis\SisZipController;
use App\Http\Controllers\Sis\SisDiffController;
use App\Http\Controllers\Sis\SisUserController;
use App\Http\Controllers\Sis\SisCourseController;
use App\Http\Controllers\Sis\SisUploadController;
use App\Http\Controllers\Sis\SisCsvFileController;
use App\Http\Controllers\Sis\SisSectionController;
use App\Http\Controllers\Sis\SisDiffUserController;
use App\Http\Controllers\Sis\SisRoleAuditController;
use App\Http\Controllers\Sis\SisDiffCourseController;
use App\Http\Controllers\Sis\SisDiffEntityController;
use App\Http\Controllers\Sis\SisEnrollmentController;
use App\Http\Controllers\Sis\SisDiffSectionController;
use App\Http\Controllers\Sis\LegacySisDiffUserController;
use App\Http\Controllers\Sis\SisDiffEnrollmentController;
use App\Http\Controllers\Sis\LegacySisDiffCourseController;
use App\Http\Controllers\Sis\LegacySisDiffSectionController;
use App\Http\Controllers\Sis\SisCsvFileValidationController;
use App\Http\Controllers\Sis\LegacySisDiffEnrollmentController;

// SIS Feed routes
Route::middleware(['permission:canvasfeed.view'])->prefix('sis')->name('sis.')->group(function () {
    Route::get('/', [SisController::class, 'index'])->name('index');
    Route::resource('csv-file-validations', SisCsvFileValidationController::class)->only([
        'index', 'show'
    ]);
    Route::resource('zips', SisZipController::class)->only([ 'index', 'show', 'update' ]);
    Route::resource('diffs', SisDiffController::class)->only([ 'index', 'show', 'update' ]);
    Route::resource('csv-files', SisCsvFileController::class)->only([ 'show' ]);

    Route::get('diff/{diff}/{type}', [SisDiffEntityController::class, 'index'])->name('diff-entities');

    Route::prefix('zip-logs')->name('zip-logs.')->group(function () {
        Route::get('users', [SisUserController::class, 'index'])->name('users');
        Route::get('enrollments', [SisEnrollmentController::class, 'index'])->name('enrollments');
        Route::get('courses', [SisCourseController::class, 'index'])->name('courses');
        Route::get('sections', [SisSectionController::class, 'index'])->name('sections');
    });

    Route::prefix('diff-logs')->name('diff-logs.')->group(function () {
        Route::get('users', [SisDiffUserController::class, 'index'])->name('users');
        Route::get('enrollments', [SisDiffEnrollmentController::class, 'index'])->name('enrollments');
        Route::get('courses', [SisDiffCourseController::class, 'index'])->name('courses');
        Route::get('sections', [SisDiffSectionController::class, 'index'])->name('sections');
    });

    Route::prefix('legacy-diff-logs')->name('legacy-diff-logs.')->group(function () {
        Route::get('users', [LegacySisDiffUserController::class, 'index'])->name('users');
        Route::get('enrollments', [LegacySisDiffEnrollmentController::class, 'index'])->name('enrollments');
        Route::get('courses', [LegacySisDiffCourseController::class, 'index'])->name('courses');
        Route::get('sections', [LegacySisDiffSectionController::class, 'index'])->name('sections');
    });


    Route::prefix('uploads')->name('uploads.')->group(function () {
        Route::middleware('throttle:120,1')->get('/', [SisUploadController::class, 'index'])->name('index');
    });

    Route::prefix('role-audit')->name('role-audit.')->group(function () {
        Route::get('/', [SisRoleAuditController::class, 'index'])->name('index');
    });
});
