<?php

use App\Http\Controllers\ApiCheckController;
use App\Http\Controllers\ApiOutageController;
use App\Http\Controllers\CcpsCore\AuthController;
use App\Http\Controllers\CcpsCore\HomeController;
use App\Http\Controllers\DownloadedFileController;
use App\Http\Controllers\CcpsCore\AccountController;
use App\Http\Controllers\CcpsCore\ChannelVerificationController;
use Laravel\Horizon\Http\Controllers\HomeController as HorizonHomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('home', [HomeController::class, 'index']);

Route::group(['prefix' => 'account'], function () {
    Route::get('/', [AccountController::class, 'index'])->name('account');
    Route::get('/settings', [AccountController::class, 'settings'])->name('account.settings');
    Route::post('/settings', [AccountController::class, 'updateSettings'])->name('account.settings.update');
    Route::get('/profile', [AccountController::class, 'profile'])->name('profile.show');
    Route::get('/profile/edit', [AccountController::class, 'editProfile'])->name('profile.edit');
    Route::patch('/profile/', [AccountController::class, 'updateProfile'])->name('profile.update');

    Route::prefix('tokens')->name('account.tokens.')->group(function () {
        Route::get('/', [AccountController::class, 'tokens'])->name('index');
        Route::get('create', [AccountController::class, 'createToken'])->name('create');
        Route::post('/', [AccountController::class, 'storeToken'])->name('store');
        Route::get('{token}', [AccountController::class, 'editToken'])->name('edit');
        Route::patch('{token}', [AccountController::class, 'updateToken'])->name('update');
        Route::delete('{token}', [AccountController::class, 'revokeToken'])->name('revoke');
    });

    Route::group(['prefix' => 'notifications'], function () {
        Route::get('/', [AccountController::class, 'notifications'])->name('account.notifications.index');
        Route::get('/channels/create', [AccountController::class, 'createChannel'])->name('account.notifications.channels.create');
        Route::post('/channels/create', [AccountController::class, 'storeChannel'])->name('account.notifications.channels.store');
        Route::delete('/channels/{channel}', [AccountController::class, 'destroyChannel'])->name('account.notifications.channels.destroy');
        // Route::get('/channels/{channel}/resend-verification', [ChannelVerificationController::class, 'resend'])->name('account.notifications.channels.resend-verification');
        // Route::get('/channels/{channel}/verify', [ChannelVerificationController::class, 'verify'])->name('account.notifications.channels.verify');
        // Route::get('/channels/{channel}/test', [ChannelVerificationController::class, 'test'])->name('account.notifications.channels.test');

        Route::get('/configure', [AccountController::class, 'configure'])->name('account.notifications.configure');
        Route::post('/configure', [AccountController::class, 'configureSave'])->name('account.notifications.configure.save');
    });
});

Route::middleware('auth')->group(function () {
    Route::prefix('api-checks')->name('api-checks.')->group(function () {
        Route::get('/', [ApiCheckController::class, 'index'])->name('index');
    });

    Route::prefix('api-outages')->name('api-outages.')->group(function () {
        Route::get('/', [ApiOutageController::class, 'index'])->name('index');
        Route::get('/{outage}', [ApiOutageController::class, 'show'])->name('show');
    });

    Route::prefix('downloads')->name('downloads.')->group(function () {
        Route::post('/', [DownloadedFileController::class, 'store'])->name('store');
    });
});


// CCPS Core routes
Uncgits\Ccps\Support\Facades\CcpsCore::routes();

// For local auth
Auth::routes();

// Socialite - User Account OAuth Routes
Route::get('auth/{provider}', [AuthController::class, 'redirectToProvider'])->name('oauth');
Route::get('auth/{provider}/callback', [AuthController::class, 'handleProviderCallback'])->name('oauth.callback');

// Canvas Auxiliary Feed
include('aux/routes.php');

// SIS feed
include('sis/routes.php');

// Horizon override
Route::group([
    'prefix'     => 'horizon',
    'namespace'  => 'Laravel\Horizon\Http\Controllers',
    'middleware' => config('horizon.middleware', 'web'),
], function () {
    // Catch-all Route...
    Route::get('/{view?}', [HorizonHomeController::class, 'horizon'])->where(
        'view',
        '(.*)'
    )->name('horizon.index');
});
