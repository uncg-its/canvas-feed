<?php

use App\Http\Controllers\Api\SisUploadController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1/')->name('api.')->group(function () {
    Route::prefix('sis-uploads')->name('sis-uploads.')->group(function () {
        Route::post('/show', [SisUploadController::class, 'show'])->name('show');
    });
});
