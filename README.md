# Canvas Feed

This application is the main engine behind feeding SIS data into Canvas at UNCG. The application ingests, processes, and validates CSV files sent from the SIS (currently Banner), and then diffs them to create SIS uploads to send to Canvas.

The application also handles provisioning of additional accounts by way of an API-based sync using Grouper as the source. This is helpful for accounts that do not show up in the SIS (e.g. non-primary accounts).

# WIP

This documentation is a work-in-progress. Please excuse our dust.

# Contributing

Please feel free to submit a pull request to this package to help in its development. The package was initially built to serve the needs of UNC Greensboro's immediate needs, and is therefore not necessarily a comprehensive solution.

# License

See the [LICENSE](LICENSE.md) file for license rights and limitations (BSD).

# Questions? Concerns?

Please use the Issue Tracker on this repository for reporting bugs. For security-related concerns, please contact its-laravel-devs-l@uncg.edu directly instead of using the Issue Tracker.

See the

# Version History

### 2.3.0

- Open Source licensing (BSD 3-clause)

### 2.2.6

- CCPS Core 3.3.1

### 2.2.5

- Remove old views for notifications so that emails don't fail

### 2.2.4

- Fixes a lingering bug from 2.1.0 (or rather fixes the fix that wasn't actually fixing anything)

### 2.2.3

- Adds proxy configuration items to Splunk rest client package config

### 2.2.2

- Fixed: default route namespace (refactors all routes to tuple syntax and removes default controller namespace in routing)
- Fixed: User Role Audit feature

### 2.2.1

- Adds ability to reset a stuck 'importing' diff

### 2.2.0

- CCPS Core 3.3

### 2.1.0

- Treat changed `login_id` as same user (alters secondary checksum of `SisUser`)

### 2.0.2

- Hotfix for dependencies (including a PHP8 fix for Canvas API Library)
- Hotfix for log viewer

### 2.0.1

- Hotfix for `checksummable()` incompatibility due to stricter PHP 8 rules

### 2.0.0

- Updates to CCPS Core 3.x, which includes a migration of the UI to TALL Stack.
- Addition of "Cron Jobs" to SIS Menu

### 1.1.5

- critical bugfix that does not allow a ZIP or DIFF to be purged before a logical end to its lifecycle. This solves a problem with data being purged from ZIPs that have not yet been validated / diffed, which results in empty diffs.

### 1.1.4

- further bugfix, placing the ZIP status changing code outside of the database transaction (which was being rolled back)
- fix for handling status of ZIPs that fail to upload to Canvas (due to import error on Canvas side)

### 1.1.3

- bugfix for 1.1.2, assigning ZIP to be validated to the class as a property, so that it can be found in the `catch` statement.

### 1.1.2

- If ZIP validation fails, the ZIP will be marked back to "imported" rather than stuck at "validating"

### 1.1.1

- Fix for missing `$outage` variable in API check reporting
- Update Composer dependencies
- Fix return value for `InitiateNewRun` job to stop throwing exceptions

## 1.1

- Fix for missing `CronjobResult` classes in cron jobs
- Handles `imported-with-messages` status from Canvas SIS Importcanvasapis

## 1.0.1

- Update to Laravel Framework ^7.22 (security patch)

## 1.0

- Official versioning scheme using semver
- CCPS Core 2.0
- Horizon 4.3
- CCPS User Feed 3.0
- Refactoring of all cronjobs to comply with CCPS Core 2.0 requirements
- Wire up missing notifications for cronjob failure and stale lock file detection
- Canvas API Wrapper ^0.2 (and Canvas API Library ^0.6)
