@extends('layouts.wrapper', [
    'pageTitle' => 'API Status Checks | Index'
])

@section('content')
    {!! Breadcrumbs::render('api-checks.index') !!}
    <x-h1>API Status Checks</x-h1>
    <div class="flex">
        <div class="col">
			<x-p>An <strong>API Status Check</strong> is a regular activity wherein a simple call is made to the Canvas API. This is done to ensure that the API is still reachable, since use of the Canvas API is necessary for functionality of this application. If enough consecutive checks fail, the application will be disabled automatically.</x-p>
			@if (!request()->has('all'))
				<x-p><strong>Note</strong>: for performance reasons, by default only the most recent 250 status checks are shown. You can choose to see the entire list by using the "View All" button below.</x-p>
			@else
				<x-p><strong>Note</strong>: showing complete API Status Check history.</x-p>
			@endif
        </div>
    </div>
    <x-hr />
	@if ($checks->isNotEmpty())
		@if (request()->has('all'))
            <x-button href="{{ route('api-checks.index') }}" color="red" size="sm">
                <x-fas>redo</x-fas> Reset
            </x-button>
		@else
            <x-button color="blue" size="sm" href="{{ route('api-checks.index', ['all' => 'y']) }}">
                <x-fas>eye</x-fas> View all
            </x-button>
		@endif
        <x-table>
            <x-slot name="th">
                <x-th>Date / Time</x-th>
                <x-th>Status</x-th>
                <x-th>Message</x-th>
                <x-th>Related Outage</x-th>
            </x-slot>
            <x-slot name="tbody">
                @foreach ($checks as $check)
                    <x-tr>
                        <x-td>{{ $check->created_at }}</x-td>
                        <x-td class="text-{{ $check->status_class }}">
                            <i class="{{ $check->status_icon }}"></i>
                            {{ $check->up ? "up" : "down" }}
                        </x-td>
                        <x-td>{{ $check->message }}</x-td>
                        <x-td>
                            @if (!is_null($check->outage))
                                <x-a href="{{ route('api-outages.show', ['outage' => $check->outage]) }}">Outage {{ $check->outage->id }}</x-a>
                            @endif
                        </x-td>
                    </x-tr>
                @endforeach
            </x-slot>
        </x-table>
    @else
    	<x-p>No API Status Checks exist.</x-p>
    @endif
@endsection
