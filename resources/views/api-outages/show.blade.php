@extends('layouts.wrapper', [
    'pageTitle' => 'API Outage ' . $outage->id
])

@section('content')
	{!! Breadcrumbs::render('api-outages.show', $outage) !!}
	<x-h1>API Outage {{ $outage->id }}</x-h1>
	<div class="flex">
		<div class="w-1/2">
            <x-card title="Status Information">
                <x-p><strong>Status</strong>:
                    <span class="text-{{ $outage->status_class }}">
                        <i class="fas fa-{{ $outage->status_icon }}"></i>
                        {{ $outage->status }}
                    </span>
                </x-p>
                <x-p><strong>Created at</strong>: {{ $outage->created_at }}</x-p>
                @if (!is_null($outage->resolved_at))
                    <x-p><strong>Resolved at</strong>: {{ $outage->resolved_at }}</x-p>
                @endif
            </x-card>
		</div>
		<div class="w-1/2">
            <x-card title="Contributing Status Checks">
                <x-ul>
                    @foreach ($outage->api_status_checks as $check)
                        <li>{{ $check->created_at }}</li>
                    @endforeach
                </x-ul>
            </x-card>
		</div>
	</div>
@endsection
