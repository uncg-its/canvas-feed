@extends('layouts.wrapper', [
    'pageTitle' => 'API Outages'
])

@section('content')
	{!! Breadcrumbs::render('api-outages.index') !!}
    <x-h1>API Outages</x-h1>
    <div class="flex">
        <div class="col">
            <x-p>An <strong>API Outage</strong> occurs when enough consecutive API Status Checks fail. (The threshold is configurable in Feed Settings.) During an API Outage the primary Cron Jobs controlling both the User Feed and Canvas Auxiliary Feed are disabled. Once the application determines that the API is reachable again, the Outage will be resolved automatically, and the Cron Jobs reactivated.</x-p>
        </div>
    </div>
    <x-hr />
    @if ($outages->isNotEmpty())
        <x-table>
            <x-slot name="th">
                <x-th>ID</x-th>
                <x-th>Created At</x-th>
                <x-th>Resolved At</x-th>
                <x-th>Status</x-th>
                <x-th># of Checks</x-th>
                <x-th>Actions</x-th>
            </x-slot>
            <x-slot name="tbody">
                @foreach ($outages as $outage)
                    <x-tr>
                        <x-td>{{ $outage->id }}</x-td>
                        <x-td>{{ $outage->created_at }}</x-td>
                        <x-td>{{ $outage->resolved_at }}</x-td>
                        <x-td class="text-{{ $outage->status_class }}">
                            <i class="fa fa-{{ $outage->status_icon }}"></i>
                            {{ $outage->status }}
                        </x-td>
                        <x-td>{{ $outage->api_status_checks->count() }}</x-td>
                        <x-td>
                            <x-button color="blue" size="sm" href="{{ route('api-outages.show', ['outage' => $outage]) }}">
                                <x-fas>list</x-fas> Details
                            </x-button>
                        </x-td>
                    </x-tr>
                @endforeach
            </x-slot>
        </x-table>
    @else
    	<x-p>No API Outages exist.</x-p>
    @endif
@endsection
