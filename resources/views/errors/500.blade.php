@extends('layouts.wrapper', [
    'pageTitle' => '500'
])

@section('content')
    <x-h2>500 - Internal Server Error</x-h2>
    <x-p>{{ $exception->getMessage() }}</x-p>
@endsection
