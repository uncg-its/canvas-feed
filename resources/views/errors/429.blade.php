@extends('layouts.wrapper', [
    'pageTitle' => '429'
])

@section('content')
    <x-h2>429 - Rate Limit Exceeded</x-h2>
    <div class="alert alert-danger">
        <i class="fas fa-exclamation-triangle"></i>
        <strong>Error:</strong> Rate Limit Exceeded. Please wait before trying to visit this page again.
    </div>
@endsection
