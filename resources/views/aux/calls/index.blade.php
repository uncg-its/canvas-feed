@extends('layouts.wrapper', [
    'pageTitle' => 'API Calls for Action ' . $action->id
])

@section('content')
	{!! Breadcrumbs::render('aux-feed.calls', $action) !!}
	<div class="flex">
		<div class="w-1/2">
			<x-h1>API Calls for Action {{ $action->id }}</x-h1>
		</div>
		<div class="w-1/2 d-flex justify-content-end align-items-center">
            <x-button color="yellow" size="sm" href="{{ route('aux.' . $action->event . 's.show', [$action->event => $action->actionable_id]) }}">
                <x-fas>arrow-left</x-fas> Back
            </x-button>
		</div>
	</div>
	<div class="flex">
		<div class="col">
			<x-p>An <strong>API Call</strong> represents a single attempt to interact with the Canvas API - one request, one response. API Calls are initiated by an Action. In the case of a failure, an Action may be retried, generating another API call. <span data-toggle="tooltip" class="text-blue-500" title="An Add Action is generated. The application will attempt to call the user.CreateUser endpoint on the Canvas API to add the user to Canvas.">Example</span></x-p>
		</div>
	</div>
	<x-hr />
	@if ($action->calls->isNotEmpty())
		@component('components.table')
		    @slot('th')
		        <x-th>ID</x-th>
		        <x-th>Status</x-th>
		        <x-th>Endpoint</x-th>
		        <x-th>Method</x-th>
		        <x-th>Response Code</x-th>
		        <x-th>Actions</x-th>
		    @endslot
		    @slot('tbody')
		        @foreach ($action->calls as $call)
		        	<x-tr>
		        		<x-td>{{ $call->id }}</x-td>
		        		<x-td class="text-{{ $call->status_class }}">
		        			<i class="{{ $call->status_icon }}"></i>
		        			{{ $call->status }}
		        		</x-td>
		        		<x-td>{{ $call->endpoint }}</x-td>
		        		<x-td>{{ $call->method }}</x-td>
		        		<x-td>{{ $call->response_code }}</x-td>
		        		<x-td>
                            <x-button color="blue" size="sm" href="{{ route('aux.calls.show', ['call' => $call->id]) }}">
                                <x-fas>list</x-fas> Details
                            </x-button>
		        		</x-td>
		        	</x-tr>
		        @endforeach
		    @endslot
		@endcomponent
	@else
		<x-p>No calls for this action.</x-p>
	@endif
@endsection
