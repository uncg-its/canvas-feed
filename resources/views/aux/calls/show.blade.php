@extends('layouts.wrapper', [
    'pageTitle' => 'API Call ' . $call->id
])

@section('content')
    {!! Breadcrumbs::render('aux-feed.calls.show', $call) !!}
	<div class="flex">
		<div class="w-1/2">
			<x-h1>API Call {{ $call->id }}</x-h1>
		</div>
		<div class="w-1/2 d-flex justify-content-end align-items-center">
            <x-button color="yellow" size="sm" href="{{ route('aux.calls.index', ['action' => $call->action_id]) }}">
                <x-fas>arrow-left</x-fas> Back to list
            </x-button>
		</div>
	</div>
    <div class="flex">
        <div class="col">
            <x-p>Details on a single API Call, including complete request and response information.</x-p>
        </div>
    </div>
    <div class="flex">
        <div class="w-1/2">
            <div class="card">
                <div class="card-header">Request Information</div>
                <div class="card-body">
                    <x-p><strong>Endpoint</strong>: {{ $call->endpoint }}</x-p>
                    <x-p><strong>Method</strong>: {{ $call->method }}</x-p>
                    <x-p><strong>Attempted at</strong>: {{ $call->created_at->toDateTimeString() }}</x-p>
                    <x-p><strong>Parameters</strong>:
                        <x-button color="blue" size="sm" x-on:click="showRequestBody=true">
                            Show
                        </x-button>
                    </x-p>
                </div>
            </div>
        </div>
        <div class="w-1/2">
            <div class="card">
                <div class="card-header">Response Information</div>
                <div class="card-body">
                    <x-p><strong>Status</strong>: <span class="text-{{ $call->status_class }}">
                    	<i class="{{ $call->status_icon }}"></i>
                    	{{ $call->status }}
                    </span></x-p>
                    <x-p><strong>Response Code</strong>: {{ $call->response_code }}</x-p>
                    <x-p><strong>Message</strong>: {{ $call->message }}</x-p>
                    <x-p><strong>Body</strong>:
                        <x-button color="blue" size="sm" x-on:click="showResponseBody=true">
                            Show
                        </x-button>
                    </x-p>
                    <x-p><strong>Raw Response</strong>:
                        <x-button color="blue" size="sm" x-on:click="showRawResponse=true">
                            Show
                        </x-button>
                    </x-p>
                </div>
            </div>
        </div>
    </div>
    <x-modal trigger="showRequestBody">
        <x-h4>Request Body</x-h4>
        <pre>{{ json_encode(json_decode($call->request_parameters), JSON_PRETTY_PRINT) }}</pre>
    </x-modal>
    <x-modal trigger="showResponseBody">
        <x-h4>Response Body</x-h4>
        <pre>{{ json_encode(json_decode($call->result)->response->body, JSON_PRETTY_PRINT) }}</pre>
    </x-modal>
    <x-modal trigger="showRawResponse">
        <x-h4>Raw Response</x-h4>
        <pre>{{ json_encode(json_decode($call->result)->response, JSON_PRETTY_PRINT) }}</pre>
    </x-modal>
@endsection
