@extends('layouts.wrapper', [
    'pageTitle' => 'User Imports | Index'
])

@section('content')
    {!! Breadcrumbs::render('aux-feed.user-imports.index') !!}
    <x-h1>User Imports</x-h1>
    <x-p>A <strong>User Import</strong> is a process that populates the local database based on a list of users that are known to exist in Canvas, so that they are also known to exist by this application for diffing purposes.</x-p>
    <x-p>This function should be used sparingly, and is not to be confused with the Override process. The purpose of this process is more for gap-filling of a population that "should" exist in Canvas organically; an Override is appropriate when trying to grant an exception to a user account.</x-p>
    <x-hr />
    <x-h3>Import Status</x-h3>
    @if ($imports->isNotEmpty())
        <x-table>
            <x-slot name="th">
                <x-th>ID</x-th>
                <x-th>Created by</x-th>
                <x-th>Login IDs</x-th>
                <x-th>Message</x-th>
                <x-th>Created at</x-th>
                <x-th>Completed at</x-th>
            </x-slot>
            <x-slot name="tbody">
                @foreach ($imports as $import)
                    <x-tr>
                        <x-td>{{ $import->id }}</x-td>
                        <x-td>
                            {{ $import->created_by_user->email }}
                        </x-td>
                        <x-td>
                            <div x-data="{showIds: false}">
                                <x-button color="blue" size="sm" x-on:click="showIds=true">
                                    <x-fas>eye</x-fas> Show
                                </x-button>
                                <x-modal trigger="showIds">
                                    <pre>{{ json_encode(json_decode($import->sis_login_ids), JSON_PRETTY_PRINT) }}</pre>
                                </x-modal>
                            </div>
                        </x-td>
                        <x-td>
                            @if(!empty($import->message))
                                <div x-data="{showMessage: false}">
                                    <x-button color="blue" size="sm" x-on:click="showMessage=true">
                                        <x-fas>eye</x-fas> Show
                                    </x-button>
                                    <x-modal trigger="showMessage">
                                        <pre>{{ json_encode(json_decode($import->message), JSON_PRETTY_PRINT) }}</pre>
                                    </x-modal>
                                </div>
                            @endif
                        </x-td>
                        <x-td>{{ $import->created_at }}</x-td>
                        <x-td>{{ $import->completed_at ?? '' }}</x-td>
                    </x-tr>
                @endforeach

            </x-slot>
        </x-table>
    @else
    	<x-p>No Imports to show.</x-p>
    @endif

    <x-hr />
    <x-h3>Create New Import</x-h3>
    <x-form-post action="{{ route('aux.user-imports.store') }}">
        <x-input-textarea
            name="sis_login_ids"
            label="SIS Login IDs"
            required
            help="Enter SIS Login IDs (UNCG Usernames) here, separated by a newline, comma, or semicolon."
        />
        <x-button color="green" size="sm">
            <x-fas>check</x-fas> Submit
        </x-button>
    </x-form-post>
@endsection
