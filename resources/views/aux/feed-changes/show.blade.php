@extends('layouts.wrapper', [
    'pageTitle' => 'Feed Changes | ' . $feedChange->id
])

@section('content')
	{!! Breadcrumbs::render('aux-feed.feed-changes.show', $feedChange) !!}
	<div class="flex">
		<div class="w-1/2">
			<x-h1>Feed Change {{ $feedChange->id }}</x-h1>
		</div>
		<div class="w-1/2 d-flex justify-content-end align-items-center">
			{{-- nothing here --}}
		</div>
	</div>
	<div class="flex mt-3">
		<div class="w-1/2">
			<div class="card">
				<div class="card-header">Basic Information</div>
				<div class="card-body">
					<x-p>
						<strong>Canvas User</strong>:
                        {{ $feedChange->canvas_user->email }}
					</x-p>
					<x-p><strong>Type</strong>: {{ $feedChange->type }}</x-p>
				</div>
			</div>
		</div>
		<div class="w-1/2">
			<div class="card">
				<div class="card-header">Run Information</div>
                <div class="card-body">
                    <x-p><strong>Created at</strong>: {{ $feedChange->run->created_at }}</x-p>
                    @if ($feedChange->run->completed_at)
                        <x-p><strong>Completed at</strong>: {{ $feedChange->run->completed_at }}</x-p>
                    @endif
                    <x-p><strong>Status</strong>:
                        <span class="text-{{ $feedChange->run->status_class }}">
                            <i class="fa fa-{{ $feedChange->run->status_icon }}"></i>
                            {{ $feedChange->run->status }}
                        </span>
                    </x-p>
                    <x-button color="blue" size="sm" href="{{ route('aux.runs.show', ['run' => $feedChange->run]) }}">
                        <x-fas>list</x-fas> Details
                    </x-button>
                </div>
			</div>
		</div>
	</div>
	<div class="flex mt-3">
		<div class="w-1/2">
			<div class="card">
				<div class="card-header">Action Information</div>
				<div class="card-body">
					<x-p><strong>Type</strong>: {{ $feedChange->action->type }}</x-p>
					<x-p><strong>Status</strong>:
						<span class="text-{{ $feedChange->action->status_class }}">
							<i class="{{ $feedChange->action->status_icon }}"></i>
							{{ $feedChange->action->status }}
						</span>
					</x-p>
					@if (!is_null($feedChange->action->completed_at))
						<x-p><strong>Completed at</strong>: {{ $feedChange->action->completed_at }}</x-p>
					@endif
                    <x-button color="blue" size="sm" href="{{ route('aux.calls.index', ['action' => $feedChange->action]) }}">
                        <x-fas>code</x-fas> API Calls
                    </x-button>
				</div>
			</div>
		</div>

	</div>
	<div class="flex mt-3">
		<div class="w-1/2">
			<div class="card">
				<div class="card-header">Actions</div>
				<div class="card-body d-flex justify-content-start align-items-center">

				</div>
			</div>

		</div>
	</div>
@endsection

@section('scripts')
    <script>
        $(function() {
            $('#notify').on('click', function() {
                return confirm('This action will manually send an email notification to the Contact Email address listed for this override, regardless of the application\'s normal notification settings. Are you sure you want to proceed?');
            });

            $('#cancel').on('click', function() {
                return confirm('Are you sure you want to cancel this override? This cannot be undone.');
            });
        });
    </script>
@endsection
