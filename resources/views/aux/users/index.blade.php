@extends('layouts.wrapper', [
    'pageTitle' => 'Canvas Users | Index'
])

@section('content')
	{!! Breadcrumbs::render('aux-feed.users.index') !!}
    <x-h1>Canvas Users</x-h1>
    <div class="flex">
        <x-card title="Search / Filter" class="w-full">
            <x-form-get action="{{ route('aux.users.index') }}">
                <div class="flex gap-2">
                    <x-input-text
                        name="email"
                        label="Email"
                    />
                    <x-input-text
                        name="first_name"
                        label="First Name"
                    />
                    <x-input-text
                        name="last_name"
                        label="Last Name"
                    />
                    <x-input-select
                        name="status"
                        label="Status"
                        :options="['' => '--- Select --- ', 'active' => 'Active', 'inactive' => 'Inactive']"
                    />
                    <div class="flex items-end mb-4">
                        <x-button color="green" size="sm">
                            <x-fas>check</x-fas> Submit
                        </x-button>
                        <x-button color="red" size="sm" href="{{ route('aux.users.index') }}">
                            <x-fas>undo</x-fas> Reset
                        </x-button>
                    </div>
                </div>
            </x-form-get>
        </x-card>
    </div>
    <div class="mt-3">
	    @if ($users->isNotEmpty())
            <x-table>
                <x-slot name="th">
                    <x-th>ID</x-th>
                    <x-th>Email</x-th>
                    <x-th>First Name</x-th>
                    <x-th>Last Name</x-th>
                    <x-th>Status</x-th>
                    <x-th>Actions</x-th>
                </x-slot>
                <x-slot name="tbody">
                    @foreach ($users as $canvasUser)
                    <x-tr>
                        <x-td>{{ $canvasUser->primary_id }}</x-td>
                        <x-td>{{ $canvasUser->email }}</x-td>
                        <x-td>{{ $canvasUser->first_name }}</x-td>
                        <x-td>{{ $canvasUser->last_name }}</x-td>
                        <x-td class="text-{{ $canvasUser->status_class }}">
                            <i class="{{ $canvasUser->status_icon }}"></i>
                            {{ $canvasUser->active ? 'active' : 'deleted' }}
                        </x-td>
                        <x-td>
                            <x-button color="blue" size="sm" href="{{ route('aux.users.show', ['id' => $canvasUser->id]) }}">
                                <x-fas>list</x-fas> Details
                            </x-button>
                        </x-td>
                    </x-tr>
                    @endforeach
                </x-slot>
            </x-table>
            {{ $users->links() }}
		@else
			<x-p>No users found</x-p>
		@endif
   </div>
@endsection
