@extends('layouts.wrapper', [
    'pageTitle' => 'Canvas User: ' . $canvasUser->primary_id
])

@section('content')
    {!! Breadcrumbs::render('aux-feed.users.show', $canvasUser) !!}
    <x-h1>Canvas User: {{ $canvasUser->email }} ({{ $canvasUser->primary_id }})</x-h1>
    <div class="flex mt-3">
        <div class="w-1/2">
            <div class="card">
                <div class="card-header">User Information</div>
                <div class="card-body">
                    <x-p><strong>First Name</strong>: {{ $canvasUser->first_name }}</x-p>
                    <x-p><strong>Last Name</strong>: {{ $canvasUser->last_name }}</x-p>
                    <x-p><strong>Primary ID</strong>: {{ $canvasUser->primary_id }}</x-p>
                    <x-p><strong>Email</strong>: {{ $canvasUser->email }}</x-p>
                    <x-p>
                        <strong>Current Canvas Status</strong>:
                        <span class="text-{{ $canvasUser->status_class }}">
                            <i class="{{ $canvasUser->status_icon }}"></i>
                            {{ $canvasUser->status }}
                        </span>
                    </x-p>
                </div>
            </div>

        </div>
        <div class="w-1/2">
            <div class="card">
                <div class="card-header">Feed Information</div>
                <div class="card-body">
                    <x-p><strong>Last 5 Events</strong>:</x-p>
                    @if ($canvasUser->events->isNotEmpty())
                        <x-ul>
                            @foreach ($canvasUser->events->sortByDesc('created_at')->take(5) as $event)
                                <li><x-a href="{{ route('aux.' . $event['type'] . 's.show', $event['actionable']) }}">{{ ucwords(str_replace('-', ' ', $event['type'])) }} {{ $event['actionable']->id }} - {{ $event['actionable']->created_at }} ({{ $event['actionable']->type }})</x-a></li>
                            @endforeach
                        </x-ul>
                        <x-button color="blue" size="sm" href="{{ route('aux.users.history', ['id' => $canvasUser->id]) }}">
                            <x-fas>stopwatch</x-fas> Complete Event History
                        </x-button>
                    @else
                        <x-ul>
                            <li>none</li>
                        </x-ul>
                    @endif
                    <x-p><strong>First Appeared</strong>:
                        @if(!is_null($canvasUser->first_appeared))
                            <x-a href="{{ route('aux.' . $canvasUser->first_appeared['type'] . 's.show', $canvasUser->first_appeared['actionable']) }}">{{ ucwords(str_replace('-', ' ', $canvasUser->first_appeared['type'])) }} {{ $canvasUser->first_appeared['actionable']->id }} - {{ $canvasUser->first_appeared['actionable']->created_at }}</x-a>
                        @else
                            <x-p><em>Manual Import</em></x-p>
                        @endif
                    </x-p>
                </div>
            </div>
            <div class="card mt-3">
                <div class="card-header">Override Information</div>
                <div class="card-body">
                    @if ($canvasUser->overrides->isNotEmpty())
                        <x-ul>
                            @foreach ($canvasUser->overrides->sortByDesc('created_at') as $override)
                                <li>
                                    <x-a href="{{ route('aux.overrides.show', ['override' => $override]) }}">Override {{ $override->id }} - {{ $override->created_at }}</x-a>
                                </li>
                            @endforeach
                        </x-ul>
                    @else
                        <x-p>No overrides for this user.</x-p>
                    @endif
                </div>
            </div>

        </div>
    </div>
@endsection
