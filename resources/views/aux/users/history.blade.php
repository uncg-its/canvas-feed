@extends('layouts.wrapper', [
    'pageTitle' => 'Canvas User Run History: ' . $canvasUser->email
])

@section('content')
	{!! Breadcrumbs::render('aux-feed.users.history', $canvasUser) !!}
    <x-h1>Canvas User Event History: {{ $canvasUser->email }}</x-h1>
    @if ($events->isNotEmpty())
        <x-table>
            <x-slot name="th">
                <x-th>Type</x-th>
                <x-th>Created at</x-th>
                <x-th>Action taken</x-th>
                <x-th>Actions</x-th>
            </x-slot>
            <x-slot name="tbody">
                @foreach ($events as $event)
                    <x-tr>
                        <x-td>{{ ucwords(str_replace('-', ' ', $event['type'])) }}</x-td>
                        <x-td>{{ $event['actionable']->created_at }}</x-td>
                        <x-td>{{ $event['actionable']->type }}</x-td>
                        <x-td>
                            <x-button color="blue" size="sm" href="{{ route('aux.' . $event['type'] . 's.show', $event['actionable']) }}">
                                <x-fas>list</x-fas> Details
                            </x-button>
                        </x-td>
                    </x-tr>
                @endforeach
            </x-slot>
        </x-table>
        {{ $events->links() }}
    @else
    	<x-p>No event history to show.</x-p>
    @endif
@endsection
