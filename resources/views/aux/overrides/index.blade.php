@extends('layouts.wrapper', [
    'pageTitle' => 'Overrides | Index'
])

@section('content')
    {!! Breadcrumbs::render('aux-feed.overrides') !!}
    <div class="flex justify-between items-center">
        <x-h1>Overrides</x-h1>
        <x-button color="green" size="sm" href="{{ route('aux.overrides.create') }}">
            <x-fas>plus</x-fas> Add new
        </x-button>
    </div>
    <div x-data="{exampleModal:false}">
        <x-p>An <strong>Override</strong> will exclude the user from normal eligibility processing. Overrides can be used to add new users, remove existing users, or extend a user's eligibility when a gap is anticipated. <x-a href="#" x-on:click.prevent="exampleModal=true">Example</x-a></x-p>
        <x-p>Overrides take effect immediately (the user will be added or removed right away). Override expiration is checked daily via a Cron Job. Once an override expires a user's status in Canvas will return to being managed by the User Feed / Canvas Auxiliary Feed process.</x-p>
        <x-modal trigger="exampleModal">
            <x-h4>Override Example</x-h4>
            <x-p>The User Feed determines that user j_smith is not eligible for Canvas under normal circumstances. An admin may create an Override if there is a legitimate need for j_smith to have Canvas access while the underlying eligibility concern is addressed (usually with HR).</x-p>
        </x-modal>
    </div>
    <x-hr />
    @if ($overrides->isNotEmpty())
        <x-table>
            <x-slot name="th">
                <x-th>ID</x-th>
                <x-th>Canvas ID</x-th>
                <x-th>Type</x-th>
                <x-th>Status</x-th>
                <x-th>Created</x-th>
                <x-th>Expires</x-th>
                <x-th>Actions</x-th>
            </x-slot>
            <x-slot name="tbody">
                @foreach ($overrides as $override)
                    <x-tr>
                        <x-td>{{ $override->id }}</x-td>
                        <x-td>
                            @if (!is_null($override->canvas_user))
                                {{ $override->canvas_user->email }}
                            @else
                                <em>{{ $override->canvas_user_id }}</em> <span class="text-blue-500" title="This add is still pending"><i class="fa fa-question-circle"></i></span>
                            @endif
                        </x-td>
                        <x-td>{{ $override->type }}</x-td>
                        <x-td class="text-{{ $override->active_status_class }}">
                            <i class="{{ $override->active_status_icon }}"></i>
                            {{ $override->active_status }}
                        </x-td>
                        <x-td>{{ $override->created_at }}</x-td>
                        <x-td>{{ $override->expires_at }}</x-td>
                        <x-td class="flex items-center">
                            <x-button color="blue" size="sm" href="{{ route('aux.overrides.show', ['override' => $override]) }}">
                                <x-fas>list</x-fas> Details
                            </x-button>
                            @if ($override->active_status == 'active')
                                <x-button color="yellow" size="sm" href="{{ route('aux.overrides.change-time', ['override' => $override]) }}">
                                    <x-fas>edit</x-fas> Change Time
                                </x-button>
                                <x-form-post action="{{ route('aux.overrides.cancel', ['override' => $override]) }}" confirm="Are you sure? This cannot be undone.">
                                    <x-button color="red" size="sm">
                                        <x-fas>times</x-fas> Cancel
                                    </x-button>
                                </x-form-post>
                            @endif
                        </x-td>
                    </x-tr>
                @endforeach
            </x-slot>
        </x-table>
    @else
    	<x-p>No Overrides to show.</x-p>
    @endif
@endsection
