@extends('layouts.wrapper', [
    'pageTitle' => 'Override ' . $override->id . ': change time'
])

@section('head')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/zebra_datepicker@1.9.12/dist/css/bootstrap/zebra_datepicker.min.css">
@endsection

@section('content')
    <x-h1>Override {{ $override->id }}: change time</x-h1>
    <div class="w-full">
        <x-alert-warning>
            NOTICE: changing the date/time on an Override will reset its Expiration Notification status.
        </x-alert-warning>
    </div>
    <x-form-patch action="{{ route('aux.overrides.change-time', ['override' => $override]) }}">
        <div class="flex">
            <div class="w-full">
                <x-card title="New Date / Time">
                    <x-input-text
                        name="expires_at"
                        label="Expires At"
                        required
                        help="Date / Time for expiration of this override"
                        value="{{ old('expires_at', $override->expires_at->format('m/d/Y H:i')) }}"
                    />
                </x-card>
            </div>
        </div>
        <div class="flex mt-3">
            <x-button color="green" size="sm">
                <x-fas>check</x-fas> Submit
            </x-button>
        </div>
    </x-form-patch>
@endsection
