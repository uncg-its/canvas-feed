@extends('layouts.wrapper', [
    'pageTitle' => 'Overrides | ' . $override->id
])

@section('content')
	{!! Breadcrumbs::render('aux-feed.overrides.show', $override) !!}
	<div class="flex items-center justify-between">
        <x-h1>Override {{ $override->id }}</x-h1>
        <x-button color="yellow" size="sm" href="{{ route('aux.overrides.index') }}">
            <x-fas>arrow-left</x-fas> Back to List
        </x-button>
	</div>
	<div class="flex gap-3 mt-3">
		<div class="w-1/2">
            <x-card title="Basic Information">
                <x-p>
                    <strong>Canvas User</strong>:
                    @if (!is_null($override->canvas_user))
                        {{ $override->canvas_user->email }}
                    @else
                        <em>{{ $override->canvas_user_id }}</em> <span class="text-blue-500" data-toggle="tooltip" data-placement="top" title="This add is still pending"><i class="fa fa-question-circle"></i></span>
                    @endif
                </x-p>
                <x-p><strong>Type</strong>: {{ $override->type }}</x-p>
                <x-p><strong>Submitted by</strong>: {{ $override->submitter->email }}</x-p>
                <x-p><strong>Contact email</strong>: {{ $override->contact_email }}</x-p>
                <x-p><strong>Reason</strong>: {{ $override->reason }}</x-p>
            </x-card>
		</div>
		<div class="w-1/2">
            <x-card title="Timing Information">
                <x-p>
                    <strong>Status</strong>: <span class="text-{{ $override->active_status_class }}"><i class="{{ $override->active_status_icon }}"></i> {{ $override->active_status }}</span>
                </x-p>
                <x-p><strong>Created at</strong>: {{ $override->created_at }}</x-p>

                <x-p><strong>Expires at</strong>: {{ $override->expires_at }}<span class="{{ ($override->active_status == 'expired' || $override->active_status == 'expired (unresolved)') ? 'text-yellow-500' : '' }}"> ({{ $override->expires_at_relative }})</span></x-p>
                @if ($override->active_status == 'canceled')
                    <x-p><strong>Canceled at</strong>: {{ $override->canceled_at }}</x-p>
                    <x-p><strong>Canceled by</strong>: {{ $override->canceled_by->email }}</x-p>
                @endif
                @if (!is_null($override->resolved_at))
                    <x-p><strong>Resolved at</strong>: {{ $override->resolved_at }}</x-p>
                @endif
            </x-card>
		</div>
	</div>
	<div class="flex gap-3 mt-3">
		<div class="w-1/2">
            <x-card title="Action Information">
                <x-p><strong>Type</strong>: {{ $override->action->type }}</x-p>
                <x-p><strong>Status</strong>:
                    <span class="text-{{ $override->action->status_class }}">
                        <i class="{{ $override->action->status_icon }}"></i>
                        {{ $override->action->status }}
                    </span>
                </x-p>
                @if (!is_null($override->action->completed_at))
                    <x-p><strong>Completed at</strong>: {{ $override->action->completed_at }}</x-p>
                @endif
                <x-button color="blue" size="sm" href="{{ route('aux.calls.index', ['action' => $override->action]) }}">
                    <x-fas>code</x-fas> API Calls
                </x-button>
            </x-card>
		</div>
		<div class="w-1/2">
            <x-card title="Sent notifications">
                @if ($override->notifications->isNotEmpty())
                    <x-ul>
                        @foreach ($override->notifications->sortByDesc('created_at') as $notification)
                            <li>{{ $notification->created_at }} - {{ $notification->interval_days }} day {!! $notification->manual ? '<em>(sent manually)</em>' : '' !!}</li>
                        @endforeach
                    </x-ul>
                @else
                    <x-p>No notifications sent</x-p>
                @endif
            </x-card>
		</div>
	</div>
	<div class="flex gap-3 mt-3">
		<div class="w-1/2">
            <x-card title="Actions">
                <div class="flex">
                    @if ($override->active_status == 'active')
                        <x-button color="yellow" size="sm" href="{{ route('aux.overrides.change-time', ['override' => $override]) }}">
                            <x-fas>edit</x-fas> Change Time
                        </x-button>
                        <x-form-post
                            action="{{ route('aux.overrides.notify', ['override' => $override]) }}"
                            confirm="This action will manually send an email notification to the Contact Email address listed for this override, regardless of the normal notification settings. Are you sure you want to proceed?"
                        >
                            <x-button color="blue" size="sm">
                                <x-fas>envelope</x-fas> Send Notification
                            </x-button>
                        </x-form-post>
                        <x-form-post
                            action="{{ route('aux.overrides.cancel', ['override' => $override]) }}"
                            confirm="Are you sure you want to cancel this override? This cannot be undone."
                        >
                            <x-button color="red" size="sm">
                                <x-fas>times</x-fas> Cancel
                            </x-button>
                        </x-form-post>
                    @endif
                </div>
            </x-card>
		</div>
	</div>
@endsection
