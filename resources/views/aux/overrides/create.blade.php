@extends('layouts.wrapper', [
    'pageTitle' => 'Overrides | Create'
])

@section('content')
    {!! Breadcrumbs::render('aux-feed.overrides.create') !!}
    <x-h1>Create New Override</x-h1>
    <x-form-post action="{{ route('aux.overrides.create') }}">
        @include('aux.overrides.form')
    </x-form-post>
@endsection
