<div class="flex gap-3">
	<div class="w-1/3">
        <x-card title="User / Contact Information">
            <x-input-text
                name="uncgUsername"
                label="UNCG Username"
                required
                help="UNCG username for the user receiving the override"
                value="{{ old('uncgUsername', '') }}"
            />
            <x-input-text
                name="contact_email"
                label="Contact Email"
                type="email"
                help="Additional responsible party for managing the override (submitter will always be notified)."
                value="{{ old('contact_email') }}"
            />
        </x-card>

	</div>
	<div class="w-2/3">
        <x-card title="Override Information">
            <x-input-select
                name="type"
                label="Type"
                required
                :options="['add' => 'Add / Activate', 'delete' => 'Remove / Deactivate']"
                value="{{ old('type', 'add') }}"
            />
            <x-input-textarea
                name="reason"
                label="Reason"
                required
                help="Explanation for this override"
                placeholder="{{ old('reason') }}"
            />
            <x-input-text
                name="expires_at"
                label="Expiration Date"
                required
                help="Date and time for expiration of this override. <strong>Must be after {{ $earliestExpiration->format('m/d/Y H:i') }}</strong>"
                value="{{ old('expires_at') }}"
            />
        </x-card>
	</div>
</div>
<div class="flex">
    <x-button color="green" size="sm">
        <x-fas>check</x-fas> Submit
    </x-button>
</div>
