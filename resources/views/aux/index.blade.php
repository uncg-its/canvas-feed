@extends('layouts.wrapper', [
    'pageTitle' => 'Canvas Auxiliary Feed | Index'
])

@section('content')
	{!! Breadcrumbs::render('aux-feed') !!}
    @include('partials.held-aux-data')
    <x-h1>Dashboard</x-h1>
    <div class="flex gap-4 mt-3">
        <div class="w-1/3">
            @if (!is_null($apiHealth))
                <x-card title="Canvas API Health">
                    <div class="text-center">
                        <x-p class="mb-2"><i class="fa fa-{{ $apiHealth['icon'] }}"></i> {{ $apiHealth['health'] }}</x-p>
                        <small>{{ $apiHealth['message'] }}</small>
                        <x-slot name="footer">
                            <small><em>Last check: {{ optional($lastApiCheck)->created_at ?? 'never' }}</em></small>
                        </x-slot>
                    </div>
                </x-card>
            @else
                <x-card title="Canvas API Health">
                    <div class="text-center">API Health has never been checked.</div>
                </x-card>
            @endif

        </div>
        <div class="w-1/3">
            <x-card title="Last Feed Run">
                <div class="text-center">
                    @if (!is_null($lastRun))
                        <x-p class="mb-2">{{ $lastRun->created_at }}</x-p>
                        <x-p class="mb-2 text-{{ $lastRun->status_class }}">
                            <i class="fa fa-{{ $lastRun->status_icon }}"></i>
                            {{ $lastRun->status }}
                        </x-p>
                        <slot name="footer">
                            <small>
                                Adds: {{ $lastRun->numberOfActionsOfType('add') }} /
                                Updates: {{ $lastRun->numberOfActionsOfType('update') }} /
                                Deletes: {{ $lastRun->numberOfActionsOfType('delete') }}
                            </small>
                        </slot>
                    @else
                        Feed has never run.
                    @endif
                </div>
            </x-card>
        </div>
        <div class="w-1/3">
            <x-card title="Active Overrides">
                <div class="text-center text-xl">{{ $activeOverrides }}</div>
            </x-card>
        </div>
    </div>
    <x-hr />
    <div class="flex">
        @include('components.panel-nav', [
            'url' => route('aux.runs.index'),
            'fa' => 'fas fa-play-circle',
            'title' => 'Runs',
            'helpText' => 'View details on all runs of the Canvas Auxiliary Feed'
        ])

        @include('components.panel-nav', [
            'url' => route('aux.users.index'),
            'fa' => 'fas fa-users',
            'title' => 'Canvas Users',
            'helpText' => 'List / Search users managed by the Canvas Auxiliary Feed, and browse their feed history'
        ])

        @include('components.panel-nav', [
            'url' => route('aux.overrides.index'),
            'fa' => 'fas fa-user-lock',
            'title' => 'User Overrides',
            'helpText' => 'Manually add or remove users from Canvas, or manage existing Overrides'
        ])

        @include('components.panel-nav', [
            'url' => route('aux.invalid-changes.index'),
            'fa' => 'fas fa-user-alt-slash',
            'title' => 'Invalid Changes',
            'helpText' => 'Manage invalid User Data Changes and restart User Feed'
        ])

        @include('components.panel-nav', [
            'url' => route('aux.user-imports.index'),
            'fa' => 'fas fa-file-import',
            'title' => 'User Imports',
            'helpText' => 'Import users into database manually'
        ])

        @include('components.panel-nav', [
            'url' => route('config'),
            'fa' => 'fas fa-cog',
            'title' => 'Feed Config',
            'helpText' => 'View and change configuration items for the Canvas Auxiliary feed, notifications, thresholds, etc.'
        ])

        @include('components.panel-nav', [
            'url' => route('cronjobs'),
            'fa' => 'fas fa-hourglass-half',
            'title' => 'Cron Jobs',
            'helpText' => 'View, change, and run application Cron Jobs.'
        ])

        {{-- @foreach($modules as $key => $module)
            @if($module['parent'] == 'Canvas-feed')
                @permission($module['required_permissions'])
                @include('components.panel-nav', [
                    'url' => route($module['index']),
                    'fa' => $module['icon'],
                    'title' => $module['title']
                ])
                @endpermission
            @endif
        @endforeach --}}
    </div>
@endsection
