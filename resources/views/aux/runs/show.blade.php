@extends('layouts.wrapper', [
    'pageTitle' => 'Run | ' . $run->id
])

@section('content')
	{!! Breadcrumbs::render('aux-feed.runs.show', $run) !!}
    <x-h1>Run {{ $run->id }}</x-h1>
    <x-card title="Information">
        <x-p><strong>Created at</strong>: {{ $run->created_at }}</x-p>
        @if ($run->completed_at)
            <x-p><strong>Completed at</strong>: {{ $run->completed_at }}</x-p>
        @endif
        <x-p><strong>Status</strong>:
            <span class="text-{{ $run->status_class }}">
                <i class="fa fa-{{ $run->status_icon }}"></i>
                {{ $run->status }}
            </span>
        </x-p>
    </x-card>
    <x-hr />
    <x-h2>Actions</x-h2>
	<div class="flex" x-data="{exampleModal: false}">
        <x-p>An <strong>Action</strong> corresponds to an add/update/delete action in Canvas. An Action always belongs to a Run (as a Feed Change) or an Override. <x-a href="#" x-on:click.prevent="exampleModal=true">Example</x-a></x-p>
            <x-modal trigger="exampleModal">
                The latest user data diff indicates that 2 new users have appeared in the source. Each of these new users would have an Add Action generated so that they can be added to Canvas.
            </x-modal>
    </div>
	<div class="flex w-full">
        <x-card title="Search / Filter" class="w-full">
            <x-form-get action="{{ route('aux.runs.show', ['run' => $run]) }}">
                <div class="flex w-full items-end gap-2">
                    <div class="w-1/4">
                        <x-input-text
                            name="username"
                            label="Username"
                            value="{{ request()->get('username') }}"
                        />
                    </div>
                    <div class="w-1/4">
                        <x-input-select
                            name="type"
                            label="Type"
                            value="{{ request()->get('type') }}"
                            :options="['' => '--- Select ---', 'add' => 'add', 'update' => 'update', 'delete' => 'delete']"
                        />
                    </div>
                    <div class="w-1/4">
                        <x-input-select
                            name="status"
                            label="Status"
                            value="{{ request()->get('status') }}"
                            :options="$actionStatuses"
                        />
                    </div>

                    <div class="flex items-center mb-4">
                        <x-button color="green" size="sm">
                            <x-fas>check</x-fas> Submit
                        </x-button>
                        <x-button color="red" size="sm" href="{{ route('aux.runs.show', ['run' => $run]) }}">
                            <x-fas>undo</x-fas> Reset
                        </x-button>
                    </div>
                </div>
            </x-form-get>
        </x-card>
	</div>
	<x-hr />
	@if ($actions->isNotEmpty())
		@if ($actions->whereIn('status_id', [\SuccessStatus::FAILURE, \SuccessStatus::ERROR])->isNotEmpty())
            <div class="flex justify-end mb-3">
                <x-form-post action="{{ route('aux.actions.retry-all', ['run' => $run]) }}">
                    <x-button color="red">
                        <x-fas>redo</x-fas> Retry all
                    </x-button>
                </x-form-post>
            </div>
		@endif
        <x-table>
            <x-slot name="th">
                <x-th>ID</x-th>
                <x-th>User Data Change</x-th>
                <x-th>Username</x-th>
                <x-th>Status</x-th>
                <x-th>Type</x-th>
                <x-th>Created At</x-th>
                <x-th>Completed At</x-th>
                <x-th>Actions</x-th>
            </x-slot>
            <x-slot name="tbody">
                @foreach ($actions as $action)
                    <x-tr>
                        <x-td>{{ $action->id }}</x-td>
                        <x-td>
                            <div x-data="{showChange: false}">
                                <x-a href="#" x-on:click.prevent="showChange=true">{{ $action->actionable->user_data_change_id }}</x-a>
                                <x-modal trigger="showChange">
                                    <x-h2>User Data Change</x-h2>
                                    <x-h4>Data</x-h4>
                                    <pre>{{ json_encode($action->userTemplate->getSummary()['data'], JSON_PRETTY_PRINT) }}</pre>
                                    <x-h4>Changed Data</x-h4>
                                    <pre>{{ json_encode($action->userTemplate->getSummary()['changed_data'], JSON_PRETTY_PRINT) }}</pre>
                                </x-modal>
                            </div>
                        </x-td>
                        <x-td>
                            @if (!is_null($action->canvasUser))
                                <x-a href="{{ route('aux.users.show', ['user' => $action->canvasUser]) }}">
                                    {{ $action->canvasUser->email }}
                                </x-a>
                            @else
                                {{ $action->userTemplate->getUsername() }}
                            @endif

                        </x-td>
                        <x-td class="text-{{ $action->status_class }}">
                            <i class="{{ $action->status_icon }}"></i>
                            {{ $action->status }}
                        </x-td>
                        <x-td>{{ $action->type }}</x-td>
                        <x-td>{{ $action->created_at }}</x-td>
                        <x-td>{{ $action->completed_at }}</x-td>
                        <x-td class="flex items-center">
                            <x-button color="blue" size="sm" href="{{ route('aux.calls.index', ['action' => $action->id]) }}">
                                <x-fas>code</x-fas> <nobr>API calls</nobr>
                            </x-button>
                            @if ($action->status == 'failure' || $action->status == 'error')
                            <x-form-post action="{{ route('aux.actions.retry', ['action' => $action]) }}">
                                <x-button color="red">
                                    <x-fas>redo</x-fas> Retry
                                </x-button>
                            </x-form-post>

                            <x-form-post action="{{ route('aux.actions.ignore', ['action' => $action]) }}">
                                <x-button color="yellow">
                                    <x-fas>ban</x-fas> Ignore
                                </x-button>
                            </x-form-post>
                            @endif
                        </x-td>
                    </x-tr>
                @endforeach
            </x-slot>
        </x-table>
        {{ $actions->links() }}
    @else
    	<x-p>No actions found.</x-p>
    @endif
@endsection
