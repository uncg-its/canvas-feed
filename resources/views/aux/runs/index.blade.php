@extends('layouts.wrapper', [
    'pageTitle' => 'Runs | Index'
])

@section('content')
    {!! Breadcrumbs::render('aux-feed.runs') !!}
    <x-h1>Runs</x-h1>
    <div class="flex" x-data="{exampleModal:false}">
        <x-p>A <strong>Run</strong> of the Canvas Auxiliary Feed is when a user data diff is examined from the User Feed to determine what actions need to take place in Canvas. <x-a href="#" x-on:click.prevent="exampleModal=true">Example</x-a></x-p>
        <x-modal trigger="exampleModal">
            <x-h4>Run Example</x-h4>
            <x-p>If the diff indicates, for example, that 2 users have appeared in the source, 1 user has changed, and 3 have disappeared from the source, the next Run would spawn the appropriate Actions (2 adds / 1 update / 3 deletes) and coordinate / record their execution.</x-p>
        </x-modal>
    </div>
    <x-hr />
    @if($runs->isNotEmpty())
        <x-table>
            <x-slot name="th">
                <x-th>ID</x-th>
                <x-th>Created At</x-th>
                <x-th>Status</x-th>
                <x-th>User Data Diff</x-th>
                <x-th>Changes</x-th>
                <x-th>Actions</x-th>
            </x-slot>
            <x-slot name="tbody">
                @foreach ($runs as $run)
                    <x-tr>
                        <x-td>{{ $run->id }}</x-td>
                        <x-td>{{ $run->created_at }}</x-td>
                        <x-td class="text-{{ $run->status_class }}">
                            <i class="{{ $run->status_icon }}"></i>
                            {{ $run->status }}
                        </x-td>
                        <x-td>
                            <div x-data="{showDiff:false}">
                                <x-a href="#" x-on:click.prevent="showDiff=true">{{ $run->user_data_diff_id }}</x-a>
                                <x-modal trigger="showDiff">
                                    <x-h2>Diff Information</x-h2>
                                    <x-h4>Metrics</x-h4>
                                    <pre>{{ json_encode($run->diff_info->metrics, JSON_PRETTY_PRINT) }}</pre>
                                    <x-h4>Data Summary</x-h4>
                                    <pre>{{ json_encode($run->diff_info->summary, JSON_PRETTY_PRINT) }}</pre>
                                </x-modal>
                            </div>
                        </x-td>
                        <x-td>{{ $run->diff_info->change_count }}</x-td>
                        <x-td>
                            <x-button color="blue" size="sm" href="{{ route('aux.runs.show', ['run' => $run]) }}">
                                <x-fas>list</x-fas> Details
                            </x-button>
                        </x-td>
                    </x-tr>
                @endforeach
            </x-slot>
        </x-table>
    @else
        <x-p>No runs to show.</x-p>
    @endif
@endsection
