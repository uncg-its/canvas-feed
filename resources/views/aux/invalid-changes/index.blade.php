@extends('layouts.wrapper', [
    'pageTitle' => 'Invalid Changes | Index'
])

@section('content')
    <div class="flex justify-between items-center">
        <x-h1>Invalid User Data Changes</x-h1>
        <x-button color="green" size="sm" href="{{ route('aux.restart-crons') }}">
            <x-fas>power-off</x-fas> Restart Crons
        </x-button>
    </div>
    <x-p>Lists all User Data Changes that are invalid, causing stoppage of the Canvas Auxiliary Feed</x-p>
    <x-hr />
    @if ($invalidChanges->isNotEmpty())
        <x-table>
            <x-slot name="th">
                <x-th>ID</x-th>
                <x-th>Diff ID</x-th>
                <x-th>Data</x-th>
                <x-th>Changed Data</x-th>
                <x-th>Created at</x-th>
                <x-th>Status</x-th>
                <x-th>Actions</x-th>
            </x-slot>
            <x-slot name="tbody">
                @foreach ($invalidChanges as $change)
                    <x-tr>
                        <x-td>{{ $change->id }}</x-td>
                        <x-td>{{ $change->user_data_diff_id }}</x-td>
                        <x-td>
                            <div x-data="{showChange: false}">
                                <x-button color="blue" size="sm" x-on:click="showChange=true">
                                    <x-fas>eye</x-fas> Show
                                </x-button>
                                <x-modal trigger="showChange">
                                    <x-h4>Data</x-h4>
                                    <pre>{{ json_encode($change->data, JSON_PRETTY_PRINT) }}</pre>
                                </x-modal>
                            </div>
                        </x-td>
                        <x-td>
                            <div x-data="{showChange: false}">
                                <x-button color="blue" size="sm" x-on:click="showChange=true">
                                    <x-fas>eye</x-fas> Show
                                </x-button>
                                <x-modal trigger="showChange">
                                    <x-h4>Data</x-h4>
                                    <pre>{{ json_encode($change->data, JSON_PRETTY_PRINT) }}</pre>
                                </x-modal>
                            </div>
                        </x-td>
                        <x-td>{{ $change->created_at }}</x-td>
                        <x-td>{{ $change->status }}</x-td>
                        <x-td>
                            @if($change->status == 'invalid')
                                <x-form-post action="{{ route('aux.invalid-changes.ignore', ['change' => $change]) }}">
                                    <x-button color="yellow" size="sm">
                                        <x-fas>trash</x-fas> Ignore
                                    </x-button>
                                </x-form-post>
                            @endif
                        </x-td>
                    </x-tr>
                @endforeach
            </x-slot>
        </x-table>
    @else
        <x-p>No invalid changes to show.</x-p>
    @endif
@endsection
