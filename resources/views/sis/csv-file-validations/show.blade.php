@extends('layouts.wrapper', [
    'pageTitle' => 'SIS CSV File Validation | Validation #' . $validation->id
])

@section('content')
    <x-h1>SIS CSV File Validation #{{ $validation->id }}</x-h1>
    <div class="flex gap-4">
        <div class="w-1/2">
            <x-card title="Validation Details">
                <x-p><strong>Internal ID</strong>: {{ $validation->id }}</x-p>
                <x-p><strong>Created at</strong>: {{ $validation->created_at }}</x-p>
                @include('partials.statuses.p.' . ($validation->success ? 'success' : 'failure'))
            </x-card>
        </div>

        <div class="w-1/2">
            <x-card title="CSV File info">
                <x-p><strong>Format</strong>: {{ $validation->sis_csv_file->format }}</x-p>
                <x-p><strong># of records</strong>: {{ $validation->sis_csv_file->row_count }}</x-p>
                <x-p><strong>Parent SIS ZIP</strong>: <x-a href="{{ route('sis.zips.show',$validation->sis_csv_file->sis_zip_id) }}">{{ $validation->sis_csv_file->sis_zip_id }}</x-a></x-p>
                <x-p>
                    <x-button color="blue" size="sm" href="{{ route('sis.csv-files.show', $validation->sis_csv_file) }}">
                        <x-fas>list</x-fas> View CSV file details
                    </x-button>
                </x-p>
            </x-card>
        </div>

    </div>
    <div class="flex mt-4">
        <div class="w-full">
            <x-card title="Messages">
                @empty($validation->messages)
                    <x-p>No messages to show.</x-p>
                @else
                    <pre>{{ json_encode(json_decode($validation->messages), JSON_PRETTY_PRINT) }}</pre>
                @endempty
            </x-card>
        </div>
    </div>
@endsection
