@extends('layouts.wrapper', [
    'pageTitle' => 'SIS CSV File Validations | Index'
])

@section('content')
    <x-h1>SIS CSV File Validations</x-h1>
    @empty($zip)
    @else
        <x-alert-info :dismissable="false">
            <em>Showing validations for <x-a href="{{ route('sis.zips.show', $zip) }}">SIS Zip #{{ $zip->id }}</x-a></em>
        </x-alert-info>
    @endempty

    @empty($validations)
        <x-p>No validations to show.</x-p>
    @else
        <x-table>
            <x-slot name="th">
                <x-th>ID</x-th>
                <x-th>CSV File</x-th>
                <x-th>ZIP File ID</x-th>
                <x-th>Status</x-th>
                <x-th>Created At</x-th>
                <x-th>Actions</x-th>
            </x-slot>
            <x-slot name="tbody">
                @foreach ($validations as $validation)
                    <x-tr>
                        <x-td>{{ $validation->id }}</x-td>
                        <x-td>
                            <x-a href="{{ route('sis.csv-files.show', $validation->sis_csv_file) }}">{{ $validation->sis_csv_file->id }} - {{ $validation->sis_csv_file->filename }}</x-a>
                        </x-td>
                        <x-td>
                            <x-a href="{{ route('sis.zips.show', $validation->sis_csv_file->sis_zip->id) }}">{{ $validation->sis_csv_file->sis_zip->id }}</x-a>
                        </x-td>
                        @include('partials.statuses.td.' . ($validation->success ? 'success' : 'failure'))
                        <x-td>
                            {{ $validation->created_at }}
                        </x-td>
                        <x-td>
                            <x-button color="blue" size="sm" href="{{ route('sis.csv-file-validations.show', $validation) }}">
                                <x-fas>list</x-fas> Details
                            </x-button>
                        </x-td>
                    </x-tr>
                @endforeach
            </x-slot>
        </x-table>
        {{ $validations->links() }}
    @endempty
@endsection
