@extends('layouts.wrapper', [
    'pageTitle' => 'SIS Diffs | Index'
])

@section('content')
    <x-h1>SIS Diffs</x-h1>
    <livewire:sis-diffs-table />
@endsection
