@extends('layouts.wrapper', [
    'pageTitle' => 'SIS Diff | Diff #' . $diff->id
])

@section('content')
    <x-h1>SIS Diff #{{ $diff->id }}</x-h1>
    <div class="flex gap-4">
        <div class="w-1/2">
            <x-card title="Diff Details">
                <x-p><strong>Internal ID</strong>: {{ $diff->id }}</x-p>
                <x-p><strong>Created at</strong>: {{ $diff->created_at }}</x-p>
                <x-p><strong>Filename</strong>: {{ $diff->filename }}</x-p>
                <x-p><strong># of CSV Files</strong>: {{ $diff->zip_csv_count }}</x-p>
                @include('partials.statuses.p.' . $diff->status)
            </x-card>
        </div>

        <div class="w-1/2">
            <x-card title="Zip Inclusion">
                <x-p><strong>Old Zip</strong>: <x-a href="{{ route('sis.zips.show', $diff->old_zip) }}">{{ $diff->old_zip->id }} - {{ $diff->old_zip->filename }}</x-a></x-p>
                <x-p><strong>New Zip</strong>: <x-a href="{{ route('sis.zips.show', $diff->new_zip) }}">{{ $diff->new_zip->id }} - {{ $diff->new_zip->filename }}</x-a></x-p>
            </x-card>
            <x-card title="Canvas Import Status">
                <x-p><strong>Import Result</strong>: {{ $diff->result ?? 'n/a' }}</x-p>
                @if (! is_null($diff->uploaded_at))
                    <x-p>
                        <strong>Uploaded at</strong>: {{ $diff->uploaded_at }}
                    </x-p>
                @endif
                @if (! is_null($diff->canvas_id))
                    <livewire:diff-upload-details :diff="$diff" />
                @endif
            </x-card>
        </div>

    </div>
    <div class="flex mt-4 gap-4">
        <div class="w-1/2">
            <x-card title="Diff Contents">
                <table class="table-auto w-full">
                    <tbody>
                        @if ($diff->is_purged)
                            <div class="alert alert-warning">
                                <i class="fa fa-exclamation-triangle"></i> Note: this Diff's information has been purged from the local database
                            </div>
                        @endif
                        @foreach (config('canvas-feed.sis.formats_to_diff') as $format)
                            <x-tr>
                                <x-th scope="row">{{ ucfirst($format) }}</x-th>
                                <x-td>
                                    {{ $diff->$format }}
                                </x-td>
                                <x-td>
                                    @if($diff->$format > 0 && !$diff->is_purged)
                                        <x-button color="blue" size="sm" href="{{ route('sis.diff-entities', ['diff' => $diff, 'type' => $format]) }}">
                                            <x-fas>info-circle</x-fas> Details
                                        </x-button>
                                    @endif
                                </x-td>
                            </x-tr>
                        @endforeach
                        <x-tr>
                            <x-th scope="row">TOTAL CHANGES</x-th>
                            <x-td>{{ $diff->total }}</x-td>
                            <x-td></x-td>
                        </x-tr>
                    </tbody>
                </table>
            </x-card>
        </div>
        <div class="w-1/2">
            <x-card title="Actions / Controls">
                <div class="flex">
                    @include('partials.buttons.download-file', [
                        'disk' => 'sis-diffs',
                        'filename' => $diff->filename,
                        'buttonText' => 'Download raw diff'
                    ])

                    @if ($diff->holdable)
                        <form action="{{ route('sis.diffs.update', ['diff' => $diff->id, 'operation' => 'hold']) }}" method="post" onsubmit="return confirm('Holding this Diff will prevent it from being imported and will automatically shut down the Import cron jobs. Are you sure you want to continue?')">
                            {{ csrf_field() }}
                            {{ method_field('patch') }}
                            <x-button color="red" size="sm">
                                <x-fas>stop-circle</x-fas> Hold Diff
                            </x-button>
                        </form>
                    @endif
                    @if ($diff->approvable)
                        <form action="{{ route('sis.diffs.update', ['diff' => $diff->id, 'operation' => 'approve', 'restartCrons' => true]) }}" onsubmit="return confirm('Are you sure you want to approve this Diff?')" method="post">
                            {{ csrf_field() }}
                            {{ method_field('patch') }}
                            <x-button color="green" size="sm">
                                <x-fas>thumbs-up</x-fas> Approve Diff &amp; restart SIS Crons
                            </x-button>
                        </form>
                        <form action="{{ route('sis.diffs.update', ['diff' => $diff->id, 'operation' => 'approve', 'restartCrons' => false]) }}" onsubmit="return confirm('Are you sure you want to approve this Diff?')" method="post">
                            {{ csrf_field() }}
                            {{ method_field('patch') }}
                            <x-button color="orange" size="sm">
                                <x-fas>thumbs-up</x-fas> Approve Diff only
                            </x-button>
                        </form>
                    @endif
                    @if ($diff->ignorable)
                        <form action="{{ route('sis.diffs.update', ['diff' => $diff->id, 'operation' => 'ignore']) }}" onsubmit="return confirm('Ignoring this Diff will prevent it from being imported and also mark its New ZIP as ignored. Are you sure you want to continue?')" method="post">
                            {{ csrf_field() }}
                            {{ method_field('patch') }}
                            <x-button color="yellow" size="sm">
                                <x-fas>minus-circle</x-fas> Ignore Diff
                            </x-button>
                        </form>
                    @endif
                    @if($diff->resettable)
                        <form action="{{ route('sis.diffs.update', ['diff' => $diff->id, 'operation' => 'reset']) }}" onsubmit="return confirm('Resetting this Diff will change its status back to \'validated\' and thus will reattempt an import. You should use this only when you are sure there was a problem with an import on the Canvas side.')" method="post">
                            {{ csrf_field() }}
                            {{ method_field('patch') }}
                            <x-button color="yellow" size="sm">
                                <x-fas>undo</x-fas> Reset Diff
                            </x-button>
                        </form>

                    @endif
                </div>
            </x-card>
        </div>
    </div>
@endsection
