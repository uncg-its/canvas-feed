@extends('layouts.wrapper', [
    'pageTitle' => 'SIS Diff ' . $type . ' | Diff #' . $diff->id
])

@section('content')
    <x-h1>SIS Diff {{ $type }} - Diff #{{ $diff->id }}</x-h1>
    @if($entities->isEmpty())
        <x-p>No {{ $type }} to show</x-p>
    @else
        <x-table>
            <x-slot name="th">
                @foreach ($entities->first()->toArray() as $key => $value)
                    <x-th>{{ $key }}</x-th>
                @endforeach
            </x-slot>
            <x-slot name="tbody">
                @foreach ($entities as $entity)
                    <x-tr>
                        @foreach ($entity->toArray() as $value)
                            <x-td>{{ $value }}</x-td>
                        @endforeach
                    </x-tr>
                @endforeach
            </x-slot>
        </x-table>
        {{ $entities->links() }}
    @endempty
@endsection
