@extends('layouts.wrapper', [
    'pageTitle' => 'Canvas SIS Feed | Index'
])

@section('content')
@include('partials.held-sis-data')
<x-h1>SIS Feed</x-h1>
    <div class="flex">
        @include('components.panel-nav', [
            'url' => route('sis.zips.index'),
            'fa' => 'fas fa-file-archive',
            'title' => 'SIS Zips',
            'helpText' => 'Information on the SIS Zips that are imported to the database and used in Diffs'
        ])
        @include('components.panel-nav', [
            'url' => route('sis.diffs.index'),
            'fa' => 'fas fa-balance-scale-right',
            'title' => 'SIS Diffs',
            'helpText' => 'The Diffs that are created between SIS Zips and uploaded to Canvas'
        ])
        @include('components.panel-nav', [
            'url' => route('sis.csv-file-validations.index'),
            'fa' => 'fas fa-search',
            'title' => 'Validation Results',
            'helpText' => 'Get information on validations of SIS CSV files contained in the imported SIS Zips'
        ])
        @include('components.panel-nav', [
            'url' => route('sis.uploads.index'),
            'fa' => 'fas fa-cloud-upload-alt',
            'title' => 'Live SIS Upload Progress',
            'helpText' => 'Get information on progress of live SIS Uploads'
        ])
        @include('components.panel-nav', [
            'url' => route('sis.role-audit.index'),
            'fa' => 'fas fa-user-tie',
            'title' => 'User Role Audit',
            'helpText' => 'Browse latest ZIP for all enrollments of a certain type'
        ])
        @include('components.panel-nav', [
            'url' => route('config'),
            'fa' => 'fas fa-cog',
            'title' => 'Feed Config',
            'helpText' => 'View and change configuration items for the Canvas SIS feed, notifications, thresholds, etc.'
        ])
        @include('components.panel-nav', [
            'url' => route('cronjobs'),
            'fa' => 'fas fa-hourglass-half',
            'title' => 'Cron Jobs',
            'helpText' => 'View, change, and run application Cron Jobs.'
        ])
    </div>
    <x-hr />
    <x-h2>Diff Logs</x-h2>
    <div class="flex">
        @include('components.panel-nav', [
            'url' => route('sis.diff-logs.users'),
            'fa' => 'fas fa-user',
            'title' => 'SIS Diff Users',
            'helpText' => 'Show users who are contained in Diff ZIPs'
        ])
        @include('components.panel-nav', [
            'url' => route('sis.diff-logs.enrollments'),
            'fa' => 'fas fa-school',
            'title' => 'SIS Diff Enrollments',
            'helpText' => 'Show enrollments contained in Diff ZIPs'
        ])
        @include('components.panel-nav', [
            'url' => route('sis.diff-logs.courses'),
            'fa' => 'fas fa-chalkboard-teacher',
            'title' => 'SIS Diff Courses',
            'helpText' => 'Show courses contained in Diff ZIPs'
        ])
        @include('components.panel-nav', [
            'url' => route('sis.diff-logs.sections'),
            'fa' => 'fas fa-puzzle-piece',
            'title' => 'SIS Diff Sections',
            'helpText' => 'Show sections contained in Diff ZIPs'
        ])
    </div>
    <x-h2>ZIP Logs</x-h2>
    <div class="flex">
        @include('components.panel-nav', [
            'url' => route('sis.zip-logs.users'),
            'fa' => 'fas fa-user',
            'title' => 'SIS ZIP Users',
            'helpText' => 'Show users who are contained in SIS ZIPs'
        ])
        @include('components.panel-nav', [
            'url' => route('sis.zip-logs.enrollments'),
            'fa' => 'fas fa-school',
            'title' => 'SIS ZIP Enrollments',
            'helpText' => 'Show enrollments contained in SIS ZIPs'
        ])
        @include('components.panel-nav', [
            'url' => route('sis.zip-logs.courses'),
            'fa' => 'fas fa-chalkboard-teacher',
            'title' => 'SIS ZIP Courses',
            'helpText' => 'Show courses contained in SIS ZIPs'
        ])
        @include('components.panel-nav', [
            'url' => route('sis.zip-logs.sections'),
            'fa' => 'fas fa-puzzle-piece',
            'title' => 'SIS ZIP Sections',
            'helpText' => 'Show sections contained in SIS ZIPs'
        ])
    </div>
    <x-hr />
    <x-h2>Legacy Diff Logs</x-h2>
    <x-p>Data is from old CST, and contains records dated 1/8/2020 15:10 and prior.</x-p>
    <div class="flex">
        @include('components.panel-nav', [
            'url' => route('sis.legacy-diff-logs.users'),
            'classes' => 'text-blue-500',
            'fa' => 'fas fa-user',
            'title' => 'SIS Diff Users (legacy)',
            'helpText' => 'Show users contained in legacy Diff ZIPs'
        ])
        @include('components.panel-nav', [
            'url' => route('sis.legacy-diff-logs.enrollments'),
            'classes' => 'text-blue-500',
            'fa' => 'fas fa-school',
            'title' => 'SIS Diff Enrollments (legacy)',
            'helpText' => 'Show enrollments contained in legacy Diff ZIPs'
        ])
        @include('components.panel-nav', [
            'url' => route('sis.legacy-diff-logs.courses'),
            'classes' => 'text-blue-500',
            'fa' => 'fas fa-chalkboard-teacher',
            'title' => 'SIS Diff Courses (legacy)',
            'helpText' => 'Show courses contained in legacy Diff ZIPs'
        ])
        @include('components.panel-nav', [
            'url' => route('sis.legacy-diff-logs.sections'),
            'classes' => 'text-blue-500',
            'fa' => 'fas fa-puzzle-piece',
            'title' => 'SIS Diff Sections (legacy)',
            'helpText' => 'Show sections contained in legacy Diff ZIPs'
        ])
    </div>
@endsection
