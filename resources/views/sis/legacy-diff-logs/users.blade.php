@extends('layouts.wrapper', [
    'pageTitle' => 'Diff Log: Users'
])

@section('content')
    <x-h1>Diff Log: Users</x-h1>
    @empty($users)
        No users to show in Diff Log.
    @else
        @include('sis.partials.logs.users', ['type' => 'diff'])
    @endempty
@endsection
