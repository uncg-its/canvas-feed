@extends('layouts.wrapper', [
    'pageTitle' => 'Diff Log: Sections'
])

@section('content')
    <x-h1>Diff Log: Sections</x-h1>
    @empty($sections)
        No sections to show in Diff Log.
    @else
        @include('sis.partials.logs.sections', ['type' => 'diff'])
    @endempty
@endsection
