@extends('layouts.wrapper', [
    'pageTitle' => 'Diff Log: Courses'
])

@section('content')
    <x-h1>Diff Log: Courses</x-h1>
    @empty($courses)
        No courses to show in Diff Log.
    @else
        @include('sis.partials.logs.courses', ['type' => 'diff'])
    @endempty
@endsection
