@extends('layouts.wrapper', [
    'pageTitle' => 'Diff Log: Enrollments'
])

@section('content')
    <x-h1>Diff Log: Enrollments</x-h1>
    @empty($enrollments)
        No enrollments to show in Diff Log.
    @else
        @include('sis.partials.logs.enrollments', ['type' => 'diff'])
    @endempty
@endsection
