@extends('layouts.wrapper', [
    'pageTitle' => 'SIS Feed | Zip #' . $zip->id
])

@section('content')
    <x-h1>SIS Zip #{{ $zip->id }}</x-h1>
    <div class="flex gap-4">
        <div class="w-1/2">
            <x-card title="ZIP Details">
                <x-p><strong>Internal ID</strong>: {{ $zip->id }}</x-p>
                <x-p><strong>Created at</strong>: {{ $zip->created_at }}</x-p>
                <x-p><strong>Filename</strong>: {{ $zip->filename }}</x-p>
                @include('partials.statuses.p.' . $zip->status)
            </x-card>
        </div>

        <div class="w-1/2">
            <x-card title="Diff Inclusion">
                @if (!is_null($zip->sis_diff_as_old))
                    <x-p><strong>As old ZIP in Diff</strong>: <x-a href="{{ route('sis.diffs.show', $zip->sis_diff_as_old) }}">{{ $zip->sis_diff_as_old->filename }}</x-a></x-p>
                @else
                    <x-p>Not used as <strong>old ZIP</strong> in any Diff</x-p>
                @endif
                @if (!is_null($zip->sis_diff_as_new))
                    <x-p><strong>As new ZIP in Diff</strong>: <x-a href="{{ route('sis.diffs.show', $zip->sis_diff_as_new) }}">{{ $zip->sis_diff_as_new->filename }}</x-a></x-p>
                @else
                    <x-p>Not used as <strong>new ZIP</strong> in any Diff</x-p>
                @endif
            </x-card>
        </div>

    </div>
    <div class="flex mt-4 gap-4">
        <div class="w-1/2">
            <x-card title="ZIP Contents">
                <x-ul>
                    @foreach ($zip->csvs as $csv)
                        <li>
                            <x-a href="{{ route('sis.csv-files.show', $csv) }}">{{ $csv->filename }} ({{ $csv->row_count }} records)</x-a>
                        </li>
                    @endforeach
                </x-ul>
                <x-button color="blue" size="sm" href="{{ route('sis.csv-file-validations.index', ['zipId' => $zip->id]) }}">
                    <x-fas>search</x-fas> Browse Validation Reports
                </x-button>
            </x-card>
        </div>
        <div class="w-1/2">
            <x-card class="pb-4" title="Actions / Controls">
               <div class="flex">
                    @include('partials.buttons.download-file', [
                        'disk' => 'sis-zips',
                        'filename' => $zip->filename,
                        'buttonText' => 'Download raw ZIP'
                    ])
                    @if ($zip->holdable)
                        <form action="{{ route('sis.zips.update', ['zip' => $zip->id, 'operation' => 'hold']) }}" onsubmit="return confirm('Holding this ZIP will prevent it from being diffed and will automatically shut down the Diffing cron jobs. Are you sure you want to continue?')" method="post">
                            {{ csrf_field() }}
                            {{ method_field('patch') }}
                            <x-button color="red" size="sm">
                                <x-fas>stop-circle</x-fas> Hold ZIP (prevent diffing)
                            </x-button>
                        </form>
                    @endif
                    @if ($zip->approvable)
                        <form action="{{ route('sis.zips.update', ['zip' => $zip->id, 'operation' => 'approve', 'restartCrons' => true]) }}" onsubmit="return confirm('Approving this ZIP will allow it to be diffed. Are you sure you want to continue?')" method="post">
                            {{ csrf_field() }}
                            {{ method_field('patch') }}
                            <x-button color="green" size="sm">
                                <x-fas>thumbs-up</x-fas> Approve ZIP &amp; restart SIS Crons
                            </x-button>
                        </form>
                        <form action="{{ route('sis.zips.update', ['zip' => $zip->id, 'operation' => 'approve', 'restartCrons' => false]) }}" onsubmit="return confirm('Approving this ZIP will allow it to be diffed. Are you sure you want to continue?')" method="post">
                            {{ csrf_field() }}
                            {{ method_field('patch') }}
                            <x-button color="orange" size="sm">
                                <x-fas>thumbs-up</x-fas> Approve ZIP
                            </x-button>
                        </form>
                    @endif
                    @if ($zip->ignorable)
                        <form action="{{ route('sis.zips.update', ['zip' => $zip->id, 'operation' => 'ignore']) }}" onsubmit="return confirm('Ignoring this ZIP will permanently remove it from consideration from diffs. Are you sure you want to continue?')" method="post">
                            {{ csrf_field() }}
                            {{ method_field('patch') }}
                            <x-button color="yellow" size="sm">
                                <x-fas>minus-circle</x-fas> Ignore ZIP
                            </x-button>
                        </form>
                    @endif
                    @if ($zip->isLatest() && $zip->status == 'ignored')
                        <form action="{{ route('sis.zips.update', ['zip' => $zip->id, 'operation' => 'reset']) }}" onsubmit="return confirm('Resetting this ZIP status will change it back to validated, which means it will be considered for the next diff. Are you sure you want to continue?')" method="post">
                            {{ csrf_field() }}
                            {{ method_field('patch') }}
                            <x-button color="yellow" size="sm">
                                <x-fas>undo</x-fas> Reset ZIP status
                            </x-button>
                        </form>
                    @endif
                </div>
            </x-card>
        </div>
    </div>
@endsection
