@extends('layouts.wrapper', [
    'pageTitle' => 'SIS Feed | Zips'
])

@section('content')
    <x-h1>SIS Zips</x-h1>
    <x-p>On this page you can view the current status of all SIS Zips that are known to the Canvas Feed application.</x-p>
    <livewire:sis-zips-table />
@endsection
