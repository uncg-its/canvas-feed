@extends('layouts.wrapper', [
    'pageTitle' => 'ZIP Log: Sections'
])

@section('content')
    <x-h1>ZIP Log: Sections</x-h1>
    <livewire:sis-zip-sections-table />
@endsection
