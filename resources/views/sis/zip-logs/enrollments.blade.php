@extends('layouts.wrapper', [
    'pageTitle' => 'ZIP Log: Enrollments'
])

@section('content')
    <x-h1>ZIP Log: Enrollments</x-h1>
    <livewire:sis-zip-enrollments-table />
@endsection
