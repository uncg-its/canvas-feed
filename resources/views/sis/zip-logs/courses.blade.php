@extends('layouts.wrapper', [
    'pageTitle' => 'ZIP Log: Courses'
])

@section('content')
    <x-h1>ZIP Log: Courses</x-h1>
    <livewire:sis-zip-courses-table />
@endsection
