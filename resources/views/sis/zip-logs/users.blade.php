@extends('layouts.wrapper', [
    'pageTitle' => 'ZIP Log: Users'
])

@section('content')
    <x-h1>ZIP Log: Users</x-h1>
    <livewire:sis-zip-users-table />
@endsection
