@extends('layouts.wrapper', [
    'pageTitle' => 'Role Audit'
])

@section('content')
    <x-h1>SIS Role Audit</x-h1>
    <x-p>This page provides a listing of users who were listed with the specified role in the latest diffed SIS ZIP data. Optionally, you can choose to use the latest raw ZIP instead (if one exists that has not been diffed yet).</x-p>
    <div class="card mb-4">
        <div class="card-header">Search</div>
        <div class="card-body">
            <x-form-get action="{{ route('sis.role-audit.index') }}">
                <div class="flex gap-4">
                    <div class="w-1/4">
                        <x-input-select
                            name="role"
                            :options="$rolesArray"
                            :selected="request('role')"
                            required
                        />
                    </div>
                    <div class="w-1/4">
                        <x-input-select
                            name="zip_type"
                            label="ZIP Type"
                            :options="['diffed' => 'Latest Diffed', 'validated' => 'Latest Validated (pre-diff)']"
                            :selected="request('zip_type')"
                            required
                        />
                    </div>
                    <div class="w-1/4 flex justify-center items-center pt-4">
                        <x-button color="green" size="sm">
                            <x-fas>check</x-fas> Submit
                        </x-button>
                        <x-button color="red" size="sm" href="{{ route('sis.role-audit.index') }}">
                            <x-fas>undo</x-fas> Reset
                        </x-button>
                    </div>
                </div>
            </x-form-get>

        </div>
    </div>
    @if ($users->isEmpty())
        @if (request()->filled('role'))
            <x-p>No users match the given query.</x-p>
        @else
            <x-p>Select Role and submit form to see results</x-p>
        @endif
    @else
        <x-alert-info :dismissable="false">
                <strong>SIS ZIP searched</strong>: #{{ $zip->id }} ({{ $zip->filename }})
        </x-alert-info>
        <x-table>
            <x-slot name="th">
                <x-th>User ID</x-th>
                <x-th>Login ID</x-th>
                <x-th>First Name</x-th>
                <x-th>Last Name</x-th>
                <x-th>Email</x-th>
                <x-th>Status</x-th>
            </x-slot>
            <x-slot name="tbody">
                @foreach ($users as $user)
                    <x-tr>
                        <x-td>{{ $user->user_id }}</x-td>
                        <x-td>{{ $user->login_id }}</x-td>
                        <x-td>{{ $user->first_name }}</x-td>
                        <x-td>{{ $user->last_name }}</x-td>
                        <x-td>{{ $user->email }}</x-td>
                        <x-td>{{ $user->status }}</x-td>
                    </x-tr>
                @endforeach
            </x-slot>
        </x-table>
        {{ $users->links() }}
    @endif

@endsection
