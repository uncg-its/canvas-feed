@extends('layouts.wrapper', [
    'pageTitle' => 'Diff Log: Users'
])

@section('content')
    <x-h1>Diff Log: Users</x-h1>
    @if ($isLegacy)
        <livewire:legacy-sis-diff-users-table />
    @else
        <livewire:sis-diff-users-table />
    @endif
@endsection
