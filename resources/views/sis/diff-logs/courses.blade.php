@extends('layouts.wrapper', [
    'pageTitle' => 'Diff Log: Courses'
])

@section('content')
    <x-h1>Diff Log: Courses</x-h1>
    @if ($isLegacy)
        <livewire:legacy-sis-diff-courses-table />
    @else
        <livewire:sis-diff-courses-table />
    @endif
@endsection
