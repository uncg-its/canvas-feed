@extends('layouts.wrapper', [
    'pageTitle' => 'Diff Log: Sections'
])

@section('content')
    <x-h1>Diff Log: Sections</x-h1>
    @if ($isLegacy)
        <livewire:legacy-sis-diff-sections-table />
    @else
        <livewire:sis-diff-sections-table />
    @endif
@endsection
