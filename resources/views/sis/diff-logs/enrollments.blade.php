@extends('layouts.wrapper', [
    'pageTitle' => 'Diff Log: Enrollments'
])

@section('content')
    <x-h1>Diff Log: Enrollments</x-h1>
    @if ($isLegacy)
        <livewire:legacy-sis-diff-enrollments-table />
    @else
        <livewire:sis-diff-enrollments-table />
    @endif
@endsection
