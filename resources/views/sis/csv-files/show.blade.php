@extends('layouts.wrapper', [
    'pageTitle' => 'SIS CSV File | CSV #' . $csv->id
])

@section('content')
    <x-h1>SIS CSV File #{{ $csv->id }}</x-h1>
    <div class="flex gap-4">
        <div class="w-1/2">
            <x-card title="CSV Details">
                <x-p><strong>Internal ID</strong>: {{ $csv->id }}</x-p>
                <x-p><strong>Created at</strong>: {{ $csv->created_at }}</x-p>
                <x-p><strong>File path</strong>: {{ $csv->filePath }}</x-p>
                @include('partials.statuses.p.' . $csv->status)
            </x-card>
        </div>

        <div class="w-1/2">
            <x-card title="Contents Overview">
                <x-p><strong>SIS format</strong>: {{ $csv->format }}</x-p>
                <x-p><strong># of rows</strong>: {{ $csv->row_count }}</x-p>
                @include('partials.buttons.download-file', [
                    'disk' => 'sis-zips',
                    'filename' => $csv->filePath,
                    'buttonText' => 'Download raw CSV'
                ])
            </x-card>
        </div>

    </div>
@endsection
