@extends('layouts.wrapper', [
    'pageTitle' => 'SIS Uploads | Index'
])

@section('content')
    <x-h1>SIS Uploads in progress</x-h1>
    <x-p>This page provides live information on the progress of all SIS Diffs that are on record in the database with a status of 'importing'. Remember that the database is only updated periodically, according to the schedule of the corresponding Cron Job.</x-p>
    <x-p>The progress information displayed on this page is looked up live, via the Canvas API - so you can be certain that the information is current as of page load.</x-p>
    <x-p><em>NOTE: Due to Canvas API rate limits, access to this page is restricted to 120 requests per minute (average 2 requests per second).</em></x-p>
    <x-hr />
    @if($uploads->isEmpty())
        <x-p>No SIS Uploads are in progress.</x-p>
    @else
        <x-table>
            <x-slot name="th">
                <x-th>Diff ID</x-th>
                <x-th>Filename</x-th>
                <x-th>CSV Count</x-th>
                <x-th>Total Changes</x-th>
                <x-th>Canvas Import ID</x-th>
                <x-th>% Complete</x-th>
            </x-slot>
            <x-slot name="tbody">
                @foreach ($uploads as $upload)
                    <x-tr>
                        <x-td>{{ $upload->id }}</x-td>
                        <x-td>{{ $upload->filename }}</x-td>
                        <x-td>{{ $upload->zip_csv_count }}</x-td>
                        <x-td>{{ $upload->total }}</x-td>
                        <x-td>{{ $upload->canvas_id }}</x-td>
                        <x-td>{{ $upload->progress }}</x-td>
                    </x-tr>
                @endforeach
            </x-slot>
        </x-table>
        {{ $uploads->links() }}
    @endif
@endsection
