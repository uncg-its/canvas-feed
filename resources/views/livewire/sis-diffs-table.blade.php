<div>
    <div class="flex mb-4">
        <x-card title="Search and Filter" class="w-full">
            <div class="flex gap-3">
                <div class="w-1/4">
                    <x-input-text
                        name="filename"
                        label="Filename"
                        wire:model.lazy="searchTerms.filename"
                    />
                </div>
                <div class="w-1/4">
                    <x-input-text
                        name="createdAfter"
                        label="Created After"
                        wire:model.lazy="searchTerms.created_after"
                    />
                </div>
                <div class="w-1/4">
                    <x-input-text
                        name="createdBefore"
                        label="Created Before"
                        wire:model.lazy="searchTerms.created_before"
                    />
                </div>
                <div class="w-1/4">
                    <x-input-select
                        name="status"
                        label="Status"
                        wire:model.lazy="searchTerms.status"
                        :options="[
                            '' => '--- Select --- ',
                            'new' => 'new',
                            'validated' => 'validated',
                            'held' => 'held',
                            'ignored' => 'ignored',
                            'importing' => 'importing',
                            'imported' => 'imported',
                        ]"
                    />
                </div>
            </div>
            <div class="flex">
                <div class="w-1/3 d-flex justify-content-start align-items-center">
                    <x-button color="green" size="sm" wire:click="search">
                        <x-fas>check</x-fas> Submit
                    </x-button>
                    <x-button color="red" size="sm" wire:click="resetSearch">
                        <x-fas>undo</x-fas> Reset
                    </x-button>
                </div>
            </div>
        </x-card>
    </div>
    <div class="w-full" wire:loading>
        <x-alert-info :dismissable="false" icon="spin fa-spinner">Loading...</x-alert-info>
    </div>
    <div wire:loading.remove>
        @if($diffs->isEmpty())
        <x-p>No Diffs match the provided criteria</x-p>
        @else
        <x-table>
            <x-slot name="th">
                <x-th>ID</x-th>
                <x-th>Filename</x-th>
                <x-th>Created At</x-th>
                <x-th>Old ZIP</x-th>
                <x-th>New ZIP</x-th>
                <x-th>Status</x-th>
                <x-th>Actions</x-th>
            </x-slot>
            <x-slot name="tbody">
                @foreach ($diffs as $diff)
                    <x-tr>
                        <x-td>{{ $diff->id }}</x-td>
                        <x-td>{{ $diff->filename }}</x-td>
                        <x-td>{{ $diff->created_at }}</x-td>
                        <x-td>
                            <x-a href="{{ route('sis.zips.show', $diff->old_zip) }}">{{ $diff->old_zip_id }}</x-a>
                        </x-td>
                        <x-td>
                            <x-a href="{{ route('sis.zips.show', $diff->new_zip) }}">{{ $diff->new_zip_id }}</x-a>
                        </x-td>
                        @include('partials.statuses.td.' . $diff->status)
                        <x-td>
                            <x-button color="blue" size="sm" href="{{ route('sis.diffs.show', $diff) }}">
                                <x-fas>list</x-fas> Details
                            </x-button>
                        </x-td>
                    </x-tr>
                @endforeach
            </x-slot>
        </x-table>
        @endif
    </div>
</div>
