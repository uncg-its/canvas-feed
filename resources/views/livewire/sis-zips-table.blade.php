<div>
    <div class="flex mb-4">
        <x-card title="Search and Filter" class="w-full">
            <div class="flex gap-3">
                <div class="w-1/4">
                    <x-input-text
                        name="filename"
                        label="Filename"
                        wire:model.lazy="searchTerms.filename"
                    />
                </div>
                <div class="w-1/4">
                    <x-input-text
                        name="createdAfter"
                        label="Created After"
                        wire:model.lazy="searchTerms.created_after"
                    />
                </div>
                <div class="w-1/4">
                    <x-input-text
                        name="createdBefore"
                        label="Created Before"
                        wire:model.lazy="searchTerms.created_before"
                    />
                </div>
                <div class="w-1/4">
                    <x-input-select
                        name="status"
                        label="Status"
                        wire:model.lazy="searchTerms.status"
                        :options="[
                            '' => '--- Select --- ',
                            'new' => 'new',
                            'imported' => 'imported',
                            'validated' => 'validated',
                            'held' => 'held',
                            'ignored' => 'ignored',
                            'diffed' => 'diffed',
                        ]"
                    />
                </div>
            </div>
            <div class="flex">
                <div class="w-1/3 d-flex justify-content-start align-items-center">
                    <x-button color="green" size="sm" wire:click="search">
                        <x-fas>check</x-fas> Submit
                    </x-button>
                    <x-button color="red" size="sm" wire:click="resetSearch">
                        <x-fas>undo</x-fas> Reset
                    </x-button>
                </div>
            </div>
        </x-card>
    </div>
    <div class="w-full" wire:loading>
        <x-alert-info :dismissable="false" icon="spin fa-spinner">Loading...</x-alert-info>
    </div>
    <div wire:loading.remove>
        @if($zips->isEmpty())
            <x-p>No ZIPs match the provided criteria</x-p>
        @else
            <x-table>
                <x-slot name="th">
                    <x-th>ID</x-th>
                    <x-th>Created</x-th>
                    <x-th>Filename</x-th>
                    <x-th>Status</x-th>
                    <x-th># CSVs</x-th>
                    <x-th>Actions</x-th>
                </x-slot>
                <x-slot name="tbody">
                    @foreach($zips as $zip)
                        <x-tr>
                            <x-td>{{ $zip->id }}</x-td>
                            <x-td>{{ $zip->created_at }}</x-td>
                            <x-td>{{ $zip->filename }}</x-td>
                            @include('partials.statuses.td.' . $zip->status)
                            <x-td>{{ $zip->csvs()->count() }}</x-td>
                            <x-td>
                                <x-button color="blue" size="sm" href="{{ route('sis.zips.show', $zip) }}">
                                    <x-fas>list</x-fas> Details
                                </x-button>
                            </x-td>
                        </x-tr>
                    @endforeach
                </x-slot>
            </x-table>
            {{ $zips->links() }}
        @endif
    </div>
</div>
