<div>
    @if ($legacy)
        <x-alert color="gray" icon="info-circle">
            <em>Note: Searching legacy database (prior to 1/8/2020, 15:10).</em>
        </x-alert>
    @endif

    <x-card title="Search / Filter">
        <div class="flex gap-2">
            <div class="w-1/6">
                <x-input-text
                    name="user_id"
                    label="User ID"
                    wire:model="searchTerms.user_id"
                />
            </div>
            <div class="w-1/6">
                <x-input-text
                    name="login_id_fuzzy"
                    label="Login ID"
                    wire:model="searchTerms.login_id_fuzzy"
                />
            </div>
            <div class="w-1/6">
                <x-input-text
                    name="first_name"
                    label="First Name"
                    wire:model="searchTerms.first_name"
                />
            </div>
            <div class="w-1/6">
                <x-input-text
                    name="last_name"
                    label="Last Name"
                    wire:model="searchTerms.last_name"
                />
            </div>
            <div class="w-1/6 d-flex justify-content-start align-items-center mt-3">
                <x-button color="green" size="sm" wire:click="search">
                    <x-fas>check</x-fas> Submit
                </x-button>
                <x-button color="red" size="sm" wire:click="resetSearch">
                    <x-fas>undo</x-fas> Reset
                </x-button>
            </div>
        </div>
    </x-card>


    <div wire:loading class="w-full">
        <x-alert-info :dismissable="false" icon="spin fa-spinner">
            Loading...
        </x-alert-info>
    </div>
    <div wire:loading.remove>
        @if ($users->isEmpty())
            <x-p>No users match the given criteria</x-p>
        @else
            <x-table>
                <x-slot name="th">
                    <x-th>Zip</x-th>
                    <x-th>Zip Creation</x-th>
                    <x-th>User ID</x-th>
                    <x-th>Login ID</x-th>
                    <x-th>First Name</x-th>
                    <x-th>Last Name</x-th>
                    <x-th>Email</x-th>
                    <x-th>Status</x-th>
                    <x-th>Actions</x-th>
                </x-slot>
                <x-slot name="tbody">
                    @foreach ($users as $user)
                        <x-tr wire:key="user_{{ $user->id }}">
                            <x-td>
                                <x-a href="{{ route('sis.zips.show', $user->sis_zip_id) }}">{{ $user->sis_zip_id }}</x-a>
                            </x-td>
                            <x-td>
                                {{ $user->sis_zip->created_at }}
                            </x-td>
                            <x-td>{{ $user->user_id }}</x-td>
                            <x-td>{{ $user->login_id }}</x-td>
                            <x-td>{{ $user->first_name }}</x-td>
                            <x-td>{{ $user->last_name }}</x-td>
                            <x-td>{{ $user->email }}</x-td>
                            <x-td>{{ $user->status }}</x-td>
                            <x-td>
                                <x-button color="blue" size="sm" wire:click="setLoginId('{{ $user->login_id }}')">
                                    <x-fas>user</x-fas> Isolate user
                                </x-button>
                                <x-button color="indigo" size="sm" href="{{ route('sis.zip-logs.enrollments', ['user_id_fuzzy' => $user->user_id]) }}">
                                    <x-fas>user-graduate</x-fas> Isolate enrollments
                                </x-button>
                            </x-td>
                        </x-tr>
                    @endforeach
                </x-slot>
            </x-table>
            {{ $users->links() }}
        </div>
    @endif
</div>
