<div x-data="{showUploadModal: false}" x-on:close-upload-modal="showUploadModal=false">
    <x-p>
        <strong>Canvas Upload ID</strong>: {{ $diff->canvas_id }}
    </x-p>
    <x-button color="blue" size="sm" x-on:click="showUploadModal=true" wire:click="fetchUploadDetails" wire:loading.attr="disabled">
        <span wire:loading wire:target="fetchUploadDetails"><x-fas>spin fa-spinner</x-fas> Loading details from API...</span>
        <span wire:loading.remove wire:target="fetchUploadDetails">
            <x-fas>eye</x-fas> Fetch upload details from API
        </span>
    </x-button>

    <x-modal trigger="showUploadModal">
        <x-h4>SIS Upload Details</x-h4>
        <div wire:loading wire:target="fetchUploadDetails" class="w-full">
            <x-alert-info :dismissable="false" icon="spin fa-spinner">Loading details from API...</x-alert-info>
        </div>
        <span wire:loading.remove wire:target="fetchUploadDetails">
            <pre>{{ $uploadDetails }}</pre>
        </span>
    </x-modal>
</div>
