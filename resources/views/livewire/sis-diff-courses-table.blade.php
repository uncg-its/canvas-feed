<div>
    @if ($legacy)
        <x-alert color="gray" icon="info-circle" :dismissable="false">
            <em>Note: Searching legacy database (prior to 1/8/2020, 15:10).</em>
        </x-alert-info>
    @endif

    <x-card title="Search / Filter">
       <div class="flex gap-2">
           <div class="w-1/6">
                <x-input-text
                    name="course_id"
                    label="Course ID"
                    wire:model.lazy="searchTerms.course_id"
                />
           </div>
           <div class="w-1/6">
               <x-input-text
                    name="short_name"
                    label="Short Name"
                    wire:model.lazy="searchTerms.short_name"
               />
           </div>
           <div class="w-1/6">
               <x-input-text
                    name="long_name"
                    label="Long Name"
                    wire:model.lazy="searchTerms.long_name"
               />
           </div>
           <div class="w-1/6">
                <x-input-text
                    name="account_id"
                    label="Account ID"
                    wire:model.lazy="searchTerms.account_id"
                />
           </div>
           <div class="w-1/6">
               <x-input-text
                   name="term_id"
                   label="Term ID"
                   wire:model.lazy="searchTerms.term_id"
               />
           </div>
           <div class="w-1/6">
               <x-input-text
                    name="status"
                    label="Status"
                    wire:model.lazy="searchTerms.status"
               />
           </div>
           <div class="w-1/6 flex items-end pb-4">
               <x-button color="green" size="sm" wire:click="search" class="mr-1">
                   <x-fas>check</x-fas> Submit
               </x-button>
               <x-button color="red" size="sm" wire:click="resetSearch">
                   <x-fas>undo</x-fas> Reset
               </x-button>
           </div>
       </div>
    </x-card>

    <div wire:loading class="w-full">
        <x-alert-info :dismissable="false" icon="spin fa-spinner">
            Loading...
        </x-alert-info>
    </div>
    <div wire:loading.remove>
        @if ($courses->isEmpty())
            <x-p>No courses match the given criteria</x-p>
        @else
            <x-table>
                <x-slot name="th">
                    <x-th>Diff</x-th>
                    <x-th>Zips</x-th>
                    <x-th>Diff Creation</x-th>
                    <x-th>Course / Account</x-th>
                    <x-th>Short/Long Name</x-th>
                    <x-th>Term ID</x-th>
                    <x-th>Status</x-th>
                    <x-th>Actions</x-th>
                </x-slot>
                <x-slot name="tbody">
                    @foreach ($courses as $course)
                        <x-tr wire:key="course_{{ $course->id }}">
                            <x-td>
                                @if ($legacy)
                                    <span class="text-muted"><em>{{ $course->diff_id }} (legacy)</em></span>
                                @else
                                    <x-a href="{{ route('sis.diffs.show', $course->sis_diff) }}">{{ $course->sis_diff->id }}</x-a>
                                @endif
                            </x-td>
                            <x-td>
                                @if ($legacy)
                                    <span class="text-muted"><em>{{ $course->sis_diff->diffOldZipId }} / {{ $course->sis_diff->diffNewZipId }} (legacy)</em></span>
                                @else
                                    <x-a href="{{ route('sis.zips.show', $course->sis_diff->old_zip_id) }}">{{ $course->sis_diff->old_zip_id }}</x-a> /
                                    <x-a href="{{ route('sis.zips.show', $course->sis_diff->new_zip_id) }}">{{ $course->sis_diff->new_zip_id }}</x-a>
                                @endif
                            </x-td>
                            <x-td>
                                {{ $course->sis_diff->created_at }}
                            </x-td>
                            <x-td>
                                {{ $course->course_id }}<br>
                                <small>{{ $course->account_id }}</small>
                            </x-td>
                            <x-td>
                                {{ $course->short_name }}<br>
                                <small>{{ $course->long_name }}</small>
                            </x-td>
                            <x-td>{{ $course->term_id }}</x-td>
                            <x-td>{{ $course->status }}</x-td>
                            <x-td>
                                <x-button color="blue" size="sm" wire:click="setAccount('{{ $course->account_id }}')">
                                    <x-fas>network-wired</x-fas> Isolate account
                                </x-button>
                            </x-td>
                        </x-tr>
                    @endforeach
                </x-slot>
            </x-table>
            {{ $courses->links() }}
        @endif
    </div>
</div>
