<div>
    @if ($legacy)
        <x-alert color="gray" icon="info-circle"><em>Note: Searching legacy database (prior to 1/8/2020, 15:10).</em></x-alert>
    @endif

    <x-card title="Search / Filter">
        <div class="flex gap-2">
            <div class="w-1/6">
                <x-input-text
                    name="user_id_fuzzy"
                    label="User ID"
                    wire:model.lazy="searchTerms.user_id_fuzzy"
                />
            </div>
            <div class="w-1/6">
                <x-input-text
                    name="section_id_fuzzy"
                    label="Section ID"
                    wire:model.lazy="searchTerms.section_id_fuzzy"
                />
            </div>
            <div class="w-1/6">
                <x-input-text
                    name="role"
                    label="Role"
                    wire:model.lazy="searchTerms.role"
                />
            </div>
            <div class="w-1/6">
                <x-input-text
                    name="status"
                    label="Status"
                    wire:model.lazy="searchTerms.status"
                />
            </div>
            <div class="w-1/6 flex items-end mb-4">
                <x-button color="green" size="sm" wire:click="search">
                    <x-fas>check</x-fas> Submit
                </x-button>
                <x-button color="red" size="sm" wire:click="resetSearch">
                    <x-fas>undo</x-fas> Reset
                </x-button>
            </div>
        </div>
    </x-card>

    <div wire:loading class="w-full">
        <x-alert-info :dismissable="false" icon="spin fa-spinner">
            Loading...
        </x-alert-info>
    </div>
    <div wire:loading.remove>
        @if ($enrollments->isEmpty())
            <x-p>No enrollments match the given criteria</x-p>
        @else

        <x-table>
            <x-slot name="th">
                <x-th>Diff</x-th>
                <x-th>Zip</x-th>
                <x-th>Diff Creation</x-th>
                <x-th>User ID</x-th>
                <x-th>Role</x-th>
                <x-th>Section ID</x-th>
                <x-th>Status</x-th>
                <x-th>Actions</x-th>
            </x-slot>
            <x-slot name="tbody">
                @foreach ($enrollments as $enrollment)
                    <x-tr>
                        <x-td>
                            @if ($legacy)
                                <span class="text-muted"><em>{{ $enrollment->diff_id }} (legacy)</em></span>
                            @else
                                <x-a href="{{ route('sis.diffs.show', $enrollment->sis_diff) }}">{{ $enrollment->sis_diff->id }}</x-a>
                            @endif
                        </x-td>
                        <x-td>
                            @if ($legacy)
                                <span class="text-muted"><em>{{ $enrollment->sis_diff->diffOldZipId }} (legacy)</em></span> /
                                <span class="text-muted"><em>{{ $enrollment->sis_diff->diffNewZipId }} (legacy)</em></span>
                            @else
                                <x-a href="{{ route('sis.zips.show', $enrollment->sis_diff->old_zip_id) }}">{{ $enrollment->sis_diff->old_zip_id }}</x-a> /
                                <x-a href="{{ route('sis.zips.show', $enrollment->sis_diff->new_zip_id) }}">{{ $enrollment->sis_diff->new_zip_id }}</x-a>
                            @endif
                        </x-td>
                        <x-td>
                            {{ $enrollment->sis_diff->created_at }}
                        </x-td>
                        <x-td>{{ $enrollment->user_id }}</x-td>
                        <x-td>{{ $enrollment->role }}</x-td>
                        <x-td>{{ $enrollment->section_id }}</x-td>
                        <x-td>{{ $enrollment->status }}</x-td>
                        <x-td>
                            <x-button color="blue" size="sm" wire:click="setUserIdExact('{{ $enrollment->user_id }}')">
                                <x-fas>user</x-fas> Isolate user
                            </x-button>
                            <x-button color="indigo" size="sm" wire:click="setSectionIdExact('{{ $enrollment->section_id }}')">
                                <x-fas>puzzle-piece</x-fas> Isolate section
                            </x-button>
                        </x-td>
                    </x-tr>
                @endforeach
            </x-slot>
        </x-table>
        {{ $enrollments->links() }}
    @endif
</div>
