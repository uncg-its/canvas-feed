<div>
    @if ($legacy)
        <x-alert color="gray" icon="info-circle">
            <em>Note: Searching legacy database (prior to 1/8/2020, 15:10).</em>
        </x-alert>
    @endif

    <x-card title="Search / Filter">
        <div class="flex gap-2">
            <div class="w-1/6">
                <x-input-text
                    name="section_id"
                    label="Section ID"
                    wire:model.lazy="searchTerms.section_id"
                />
            </div>
            <div class="w-1/6">
                <x-input-text
                    name="course_id"
                    label="Course ID"
                    wire:model.lazy="searchTerms.course_id"
                />
            </div>
            <div class="w-1/6">
                <x-input-text
                    name="name"
                    label="Name"
                    wire:model.lazy="searchTerms.name"
                />
            </div>
            <div class="w-1/6">
                <x-input-text
                    name="status"
                    label="Status"
                    wire:model.lazy="searchTerms.status"
                />
            </div>
            <div class="w-1/6 flex items-end mb-4">
                <x-button color="green" size="sm" wire:click.lazy="search">
                    <x-fas>check</x-fas> Submit
                </x-button>
                <x-button color="red" size="sm" wire:click.lazy="resetSearch">
                    <x-fas>undo</x-fas> Reset
                </x-button>
            </div>
        </div>
    </x-card>


    <div wire:loading class="w-full">
        <x-alert-info :dismissable="false" icon="spin fa-spinner">
            Loading...
        </x-alert-info>
    </div>
    <div wire:loading.remove>
        @if ($sections->isEmpty())
            <x-p>No sections match the given criteria</x-p>
        @else
            <x-table>
                <x-slot name="th">
                    <x-th>Zip</x-th>
                    <x-th>Zip Creation</x-th>
                    <x-th>Section ID</x-th>
                    <x-th>Course ID</x-th>
                    <x-th>Name</x-th>
                    <x-th>Status</x-th>
                    <x-th>Actions</x-th>
                </x-slot>
                <x-slot name="tbody">
                    @foreach ($sections as $section)
                        <x-tr>
                            <x-td>
                                <x-a href="{{ route('sis.zips.show', $section->sis_zip_id) }}">{{ $section->sis_zip_id }}</x-a>
                            </x-td>
                            <x-td>
                                {{ $section->sis_zip->created_at }}
                            </x-td>
                            <x-td>{{ $section->section_id }}</x-td>
                            <x-td>{{ $section->course_id }}</x-td>
                            <x-td>{{ $section->name }}</x-td>
                            <x-td>{{ $section->status }}</x-td>
                            <x-td>
                                <x-button color="blue" size="sm" wire:click="setName('{{ \Str::limit($section->name, 7, '') }}')">
                                    <x-fas>puzzle-piece</x-fas> Find Similar
                                </x-button>
                            </x-td>
                        </x-tr>
                    @endforeach
                </x-slot>
            </x-table>
            {{ $sections->links() }}
        @endif
    </div>
</div>
