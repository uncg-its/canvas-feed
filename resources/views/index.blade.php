@extends('layouts.wrapper', [
    'pageTitle' => 'Home'
])

@section('content')
    @include('partials.held-aux-data')
    @include('partials.held-sis-data')
    <x-h1>{{ config('app.name') }}</x-h1>
    <div class="flex">
        @if($user)
            <x-p>This application controls the provisioning of information into Canvas. It is segmented into two feeds - the SIS (Student Information System) feed, and the Auxiliary feed for user accounts not managed by the SIS Feed.</x-p>
        </div>

        <div class="flex gap-4">
            @permission('canvasfeed.*')
                <div class="w-1/2">
                    <x-h2>SIS Feed</x-h2>
                    <div class="flex">
                        @include('components.panel-nav', [
                            'url' => route('sis.index'),
                            'fa' => 'fas fa-user-graduate',
                            'title' => 'SIS Home',
                            'helpText' => 'Information and settings for the Canvas SIS feed from Banner, including Zips, Diffs, Validations, and more'
                        ])
                    </div>
                </div>
            @endpermission
            @permission('user-feed.*')
            <div class="w-1/2">
                <x-h2>Auxiliary Feed</x-h2>
                <div class="flex">
                    @include('components.panel-nav', [
                        'url' => route('aux.index'),
                        'fa' => 'fas fa-road',
                        'title' => 'Canvas Auxiliary Feed',
                        'helpText' => 'Information and settings for the Canvas Auxiliary (non-SIS) feed, including Runs, Overrides, Provisionsed Users, and more'
                    ])
                    @foreach($modules as $key => $module)
                        @if($key == 'user-feed')
                            @permission($module['required_permissions'])
                            @include('components.panel-nav', [
                                'url' => route($module['index']),
                                'fa' => $module['icon'],
                                'title' => $module['title'],
                                'helpText' => $module['helpText'] ?? null
                            ])
                            @endpermission
                        @endif
                    @endforeach
                </div>
            </div>
            @endpermission
        </div>
        @role('admin|lms')
        <div class="flex">
            <div class="w-1/2">
                <x-h2>API Health</x-h2>
                <div class="flex">
                    @include('components.panel-nav', [
                        'url' => route('api-checks.index'),
                        'fa' => 'fas fa-cloud',
                        'title' => 'API Status Checks',
                        'helpText' => 'View history of all API Status Checks'
                    ])

                    @include('components.panel-nav', [
                        'url' => route('api-outages.index'),
                        'fa' => 'fas fa-exclamation-triangle',
                        'title' => 'API Outages',
                        'helpText' => 'View history of all API Outages (an outage occurs when there are more than the allowed number of consecutive failed API Status Checks)'
                    ])
                </div>
            </div>
            <div class="w-1/2">
                <x-h2>Application Management</x-h2>
                <div class="flex">
                    @foreach($modules as $key => $module)
                        @if(empty($module['parent']) && $key !== 'user-feed')
                            @permission($module['required_permissions'])
                            @include('components.panel-nav', [
                                'url' => route($module['index']),
                                'fa' => $module['icon'],
                                'title' => $module['title'],
                                'helpText' => $module['helpText'] ?? null
                            ])
                            @endpermission
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
        @endrole

        @role('admin')
        <x-h2>Admin</x-h2>
        <div class="flex">
            @include('components.panel-nav', [
                'url' => route('admin'),
                'fa' => 'fas fa-key',
                'title' => 'Administrator',
                'helpText' => 'Administrator controls, including cron job panel, queues, etc.'
            ])
        </div>
        @endrole

        @else
            <x-p>You are not logged in - please <x-a href="{{ route('login') }}">log in</x-a> to proceed.</x-p>
        @endif
    </div>
@endsection
