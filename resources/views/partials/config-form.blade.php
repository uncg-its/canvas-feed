<x-form-patch :action="route('config.save')">

<x-h1>Aux Feed</x-h1>
<div class="flex mt-3 mb-3 gap-3">
	<div class="w-1/2">
        <x-card title="Thresholds">
            <x-input-text
                name="expected_source_min_count"
                label="Expected Source Min Count"
                :value="old('expected_source_min_count', $dbConfig->get('expected_source_min_count') ?: '')"
                help="Expected <strong>minimum number of user records</strong> that should be in the source Grouper group. A value below this will raise an Alarm when processing the User Feed."
            />
            <x-input-text
                name="expected_change_max_count"
                label="Expected Change Max Count"
                :value="old('expected_change_max_count', $dbConfig->get('expected_change_max_count') ?: '')"
                help="Expected <strong>maximum number of user data changes</strong> that should be in a single diff from the User Feed. A value above this amount will raise an Alarm when processing the User Feed"
            />
            <x-input-text
                name="api_check_threshold"
                label="API Check Threshold"
                :value="old('api_check_threshold', $dbConfig->get('api_check_threshold') ?: '')"
                help="The <strong>maximum number of API check failures</strong> allowed before triggering an automatic shutdown of feed operation. Also serves as the <strong>minimum number of API check successes</strong> to restore the application after feed shutdown"
            />
            <x-input-text
                name="max_hours_since_last_feed_run"
                label="Max Hours Since Last Feed Run"
                :value="old('max_hours_since_last_feed_run', $dbConfig->get('max_hours_since_last_feed_run') ?: '')"
                help="The <strong>maximum number of hours</strong> for which the feed should be allowed not to run. If this threshold is exceeded then warnings will be sent. This check ensures that the feed is operating often enough to keep up with source data."
            />
        </x-card>
	</div>
	<div class="w-1/2">
        <x-card title="Notification Settings">
            <x-input-text
                name="notification_intervals"
                label="Notification Intervals"
                :value="old('notification_intervals', $dbConfig->get('notification_intervals') ?: '')"
                help="Comma-separated list of numbers, with each number representing the number of days before an override expires. The responsible party for the override will receive notifications at these intervals. <em><span class='text-blue-500'>Example: 30,14,7,3,1</span></em>"
            />
            <x-input-text
                name="admin_override_notification_bcc"
                label="Admin email address"
                :value="old('admin_override_notification_bcc', $dbConfig->get('admin_override_notification_bcc') ?: '')"
                help="Override notifications will be BCC'ed to this address in addition to the Contact Email for the individual override."
            />
        </x-card>
        <x-card title="Enrollment Settings">
            <x-input-select
                name="aux_restore_data"
                label="Restore User Data?"
                :selected="old('aux_restore_data', $dbConfig->get('aux_restore_data', 1))"
                help="Whether to restore a user's last-known data (enrollments, groups) upon re-adding a previous user to Canvas"
                :options="[0 => 'No', 1 => 'Yes']"
            />
            <x-input-select
                name="aux_restoration_cleanup_days"
                label="Days to keep Restoration data"
                :selected="old('aux_restoration_cleanup_days', $dbConfig->get('aux_restoration_cleanup_days', 7))"
                help="Days to keep data on which items were restored when re-adding a previous user to Canvas"
                :options="[1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 14 => 14, 21 => 21, 30 => 30, 60 => 60, 90 => 90, 180 => 180, 365 => 365]"
            />
        </x-card>
	</div>
</div>

<x-h1>SIS Feed</x-h1>
<x-card title="SIS Zip/Diff Timing">
    <div class="flex gap-3">
        <div class="w-1/4">
            <x-input-select
                name="max_time_between_sis_zips"
                label="Max time between SIS Feed files"
                :selected="$dbConfig->get('max_time_between_sis_zips', '')"
                help="The maximum amount of time that should be between each SIS File that CST is sent. Alerts will be send if a SIS file is not seen within this time window."
                :options="$timeOptions"
            />
        </div>
        <div class="w-1/4">
            <x-input-select
                name="max_time_between_sis_diffs"
                label="Max time between SIS Diff creation"
                :selected="$dbConfig->get('max_time_between_sis_diffs', '')"
                help="The maximum amount of time that should be between each SIS Diff created. Alerts will be send if a SIS Diff has not been created within this time window."
                :options="$timeOptions"
            />
        </div>
        <div class="w-1/4">
            <x-input-select
                name="max_time_between_sis_diff_uploads"
                label="Max time between SIS Diff uploads"
                :selected="$dbConfig->get('max_time_between_sis_diff_uploads', '')"
                help="The maximum amount of time that should pass without a SIS Diff uploading to Canvas. Alerts will be send if a SIS Diff has not been uploaded within this time window."
                :options="$timeOptions"
            />
        </div>
        <div class="w-1/4">
            <x-input-select
                name="time_between_diff_creation_and_upload"
                label="Time between Diff creation and upload"
                :selected="$dbConfig->get('time_between_diff_creation_and_upload', '')"
                help="The elapsed time between when a diff is created and when it is uploaded"
                :options="$timeOptions"
            />
        </div>
    </div>
</x-card>

<x-card title="SIS Zip/Diff Content">
    <div class="flex gap-3">
        <div class="w-1/6">
            <x-p><strong>SIS Zip required files</strong></x-p>
            <small class="form-text text-muted">Identify which CSV files should be present in SIS ZIPs imported into Canvas Feed app.</small>
        </div>
        <div class="w-1/3">
            @foreach (config('canvas-feed.sis.acceptable_csv_fields') as $file => $fieldss)
            <x-input-checkbox
                name="required_zip_csv_files[]"
                :label="$file"
                :value="$file"
                :checked="collect(old('required_zip_csv_files', $requiredZipCsvFiles))->contains($file)"
            />
            @endforeach
        </div>
        <div class="w-1/4">
            <x-input-select
                name="sis_zip_max_change"
                label="Max % change from one SIS Zip to the next"
                :selected="$dbConfig->get('sis_zip_max_change', '')"
                help="IE: Users.csv in Zip #1 = 10000 rows, Users.csv in Zip #2 = 5000 rows. That is a 50% change. Is that within an acceptable range?"
                :options="$sisThresholdPercentageOptions"
            />
        </div>
        <div class="w-1/4">
            <x-input-select
                name="diff_threshold_max_value"
                label="Max Diff Contents"
                :selected="$dbConfig->get('diff_threshold_max_value', '')"
                help="The maximum value that a SIS type can have in a single Diff Zip. IE: Users.csv in a diff can have X rows maximum"
                :options="$diffThresholdMaxValueOptions"
            />
    </div>
</div>
</x-card>
<x-card title="SIS Zip/Diff Retention">
<div class="flex gap-3">
    <div class="w-1/4">
        <x-input-select
            name="sis_retention_zip"
            label="SIS Retention (Zip)"
            :selected="$dbConfig->get('sis_retention_zip', '')"
            help="The number of days that the SIS Zip file will be kept on disk"
            :options="$sisDiskRetentionOptions"
        />
    </div>
    <div class="w-1/4">
        <x-input-select
            name="sis_retention_db"
            label="SIS Retention (DB)"
            :selected="$dbConfig->get('sis_retention_db', '')"
            help="The number of days that the imported SIS information will be kept in the CST database. Ensure this is compatible with the diffing process and schedule."
            :options="$sisZipDbRetentionOptions"
        />
    </div>
    <div class="w-1/4">
        <x-input-select
            name="diff_retention_zip"
            label="Diff Retention (Zip)"
            :selected="$dbConfig->get('diff_retention_zip', '')"
            help="The number of days that the Diff file will be kept on disk"
            :options="$sisDiskRetentionOptions"
        />
    </div>
    <div class="w-1/4">
        <x-input-select
            name="diff_retention_db"
            label="Diff Retention (DB)"
            :selected="$dbConfig->get('diff_retention_db', '')"
            help="The number of days that the imported Diff information will be kept in the CST database. Ensure this is compatible with the diffing process and schedule."
            :options="$sisDiffDbRetentionOptions"
        />
    </div>
</div>
</x-card>
<x-card title="Active Terms">
    <div class="flex gap-3">
        <div class="w-1/4">
            <x-input-text
                name="active_term_ids"
                label="Active term_ids (Courses)"
                :value="$dbConfig->get('active_term_ids', '')"
                help="Comma seperated list of 'term_id' codes. Only courses with one of these term_ids will be processed during the diff process."
            />
        </div>
        <div class="w-1/4">
            <x-input-text
                name="active_section_prefixes"
                label="Active section_id prefixes"
                :value="$dbConfig->get('active_section_prefixes', '')"
                help="Comma seperated list of section_id prefix strings. Only enrollments & sections with one of these strings in the section_id field will be processed during the diff process."
            />
        </div>
        <div class="w-1/4">
            <x-input-text
                name="unpublished_courses_report_terms"
                label="Unpublished Courses Report Terms"
                :value="$dbConfig->get('unpublished_courses_report_terms', '')"
                help="Comma seperated list of 'sis_term_id' codes. Only courses with one of these term_ids will be included in the Unpublished Courses Metrics."
            />
        </div>
    </div>
</x-card>
<div class="flex">
    <div class="mt-3">
        <x-button color="green" size="md">
            <x-fas>check</x-fas> Submit
        </x-button>
    </div>
</div>

</x-form-patch>
