<form action="{{ route('downloads.store', ['disk' => $disk, 'filename' => $filename]) }}" method="post">
    {{ csrf_field() }}
    <x-button color="green" size="sm">
        <x-fas>download</x-fas> {{ $buttonText }}
    </x-button>
</form>
