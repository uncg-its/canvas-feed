@if($user && $user->isAbleTo('canvasfeed.*') && $sisHeldZipExists)
    <x-alert-warning>
        WARNING: Held SIS ZIP exists! Visit <x-a href="{{ route('sis.zips.index') }}">the SIS ZIPs page</x-a> to address.
    </x-alert-warning>
@endif
@if($user && $sisHeldDiffExists)
    <x-alert-warning>
        WARNING: Held SIS Diff exists! Visit <x-a href="{{ route('sis.diffs.index') }}">the SIS Diffs page</x-a> to address.
    </x-alert-warning>
@endif
