@if($user && $user->isAbleTo('user-feed.*') && $heldDataExists)
    <x-alert-warning>
        WARNING: Held Data exists (Aux Feed)! Visit <x-a href="{{ route('user-feed.held-data.index') }}">Manage Held Data</x-a> to address.
    </x-alert-warning>
@endif
