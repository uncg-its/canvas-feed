@component('mail::message')
# Feed Action FAILURE

{{ $message }}

@component('mail::button', ['url' => route('aux.runs.show', ['run' => $action->actionable->run]) ])
    View Run Details
@endcomponent

Generated by<br>
{{ config('app.name') }}
@endcomponent
