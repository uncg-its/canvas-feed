@component('mail::message')
# Canvas User Feed Notification

You are listed as a contact person for manual Canvas user override for user '{{ $override->canvas_user->email }}'. This override will expire in **{{ $days . ' ' . \Illuminate\Support\Str::plural('day', $days) }}**.

## Override Details

* Type of transaction: {{ $override->type }}
* Start date: {{ $override->created_at->format('m/d/Y h:i:s a') }}
* End date: {{ $override->expires_at->format('m/d/Y h:i:s a') }}

Overrides are usually granted in cases where a user's status in Canvas needs to be changed to something other than what UNCG records currently indicate; for instance, when a user would not normally qualify for a Canvas account, but has a legitimate reason for needing access to the service.

For information about Canvas including how to sign in, go to http://its.uncg.edu/canvas

If this override needs to continue past this time, or if you are unsure why you received this message, please contact 6-TECH at (336) 256-TECH (8324) or 6-TECH@uncg.edu

As always, ITS reminds you to confirm the safety of all links in electronic communications by hovering over them with your cursor and confirming legitimacy of the destination.

Never click on unverified website links. Report suspicious messages and links to 6-TECH at (336) 256-TECH (8324) or 6-TECH@uncg.edu.

This email is an official communication from the University of North Carolina at Greensboro. You may verify official university emails by checking the Verified Campus Communications Repository. If you have questions about the VCCR, or the authenticity of an email message you have received, please contact the sender of the message or search the UNCG website for "VCCR".

Thanks,<br>
UNCG ITS
@endcomponent
