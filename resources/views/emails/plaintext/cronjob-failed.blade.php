[{{ config('app.name') }}] The Cron Job with name {{ $cronjob->getDisplayName() }} has FAILED at {{ \Carbon\Carbon::now()->toDateTimeString() }}. Error message: {{ $exception->getMessage() }}
