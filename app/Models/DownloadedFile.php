<?php

namespace App\Models;

use App\CcpsCore\User;
use Illuminate\Database\Eloquent\Model;

class DownloadedFile extends Model
{
    protected $guarded = [];

    public $timestamps = false;

    protected $dates = [
        'created_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
