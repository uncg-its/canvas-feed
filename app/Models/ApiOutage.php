<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApiOutage extends Model
{
    protected $guarded = [];
    public $timestamps = false;
    protected $dates = [
        'created_at',
        'resolved_at'
    ];

    // relationships

    public function api_status_checks()
    {
        return $this->hasMany(ApiStatusCheck::class);
    }

    // scopes

    public function scopeInEffect($query)
    {
        return $query->whereNull('resolved_at');
    }

    // accessors

    public function getStatusAttribute()
    {
        return is_null($this->resolved_at) ? 'in effect' : 'resolved';
    }

    public function getStatusIconAttribute()
    {
        return is_null($this->resolved_at) ? 'exclamation-triangle' : 'thumbs-up';
    }

    public function getStatusClassAttribute()
    {
        return is_null($this->resolved_at) ? 'danger' : 'success';
    }
}
