<?php

namespace App\Models\Sis;

use App\Traits\Holdable;
use App\Traits\Purgeable;
use App\Traits\Resettable;
use App\Interfaces\Sis\HoldableSis;
use Illuminate\Database\Eloquent\Model;

class SisDiff extends Model implements HoldableSis
{
    use Holdable, Purgeable, Resettable;

    public $holdableStatuses = [
        'new',
        'validated',
    ];
    public $approvableStatuses = [
        'held',
    ];
    public $ignorableStatuses = [
        'held',
    ];
    public $resettableStatuses = [
        'importing',
    ];

    protected $guarded = [];
    public $timestamps = false;
    protected $dates = [
        'created_at',
        'uploaded_at',
    ];

    protected $hidden = [
        'folder_name',
        'sis_diff_courses',
        'sis_diff_enrollments',
        'sis_diff_section',
        'sis_diff_users',
    ];

    // relationships

    public function old_zip()
    {
        return $this->belongsTo(SisZip::class, 'old_zip_id');
    }

    public function new_zip()
    {
        return $this->belongsTo(SisZip::class, 'new_zip_id');
    }

    public function sis_diff_courses()
    {
        return $this->hasMany(SisDiffCourse::class, 'diff_id');
    }

    public function sis_diff_enrollments()
    {
        return $this->hasMany(SisDiffEnrollment::class, 'diff_id');
    }

    public function sis_diff_sections()
    {
        return $this->hasMany(SisDiffSection::class, 'diff_id');
    }

    public function sis_diff_users()
    {
        return $this->hasMany(SisDiffUser::class, 'diff_id');
    }

    // accessors

    public function getFolderNameAttribute()
    {
        return str_replace('.zip', '', $this->filename);
    }
}
