<?php

namespace App\Models\Sis;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

abstract class LegacySisDiffEntity extends Model
{
    public $timestamps = false;

    public $incrementing = false;

    public function sis_diff()
    {
        return $this->belongsTo(LegacySisDiff::class, 'diffZipId', 'diffZipId');
    }

    // aliases

    public function diff()
    {
        return $this->sis_diff();
    }

    // adapter methods to map to "normal" version

    public function getDiffIdAttribute()
    {
        return $this->diffZipId;
    }

    public function getCreatedAtAttribute()
    {
        return Carbon::createFromTimestamp($this->timestamp);
    }
}
