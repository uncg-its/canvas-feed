<?php

namespace App\Models\Sis;

use Illuminate\Database\Eloquent\Model;

abstract class SisDiffEntity extends Model
{
    protected $guarded = [];
    public $timestamps = false;
    protected $dates = [
        'created_at',
    ];
    protected $hidden = ['diff'];

    // relationships

    public function sis_diff()
    {
        return $this->belongsTo(SisDiff::class, 'diff_id');
    }

    // aliases

    public function diff()
    {
        return $this->sis_diff();
    }

    // formatters / output

    public function toCsvHeader()
    {
        return collect($this->makeHidden($this->csvHiddenColumns))
            ->keys()
            ->toArray();
    }
}
