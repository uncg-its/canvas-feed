<?php

namespace App\Models\Sis;

use Illuminate\Database\Eloquent\Model;

abstract class SisEntity extends Model
{
    protected $hidden = ['csv', 'zip'];

    protected $csvHiddenColumns = ['id', 'checksum', 'sis_upload_id', 'sis_zip_id', 'sis_csv_file_id', 'created_at'];

    // relationships

    public function sis_csv_file()
    {
        return $this->belongsTo(SisCsvFile::class);
    }

    public function sis_zip()
    {
        return $this->belongsTo(SisZip::class);
    }

    // aliases

    public function csv()
    {
        return $this->sis_csv_file();
    }

    public function zip()
    {
        return $this->sis_zip();
    }

    public function toCsvHeader()
    {
        return collect($this->makeHidden($this->csvHiddenColumns))
            ->keys()
            ->toArray();
    }

    public function toCsvRow()
    {
        return collect($this->makeHidden($this->csvHiddenColumns))
            ->values()
            ->toArray();
    }

    public function getSecondaryChecksumAttribute()
    {
        $checksumFields = array_diff($this->checksummable(), ['status']);
        return $this->generateChecksum($checksumFields);
    }
}
