<?php

namespace App\Models\Sis;

use App\Traits\Checksummable;
use App\Traits\ImportableSis;

class SisSection extends SisEntity
{
    use Checksummable, ImportableSis;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sis_sections';

    protected $guarded = [];
    public $timestamps = false;
    protected $dates = [
        'created_at'
    ];

    public function checksummable(): array
    {
        return [
            'section_id',
            'course_id',
            'name',
            'status',
            'start_date',
            'end_date',
        ];
    }

    // overrides

    public function getSecondaryChecksumAttribute()
    {
        $checksumFields = array_diff($this->checksummable(), ['name', 'status']);
        return $this->generateChecksum($checksumFields);
    }
}
