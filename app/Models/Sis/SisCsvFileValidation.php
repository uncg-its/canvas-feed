<?php

namespace App\Models\Sis;

use Illuminate\Database\Eloquent\Model;

class SisCsvFileValidation extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sis_csv_file_validations';

    protected $guarded = [];
    public $timestamps = false;
    protected $dates = [
        'created_at',
    ];

    // relationships

    public function sis_csv_file()
    {
        return $this->belongsTo(SisCsvFile::class);
    }
}
