<?php

namespace App\Models\Sis;

use App\Traits\Checksummable;
use App\Traits\ImportableSis;

class SisUser extends SisEntity
{
    use Checksummable, ImportableSis;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sis_users';

    protected $guarded = [];
    public $timestamps = false;
    protected $dates = [
        'created_at'
    ];

    public function csvFieldMap()
    {
        return [
            'user_id'       => 'user_id',
            'login_id'      => 'login_id',
            'password'      => $this->generatePassword(),
            'first_name'    => 'first_name',
            'last_name'     => 'last_name',
            'full_name'     => $this->generateFullName(),
            'sortable_name' => $this->generateSortableName(),
            'short_name'    => 'short_name',
            'email'         => 'email',
            'status'        => 'status',
        ];
    }

    protected function generatePassword()
    {
        return null;
    }

    protected function generateFullName()
    {
        return null;
    }

    protected function generateSortableName()
    {
        return null;
    }

    public function checksummable(): array
    {
        return [
            'user_id',
            'login_id',
            'password',
            'first_name',
            'last_name',
            'full_name',
            'sortable_name',
            'short_name',
            'email',
            'status',
        ];
    }

    // custom relationships

    public function sis_enrollments()
    {
        return $this->hasMany(SisEnrollment::class, 'user_id', 'user_id');
    }

    // overrides

    public function getSecondaryChecksumAttribute()
    {
        $checksumFields = ['user_id'];
        return $this->generateChecksum($checksumFields);
    }
}
