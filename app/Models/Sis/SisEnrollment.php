<?php

namespace App\Models\Sis;

use App\Traits\Checksummable;
use App\Traits\ImportableSis;

class SisEnrollment extends SisEntity
{
    use Checksummable, ImportableSis;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sis_enrollments';

    protected $guarded = [];
    public $timestamps = false;
    protected $dates = [
        'created_at'
    ];

    public function checksummable(): array
    {
        return [
            'course_id',
            'root_account',
            'user_id',
            'role',
            'section_id',
            'status',
            'associated_user_id',
        ];
    }

    public function sis_user()
    {
        return $this->belongsTo(SisUser::class, 'user_id', 'user_id');
    }
}
