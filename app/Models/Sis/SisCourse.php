<?php

namespace App\Models\Sis;

use App\Traits\Checksummable;
use App\Traits\ImportableSis;
use App\Traits\AppendsTermToLongName;

class SisCourse extends SisEntity
{
    use Checksummable, ImportableSis, AppendsTermToLongName;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sis_courses';

    protected $guarded = [];
    public $timestamps = false;
    protected $dates = [
        'created_at'
    ];

    public function checksummable(): array
    {
        return [
            'course_id',
            'short_name',
            'long_name',
            'account_id',
            'term_id',
            'status',
            'start_date',
            'end_date',
        ];
    }

    // overrides

    public function getSecondaryChecksumAttribute()
    {
        $checksumFields = array_diff($this->checksummable(), ['short_name', 'long_name', 'account_id', 'status']);
        return $this->generateChecksum($checksumFields);
    }
}
