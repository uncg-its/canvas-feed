<?php

namespace App\Models\Sis;

use Illuminate\Database\Eloquent\Model;

class UnpublishedCoursesReport extends Model
{
    protected $fillable = [
        'canvas_id',
        'term_id',
        'created_at'
    ];

    public $timestamps = false;

    protected $dates = [
        'created_at',
        'completed_at',
    ];
}
