<?php

namespace App\Models\Sis;

use App\Traits\Holdable;
use App\Traits\Purgeable;
use App\Interfaces\Sis\HoldableSis;
use App\Repositories\Sis\SisZipRepository;
use Illuminate\Database\Eloquent\Model;

class SisZip extends Model implements HoldableSis
{
    use Holdable, Purgeable;

    public $holdableStatuses = [
        'new',
        'imported',
        'validated',
    ];
    public $approvableStatuses = [
        'held',
    ];
    public $ignorableStatuses = [
        'held',
    ];

    protected $table = 'sis_zips';

    protected $guarded = [];
    public $timestamps = false;
    protected $dates = [
        'created_at',
        'in_canvas_at',
    ];

    protected $hidden = [
        'csvs'
    ];

    protected $casts = [
        'csv_contents' => 'array'
    ];

    // relationships

    public function sis_csv_files()
    {
        return $this->hasMany(SisCsvFile::class);
    }

    public function sis_diff_as_old()
    {
        return $this->hasOne(SisDiff::class, 'old_zip_id');
    }

    public function sis_diff_as_new()
    {
        return $this->hasOne(SisDiff::class, 'new_zip_id');
    }

    public function sis_users()
    {
        return $this->hasMany(SisUser::class);
    }

    public function sis_enrollments()
    {
        return $this->hasMany(SisEnrollment::class);
    }

    public function sis_courses()
    {
        return $this->hasMany(SisCourse::class);
    }

    public function sis_sections()
    {
        return $this->hasMany(SisSection::class);
    }

    // aliases

    public function csvs()
    {
        return $this->sis_csv_files();
    }

    // accessors

    public function getFilesystemAttribute()
    {
        $disk = ($this->type == 'auto') ? 'sis-zips' : 'sis-uploads';
        return \Storage::disk($disk);
    }

    public function getExtractedFolderAttribute()
    {
        return substr($this->filename, 0, -4);
    }

    public function getCsvFilenamesAttribute()
    {
        return collect($this->csv_contents, true)->map(function ($content, $file) {
            return $file . '.csv';
        });
    }

    public function getCsvFilesAttribute()
    {
        $files = $this->csv_filenames;
        return collect($files)->map(function ($file) {
            return $this->extracted_folder . '/' . $file;
        });
    }

    public function isLatest()
    {
        $latestZip = (new SisZipRepository)->latest();
        return $this->id == $latestZip->id;
    }
}
