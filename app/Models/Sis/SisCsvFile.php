<?php

namespace App\Models\Sis;

use Illuminate\Database\Eloquent\Model;

class SisCsvFile extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sis_csv_files';

    protected $guarded = [];
    public $timestamps = false;
    protected $dates = [
        'created_at',
        'imported_at',
        'validated_at'
    ];

    protected $hidden = [
        'zip',
        'sis_entities',
        'validations'
    ];

    // relationships

    public function sis_zip()
    {
        return $this->belongsTo(SisZip::class);
    }

    public function sis_courses()
    {
        return $this->hasMany(SisCourse::class);
    }

    public function sis_enrollments()
    {
        return $this->hasMany(SisEnrollment::class);
    }

    public function sis_sections()
    {
        return $this->hasMany(SisSection::class);
    }

    public function sis_users()
    {
        return $this->hasMany(SisUser::class);
    }

    public function sis_csv_file_validations()
    {
        return $this->hasMany(SisCsvFileValidation::class);
    }

    // aliases

    public function sis_entities()
    {
        $method = 'sis_' . $this->format;
        return $this->$method();
    }

    public function zip()
    {
        return $this->sis_zip();
    }

    public function validations()
    {
        return $this->sis_csv_file_validations();
    }

    // accessors

    public function getLatestValidationAttribute()
    {
        return $this->sis_csv_file_validations()->latest()->first();
    }

    // accessors

    public function getFilePathAttribute()
    {
        return $this->extraction_folder . '/' . $this->filename;
    }

    public function getStatusAttribute()
    {
        if (is_null($this->imported_at)) {
            return 'new';
        }

        if (is_null($this->validated_at)) {
            return 'imported';
        }

        if ($this->validations()->where('success', false)->get()->isNotEmpty()) {
            return 'validation-error';
        }

        return 'validated';
    }
}
