<?php

namespace App\Models\Sis;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class LegacySisDiff extends Model
{
    public $timestamps = false;

    protected $primaryKey = 'diffZipId';

    public $incrementing = false;

    public function enrollments()
    {
        return $this->hasMany(LegacySisEnrollment::class);
    }

    public function courses()
    {
        return $this->hasMany(LegacySisCourse::class);
    }

    public function sections()
    {
        return $this->hasMany(LegacySisSection::class);
    }

    public function users()
    {
        return $this->hasMany(LegacySisUsers::class);
    }

    // accessors for normalizing data

    public function getCreatedAtAttribute()
    {
        return Carbon::createFromTimestamp($this->diffZipCreateTime);
    }
}
