<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApiStatusCheck extends Model
{
    protected $guarded = [];
    public $timestamps = false;
    protected $dates = [
        'created_at'
    ];

    // relationships

    public function api_outage()
    {
        return $this->belongsTo(ApiOutage::class);
    }

    // accessors

    public function getStatusClassAttribute()
    {
        return $this->up ? 'green-500' : 'red-500';
    }

    public function getStatusIconAttribute()
    {
        return $this->up ? 'fas fa-check' : 'fas fa-times';
    }

    // public function getHealthAttribute()
    // {
    // }

    // aliases

    public function outage()
    {
        return $this->api_outage();
    }
}
