<?php

namespace App\Models\Aux;

use App\Traits\HasSuccessStatus;
use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
    use HasSuccessStatus;

    protected $guarded = [];

    public $timestamps = false;

    protected $dates = [
        'created_at',
        'completed_at'
    ];

    // relationships

    public function calls()
    {
        return $this->hasMany(Call::class);
    }

    public function actionable()
    {
        return $this->morphTo();
    }

    public function canvas_user()
    {
        return $this->belongsTo(CanvasUser::class, 'canvas_user_id');
    }

    public function last_enrollments()
    {
        return $this->hasMany(LastEnrollment::class);
    }

    // accessors

    public function getCanvasUserTemplateAttribute()
    {
        return $this->actionable->user_template;
    }

    public function getUserTemplateAttribute()
    {
        return $this->canvasUserTemplate;
    }

    public function getEventAttribute()
    {
        switch ($this->actionable_type) {
            case FeedChange::class:
                return 'feed-change';
            case Override::class:
                return 'override';

            throw new \OutOfBoundsException('Error: invalid actionable_type on Action ' . $this->id);
        }
    }
}
