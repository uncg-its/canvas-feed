<?php

namespace App\Models\Aux;

use Carbon\Carbon;
use App\CcpsCore\User;
use App\Traits\SpawnsAction;
use App\Constants\SuccessStatus;
use App\Traits\HasSuccessStatus;
use Uncgits\Ccps\Traits\Encryptable;
use Illuminate\Database\Eloquent\Model;

class Override extends Model
{
    use HasSuccessStatus {
        getStatusAttribute as public getStatusTextAttribute;
    }

    use Encryptable, SpawnsAction;

    protected $guarded = [];

    protected $dates = [
        'created_at',
        'updated_at',
        'canceled_at',
        'expires_at',
        'resolved_at'
    ];

    public $with = ['canvas_user', 'notifications'];

    protected $encryptable = [
        'user_template'
    ];

    // relationships

    public function submitter()
    {
        return $this->belongsTo(User::class, 'submitter_id');
    }

    public function canvas_user()
    {
        return $this->belongsTo(CanvasUser::class, 'canvas_user_id')->withTrashed();
    }

    public function action()
    {
        return $this->morphOne(Action::class, 'actionable');
    }

    public function canceled_by()
    {
        return $this->belongsTo(User::class, 'canceled_by_id');
    }

    public function notifications()
    {
        return $this->hasMany(OverrideNotification::class);
    }

    // scopes

    public function scopeActive($query)
    {
        return $query->where('expires_at', '>', Carbon::now())
            ->whereNull('canceled_at');
    }

    public function scopeExpired($query)
    {
        return $query->where('expires_at', '<=', Carbon::now());
    }

    public function scopeCanceled($query)
    {
        return $query->whereNotNull('canceled_at');
    }

    public function scopeResolved($query)
    {
        return $query->whereNotNull('resolved_at');
    }

    public function scopeUnresolved($query)
    {
        return $query->whereNull('resolved_at');
    }

    // accessors

    public function getActiveStatusAttribute()
    {
        if (!is_null($this->canceled_at)) {
            return 'canceled';
        }

        if ($this->expires_at <= Carbon::now()) {
            if (is_null($this->resolved_at)) {
                return 'expired (unresolved)';
            }
            return 'expired';
        }

        return 'active';
    }

    public function getActiveStatusClassAttribute()
    {
        switch ($this->active_status) {
            case 'canceled':
                return 'red-500';
            case 'expired':
            case 'expired (unresolved)':
                return 'yellow-500';
            default:
                return 'green-500';
        }
    }

    public function getActiveStatusIconAttribute()
    {
        switch ($this->active_status) {
            case 'canceled':
                return 'fas fa-times-circle';
            case 'expired':
            case 'expired (unresolved)':
                return 'fas fa-stopwatch';
            default:
                return 'fas fa-check';
        }
    }

    public function getExpiresAtRelativeAttribute()
    {
        return $this->expires_at->diffForHumans();
    }

    public function getStatusAttribute()
    {
        $actions = $this->actions->groupBy('status_id');

        // check whether it's still in progress first.
        if (isset($actions[SuccessStatus::IN_PROGRESS]) && $actions[SuccessStatus::IN_PROGRESS]->isNotEmpty()) {
            return 'in-progress';
        }
        if (isset($actions[SuccessStatus::PENDING]) && $actions[SuccessStatus::PENDING]->isNotEmpty()) {
            return 'pending';
        }

        // it's not still in progress...
        if (isset($actions[SuccessStatus::ERROR]) && $actions[SuccessStatus::ERROR]->isNotEmpty()) {
            return 'error';
        }
        if (isset($actions[SuccessStatus::FAILURE]) && $actions[SuccessStatus::FAILURE]->isNotEmpty()) {
            return 'failure';
        }

        return 'success';
    }

    public function getStatusIdAttribute()
    {
        $actions = $this->actions->groupBy('status_id');

        // check whether it's still in progress first.
        if (isset($actions[SuccessStatus::IN_PROGRESS]) && $actions[SuccessStatus::IN_PROGRESS]->isNotEmpty()) {
            return SuccessStatus::IN_PROGRESS;
        }
        if (isset($actions[SuccessStatus::PENDING]) && $actions[SuccessStatus::PENDING]->isNotEmpty()) {
            return SuccessStatus::PENDING;
        }

        // it's not still in progress...
        if (isset($actions[SuccessStatus::ERROR]) && $actions[SuccessStatus::ERROR]->isNotEmpty()) {
            return SuccessStatus::ERROR;
        }
        if (isset($actions[SuccessStatus::FAILURE]) && $actions[SuccessStatus::FAILURE]->isNotEmpty()) {
            return SuccessStatus::FAILURE;
        }

        return SuccessStatus::SUCCESS;
    }

    public function getCompletedAtAttribute()
    {
        $lastCompletedAction = $this->actions->sortByDesc('completed_at')->first();
        if (is_null($lastCompletedAction) || is_null($lastCompletedAction->completed_at)) {
            return false;
        }

        return $lastCompletedAction->completed_at;
    }

    public function getApplicableNotificationIntervalsAttribute()
    {
        $overrideIntervals = collect(explode(',', app('dbConfig')->get('notification_intervals')));
        if ($overrideIntervals->isEmpty()) {
            return [];
        }

        $lengthOfOverride = $this->created_at->diffInDays($this->expires_at);
        return $overrideIntervals->filter(function ($interval) use ($lengthOfOverride) {
            return $interval <= $lengthOfOverride + 1;
        });
    }

    public function getUnsentNotificationIntervalsAttribute()
    {
        $sentNotifications = $this->notifications->where('reset_at', null)->pluck('interval_days');
        return collect($this->applicable_notification_intervals)->diff($sentNotifications);
    }
}
