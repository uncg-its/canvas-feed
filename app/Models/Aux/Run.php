<?php

namespace App\Models\Aux;

use App\Constants\SuccessStatus;
use App\Traits\HasSuccessStatus;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Run
 *
 * @property int $id
 * @property string $user_data_diff_id
 * @property int $status_id
 * @property string|null $result
 * @property string|null $messages
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $completed_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Action[] $actions
 * @property-read mixed $status
 * @property-read mixed $status_class
 * @property-read mixed $status_icon
 * @property-read \App\UserDataDiff $user_data_diff
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Run newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Run newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Run query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Run whereCompletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Run whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Run whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Run whereMessages($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Run whereResult($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Run whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Run whereUserDataDiffId($value)
 * @mixin \Eloquent
 */
class Run extends Model
{
    use HasSuccessStatus;

    protected $guarded = [];
    protected $actions_count = null;

    public $timestamps = false;

    protected $dates = [
        'created_at',
    ];

    // relationships

    public function actions()
    {
        return $this->hasManyThrough(Action::class, FeedChange::class, 'run_id', 'actionable_id');
    }

    public function feed_changes()
    {
        return $this->hasMany(FeedChange::class);
    }

    public function user_data_diff()
    {
        return $this->belongsTo(UserDataDiff::class);
    }

    // accessors
    public function getStatusIdAttribute()
    {
        if (is_null($this->actions_count)) {
            $this->actions_count = \DB::table('actions')
                ->whereIn('id', $this->feed_changes()->pluck('id'))
                ->select('status_id', \DB::raw('COUNT(status_id) AS count'))
                ->groupBy('status_id')
                ->get()
                ->keyBy('status_id');
        }

        // check whether it's still in progress first.
        if (isset($this->actions_count[SuccessStatus::IN_PROGRESS])) {
            return SuccessStatus::IN_PROGRESS;
        }
        if (isset($this->actions_count[SuccessStatus::PENDING])) {
            return SuccessStatus::PENDING;
        }

        // it's not still in progress...
        if (isset($this->actions_count[SuccessStatus::ERROR])) {
            return SuccessStatus::ERROR;
        }
        if (isset($this->actions_count[SuccessStatus::FAILURE])) {
            return SuccessStatus::FAILURE;
        }

        return SuccessStatus::SUCCESS;
    }

    public function getCompletedAtAttribute()
    {
        $lastCompletedAction = $this->actions->sortByDesc('completed_at')->first();
        if (is_null($lastCompletedAction) || is_null($lastCompletedAction->completed_at)) {
            return false;
        }

        return $lastCompletedAction->completed_at;
    }


    public function getChangesAttribute()
    {
        return $this->diff_info->change_count;
    }

    // mutators

    public function getDiffInfoAttribute($value)
    {
        return json_decode($value); // as object
    }

    // aliases

    public function diff()
    {
        return $this->user_data_diff();
    }

    // other methods

    public function numberOfActionsOfType($type)
    {
        return $this->actions()->where('actions.type', $type)->count('actions.id');
    }
}
