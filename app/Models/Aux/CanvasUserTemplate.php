<?php

namespace App\Models\Aux;

use Uncgits\CanvasApi\Exceptions\CanvasApiException;

class CanvasUserTemplate
{
    public $userData;

    public $primaryId;
    protected $email;
    protected $sis_id;

    protected $oldData;
    protected $changedData;

    private function parseUserData(array $data)
    {
        // TODO - Handle cases where the required fields are blank (e.g. uncgpreferredname fields blank from LDAP)

        $userFieldMap = collect(config('canvas-feed.aux.canvas.user_field_map'));

        // basic fields
        $fields = $userFieldMap->filter(function ($item) use ($data) {
            return isset($data[$item]);
        })->mapWithKeys(function ($item, $key) use ($data) {
            return [$key => $data[$item]];
        });

        return $fields;
    }

    public function __construct($userData, array $changedData = null)
    {
        $this->initialize($userData, $changedData);
    }

    public function initialize($userData, array $changedData = null)
    {
        if (!is_a($userData, UserDataChange::class) && !is_array($userData)) {
            throw new \Exception('Error: CanvasUserTemplate class expects either a UserDataChange or an array.');
        }

        $this->userData = $userData;

        if (is_array($userData)) {
            $this->userData = new \stdClass;
            $this->userData->data = $userData;

            $this->userData->changed_data = $changedData ?? $userData;
        }

        $this->primaryId = $this->userData->data[config('canvas-feed.aux.canvas.primary_id')];
        $this->email = $this->userData->data[config('canvas-feed.aux.canvas.user_field_map.email')];

        $this->sis_id = $this->userData->data[config('canvas-feed.aux.canvas.user_field_map.sis_user_id')];
        // basic user data
        $this->oldData = $this->parseUserData($this->userData->data);
        $this->changedData = $this->parseUserData($this->userData->changed_data);
    }

    /**
     * Returns parsed old data for user
     *
     * @return mixed
     */
    public function getOldData()
    {
        return $this->oldData;
    }

    /**
     * Returns parsed changed data for user
     *
     * @return mixed
     */
    public function getChangedData()
    {
        return $this->changedData;
    }

    /**
     * Returns username for user
     *
     * @return mixed
     */
    public function getUsername()
    {
        return strtolower($this->primaryId);
    }

    public function getSisId()
    {
        return $this->sis_id;
    }

    public function getEmail()
    {
        return strtolower($this->email);
    }

    public function getSummary()
    {
        return [
            'data'         => $this->getOldData(),
            'changed_data' => $this->getChangedData()
        ];
    }

    public function createCanvasUserApi()
    {
        // an ADD action must always contain fields - it's possible they don't
        // exist in the CHANGED section at the time of the add. so, we must use
        // the old data to perform the add.

        $firstname = $this->getOldData()['firstname'];
        $lastname = $this->getOldData()['lastname'];

        $canvasUser = [
            'user' => [
                'name'              => $firstname . ' ' . $lastname,
                'short_name'        => $firstname . ' ' . $lastname,
                'sortable_name'     => $lastname . ', ' . $firstname,
                'time_zone'         => config('app.timezone'),
                'locale'            => 'en-US',
                'terms_of_use'      => true,
                'skip_registration' => true
            ],
            'pseudonym' => [
                'unique_id'         => $this->getUsername(),
                'sis_user_id'       => $this->getOldData()['sis_user_id'],
                'send_confirmation' => false,
            ],
            'communication_channel' => [
                'type'              => 'email',
                'address'           => $this->getEmail(),
                'skip_confirmation' => true
            ],
            'enable_sis_reactivation' => true,
        ];

        return \CanvasApi::using('users')
            ->addParameters($canvasUser)
            ->createUser(config('canvas-feed.canvas.root_account_id'));
    }

    public function updateCanvasUserApi()
    {
        // UPDATE action - what has changed?

        $changed = $this->getChangedData();

        $canvasUser = ['user' => []];
        if (isset($changed['firstname']) || isset($changed['lastname'])) {
            $firstname = $changed['firstname'] ?? $this->getOldData()['firstname'];
            $lastname = $changed['lastname'] ?? $this->getOldData()['lastname'];

            $canvasUser['user']['name'] = $firstname . ' ' . $lastname;
            $canvasUser['user']['short_name'] = $firstname . ' ' . $lastname;
            $canvasUser['user']['sortable_name'] = $lastname . ', ' . $firstname;
        }

        if (isset($changed['email'])) {
            $canvasUser['user']['email'] = $changed['email'];
        }

        if (isset($changed['username'])) {
            // modify the login instead
        }

        if (isset($changed['sis_user_id'])) {
            // shouldn't ever be ther case.modify the login instead
        }

        return \CanvasApi::using('users')
            ->addParameters($canvasUser)
            ->editUser('sis_user_id:' . $this->sis_id);
    }

    public function deleteCanvasUserApi()
    {
        return \CanvasApi::using('accounts')->deleteUserFromRootAccount(
            config('canvas-feed.canvas.root_account_id'),
            'sis_user_id:' . $this->getSisId()
        );
    }


    public function createCanvasUserLocal()
    {
        // compile data based on app settings

        // an ADD action must always contain fields - it's possible they don't
        // exist in the CHANGED section at the time of the add. so, we must use
        // the old data to perform the add.

        $userData = [
            'sis_id'         => $this->getSisId(),
            'data'           => json_encode($this->getOldData()),
        ];

        // updateOrCreate since we're potentially handling this as the result
        // of an ADD of a previously DELETED user
        return CanvasUser::updateOrCreate(['primary_id' => $this->primaryId], $userData);
    }

    public static function createFromApi($id)
    {
        $result = \CanvasApi::using('users')->getUser($id);
        if ($result->getStatus() !== 'success') {
            throw new CanvasApiException('Could not fetch user ' . $id . ' from Canvas API');
        }

        $user = $result->getContent();

        list($lastname, $firstname) = explode(', ', $user->sortable_name);
        $data = [
            config('canvas-feed.aux.canvas.user_field_map.firstname')   => $firstname,
            config('canvas-feed.aux.canvas.user_field_map.lastname')    => $lastname,
            config('canvas-feed.aux.canvas.user_field_map.email')       => $user->email,
            config('canvas-feed.aux.canvas.primary_id')                 => $user->login_id,
            config('canvas-feed.aux.canvas.user_field_map.sis_user_id') => $user->sis_user_id
        ];

        return new self($data);
    }

    public function getLastEnrollments()
    {
        $result = \CanvasApi::using('enrollments')
            ->listUserEnrollments('sis_login_id:' . $this->primaryId);

        if ($result->getStatus() !== 'success') {
            throw new CanvasApiException('Error while retrieving last enrollments for user ' . $this->primaryId);
        }

        return $result->getContent();
    }

    public function getLastGroups()
    {
        $result = \CanvasApi::using('groups')
            ->asUser('sis_login_id:' . $this->primaryId)
            ->listGroups();

        if ($result->getStatus() !== 'success') {
            throw new CanvasApiException('Error while retrieving last enrollments for user ' . $this->primaryId);
        }

        return $result->getContent();
    }
}
