<?php

namespace App\Models\Aux;

use App\Models\Aux\Action;
use App\Traits\HasSuccessStatus;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Call
 *
 * @property int $id
 * @property int $action_id
 * @property int $status_id
 * @property string|null $endpoint
 * @property string|null $method
 * @property string|null $headers
 * @property int|null $response_code
 * @property string|null $message
 * @property string|null $body
 * @property string|null $raw
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $completed_at
 * @property-read \App\Action $action
 * @property-read mixed $status
 * @property-read mixed $status_class
 * @property-read mixed $status_icon
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Call newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Call newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Call query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Call whereActionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Call whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Call whereCompletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Call whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Call whereEndpoint($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Call whereHeaders($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Call whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Call whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Call whereMethod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Call whereRaw($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Call whereResponseCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Call whereStatusId($value)
 * @mixin \Eloquent
 */
class Call extends Model
{
    use HasSuccessStatus;

    protected $guarded = [];

    public $timestamps = false;

    protected $dates = [
        'created_at'
    ];

    // relationships

    public function action()
    {
        return $this->belongsTo(Action::class);
    }
}
