<?php

namespace App\Models\Aux;

use Illuminate\Database\Eloquent\Model;

class LastEnrollment extends Model
{
    public $timestamps = false;

    protected $dates = [
        'created_at',
        'restored_at'
    ];

    // relationships

    public function action()
    {
        return $this->belongsTo(Action::class);
    }

    public function canvas_user()
    {
        return $this->belongsTo(CanvasUser::class);
    }
}
