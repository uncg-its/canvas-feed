<?php

namespace App\Models\Aux;

use Uncgits\Ccps\UserFeed\UserDataChange as BaseModel;

/**
 * App\UserDataChange
 *
 * @property string $id
 * @property string $user_data_diff_id
 * @property string $data
 * @property string $changed_data
 * @property int $encrypted
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Action[] $actions
 * @property-read \Uncgits\Ccps\UserFeed\UserDataDiff $user_data_diff
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserDataChange newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserDataChange newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserDataChange query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserDataChange whereChangedData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserDataChange whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserDataChange whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserDataChange whereEncrypted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserDataChange whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserDataChange whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserDataChange whereUserDataDiffId($value)
 * @mixin \Eloquent
 */
class UserDataChange extends BaseModel
{
    // property definitions

    public $timestamps = false;
    protected $dates = [
        'created_at',
        'updated_at',
        'ignored_at',
    ];

    // added relationships

    public function actions()
    {
        return $this->hasMany(Action::class);
    }

    // accessors

    public function getStatusAttribute()
    {
        if (!is_null($this->ignored_at)) {
            return 'ignored';
        }

        return $this->invalid ? 'invalid' : 'valid';
    }
}
