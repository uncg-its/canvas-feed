<?php

namespace App\Models\Aux;

use Uncgits\Ccps\UserFeed\UserDataDiff as BaseModel;

/**
 * App\UserDataDiff
 *
 * @property string $id
 * @property string|null $old_batch_id
 * @property string $new_batch_id
 * @property string $metrics
 * @property string $data
 * @property int $encrypted
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $metrics_decoded
 * @property-read mixed $status_class
 * @property-read mixed $status_icon
 * @property-read \Uncgits\Ccps\UserFeed\Batch $new_batch
 * @property-read \Uncgits\Ccps\UserFeed\Batch|null $old_batch
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Run[] $runs
 * @property-read \Illuminate\Database\Eloquent\Collection|\Uncgits\Ccps\UserFeed\UserDataChange[] $user_data_changes
 * @method static \Illuminate\Database\Eloquent\Builder|\Uncgits\Ccps\UserFeed\UserDataDiff latestChanges()
 * @method static \Illuminate\Database\Eloquent\Builder|\Uncgits\Ccps\UserFeed\UserDataDiff latestExpanded()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserDataDiff newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserDataDiff newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserDataDiff query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserDataDiff whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserDataDiff whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserDataDiff whereEncrypted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserDataDiff whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserDataDiff whereMetrics($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserDataDiff whereNewBatchId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserDataDiff whereOldBatchId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserDataDiff whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserDataDiff whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class UserDataDiff extends BaseModel
{
    // added relationships

    public function runs()
    {
        return $this->hasMany(Run::class);
    }

    // overridden relationships

    public function user_data_changes()
    {
        return $this->hasMany(UserDataChange::class);
    }

    // accessors

    public function getDataSummaryAttribute()
    {
        return collect($this->data)->mapWithKeys(function ($item, $key) {
            return [$key => collect($item)->pluck('data.' . config('canvas-feed.aux.canvas.user_field_map.username'))];
        });
    }
}
