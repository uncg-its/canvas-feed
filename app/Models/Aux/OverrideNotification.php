<?php

namespace App\Models\Aux;

use Illuminate\Database\Eloquent\Model;

class OverrideNotification extends Model
{
    protected $guarded = [];
    public $timestamps = false;
    protected $dates = [
        'reset_at',
        'created_at',
    ];

    // relationships
    public function override()
    {
        return $this->belongsTo(Override::class);
    }
}
