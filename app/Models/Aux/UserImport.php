<?php

namespace App\Models\Aux;

use App\CcpsCore\User;
use Illuminate\Database\Eloquent\Model;

class UserImport extends Model
{
    public $timestamps = false;

    protected $dates = [
        'created_at',
        'completed_at'
    ];

    // relationships

    public function created_by_user()
    {
        return $this->belongsTo(User::class);
    }

    // query scopes

    public function scopePending($query)
    {
        return $query->whereNull('completed_at');
    }

    public function scopeCompleted($query)
    {
        return $query->whereNotNull('completed_at');
    }

    public function scopeWithMessage($query)
    {
        return $query->whereNotNull('message');
    }
}
