<?php

namespace App\Models\Aux;

use Uncgits\Ccps\Traits\Encryptable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Uncgits\CanvasApi\Exceptions\CanvasApiException;

class CanvasUser extends Model
{
    use Encryptable, SoftDeletes;

    protected $guarded = [];
    public $incrementing = false;
    protected $primaryKey = 'primary_id';

    protected $encryptable = [
        'data'
    ];

    // relationships

    public function overrides()
    {
        return $this->hasMany(Override::class, 'canvas_user_id');
    }

    public function feed_changes()
    {
        return $this->hasMany(FeedChange::class, 'canvas_user_id');
    }

    public function actions()
    {
        return $this->hasMany(Action::class, 'canvas_user_id');
    }

    public function last_enrollments()
    {
        return $this->hasMany(LastEnrollment::class, 'canvas_user_id');
    }

    public function last_groups()
    {
        return $this->hasMany(LastGroup::class, 'canvas_user_id');
    }

    // accessors

    public function getIdAttribute()
    {
        return $this->primary_id;
    }

    public function getStatusAttribute()
    {
        return $this->trashed() ? 'deleted' : 'active';
    }

    public function getStatusClassAttribute()
    {
        switch ($this->status) {
            case 'active':
                return 'green-500';
            case 'deleted':
                return 'red-500';

            return '';
        }
    }

    public function getStatusIconAttribute()
    {
        switch ($this->status) {
            case 'active':
                return 'fas fa-check';
            case 'deleted':
                return 'fas fa-times';

            return '';
        }
    }

    public function getFirstNameAttribute()
    {
        return json_decode($this->data)->firstname;
    }

    public function getLastNameAttribute()
    {
        return json_decode($this->data)->lastname;
    }

    public function getUncgUsernameAttribute()
    {
        return $this->primary_id;
    }

    public function getEmailAttribute()
    {
        return json_decode($this->data)->email;
    }

    public function getEventsAttribute()
    {
        return $this->actions
            ->load(['actionable'])
            ->mapWithKeys(function ($action) {
                return collect([
                    $action->id => [
                        'type'       => $action->event,
                        'actionable' => $action->actionable
                    ]
                ]);
            })->sortByDesc(function ($item) {
                return $item['actionable']->created_at;
            });
    }

    public function getRunsAttribute()
    {
        return $this->actions->load('run')->pluck('run')->filter(function ($run) {
            return !is_null($run);
        });
    }

    public function getFirstAppearedAttribute()
    {
        return $this->events->sortBy(function ($item) {
            return $item['actionable']->created_at;
        })->first();
    }

    public function getEnrollmentsToRestoreAttribute()
    {
        return $this->last_enrollments()->whereNull('restored_at')->get();
    }

    public function getGroupsToRestoreAttribute()
    {
        return $this->last_groups()->whereNull('restored_at')->get();
    }

    // aliases

    public function getCanvasIdAttribute()
    {
        return $this->username;
    }

    public function getActiveAttribute()
    {
        return ! $this->trashed();
    }

    //  other methods

    public function restoreEnrollment(LastEnrollment $enrollment)
    {
        $result = \CanvasApi::using('enrollments')
            ->addParameters([
                'enrollment' => [
                    'user_id'                            => 'sis_login_id:' . $this->primary_id,
                    'type'                               => $enrollment->type,
                    'role_id'                            => $enrollment->role_id,
                    'enrollment_state'                   => $enrollment->enrollment_state,
                    'limit_privileges_to_course_section' => $enrollment->limit_privileges_to_course_section,
                    'notify'                             => false,
                ]
            ])
            ->enrollUserInSection($enrollment->course_section_id);

        if ($result->getStatus() !== 'success') {
            // if the error wasn't about concluded courses, then abort this section.
            if ($result->getContent()->message != "Can't add an enrollment to a concluded course.") {
                throw new CanvasApiException($result->getContent()->message ?? 'unknown error');
            }

            // get original course information
            $originalCourseInfo = \CanvasApi::using('courses')
            ->getCourse($enrollment->course_id)
            ->getContent();

            // make course temporarily active again
            $courseUpdateResult = \CanvasApi::using('courses')
                ->addParameters([
                    'course' => [
                        'start_at'                             => now()->subDays(1)->format('Y-m-d\TH:i\Z'),
                        'end_at'                               => now()->addDays(1)->format('Y-m-d\TH:i\Z'),
                        'restrict_enrollments_to_course_dates' => true
                    ]
                ])->updateCourse($enrollment->course_id);

            // try enrollment again
            $result = \CanvasApi::using('enrollments')
                ->addParameters([
                    'enrollment' => [
                        'user_id'                            => 'sis_login_id:' . $this->primary_id,
                        'type'                               => $enrollment->type,
                        'role_id'                            => $enrollment->role_id,
                        'enrollment_state'                   => $enrollment->enrollment_state,
                        'limit_privileges_to_course_section' => $enrollment->limit_privileges_to_course_section,
                        'notify'                             => false,
                    ]
                ])
                ->enrollUserInSection($enrollment->course_section_id);

            // restore course to original state
            $courseUpdateResult = \CanvasApi::using('courses')
                ->addParameters([
                    'course' => [
                        'start_at'                             => $originalCourseInfo->start_at,
                        'end_at'                               => $originalCourseInfo->end_at,
                        'restrict_enrollments_to_course_dates' => $originalCourseInfo->restrict_enrollments_to_course_dates
                    ]
                ])->updateCourse($enrollment->course_id);

            // make sure the enrollment was successful the second time
            if ($result->getStatus() !== 'success') {
                throw new CanvasApiException($result->getContent()->message ?? 'unknown error');
            }
        }
    }

    public function restoreGroup(LastGroup $group)
    {
        $result = \CanvasApi::using('groups')
            ->addParameters([
                'invitees' => $this->email,
            ])
            ->inviteOthersToGroup($group->group_id);

        // make sure the enrollment was successful the second time
        if ($result->getStatus() !== 'success') {
            throw new CanvasApiException($result->getContent()->message ?? 'unknown error');
        }
    }
}
