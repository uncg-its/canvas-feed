<?php

namespace App\Models\Aux;

use App\Traits\SpawnsAction;
use Uncgits\Ccps\Traits\Encryptable;
use Illuminate\Database\Eloquent\Model;

class FeedChange extends Model
{
    use Encryptable, SpawnsAction;

    protected $guarded = [];
    public $timestamps = false;
    protected $dates = [
        'created_at'
    ];

    protected $encryptable = [
        'user_template'
    ];

    // relationships

    public function run()
    {
        return $this->belongsTo(Run::class);
    }

    public function user_data_change()
    {
        return $this->belongsTo(UserDataChange::class);
    }

    public function action()
    {
        return $this->morphOne(Action::class, 'actionable');
    }

    public function canvas_user()
    {
        return $this->belongsTo(CanvasUser::class, 'canvas_user_id')->withTrashed();
    }

    // aliases

    public function change()
    {
        return $this->user_data_change();
    }
}
