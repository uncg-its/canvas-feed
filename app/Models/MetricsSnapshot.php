<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MetricsSnapshot extends Model
{
    public $timestamps = false;
    protected $dates = [
        'created_at',
        'verified_at',
        'pushed_at'
    ];

    protected $guarded = [];

    protected $casts = [
        'raw_data' => 'array'
    ];
}
