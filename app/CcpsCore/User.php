<?php

namespace App\CcpsCore;

use App\Models\DownloadedFile;
use App\Models\Aux\UserImport;
use Uncgits\Ccps\Models\User as BaseModel;

class User extends BaseModel
{
    // relationships

    public function user_imports()
    {
        return $this->hasMany(UserImport::class);
    }

    public function downloaded_files()
    {
        return $this->hasMany(DownloadedFile::class);
    }
}
