<?php

namespace App\Alarms;

use App\Cronjobs\InitiateNewRun;
use Uncgits\Ccps\UserFeed\BaseAlarm;
use Uncgits\Ccps\UserFeed\Cronjobs\CreateUserDataDiff;
use Uncgits\Ccps\UserFeed\Cronjobs\FetchNewUserDataBatch;
use Uncgits\Ccps\UserFeed\Cronjobs\ExpandUserDataFromBatch;
use Uncgits\Ccps\UserFeed\Cronjobs\PruneOldDiffsAndBatches;
use Uncgits\Ccps\UserFeed\Cronjobs\ExpandUserDataChangesFromDiff;

class BatchContainsExpectedNumberOfSourceRecords extends BaseAlarm
{
    protected $cronjobClassesToStop = [
        FetchNewUserDataBatch::class,
        ExpandUserDataFromBatch::class,
        CreateUserDataDiff::class,
        ExpandUserDataChangesFromDiff::class,
        PruneOldDiffsAndBatches::class,
        InitiateNewRun::class,
    ];

    public function passes($data)
    {
        // return true for a successful pass
        return $data->metrics['source_records_count'] >= app('dbConfig')->get('expected_source_min_count');
    }

    public function onFail($data)
    {
        $data->status = 'held';
        $data->save();

        $sourceRecordsCount = $data->metrics['source_records_count'];
        $expectedCount = app('dbConfig')->get('expected_source_min_count');
        \Log::channel('aux-feed')->warning('Batch contains fewer source records than expected. Stopping feed cronjobs.', [
                'category'  => 'aux-feed',
                'operation' => 'alarm',
                'result'    => 'failure',
                'data'      => [
                    'batch_id'       => $data->id,
                    'record_count'   => $sourceRecordsCount,
                    'expected_count' => $expectedCount
                ]
            ]);
        $this->stopCronjobs($this->cronjobClassesToStop);
    }
}
