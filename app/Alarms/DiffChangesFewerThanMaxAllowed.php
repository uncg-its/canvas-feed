<?php

namespace App\Alarms;

use App\Cronjobs\InitiateNewRun;
use Uncgits\Ccps\UserFeed\BaseAlarm;
use Uncgits\Ccps\UserFeed\Cronjobs\CreateUserDataDiff;
use Uncgits\Ccps\UserFeed\Cronjobs\FetchNewUserDataBatch;
use Uncgits\Ccps\UserFeed\Cronjobs\ExpandUserDataFromBatch;
use Uncgits\Ccps\UserFeed\Cronjobs\PruneOldDiffsAndBatches;
use Uncgits\Ccps\UserFeed\Cronjobs\ExpandUserDataChangesFromDiff;

class DiffChangesFewerThanMaxAllowed extends BaseAlarm
{
    protected $cronjobClassesToStop = [
        FetchNewUserDataBatch::class,
        ExpandUserDataFromBatch::class,
        CreateUserDataDiff::class,
        ExpandUserDataChangesFromDiff::class,
        PruneOldDiffsAndBatches::class,
        InitiateNewRun::class,
    ];

    public function passes($data)
    {
        // return true for a successful pass
        return $data->metrics['total_changes'] <= app('dbConfig')->get('expected_change_max_count');
    }

    public function onFail($data)
    {
        $data->status = 'held';
        $data->save();

        $changeCount = $data->metrics['total_changes'];
        $expectedCount = app('dbConfig')->get('expected_change_max_count');
        \Log::channel('aux-feed')->warning('Diff contains more changes than expected. Stopping feed cronjobs.', [
                'category'  => 'aux-feed',
                'operation' => 'alarm',
                'result'    => 'failure',
                'data'      => [
                    'diff_id'        => $data->id,
                    'change_count'   => $changeCount,
                    'expected_count' => $expectedCount
                ]
            ]);
        $this->stopCronjobs($this->cronjobClassesToStop);
    }
}
