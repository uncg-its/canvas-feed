<?php

namespace App\Mail;

use App\Models\Aux\Override;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OverrideNotificationMail extends Mailable
{
    protected $override;
    protected $days;

    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Override $override, $days)
    {
        $this->onQueue('email');

        $this->override = $override;
        $this->days = $days;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $adminBccAddress = app('dbConfig')->get('admin_override_notification_bcc');
        if (!is_null($adminBccAddress)) {
            $this->bcc($adminBccAddress);
        }

        return $this->to($this->override->contact_email)
            ->markdown('email.override-notification')
            ->with(['override' => $this->override, 'days' => $this->days]);
    }
}
