<?php

namespace App\Observers;

use App\Models\Aux\UserImport;
use App\Jobs\Aux\ProcessUserImport;

class UserImportObserver
{
    public function created(UserImport $import)
    {
        dispatch(new ProcessUserImport($import));
    }
}
