<?php

namespace App\Observers;

use App\Models\Aux\CanvasUser;

class CanvasUserObserver
{
    private function log($message)
    {
        \Log::channel('user-audit-trail')->info($message);
    }

    public function creating(CanvasUser $canvasUser)
    {
        $this->log('User ' . $canvasUser->uncgUsername . ' was created in Canvas.');
    }

    public function updating(CanvasUser $canvasUser)
    {
        $this->log('User ' . $canvasUser->uncgUsername . ' was updated in Canvas. Updated data: ' . $canvasUser->data);
    }
}
