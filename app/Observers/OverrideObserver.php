<?php

namespace App\Observers;

use App\Models\Aux\Override;

class OverrideObserver
{
    public function created(Override $override)
    {
        $override->spawnAction($override->type);
    }
}
