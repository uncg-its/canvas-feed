<?php

namespace App\Observers;

use Carbon\Carbon;
use App\Models\ApiOutage;
use App\Events\CanvasApiIsUp;
use App\Events\CanvasApiIsDown;
use App\Models\ApiStatusCheck;

class ApiStatusCheckObserver
{
    public function created(ApiStatusCheck $check)
    {
        try {
            \DB::beginTransaction();

            // how many other failures are allowed?
            $failureThreshold = app('dbConfig')->get('api_check_threshold');
            // get that many recent records
            $closestChecks = ApiStatusCheck::where('created_at', '<', $check->created_at)
                ->orderBy('created_at', 'desc')
                ->limit($failureThreshold)
                ->get();

            // do we have a current outage in effect?
            $outage = ApiOutage::inEffect()->first();

            if ($check->up) {
                // do we have enough successes to resolve an existing outage?
                if (!is_null($outage) && $closestChecks->where('up', true)->count() == $failureThreshold) {
                    // we should get here if we have an existing outage to resolve and we can resolve it.
                    $outage->resolved_at = Carbon::now()->toDateTimeString();
                    $outage->save();

                    event(new CanvasApiIsUp($check, $outage));
                    \Log::channel('api')->notice('RESOLVED: Canvas API is up. Resolving Outage', [
                        'category'  => 'api-outage',
                        'operation' => 'resolve',
                        'result'    => 'success',
                        'data'      => [
                            'outage' => $outage,
                            'check'  => $check
                        ]
                    ]);
                }
            } else {
                // have we breached the threshold?
                if ($closestChecks->where('up', false)->count() == $failureThreshold) {
                    // update this call to reference the current outage
                    if (is_null($outage)) {
                        // create a new outage
                        $outage = new ApiOutage;
                        $outage->created_at = Carbon::now()->toDateTimeString();
                        $outage->save();

                        // attach the other outages
                        $closestChecks->each(function ($oldCheck) use ($outage) {
                            $oldCheck->api_outage_id = $outage->id;
                            $oldCheck->save();
                        });

                        event(new CanvasApiIsDown($check, $outage));

                        \Log::channel('api')->critical('Canvas API is down.', [
                            'category'  => 'api-outage',
                            'operation' => 'create',
                            'result'    => 'success',
                            'data'      => [
                                'outage' => $outage,
                                'check'  => $check
                            ]
                        ]);
                    }

                    // attach this call to the outage regardless
                    $check->api_outage_id = $outage->id;
                    $check->save();
                }
            }
            \DB::commit();
        } catch (\Exception $e) {
            \Log::channel('api')->error('Unexpected error while checking API outage status', [
                'category'  => 'api-outage',
                'operation' => $check->up ? 'resolve' : 'create',
                'result'    => 'error',
                'data'      => [
                    'outage' => $outage ?? 'n/a',
                    'check'  => $check
                ]
            ]);
            \DB::rollBack();
            throw $e;
        }
    }
}
