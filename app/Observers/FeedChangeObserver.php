<?php

namespace App\Observers;

use App\Models\Aux\FeedChange;

class FeedChangeObserver
{
    public function created(FeedChange $change)
    {
        $change->spawnAction($change->type);
    }
}
