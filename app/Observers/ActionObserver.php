<?php

namespace App\Observers;

use App\Models\Aux\Action;

class ActionObserver
{
    public function created(Action $action)
    {
        // AddCanvasUser, UpdateCanvasUser, DeleteCanvasUser
        $jobClass = '\\App\\Jobs\\Aux\\' . ucwords($action->type) . 'CanvasUser';
        dispatch(new $jobClass($action));
    }
}
