<?php

namespace App\SisValidators;

use App\Models\Sis\SisZip;
use Illuminate\Support\Collection;

class Sections extends SisValidator
{
    public function validate(Collection $items, SisZip $sisZip) : array
    {
        if ($items->isEmpty()) {
            return [];
        }

        // get all courses from the _sis_courses table for this same zip file
        $courses = $sisZip->sis_courses->keyBy('course_id');

        $validationErrors = $items->filter(function ($item) use ($courses) {
            return ! $courses->has($item->course_id);
        })->mapWithKeys(function ($badRecord) {
            return [$badRecord->id => 'Course ' . $badRecord->course_id . ' does not exist in zip file'];
        });

        return $validationErrors->toArray();
    }
}
