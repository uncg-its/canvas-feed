<?php

namespace App\SisValidators;

use App\Exceptions\SisValidationException;
use App\Models\Sis\SisZip;
use Illuminate\Support\Collection;

class Courses extends SisValidator
{
    public function validate(Collection $items, SisZip $sisZip) : array
    {
        //will be loaded from cache as appropriate
        $accountsResult = \CanvasApi::using('accounts')
            ->addParameters(['recursive' => true])
            ->getSubaccounts(config('canvas-feed.sis.root_account_id'));

        if ($accountsResult->getStatus() !== 'success') {
            throw new SisValidationException('Error while querying Canvas API for Account data: ' . $accountsResult->getMessage());
        }

        $accounts = collect($accountsResult->getContent());

        $failedValidation = $items->filter(function ($item) use ($accounts) {
            return ! $accounts->pluck('sis_account_id')->contains($item->account_id);
        });

        $errorsArray = $failedValidation->mapWithKeys(function ($failed) {
            return [$failed->course_id => 'Account ID: ' . $failed->account_id . ' does not exist'];
        })->toArray();

        return $errorsArray;
    }
}
