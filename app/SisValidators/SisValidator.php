<?php

namespace App\SisValidators;

use App\Models\Sis\SisZip;
use Illuminate\Support\Collection;

abstract class SisValidator
{
    abstract public function validate(Collection $items, SisZip $sisZip) : array;
}
