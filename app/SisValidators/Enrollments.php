<?php

namespace App\SisValidators;

use App\Models\Sis\SisZip;
use Illuminate\Support\Collection;

class Enrollments extends SisValidator
{
    public function validate(Collection $items, SisZip $sisZip) : array
    {
        if ($items->isEmpty()) {
            return [];
        }

        $validationArray = [
            'courses'  => $sisZip->sis_courses->keyBy('course_id'),
            'sections' => $sisZip->sis_sections->keyBy('section_id'),
            'users'    => $sisZip->sis_users->keyBy('user_id'),
            'roles'    => collect(config('canvas-feed.sis.expected_roles')),
        ];

        $validationErrors = $items->mapWithKeys(function ($item) use ($validationArray) {
            // user must be in latest zip
            if (! $validationArray['users']->has($item->user_id)) {
                return [$item->id => 'User ' . $item->user_id . ' does not exist in zip file'];
            }
            // course must be in latest zip
            if (! is_null($item->course_id) && ! $validationArray['courses']->has($item->course_id)) {
                return [$item->id => 'Course ' . $item->course_id . ' does not exist in zip file'];
            }
            // section must be in latest zip
            if (! is_null($item->section_id) && ! $validationArray['sections']->has($item->section_id)) {
                return [$item->id => 'Section ' . $item->section_id . ' does not exist in zip file'];
            }
            // role must be one that we expect
            if ($validationArray['roles']->search($item->role) === false) {
                return [$item->id => 'Role "' . $item->role . '" is not valid'];
            }
            // no errors
            return [];
        })->filter();

        return $validationErrors->toArray();
    }
}
