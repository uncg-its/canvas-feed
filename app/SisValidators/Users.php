<?php

namespace App\SisValidators;

use App\Models\Sis\SisZip;
use Illuminate\Support\Collection;

class Users extends SisValidator
{
    public function validate(Collection $items, SisZip $sisZip) : array
    {
        // no current validation rules for users
        return [];
    }
}
