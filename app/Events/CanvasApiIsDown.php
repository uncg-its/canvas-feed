<?php

namespace App\Events;

use App\CcpsCore\User;
use App\Models\ApiOutage;
use App\Models\ApiStatusCheck;
use Illuminate\Queue\SerializesModels;
use Uncgits\Ccps\Traits\NotifiesChannels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use App\Notifications\CanvasApiDownNotification;
use Illuminate\Broadcasting\InteractsWithSockets;

class CanvasApiIsDown
{
    protected $check;
    protected $outage;

    use Dispatchable, InteractsWithSockets, SerializesModels, NotifiesChannels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(ApiStatusCheck $check, ApiOutage $outage)
    {
        $this->check = $check;
        $this->check = $outage;

        $this->notificationClass = CanvasApiDownNotification::class;
        $this->notificationClassArgs = [$check, $outage];

        $this->userIdsToNotify = User::all()->filter(function ($user) {
            return $user->isAbleTo('user-feed.*');
        })->pluck('id');
    }

    public static function canBeNotified(User $user = null)
    {
        if (is_null($user)) {
            $user = auth()->user();
        }

        return $user->isAbleTo('user-feed.*');
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
