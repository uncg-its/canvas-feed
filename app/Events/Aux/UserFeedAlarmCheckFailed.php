<?php

namespace App\Events\Aux;

use App\CcpsCore\User;
use Illuminate\Queue\SerializesModels;
use Uncgits\Ccps\Traits\NotifiesChannels;
use App\Notifications\Aux\AlarmCheckFailed;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Uncgits\Ccps\UserFeed\Events\UserFeedAlarmCheckFailed as BaseEvent;

class UserFeedAlarmCheckFailed extends BaseEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels, NotifiesChannels;

    public $alarmClass;
    public $data;
    public $cronjobClass;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($alarmClass, $data, $cronjobClass)
    {
        $this->notificationClass = AlarmCheckFailed::class;

        $message = 'Alarm check ' . $alarmClass . ' has failed during cronjob ' . $cronjobClass . '. Metrics: ' . $data->metrics;

        $this->notificationClassArgs = [
            $message
        ];
    }

    public static function canBeNotified(User $user = null)
    {
        if (is_null($user)) {
            $user = auth()->user();
        }

        return $user->isAbleTo('user-feed.*');
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
