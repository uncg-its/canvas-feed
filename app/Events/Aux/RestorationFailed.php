<?php

namespace App\Events\Aux;

use App\CcpsCore\User;
use App\Models\Aux\CanvasUser;
use Illuminate\Support\Collection;
use Illuminate\Queue\SerializesModels;
use Uncgits\Ccps\Traits\NotifiesChannels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use App\Notifications\Aux\RestorationFailed as RestorationFailedNotification;

class RestorationFailed
{
    use Dispatchable, InteractsWithSockets, SerializesModels, NotifiesChannels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(string $type, CanvasUser $user, Collection $failures)
    {
        $this->notificationClass = RestorationFailedNotification::class;

        $message = 'Restoration of ' . $type . ' failed for user ' . $user->primaryId . '. Failed item IDs: ' . $failures->pluck('id')->implode(', ');

        $this->notificationClassArgs = [
            $message
        ];
    }

    public static function canBeNotified(User $user = null)
    {
        if (is_null($user)) {
            $user = auth()->user();
        }
        return $user->isAbleTo('user-feed.*');
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
