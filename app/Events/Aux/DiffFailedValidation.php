<?php

namespace App\Events\Aux;

use App\CcpsCore\User;
use App\Models\Aux\UserDataDiff;
use Illuminate\Queue\SerializesModels;
use Uncgits\Ccps\Traits\NotifiesChannels;
use Illuminate\Broadcasting\PrivateChannel;
use App\Notifications\Aux\InvalidDiffDetected;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class DiffFailedValidation
{
    use Dispatchable, InteractsWithSockets, SerializesModels, NotifiesChannels;

    public $invalidEntries;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(UserDataDiff $diff, iterable $invalidEntries)
    {
        $this->notificationClass = InvalidDiffDetected::class;
        $this->notificationClassArgs = [
            'User Data Diff ' . $diff->id . ' failed validation! User Feed has been disabled. To manage problem records please visit ' . route('aux.invalid-changes.index')
        ];

        $this->invalidEntries = $invalidEntries;
    }

    public static function canBeNotified(User $user = null)
    {
        if (is_null($user)) {
            $user = auth()->user();
        }

        return $user->isAbleTo('user-feed.*');
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
