<?php

namespace App\Events\Aux;

use App\CcpsCore\User;
use Illuminate\Queue\SerializesModels;
use Uncgits\Ccps\Traits\NotifiesChannels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use App\Notifications\Aux\FetchNewBatchFailure;
use Illuminate\Broadcasting\InteractsWithSockets;

class FailedToFetchNewBatch
{
    use Dispatchable, InteractsWithSockets, SerializesModels, NotifiesChannels;

    protected $message;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($message)
    {
        $this->notificationClass = FetchNewBatchFailure::class;
        $this->notificationClassArgs = [
            $message
        ];
        $this->message = $message;
    }

    public static function canBeNotified(User $user = null)
    {
        if (is_null($user)) {
            $user = auth()->user();
        }

        return $user->isAbleTo('user-feed.*');
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
