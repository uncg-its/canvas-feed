<?php

namespace App\Events\Sis;

use App\Events\Sis\Base\SisZipStatusChangeEvent;

class SisZipMarkedAsIgnored extends SisZipStatusChangeEvent
{
    public $newStatus = 'ignored';
}
