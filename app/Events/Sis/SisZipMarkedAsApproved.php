<?php

namespace App\Events\Sis;

use App\Events\Sis\Base\SisZipStatusChangeEvent;

class SisZipMarkedAsApproved extends SisZipStatusChangeEvent
{
    public $newStatus = 'approved';
}
