<?php

namespace App\Events\Sis;

use App\Notifications\SisMoveExtractFailure;
use App\Events\Sis\Base\SisMoveExtractException;

class RequiredCsvFileMissingFromSisZip extends SisMoveExtractException
{
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($missingCsvsCollection)
    {
        $message = 'Required CSV file(s) ' . $missingCsvsCollection->implode(', ') . ' not found in SIS Zip. (FAILED)';
        \Log::channel('warning')->debug($message);

        $this->notificationClass = SisMoveExtractFailure::class;
        $this->notificationClassArgs = [
            'Required CSVs missing from SIS ZIP',
            $message
        ];
    }
}
