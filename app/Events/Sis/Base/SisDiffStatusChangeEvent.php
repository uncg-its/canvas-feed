<?php

namespace App\Events\Sis\Base;

use App\Models\Sis\SisDiff;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

abstract class SisDiffStatusChangeEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $sisDiff;
    public $user;
    public $newStatus;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(SisDiff $sisDiff, Authenticatable $user)
    {
        $this->sisDiff = $sisDiff;
        $this->user = $user;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
