<?php

namespace App\Events\Sis\Base;

use App\Models\Sis\SisZip;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

abstract class SisZipStatusChangeEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $sisZip;
    public $user;
    public $newStatus;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(SisZip $sisZip, Authenticatable $user)
    {
        $this->sisZip = $sisZip;
        $this->user = $user;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
