<?php

namespace App\Events\Sis\Base;

use App\CcpsCore\User;
use Illuminate\Queue\SerializesModels;
use Uncgits\Ccps\Traits\NotifiesChannels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class SisException
{
    use Dispatchable, InteractsWithSockets, SerializesModels, NotifiesChannels;

    public static function canBeNotified(User $user = null)
    {
        if (is_null($user)) {
            $user = auth()->user();
        }
        return $user->isAbleTo('canvasfeed.*');
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
