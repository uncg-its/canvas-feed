<?php

namespace App\Events\Sis;

use App\CcpsCore\User;
use App\Models\Sis\SisDiff;
use Illuminate\Queue\SerializesModels;
use Uncgits\Ccps\Traits\NotifiesChannels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use App\Notifications\Sis\SisDiffImportedAbnormally;
use Illuminate\Broadcasting\InteractsWithSockets;

class AbnormalSisDiffImport
{
    use Dispatchable, InteractsWithSockets, SerializesModels, NotifiesChannels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(SisDiff $diff)
    {
        $this->notificationClass = SisDiffImportedAbnormally::class;
        $this->notificationClassArgs = [
            'SIS Diff ' . $diff->id . ' was imported abnormally with a status of ' . $diff->status . '. The SIS Import cronjob will be disabled as a precaution.'
        ];
    }

    public static function canBeNotified(User $user = null)
    {
        if (is_null($user)) {
            $user = auth()->user();
        }
        return $user->isAbleTo('canvasfeed.*');
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
