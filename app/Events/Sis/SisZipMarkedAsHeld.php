<?php

namespace App\Events\Sis;

use App\Events\Sis\Base\SisZipStatusChangeEvent;

class SisZipMarkedAsHeld extends SisZipStatusChangeEvent
{
    public $newStatus = 'held';
}
