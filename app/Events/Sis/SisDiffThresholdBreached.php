<?php

namespace App\Events\Sis;

use App\CcpsCore\User;
use App\Models\Sis\SisDiff;
use Illuminate\Queue\SerializesModels;
use Uncgits\Ccps\Traits\NotifiesChannels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use App\Notifications\Sis\SisDiffThresholdBreached as SisDiffThresholdBreachedNotification;
use Illuminate\Support\Collection;

class SisDiffThresholdBreached
{
    use Dispatchable, InteractsWithSockets, SerializesModels, NotifiesChannels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(SisDiff $sisDiff, Collection $breachedCsvs)
    {
        $this->notificationClass = SisDiffThresholdBreachedNotification::class;
        $this->notificationClassArgs = [
            $sisDiff, $breachedCsvs
        ];
    }

    public static function canBeNotified(User $user = null)
    {
        if (is_null($user)) {
            $user = auth()->user();
        }

        return $user->isAbleTo('canvasfeed.edit');
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
