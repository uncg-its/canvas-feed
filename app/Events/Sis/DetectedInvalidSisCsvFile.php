<?php

namespace App\Events\Sis;

use App\Notifications\SisMoveExtractFailure;
use App\Events\Sis\Base\SisMoveExtractException;

class DetectedInvalidSisCsvFile extends SisMoveExtractException
{
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($pathToFile)
    {
        $message = 'Type of SIS File not found! Path: ' . $pathToFile . ' not found to be of any known format.';
        \Log::error($message);

        $this->notificationClass = SisMoveExtractFailure::class;
        $this->notificationClassArgs = [
            'Invalid SIS CSV file detected',
            $message
        ];
    }
}
