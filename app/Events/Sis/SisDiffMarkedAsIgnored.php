<?php

namespace App\Events\Sis;

use App\Events\Sis\Base\SisDiffStatusChangeEvent;

class SisDiffMarkedAsIgnored extends SisDiffStatusChangeEvent
{
    public $newStatus = 'ignored';
}
