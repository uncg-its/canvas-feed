<?php

namespace App\Events\Sis;

use App\Events\Sis\Base\SisDiffStatusChangeEvent;

class SisDiffMarkedAsHeld extends SisDiffStatusChangeEvent
{
    public $newStatus = 'held';
}
