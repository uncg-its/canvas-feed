<?php

namespace App\Events\Sis;

use App\CcpsCore\User;
use Illuminate\Queue\SerializesModels;
use Uncgits\Ccps\Traits\NotifiesChannels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use App\Notifications\Sis\SisUploadFailure;

class SisUploadFailed
{
    use Dispatchable, InteractsWithSockets, SerializesModels, NotifiesChannels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($result)
    {
        $this->notificationClass = SisUploadFailure::class;
        $this->notificationClassArgs = [
            'Error: SIS upload has failed. API Response: ' . $result->getLastResult()['code'] . ' - ' . $result->getLastResult()['reason'],
        ];
    }

    public static function canBeNotified(User $user = null)
    {
        if (is_null($user)) {
            $user = auth()->user();
        }
        return true;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
