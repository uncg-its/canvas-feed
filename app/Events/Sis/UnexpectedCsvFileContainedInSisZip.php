<?php

namespace App\Events\Sis;

use App\Notifications\SisMoveExtractFailure;
use App\Events\Sis\Base\SisMoveExtractException;

class UnexpectedCsvFileContainedInSisZip extends SisMoveExtractException
{
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(string $format)
    {
        $message = $format . ' CSV file found in SIS ZIP, but is not expected. (FAILED)';
        \Log::channel('sis-feed')->debug($message);

        $this->notificationClass = SisMoveExtractFailure::class;
        $this->notificationClassArgs = [
            'Unexpected CSV File contained in SIS ZIP',
            $message
        ];
    }
}
