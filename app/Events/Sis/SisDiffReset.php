<?php

namespace App\Events\Sis;

use App\Events\Sis\Base\SisDiffStatusChangeEvent;

class SisDiffReset extends SisDiffStatusChangeEvent
{
    public $newStatus = 'validated';
}
