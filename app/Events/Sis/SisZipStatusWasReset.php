<?php

namespace App\Events\Sis;

use App\Events\Sis\Base\SisZipStatusChangeEvent;

class SisZipStatusWasReset extends SisZipStatusChangeEvent
{
    public $newStatus = 'validated';
}
