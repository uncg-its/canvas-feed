<?php

namespace App\Events\Sis;

use App\CcpsCore\User;
use Illuminate\Queue\SerializesModels;
use App\Models\Sis\SisCsvFileValidation;
use Uncgits\Ccps\Traits\NotifiesChannels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use App\Notifications\Sis\SisCsvValidationFailed;
use Illuminate\Broadcasting\InteractsWithSockets;

class SisCsvValidationErrorsFound
{
    use Dispatchable, InteractsWithSockets, SerializesModels, NotifiesChannels;

    public $validation;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(SisCsvFileValidation $validation)
    {
        $this->validation = $validation;

        $this->notificationClass = SisCsvValidationFailed::class;
        $this->notificationClassArgs = [
            $validation,
        ];
    }

    public static function canBeNotified(User $user = null)
    {
        if (is_null($user)) {
            $user = auth()->user();
        }
        return $user->isAbleTo('canvasfeed.*');
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('sis-feed');
    }
}
