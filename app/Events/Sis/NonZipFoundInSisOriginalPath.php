<?php

namespace App\Events\Sis;

use App\Notifications\SisMoveExtractFailure;
use App\Events\Sis\Base\SisMoveExtractException;

class NonZipFoundInSisOriginalPath extends SisMoveExtractException
{
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($filename)
    {
        $message = 'Note: non-zip file ' . $filename . ' found in SIS Original path. Skipping move.';
        \Log::channel('sis-feed')->warning($message);

        $this->notificationClass = SisMoveExtractFailure::class;
        $this->notificationClassArgs = [
            'Non-ZIP found in SIS Original Path',
            $message
        ];
    }
}
