<?php

namespace App\Events\Sis;

use App\Notifications\SisMoveExtractFailure;
use App\Events\Sis\Base\SisMoveExtractException;

class SisZipExtractionFailed extends SisMoveExtractException
{
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($destinationName)
    {
        $message = 'SIS Zip Move and Extraction: ' . $destinationName . ' (FAILED)';
        \Log::channel('warning')->debug($message);

        $this->notificationClass = SisMoveExtractFailure::class;
        $this->notificationClassArgs = [
            'SIS Zip Extraction Failure',
            $message
        ];
    }
}
