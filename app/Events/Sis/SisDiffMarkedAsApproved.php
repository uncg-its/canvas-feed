<?php

namespace App\Events\Sis;

use App\Events\Sis\Base\SisDiffStatusChangeEvent;

class SisDiffMarkedAsApproved extends SisDiffStatusChangeEvent
{
    public $newStatus = 'approved';
}
