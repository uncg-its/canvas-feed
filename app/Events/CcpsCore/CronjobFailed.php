<?php

namespace App\Events\CcpsCore;

use App\CcpsCore\User;
use Uncgits\Ccps\Models\Cronjob;
use Uncgits\Ccps\Traits\NotifiesChannels;
use Uncgits\Ccps\Events\CronjobFailed as BaseEvent;
use App\Notifications\CronjobFailed as CronjobFailedNotification;

class CronjobFailed extends BaseEvent
{
    use NotifiesChannels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Cronjob $cronjob, \Exception $exception)
    {
        $this->notificationClass = CronjobFailedNotification::class;
        $this->notificationClassArgs = [$cronjob, $exception];

        parent::__construct($cronjob, $exception);
    }

    public static function canBeNotified(User $user = null)
    {
        if (is_null($user)) {
            $user = auth()->user();
        }

        return $user->hasRole('admin|canvas.admin');
    }
}
