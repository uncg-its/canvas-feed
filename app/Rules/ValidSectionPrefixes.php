<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValidSectionPrefixes implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $prefixes = collect(explode(',', $value));

        $invalid = $prefixes->filter(function ($prefix) {
            return preg_match('/^SECTION___[0-9]{6}$/', $prefix) === 0;
        });

        return $invalid->isEmpty();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Section prefixes should be formatted as "SECTION___######"';
    }
}
