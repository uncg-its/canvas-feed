<?php

namespace App\Rules;

use App\Models\Aux\Override;
use Illuminate\Contracts\Validation\Rule;

class UserHasNoActiveOverrides implements Rule
{
    protected $activeOverrides;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->activeOverrides = Override::active()->get();
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $this->activeOverrides->filter(function ($override) use ($value) {
            return $override->user_template->getUsername() == strtolower($value);
        })->isEmpty();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The provided user has one or more active overrides and cannot be given another.';
    }
}
