<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ExistsInFeedSources implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        try {
            $grouperUser = \Grouper::prefetchSubjectIds([$value])->findSubjects($value);
        } catch (\Exception $e) {
            \Log::channel('aux-feed')->notice('Override lookup failed for user ' . $value, [
                'category'  => 'error',
                'operation' => 'validation',
                'result'    => 'failure',
                'data'      => [
                    'user'          => $value,
                    'current_user'  => auth()->user(),
                    'message'       => $e->getMessage()
                ]
            ]);
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Invalid user - must exist in Grouper.';
    }
}
