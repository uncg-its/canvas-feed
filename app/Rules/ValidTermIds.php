<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValidTermIds implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $ids = collect(explode(',', $value));

        $invalid = $ids->filter(function ($id) {
            return preg_match('/^[0-9]{6}$/', $id) === 0;
        });

        return $invalid->isEmpty();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Invalid term_id supplied. Term IDs should be six digits, and multiple entries should be separated by comma.';
    }
}
