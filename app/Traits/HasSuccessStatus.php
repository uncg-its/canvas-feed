<?php


namespace App\Traits;

use App\Constants\SuccessStatus;

trait HasSuccessStatus
{
    public function getStatusAttribute()
    {
        switch ($this->status_id) {
            case SuccessStatus::PENDING:
                return 'pending';
            case SuccessStatus::IN_PROGRESS:
                return 'in-progress';
            case SuccessStatus::SUCCESS:
                return 'success';
            case SuccessStatus::FAILURE:
                return 'failure';
            case SuccessStatus::ERROR:
                return 'error';
            case SuccessStatus::IGNORED:
                return 'ignored';
            default:
                throw new \OutOfBoundsException('Invalid Status ID - could not parse status');
        }
    }

    public function getStatusIconAttribute()
    {
        switch ($this->status_id) {
            case SuccessStatus::PENDING:
                return 'fas fa-pause';
            case SuccessStatus::IN_PROGRESS:
                return 'fas fa-spinner';
            case SuccessStatus::SUCCESS:
                return 'fas fa-thumbs-up';
            case SuccessStatus::FAILURE:
                return 'fas fa-thumbs-down';
            case SuccessStatus::ERROR:
                return 'fas fa-times-circle';
            case SuccessStatus::IGNORED:
                return 'fas fa-ban';
            default:
                throw new \OutOfBoundsException('Invalid Status ID - could not parse status icon');
        }
    }

    public function getStatusClassAttribute()
    {
        switch ($this->status_id) {
            case SuccessStatus::PENDING:
            case SuccessStatus::IGNORED:
                return 'yellow-500';
            case SuccessStatus::IN_PROGRESS:
                return 'blue-500';
            case SuccessStatus::SUCCESS:
                return 'green-500';
            case SuccessStatus::FAILURE:
            case SuccessStatus::ERROR:
                return 'red-500';
            default:
                throw new \OutOfBoundsException('Invalid Status ID - could not parse status class');
        }
    }
}
