<?php

namespace App\Traits;

trait Holdable
{
    public function getHoldableAttribute()
    {
        return in_array($this->status, $this->holdableStatuses);
    }

    public function getApprovableAttribute()
    {
        return in_array($this->status, $this->approvableStatuses);
    }

    public function getIgnorableAttribute()
    {
        return in_array($this->status, $this->ignorableStatuses);
    }
}
