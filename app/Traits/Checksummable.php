<?php

namespace App\Traits;

trait Checksummable
{
    public static function bootChecksummable()
    {
        static::saving(function ($model) {
            $model->attributes['checksum'] = $model->generateChecksum();
        });
    }

    abstract protected function checksummable(): array;

    public function generateChecksum(array $fields = null)
    {
        $checksumFields = $fields ?? $this->checksummable();

        $checksumString = '';
        foreach ($checksumFields as $field) {
            $checksumString .= $this->$field;
        }
        return md5($checksumString);
    }
}
