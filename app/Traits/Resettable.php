<?php

namespace App\Traits;

trait Resettable
{
    public function getResettableAttribute()
    {
        return in_array($this->status, $this->resettableStatuses);
    }
}
