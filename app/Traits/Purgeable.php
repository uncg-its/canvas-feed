<?php

namespace App\Traits;

trait Purgeable
{
    public function getIsPurgedAttribute()
    {
        return !is_null($this->purged_at);
    }
}
