<?php

namespace App\Traits;

use App\CcpsCore\CronjobMeta;

trait ChangesCronStatus
{
    public function disableCrons(array $cronClassNames)
    {
        $disabled = $this->changeCronStatus($cronClassNames, 'disabled');

        \Log::channel('cron')->info('Disabled cron(s) programmatically', [
            'category'  => 'cron',
            'operation' => 'disable',
            'result'    => 'success',
            'data'      => [
                'affected_cron_jobs' => $disabled
            ]
        ]);

        return $disabled;
    }

    public function enableCrons(array $cronClassNames)
    {
        $enabled = $this->changeCronStatus($cronClassNames, 'enabled');

        \Log::channel('cron')->info('Enabled cron(s) programmatically', [
            'category'  => 'cron',
            'operation' => 'enable',
            'result'    => 'success',
            'data'      => [
                'affected_cron_jobs' => $enabled
            ]
        ]);

        return $enabled;
    }

    private function changeCronStatus(array $cronClassNames, string $status)
    {
        $cronsToChange = CronjobMeta::whereIn('class', $cronClassNames)->get();

        return $cronsToChange->filter(function ($cron) use ($status) {
            if ($cron->status == $status) {
                return false;
            }
            return $cron->update(['status' => $status]);
        });
    }
}
