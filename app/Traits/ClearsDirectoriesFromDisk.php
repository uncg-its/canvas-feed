<?php

namespace App\Traits;

use Symfony\Component\Finder\Finder;

trait ClearsDirectoriesFromDisk
{
    public function purgeDisk($disk, int $numberOfDaysToKeep = 0)
    {
        $latestTimestampToDelete = now()->subDays($numberOfDaysToKeep)->getTimestamp();

        $path = \Storage::disk($disk)->path('');


        $deleted = [
            'files'       => [],
            'directories' => [],
        ];

        $finder = new Finder;
        $finder->in($path)->files();

        foreach ($finder as $file) {
            if ($file->getMTime() <= $latestTimestampToDelete) {
                $filename = $file->getFilename();
                if (! unlink($file->getRealPath())) {
                    throw new \Exception('Could not delete file ' . $filename);
                }

                $deleted['files'][] = $filename;
            }
        }

        $finder = new Finder; // re-initialize
        $finder->in($path)->directories();

        foreach (iterator_to_array($finder, true) as $dir) {
            if ($dir->getMTime() <= $latestTimestampToDelete) {
                $dirname = $dir->getFilename();
                if (! rmdir($dir->getRealPath())) {
                    throw new \Exception('Could not delete directory ' . $dirname);
                }

                $deleted['directories'][] = $dirname;
            }
        }

        return $deleted;
    }
}
