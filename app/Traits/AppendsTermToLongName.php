<?php

namespace App\Traits;

trait AppendsTermToLongName
{
    public function getLongNameAttribute($value)
    {
        return $value . ' ' . $this->getTermSuffix();
    }

    // helpers

    protected function getTermSuffix()
    {
        $termId = $this->term_id;

        if (preg_match('/^[0-9]{6}$/', $termId) === 0) {
            throw new InvalidArgumentException('Term ID must be 6 numeric characters; \'' . $termId . '\' given.');
        }

        $termSemesterMap = config('canvas-feed.sis.term_semester_map');
        $semesterCode = substr($termId, 4, 2);
        if (!isset($termSemesterMap[$semesterCode])) {
            throw new InvalidArgumentException('Invalid Term ID semester: ' . $semesterCode);
        }

        $semester = $termSemesterMap[$semesterCode];
        $shortYear = substr($termId, 2, 2);

        return '(' . $semester . $shortYear . ')';
    }
}
