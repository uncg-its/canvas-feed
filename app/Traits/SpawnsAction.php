<?php

namespace App\Traits;

use Carbon\Carbon;
use App\Models\Aux\Action;

trait SpawnsAction
{
    public function spawnAction(string $type)
    {
        // prep the action
        $action = new Action;
        $action->actionable_id = $this->id;
        $action->canvas_user_id = $this->canvas_user_id;
        $action->actionable_type = self::class;
        $action->created_at = Carbon::now()->toDateTimeString();
        $action->type = $type;
        $action->save();
    }
}
