<?php

namespace App\Traits;

use Carbon\Carbon;
use App\Models\Aux\Call;

trait LogsApiCalls
{
    public function logCall()
    {
        $call = $this->callResult->getFirstCall();

        $createArray = [
            'action_id'          => $this->feedAction->id,
            'status_id'          => $this->callStatus,
            'endpoint'           => $call['request']['endpoint'],
            'method'             => $call['request']['method'],
            'request_parameters' => json_encode($call['request']['parameters'] ?? []),
            'response_code'      => $call['response']['code'],
            'message'            => $call['response']['reason'],
            'result'             => json_encode($call),
            'created_at'         => Carbon::now()->toDateTimeString(),
        ];

        $call = Call::create($createArray);
        \Log::channel('aux-feed')->debug('Logged call ' . $call->id . ' with status_id ' . $this->callStatus);
    }
}
