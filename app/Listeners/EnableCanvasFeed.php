<?php

namespace App\Listeners;

use App\Events\CanvasApiIsUp;
use App\CcpsCore\CronjobMeta;

class EnableCanvasFeed
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(CanvasApiIsUp $event)
    {
        try {
            \DB::beginTransaction();

            // Turn on crons
            CronjobMeta::withTag('core')->update(['status' => 'enabled']);

            \Log::channel('aux-feed')->notice('Canvas Aux Feed crons enabled successfully.', [
                'category'  => 'cron',
                'operation' => 'enable_core',
                'result'    => 'success',
                'data'      => [
                    'event'   => $event,
                ]
            ]);
            \DB::commit();
        } catch (\Exception $e) {
            \Log::channel('aux-feed')->critical('Canvas Aux Feed crons could not be enabled!', [
                'category'  => 'cron',
                'operation' => 'enable_core',
                'result'    => 'failure',
                'data'      => [
                    'event'   => $event,
                    'message' => $e->getMessage()
                ]
            ]);
            \DB::rollBack();
        }
    }
}
