<?php

namespace App\Listeners\Sis;

use App\Cronjobs\Sis\CheckSisDiffUploadStatus;
use App\Traits\ChangesCronStatus;
use App\Cronjobs\Sis\CreateSisDiff;
use App\Cronjobs\Sis\UploadSisDiff;

class SisDiffHandbrake
{
    use ChangesCronStatus;

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        // disable crons
        $disabled = $this->disableCrons([
            CreateSisDiff::class,
            UploadSisDiff::class,
            CheckSisDiffUploadStatus::class,
        ]);
    }
}
