<?php

namespace App\Listeners\Sis;

use App\Cronjobs\Sis\CheckSisDiffUploadStatus;
use App\Cronjobs\Sis\UploadSisDiff;
use App\Traits\ChangesCronStatus;

class SisUploadHandbrake
{
    use ChangesCronStatus;

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        // disable crons
        $disabled = $this->disableCrons([
            UploadSisDiff::class,
            CheckSisDiffUploadStatus::class
        ]);
    }
}
