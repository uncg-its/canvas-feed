<?php

namespace App\Listeners\Sis;

class LogSisDiffStatusChange
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        \Log::channel('sis-feed')->notice('SIS Diff marked as ' . $event->newStatus, [
            'category'  => 'model',
            'operation' => 'update',
            'result'    => 'success',
            'data'      => [
                'sis_diff' => $event->sisDiff,
                'user'     => $event->user,
            ]
        ]);
    }
}
