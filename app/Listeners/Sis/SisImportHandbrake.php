<?php

namespace App\Listeners\Sis;

use App\Cronjobs\Sis\ImportSisZipIntoDatabase;
use App\Cronjobs\Sis\MoveSisZipIntoStorage;
use App\Traits\ChangesCronStatus;

class SisImportHandbrake
{
    use ChangesCronStatus;

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        // disable crons
        $disabled = $this->disableCrons([
            MoveSisZipIntoStorage::class,
            ImportSisZipIntoDatabase::class,
        ]);
    }
}
