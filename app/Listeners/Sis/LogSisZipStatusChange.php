<?php

namespace App\Listeners\Sis;

class LogSisZipStatusChange
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        \Log::channel('sis-feed')->notice('SIS Zip marked as ' . $event->newStatus, [
            'category'  => 'model',
            'operation' => 'update',
            'result'    => 'success',
            'data'      => [
                'sis_zip' => $event->sisZip,
                'user'    => $event->user,
            ]
        ]);
    }
}
