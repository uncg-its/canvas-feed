<?php

namespace App\Listeners\CcpsCore;

use App\Events\Sis\SisUploadFailed;
use App\Events\Aux\RestorationFailed;
use App\Events\CcpsCore\CronjobFailed;
use App\Events\Sis\AbnormalSisDiffImport;
use App\Events\Sis\SisFeedFrequencyBreach;
use App\Events\Sis\SisZipExtractionFailed;
use App\Events\Sis\SisZipThresholdBreached;
use App\Events\Sis\SisDiffThresholdBreached;
use App\Events\Sis\DetectedInvalidSisCsvFile;
use App\Events\CcpsCore\StaleLockFileDetected;
use App\Events\Sis\SisCsvValidationErrorsFound;
use App\Events\Sis\NonZipFoundInSisOriginalPath;
use App\Events\Sis\RequiredCsvFileMissingFromSisZip;
use App\Events\Sis\UnexpectedCsvFileContainedInSisZip;
use Uncgits\Ccps\Listeners\NotificationSubscriber as BaseListener;

class NotificationSubscriber extends BaseListener
{
    public function subscribe($events)
    {
        // SIS
        $events->listen(
            NonZipFoundInSisOriginalPath::class,
            'App\Listeners\CcpsCore\NotificationSubscriber@sendNotifications'
        );
        $events->listen(
            SisZipExtractionFailed::class,
            'App\Listeners\CcpsCore\NotificationSubscriber@sendNotifications'
        );
        $events->listen(
            DetectedInvalidSisCsvFile::class,
            'App\Listeners\CcpsCore\NotificationSubscriber@sendNotifications'
        );
        $events->listen(
            UnexpectedCsvFileContainedInSisZip::class,
            'App\Listeners\CcpsCore\NotificationSubscriber@sendNotifications'
        );
        $events->listen(
            SisZipThresholdBreached::class,
            'App\Listeners\CcpsCore\NotificationSubscriber@sendNotifications'
        );
        $events->listen(
            SisCsvValidationErrorsFound::class,
            'App\Listeners\CcpsCore\NotificationSubscriber@sendNotifications'
        );
        $events->listen(
            SisDiffThresholdBreached::class,
            'App\Listeners\CcpsCore\NotificationSubscriber@sendNotifications'
        );
        $events->listen(
            AbnormalSisDiffImport::class,
            'App\Listeners\CcpsCore\NotificationSubscriber@sendNotifications'
        );
        $events->listen(
            SisUploadFailed::class,
            'App\Listeners\CcpsCore\NotificationSubscriber@sendNotifications'
        );
        $events->listen(
            RequiredCsvFileMissingFromSisZip::class,
            'App\Listeners\CcpsCore\NotificationSubscriber@sendNotifications'
        );
        $events->listen(
            SisFeedFrequencyBreach::class,
            'App\Listeners\CcpsCore\NotificationSubscriber@sendNotifications'
        );


        // AUX
        $events->listen(
            RestorationFailed::class,
            'App\Listeners\CcpsCore\NotificationSubscriber@sendNotifications'
        );

        // Other
        $events->listen(
            CronjobFailed::class,
            'App\Listeners\CcpsCore\NotificationSubscriber@sendNotifications'
        );
        $events->listen(
            StaleLockFileDetected::class,
            'App\Listeners\CcpsCore\NotificationSubscriber@sendNotifications'
        );


        parent::subscribe($events);
    }
}
