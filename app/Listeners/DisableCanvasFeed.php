<?php

namespace App\Listeners;

use App\CcpsCore\CronjobMeta;
use App\Events\CanvasApiIsDown;

class DisableCanvasFeed
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(CanvasApiIsDown $event)
    {
        try {
            \DB::beginTransaction();

            // Turn off core crons
            CronjobMeta::withTag('core')->update(['status' => 'disabled']);

            \Log::channel('aux-feed')->notice('Canvas Aux Feed crons disabled successfully.', [
                'category'  => 'cron',
                'operation' => 'disable_core',
                'result'    => 'success',
                'data'      => [
                    'event' => $event
                ]
            ]);
            \DB::commit();
        } catch (\Exception $e) {
            \Log::channel('aux-feed')->critical('Canvas Aux Feed crons could not be disabled!', [
                'category'  => 'cron',
                'operation' => 'disable_core',
                'result'    => 'failure',
                'data'      => [
                    'event'   => $event,
                    'message' => $e->getMessage()
                ]
            ]);
            \DB::rollBack();
        }


        // Find some way to stop processing the queue...? We won't be able to do
        // this easily. Maybe just let stuff that's already in the queue fail
        // and be re-run manually at this point...
    }
}
