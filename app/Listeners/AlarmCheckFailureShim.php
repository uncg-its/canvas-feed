<?php

namespace App\Listeners;

use App\Events\UserFeedAlarmCheckFailed;

class AlarmCheckFailureShim
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        // this is necessary because the event itself is not able to be converted to notifiable
        event(new UserFeedAlarmCheckFailed($event->alarmClass, $event->data, $event->cronjobClass));
    }
}
