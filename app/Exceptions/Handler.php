<?php

namespace App\Exceptions;

use Throwable;
use Illuminate\Cache\TaggableStore;
use Illuminate\Database\QueryException;
use Predis\Connection\ConnectionException;
use App\Events\CcpsCore\UncaughtQueryException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the exception types that will be throttled (not reported consecutively)
     */
    protected $throttled = [
        ConnectionException::class,
        \Exception::class
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Determine if the exception is in the "throttle" list.
     *
     * @param  \Throwable  $th
     * @return bool
     */
    protected function shouldThrottle(Throwable $th)
    {
        return ! is_null(\Arr::first($this->throttled, function ($type) use ($th) {
            return $th instanceof $type;
        }));
    }

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Throwable  $throwable
     * @return void
     */
    public function report(Throwable $throwable)
    {
        // log uncaught query exceptions but continue processing.
        if ($throwable instanceof QueryException) {
            event(new UncaughtQueryException($throwable));
        }

        // need a cache store that supports tags
        if (\Cache::getStore() instanceof TaggableStore === false) {
            parent::report($throwable);
            return;
        }

        if ($this->shouldReport($throwable)) {
            $tags = ['canvas-feed', 'throttled', \Str::slug(get_class($throwable))];
            $key = md5($throwable->getMessage()); // simple encryption for reducing length stored

            if ($this->shouldThrottle($throwable)) {
                if (\Cache::tags($tags)->has($key)) {
                    parent::report($throwable);
                    return;
                }
            }

            // log to exception reporting channel(s)
            if (config('ccps.exceptions.should_log')) {
                \Log::channel(config('ccps.exceptions.log_channel'))->error($throwable->getMessage(), [
                    'category'  => 'exception',
                    'operation' => 'exception',
                    'result'    => 'exception',
                    'data'      => [
                        'exception' => $throwable
                    ]
                ]);
            }

            if ($this->shouldThrottle($throwable)) {
                $throttleSeconds = config('canvas-feed.exception_throttle_seconds');
                \Cache::tags($tags)->put($key, now()->toDateTimeString(), now()->addSeconds($throttleSeconds));
            }
        }


        parent::report($throwable);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $throwable
     * @return \Illuminate\Http\Response
     */
    public function render($request, Throwable $throwable)
    {
        // defer to default for JSON responses
        if ($request->expectsJson()) {
            return parent::render($request, $throwable);
        }

        // redirect to login if 403
        if (config('ccps.redirect_to_login') && $throwable instanceof HttpException) {
            if ($throwable->getStatusCode() === 403) {
                if (auth()->user()) {
                    return response()->view('errors.403', ['exception' => $throwable], 403);
                }
                flash('You must be logged in to access this page.')->warning();
                return redirect()->route('login');
            }
        }

        // fallback to catch any edge cases we haven't accounted for.
        return parent::render($request, $throwable);
    }
}
