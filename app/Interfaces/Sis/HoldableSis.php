<?php

namespace App\Interfaces\Sis;

interface HoldableSis
{
    public function getHoldableAttribute();
    public function getApprovableAttribute();
    public function getIgnorableAttribute();
}
