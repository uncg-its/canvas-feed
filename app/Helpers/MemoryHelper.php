<?php

namespace App\Helpers;

class MemoryHelper
{
    private static function formatBytes($bytes, $precision = 2)
    {
        $units = ['B', 'KB', 'MB', 'GB', 'TB'];

        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);

        // Uncomment one of the following alternatives
        // $bytes /= pow(1024, $pow);
        $bytes /= (1 << (10 * $pow));

        return round($bytes, $precision) . ' ' . $units[$pow];
    }

    public static function getCurrentMemoryUsage()
    {
        return self::formatBytes(memory_get_usage());
    }

    public static function getPeakMemoryUsage()
    {
        return self::formatBytes(memory_get_peak_usage());
    }
}
