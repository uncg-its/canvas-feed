<?php

namespace App\Helpers;

use Uncgits\GrouperApi\Exceptions\SubjectNotFoundException;

class GrouperApiHelper
{
    public static function searchSubject($searchString, $searchField)
    {
        \Grouper::setParams([
            'searchString' => $searchString,
        ]);

        $result = \Grouper::withSubjectDetail()
            ->withSubjectAttributeNamesString()
            ->getSubjectsLite();

        $resultBody = $result['body']->WsGetSubjectsResults;

        $attributeNames = $resultBody->subjectAttributeNames;

        if (isset($resultBody->wsSubjects)) {
            foreach ($resultBody->wsSubjects as $subject) {
                $attributesArray = array_combine($attributeNames, $subject->attributeValues);
                if (isset($attributesArray[$searchField]) && strtolower($attributesArray[$searchField]) == strtolower($searchString)) {
                    unset($subject->attributeValues);
                    $subject->attributes = $attributesArray;
                    return $subject;
                }
            }
        }

        throw (new SubjectNotFoundException("Could not find match for subject '$searchString' on field '$searchField'."))
            ->withCallStack(\Grouper::getCallStack());
    }
}
