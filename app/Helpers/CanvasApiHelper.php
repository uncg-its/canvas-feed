<?php

namespace App\Helpers;

class CanvasApiHelper
{
    public static function makeMultipart(array $array)
    {
        return array_map(function ($value, $key) {
            return ['name' => $key, 'contents' => $value];
        }, $array, array_keys($array));
    }
}
