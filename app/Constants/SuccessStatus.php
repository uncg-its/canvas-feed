<?php

namespace App\Constants;

class SuccessStatus
{
    const PENDING = 1;
    const IN_PROGRESS = 2;
    const SUCCESS = 3;
    const FAILURE = 4;
    const ERROR = 5;
    const IGNORED = 6;
}
