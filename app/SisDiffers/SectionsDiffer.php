<?php

namespace App\SisDiffers;

use App\Models\Sis\SisZip;
use Illuminate\Support\Enumerable;
use App\SisDiffers\Interfaces\SisDifferInterface;

class SectionsDiffer extends BaseDiffer implements SisDifferInterface
{
    public function prepare(SisZip $sisZip) : Enumerable
    {
        // get records based on active section prefixes
        return $sisZip->sis_sections()->where(function ($q) {
            // necessary because sis_sections() itself is a WHERE clause
            foreach (explode(',', app('dbConfig')->get('active_section_prefixes')) as $key => $prefix) {
                if ($key === 0) {
                    $q->where('section_id', 'LIKE', '%' . $prefix . '%');
                } else {
                    $q->orWhere('section_id', 'LIKE', '%' . $prefix . '%');
                }
            }
        })->get()->keyBy($this->keyByField);
    }
}
