<?php

namespace App\SisDiffers;

use App\Models\Sis\SisZip;
use Illuminate\Support\Enumerable;

class BaseDiffer
{
    public $thresholds;
    protected $keyByField = 'checksum';

    protected $oldZip;
    protected $newZip;

    public function __construct(SisZip $oldZip, SisZip $newZip)
    {
        // get max allowable % and # change
        $thresholds = new \stdClass;
        $thresholds->maxPctDifference = app('dbConfig')->get('sis_zip_max_change');
        $thresholds->maxChanges = app('dbConfig')->get('diff_threshold_max_value');

        $this->thresholds = $thresholds;
        $this->oldZip = $oldZip;
        $this->newZip = $newZip;
    }

    public function setKeyByField($field)
    {
        $this->keyByField = $field;
    }

    public function diff() : Enumerable
    {
        // prepare old and new recordsets
        $oldData = $this->prepare($this->oldZip);
        $newData = $this->prepare($this->newZip);

        // compare old and new records. return a structured Collection keyed by a new checksum (without status)
        $active = $newData->diffKeys($oldData)->keyBy('secondary_checksum');
        $deleted = $oldData->diffKeys($newData)->keyBy('secondary_checksum');

        // active/inactive takes precedence over deleted, so we'll diff those two collections based on new checksums
        $results = collect([
            'active'   => $active,
            'deleted'  => $deleted->diffKeys($active),
            'inactive' => collect() // placeholder for modification in process() method
        ]);

        // run the result through any additional processing that needs to happen
        return $this->process($results);
    }

    public function process(Enumerable $results): Enumerable
    {
        // override in child classes as needed
        return $results;
    }
}
