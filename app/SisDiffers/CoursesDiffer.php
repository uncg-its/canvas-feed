<?php

namespace App\SisDiffers;

use App\CcpsCore\DbConfig;
use App\Models\Sis\SisZip;
use Illuminate\Support\Enumerable;
use App\SisDiffers\Interfaces\SisDifferInterface;

class CoursesDiffer extends BaseDiffer implements SisDifferInterface
{
    public function prepare(SisZip $sisZip) : Enumerable
    {
        // get records based on active course term codes
        return $sisZip->sis_courses()
            ->whereIn('term_id', explode(',', DbConfig::getConfig('active_term_ids')))
            ->get()
            ->keyBy($this->keyByField);
    }
}
