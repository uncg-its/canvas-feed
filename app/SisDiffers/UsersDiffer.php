<?php

namespace App\SisDiffers;

use App\Models\Sis\SisZip;
use Illuminate\Support\Enumerable;
use App\SisDiffers\Interfaces\SisDifferInterface;

class UsersDiffer extends BaseDiffer implements SisDifferInterface
{
    public function prepare(SisZip $sisZip) : Enumerable
    {
        // get records without additional qualifiers
        return $sisZip->sis_users()->get()->keyBy($this->keyByField);
    }
}
