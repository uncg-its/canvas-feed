<?php

namespace App\SisDiffers\Interfaces;

use App\Models\Sis\SisZip;
use Illuminate\Support\Enumerable;

interface SisDifferInterface
{
    // prepare a dataset
    public function prepare(SisZip $data) : Enumerable;

    // perform the diff
    public function diff() : Enumerable;

    // post-process results as needed
    public function process(Enumerable $results) : Enumerable;
}
