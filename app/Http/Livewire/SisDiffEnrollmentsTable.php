<?php

namespace App\Http\Livewire;

use App\Repositories\Sis\SisDiffEnrollmentRepository;
use Illuminate\Http\Request;

class SisDiffEnrollmentsTable extends PaginatedTable
{
    public $legacy = false;

    protected $view = 'livewire.sis-diff-enrollments-table';
    protected $viewPropertyName = 'enrollments';
    protected $repositoryClass = SisDiffEnrollmentRepository::class;

    public function mount(Request $request)
    {
        if ($request->has('user_id_fuzzy')) {
            $this->searchTerms['user_id_fuzzy'] = $request->get('user_id_fuzzy');
        }
    }

    public function setUserIdExact($value)
    {
        $this->reset('searchTerms');
        $this->searchTerms['user_id_exact'] = $value;
    }

    public function setSectionIdExact($value)
    {
        $this->reset('searchTerms');
        $this->searchTerms['section_id_exact'] = $value;
    }
}
