<?php

namespace App\Http\Livewire;

use App\Repositories\Sis\SisDiffUserRepository;

class SisDiffUsersTable extends PaginatedTable
{
    public $legacy = false;

    protected $view = 'livewire.sis-diff-users-table';
    protected $viewPropertyName = 'users';
    protected $repositoryClass = SisDiffUserRepository::class;

    public function setLoginId($value)
    {
        $this->searchTerms['login_id_fuzzy'] = $value;
    }
}
