<?php

namespace App\Http\Livewire;

use App\Repositories\Sis\SisZipRepository;

class SisZipsTable extends PaginatedTable
{
    protected $view = 'livewire.sis-zips-table';
    protected $viewPropertyName = 'zips';
    protected $repositoryClass = SisZipRepository::class;
}
