<?php

namespace App\Http\Livewire;

use Illuminate\Http\Request;
use App\Repositories\Sis\LegacySisDiffEnrollmentRepository;

class LegacySisDiffEnrollmentsTable extends PaginatedTable
{
    public $legacy = true;

    protected $view = 'livewire.sis-diff-enrollments-table';
    protected $viewPropertyName = 'enrollments';
    protected $repositoryClass = LegacySisDiffEnrollmentRepository::class;

    public function mount(Request $request)
    {
        if ($request->has('user_id_fuzzy')) {
            $this->searchTerms['user_id_fuzzy'] = $request->get('user_id_fuzzy');
        }
    }

    public function setUserIdExact($value)
    {
        $this->reset('searchTerms');
        $this->searchTerms['user_id_exact'] = $value;
    }

    public function setSectionIdExact($value)
    {
        $this->reset('searchTerms');
        $this->searchTerms['section_id_exact'] = $value;
    }
}
