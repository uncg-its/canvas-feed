<?php

namespace App\Http\Livewire;

use App\Repositories\Sis\SisDiffRepository;

class SisDiffsTable extends PaginatedTable
{
    protected $view = 'livewire.sis-diffs-table';
    protected $viewPropertyName = 'diffs';
    protected $repositoryClass = SisDiffRepository::class;
}
