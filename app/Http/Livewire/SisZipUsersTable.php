<?php

namespace App\Http\Livewire;

use App\Repositories\Sis\SisUserRepository;

class SisZipUsersTable extends PaginatedTable
{
    public $legacy = false;

    protected $view = 'livewire.sis-zip-users-table';
    protected $viewPropertyName = 'users';
    protected $repositoryClass = SisUserRepository::class;

    public function setLoginId($value)
    {
        $this->searchTerms['login_id_fuzzy'] = $value;
    }
}
