<?php

namespace App\Http\Livewire;

use App\Repositories\Sis\LegacySisDiffSectionRepository;

class LegacySisDiffSectionsTable extends PaginatedTable
{
    public $legacy = true;

    protected $view = 'livewire.sis-diff-sections-table';
    protected $viewPropertyName = 'sections';
    protected $repositoryClass = LegacySisDiffSectionRepository::class;

    public function setName($value)
    {
        $this->searchTerms['name'] = $value;
    }
}
