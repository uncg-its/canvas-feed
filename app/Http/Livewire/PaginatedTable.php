<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;

abstract class PaginatedTable extends Component
{
    use WithPagination;

    protected $results;
    public $searchTerms = []; // bind to this

    // override these in child
    protected $view = '';
    protected $viewPropertyName = 'results';
    protected $repositoryClass;

    public function render()
    {
        $this->loadResults();
        return view($this->view)
            ->with([$this->viewPropertyName => $this->results]);
    }

    public function search()
    {
        $this->render();
    }

    public function resetSearch()
    {
        $this->reset('searchTerms', 'page');
    }

    public function loadResults()
    {
        $repo = new $this->repositoryClass;

        if (!empty($this->searchTerms)) {
            $this->results = $repo
                ->sorted('id', 'desc')
                ->paginated()
                ->search($this->searchTerms);
        } else {
            $this->results = $repo
                ->sorted('id', 'desc')
                ->paginated()
                ->all();
        }
    }
}
