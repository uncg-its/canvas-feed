<?php

namespace App\Http\Livewire;

use App\Repositories\Sis\LegacySisDiffUserRepository;

class LegacySisDiffUsersTable extends PaginatedTable
{
    public $legacy = true;

    protected $view = 'livewire.sis-diff-users-table';
    protected $viewPropertyName = 'users';
    protected $repositoryClass = LegacySisDiffUserRepository::class;

    public function setLoginId($value)
    {
        $this->searchTerms['login_id_fuzzy'] = $value;
    }
}
