<?php

namespace App\Http\Livewire;

use App\Repositories\Sis\SisCourseRepository;

class SisZipCoursesTable extends PaginatedTable
{
    public $legacy = false;

    protected $view = 'livewire.sis-zip-courses-table';
    protected $viewPropertyName = 'courses';
    protected $repositoryClass = SisCourseRepository::class;

    public function setAccount($value)
    {
        $this->searchTerms['account_id'] = $value;
    }
}
