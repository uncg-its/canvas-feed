<?php

namespace App\Http\Livewire;

use App\Repositories\Sis\LegacySisDiffCourseRepository;

class LegacySisDiffCoursesTable extends PaginatedTable
{
    public $legacy = true;

    protected $view = 'livewire.sis-diff-courses-table';
    protected $viewPropertyName = 'courses';
    protected $repositoryClass = LegacySisDiffCourseRepository::class;

    public function setAccount($value)
    {
        $this->searchTerms['account_id'] = $value;
    }
}
