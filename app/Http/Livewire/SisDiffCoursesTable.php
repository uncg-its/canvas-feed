<?php

namespace App\Http\Livewire;

use App\Repositories\Sis\SisDiffCourseRepository;

class SisDiffCoursesTable extends PaginatedTable
{
    public $legacy = false;

    protected $view = 'livewire.sis-diff-courses-table';
    protected $viewPropertyName = 'courses';
    protected $repositoryClass = SisDiffCourseRepository::class;

    public function setAccount($value)
    {
        $this->searchTerms['account_id'] = $value;
    }
}
