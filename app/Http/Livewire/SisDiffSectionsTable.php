<?php

namespace App\Http\Livewire;

use App\Repositories\Sis\SisDiffSectionRepository;

class SisDiffSectionsTable extends PaginatedTable
{
    public $legacy = false;

    protected $view = 'livewire.sis-diff-sections-table';
    protected $viewPropertyName = 'sections';
    protected $repositoryClass = SisDiffSectionRepository::class;

    public function setName($value)
    {
        $this->searchTerms['name'] = $value;
    }
}
