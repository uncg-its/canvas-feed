<?php

namespace App\Http\Livewire;

use App\Models\Sis\SisDiff;
use Livewire\Component;

class DiffUploadDetails extends Component
{
    public SisDiff $diff;
    public $uploadDetails = '';

    public function render()
    {
        return view('livewire.diff-upload-details');
    }

    public function fetchUploadDetails()
    {
        if ($this->uploadDetails !== '') {
            // we got them already
            return;
        }

        try {
            $rootAccountId = config('canvas-feed.canvas.root_account_id');
            $result = \CanvasApi::using('SisImports')
                ->getSisImportStatus($rootAccountId, $this->diff->id);
            if ($result->getStatus() !== 'success') {
                throw new \Exception('API error');
            }

            $data = $result->getContent();
            $this->uploadDetails = json_encode($data, JSON_PRETTY_PRINT);
        } catch (\Exception $e) {
            flash('Error retrieving SIS Diff Upload status')->error()->livewire($this);
            $this->dispatchBrowserEvent('close-upload-modal');
        }
    }
}
