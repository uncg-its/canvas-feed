<?php

namespace App\Http\Livewire;

use App\Repositories\Sis\SisSectionRepository;

class SisZipSectionsTable extends PaginatedTable
{
    public $legacy = false;

    protected $view = 'livewire.sis-zip-sections-table';
    protected $viewPropertyName = 'sections';
    protected $repositoryClass = SisSectionRepository::class;

    public function setName($value)
    {
        $this->searchTerms['name'] = $value;
    }
}
