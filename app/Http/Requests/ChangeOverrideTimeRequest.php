<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use App\Models\Aux\Override;
use Illuminate\Foundation\Http\FormRequest;

class ChangeOverrideTimeRequest extends FormRequest
{
    protected $earliestDate;

    public function __construct()
    {
        $expirationWindow = config('auf.overrides.min_length_hours');
        $this->earliestDate = Carbon::now()->addHours($expirationWindow);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->isAbleTo('user-feed.*');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'expires_at'    => 'required|date|after:' . $this->earliestDate->format('m/d/Y H:i'),
        ];
    }

    public function messages()
    {
        return [
            'expires_at.after' => 'The expiration date / time must be after ' . $this->earliestDate->format('m/d/Y H:i'),
        ];
    }

    public function persist(Override $override)
    {
        try {
            \DB::beginTransaction();

            $override->expires_at = Carbon::parse($this->expires_at)->toDateTimeString();
            $override->save();

            // reset notifications
            $override->notifications->where('reset_at', null)->each(function ($notification) {
                $notification->reset_at = Carbon::now()->toDateTimeString();
                $notification->save();
            });

            \Log::channel('adobe-feed')->info('Override time changed', [
                'category'  => 'model',
                'operation' => 'update',
                'result'    => 'success',
                'data'      => [
                    'current_user' => auth()->user(),
                    'override'     => $this->override,
                ]
            ]);

            flash('Override time changed successfully.', 'success');
            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollBack();
            \Log::channel('adobe-feed')->error('Error changing time for Override', [
                'category'  => 'model',
                'operation' => 'update',
                'result'    => 'error',
                'data'      => [
                    'current_user' => auth()->user(),
                    'override'     => $this->override,
                    'message'      => $e->getMessage()
                ]
            ]);
            flash('Error updating Override time. Please contact an administrator.', 'danger');
        }
    }
}
