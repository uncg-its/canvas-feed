<?php

namespace App\Http\Requests\CcpsCore;

use App\CcpsCore\DbConfig;
use App\Rules\ValidTermIds;
use App\Rules\ValidSectionPrefixes;
use Uncgits\Ccps\Requests\ConfigUpdateRequest as BaseRequest;

class ConfigUpdateRequest extends BaseRequest
{
    public function authorize()
    {
        return \Auth::user()->isAbleTo('config.edit');
    }

    public function rules()
    {
        return [
            'notification_intervals' => [
                'required',
                'regex:/^[0-9]+(\s*\,\s*[0-9]+)*$/'
            ],
            'admin_override_notification_bcc' => [
                'required',
                'email'
            ],
            'api_check_threshold' => [
                'required',
                'numeric',
                'min:1',
            ],
            'expected_source_min_count' => [
                'required',
                'numeric',
                'min:1'
            ],
            'expected_change_max_count' => [
                'required',
                'numeric',
                'min:1'
            ],
            'max_hours_since_last_feed_run' => [
                'required',
                'numeric',
                'min:1'
            ],
            'max_time_between_sis_zips'             => 'required|numeric',
            'max_time_between_sis_diffs'            => 'required|numeric',
            'max_time_between_sis_diff_uploads'     => 'required|numeric',
            'time_between_diff_creation_and_upload' => 'required|numeric',
            'sis_zip_max_change'                    => 'required|numeric',
            'sis_retention_zip'                     => 'required',
            'sis_retention_db'                      => 'required',
            'diff_retention_zip'                    => 'required',
            'diff_retention_db'                     => 'required',
            'diff_threshold_max_value'              => 'required',
            'active_term_ids'                       => ['required', new ValidTermIds],
            'active_section_prefixes'               => ['required', new ValidSectionPrefixes],
            'unpublished_courses_report_terms'      => ['required', new ValidTermIds],
            'aux_restore_data'                      => 'required|boolean',
            'aux_restoration_cleanup_days'          => 'required|numeric|min:1',
        ];
    }

    public function persist()
    {
        $jsonEncode = [
            'required_zip_csv_files'
        ];

        foreach ($this->except(['_token', '_method']) as $key => $value) {
            if (in_array($key, $jsonEncode)) {
                $value = json_encode($value);
            }
            DbConfig::updateOrCreate(['key' => $key], ['value' => $value]);
        }

        flash('Configuration updated successfully.', 'success');
        return redirect()->back();
    }
}
