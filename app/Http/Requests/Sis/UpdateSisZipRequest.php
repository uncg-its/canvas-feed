<?php

namespace App\Http\Requests\Sis;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSisZipRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->isAbleTo('canvasfeed.edit');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'operation'    => 'required|in:hold,approve,ignore,reset',
            'restartCrons' => 'sometimes|boolean',
        ];
    }
}
