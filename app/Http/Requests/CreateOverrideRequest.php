<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use App\Models\Aux\Override;
use App\Rules\ExistsInFeedSources;
use App\Models\Aux\CanvasUserTemplate;
use App\Rules\UserHasNoActiveOverrides;
use Illuminate\Foundation\Http\FormRequest;
use Uncgits\GrouperApi\Exceptions\ApiResultException;

class CreateOverrideRequest extends FormRequest
{
    protected $earliestDate;

    public function __construct()
    {
        $expirationWindow = config('canvas-feed.aux.overrides.min_length_hours');
        $this->earliestDate = Carbon::now()->addHours($expirationWindow);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->isAbleTo('user-feed.*');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'uncgUsername' => [
                'required',
                new ExistsInFeedSources,
                new UserHasNoActiveOverrides,
            ],
            'contact_email' => 'nullable|email',
            'type'          => 'required|in:add,delete',
            'reason'        => 'required',
            'expires_at'    => 'required|date|after:' . $this->earliestDate->format('m/d/Y H:i'),
        ];
    }

    public function messages()
    {
        return [
            'expires_at.after' => 'The expiration date / time must be after ' . $this->earliestDate->format('m/d/Y H:i'),
        ];
    }

    public function persist()
    {
        try {
            \DB::beginTransaction();

            // fetch the user from Grouper
            // don't need prefetch because we've already done it in the rule class
            $grouperUser = \Grouper::findSubjects($this->uncgUsername);

            $canvasUserTemplate = new CanvasUserTemplate($grouperUser[$this->uncgUsername]);

            // create override - observers will create the Action and spawn the job
            $createArray = [
                'submitter_id'    => auth()->user()->id,
                'canvas_user_id'  => $canvasUserTemplate->primaryId,
                'user_template'   => $canvasUserTemplate,
                'type'            => $this->type,
                'contact_email'   => $this->contact_email ?? null,
                'reason'          => $this->reason,
                'expires_at'      => Carbon::parse($this->expires_at)->toDateTimeString(),
            ];

            $override = Override::create($createArray);


            // take action in Grouper
            \Grouper::reset();
            $targetGroupConfigKey = ($this->type == 'add') ? 'canvas-feed.aux.grouper.allow_group' : 'canvas-feed.aux.grouper.deny_group';
            $targetGroup = explode(':', config($targetGroupConfigKey));
            $targetGroupGroup = array_pop($targetGroup);
            $targetGroupStem = implode(':', $targetGroup);
            $result = \Grouper::addMembers([$this->uncgUsername], $targetGroupStem, $targetGroupGroup);

            if ($result['response']['result'] === 'FAILURE') {
                throw new ApiResultException('Grouper API failure: ' . $result['response']['httpCode'] . ' - ' . $result['response']['resultCode'] . '; ' . $result['body']->WsAddMemberResults->results[0]->resultMetadata->resultCode);
            }

            flash('Override queued successfully.', 'success');
            \DB::commit();
            \Log::channel('aux-feed')->notice('Override creation successful', [
                'category'  => 'model',
                'operation' => 'create',
                'result'    => 'success',
                'data'      => [
                    'model'        => 'override',
                    'current_user' => auth()->user(),
                    'email'        => $this->uncgUsername,
                ]
            ]);
        } catch (ApiResultException | \OutOfBoundsException $e) {
            \DB::rollBack();
            \Log::channel('aux-feed')->notice('Override creation failed', [
                'category'  => 'model',
                'operation' => 'create',
                'result'    => 'failure',
                'data'      => [
                    'model'   => 'override',
                    'email'   => $this->uncgUsername,
                    'message' => $e->getMessage()
                ]
            ]);
            flash('There was an error creating the override. Please contact an administrator.', 'danger');
        } catch (\Exception $e) {
            \DB::rollBack();
            \Log::channel('aux-feed')->notice('Error creating override', [
                'category'  => 'model',
                'operation' => 'create',
                'result'    => 'error',
                'data'      => [
                    'model'       => 'override',
                    'create_data' => $createArray ?? null,
                    'email'       => $this->uncgUsername,
                    'message'     => $e->getMessage()
                ]
            ]);
            flash('There was an error creating the override. Please contact an administrator.', 'danger');
        }
    }
}
