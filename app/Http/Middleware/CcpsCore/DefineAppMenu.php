<?php

namespace App\Http\Middleware\CcpsCore;

use Closure;
use Illuminate\Support\Facades\Auth;
use Lavary\Menu\Facade as Menu;

class DefineAppMenu
{
    private function generateModuleLinks($menu)
    {
        if ($user = Auth::user()) {
            // read modules from config and auto-generate menu
            $adminPrivs = [];
            $modules = config('ccps.modules');
            foreach ($modules as $key => $module) {
                if ($module['parent'] == 'admin' && $user->isAbleTo($module['required_permissions'])) {
                    $adminPrivs[] = $module;
                }
            }

            // if ($user->isAbleTo('user-feed.*')) {
            //     $userFeedModule = $modules['user-feed'];
            //     $menu->add($userFeedModule['title'], route($userFeedModule['index']))->data('order', $userFeedModule['order']);
            // }

            if (count($adminPrivs) > 0) {
                // Define Admin menu
                $menu->add('Admin', route('admin'))->data('order', 99);

                foreach ($adminPrivs as $priv => $module) {
                    if ($module['parent'] == 'admin') {
                        $menu->admin->add($module['title'], route($module['index']));
                    } else {
                        if (empty($module['parent'])) {
                            $menu->add($module['title'], route($module['index']))->data('order', $module['order']);
                        }
                    }
                }
            }
        }
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Don't create the nav if's already been created
        if (! Menu::exists('nav')) {
            // Define App Menu - add your own links here and change/reorder as you see fit.
            Menu::make('nav', function ($menu) {
                // base for unauthenticated users
                $menu->add('Home')->data('order', 1);

                // for authenticated users, generate links and Admin menu from modules
                $this->generateModuleLinks($menu);

                if ($user = Auth::user()) {
                    if ($user->isAbleTo('canvasfeed.*')) {
                        $sisFeedMenu = $menu->add('SIS Feed', route('aux.index'))->data('order', 2);
                        $sisFeedMenu->add('Home', route('sis.index'))->data('order', 3);
                        $sisFeedMenu->add('SIS Zips', route('sis.zips.index'))->data('order', 4);
                        $sisFeedMenu->add('SIS Diffs', route('sis.diffs.index'))->data('order', 5);
                        $sisFeedMenu->add('SIS Config', route('config'))->data('order', 6);
                        $sisFeedMenu->add('Cron Jobs', route('cronjobs'))->data('order', 7);
                    }

                    if ($user->isAbleTo('user-feed.*')) {
                        $auxFeedMenu = $menu->add('Aux Feed', route('aux.index'))->data('order', 7);
                        $auxFeedMenu->add('Dashboard', route('aux.index'))->data('order', 8);
                        $auxFeedMenu->add('Runs', route('aux.runs.index'))->data('order', 9);
                        $auxFeedMenu->add('Canvas Users', route('aux.users.index'))->data('order', 10);
                        $auxFeedMenu->add('User Overrides', route('aux.overrides.index'))->data('order', 11);
                        $auxFeedMenu->add('Invalid Changes', route('aux.invalid-changes.index'))->data('order', 12);
                        $auxFeedMenu->add('User Imports', route('aux.user-imports.index'))->data('order', 13);
                        $auxFeedMenu->add('Feed Config', route('config'))->data('order', 14);
                        $auxFeedMenu->add('Cron Jobs', route('cronjobs'))->data('order', 15);
                        $auxFeedMenu->add('CCPS User Feed', route(config('ccps.modules')['user-feed']['index']))->data('order', 16);
                    }
                }
            })->sortBy('order');
        }

        return $next($request);
    }
}
