<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SisUploadController extends Controller
{
    public function show(Request $request)
    {
        $id = $request->get('canvas_id', '');
        if ($id === '') {
            return respose()->json([
                'errors' => 'You must specify a canvas_id.'
            ], 400);
        }

        try {
            $rootAccountId = config('canvas-feed.canvas.root_account_id');

            $result = \CanvasApi::using('SisImports')
                ->getSisImportStatus($rootAccountId, $id);

            if ($result->getStatus() !== 'success') {
                return response()->json([
                    'errors' => 'Canvas API responded with ' . $result->getLastResult()['code'] . ': ' . $result->getLastResult()['reason']
                ], 424);
            }

            $data = $result->getContent();

            if ($request->get('pretty_print', false)) {
                $data = json_encode($data, JSON_PRETTY_PRINT);
            }

            return response()->json([
                'data' => $data
            ], 200);
        } catch (\Exception $e) {
            return respose()->json([
                'errors' => $e->getMessage()
            ], 500);
        }
    }
}
