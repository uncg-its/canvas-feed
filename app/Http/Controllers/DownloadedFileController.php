<?php

namespace App\Http\Controllers;

use App\DownloadedFile;
use App\Http\Requests\DownloadedFileCreateRequest;
use App\Repositories\DownloadedFileRepository;
use App\Services\DownloadedFileService;
use Illuminate\Http\Request;

class DownloadedFileController extends Controller
{
    protected $repository;
    protected $service;

    public function __construct()
    {
        $this->repository = new DownloadedFileRepository;
        $this->service = new DownloadedFileService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DownloadedFileCreateRequest $request)
    {
        return $this->service->downloadFile($request->disk, $request->filename);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DownloadedFile  $downloadedFile
     * @return \Illuminate\Http\Response
     */
    public function show(DownloadedFile $downloadedFile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DownloadedFile  $downloadedFile
     * @return \Illuminate\Http\Response
     */
    public function edit(DownloadedFile $downloadedFile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DownloadedFile  $downloadedFile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DownloadedFile $downloadedFile)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DownloadedFile  $downloadedFile
     * @return \Illuminate\Http\Response
     */
    public function destroy(DownloadedFile $downloadedFile)
    {
        //
    }
}
