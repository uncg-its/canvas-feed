<?php

namespace App\Http\Controllers\CcpsCore;

use Laravel\Horizon\Horizon;
use App\Models\Aux\UserDataDiff;
use Uncgits\Ccps\UserFeed\Batch;
use App\Repositories\Sis\SisZipRepository;
use App\Repositories\Sis\SisDiffRepository;
use Uncgits\Ccps\Controllers\HomeController as BaseController;

class HomeController extends BaseController
{
    protected $sisZipRepository;
    protected $sisDiffRepository;

    public function __construct(SisZipRepository $sisZipRepository, SisDiffRepository $sisDiffRepository)
    {
        $this->sisZipRepository = $sisZipRepository;
        $this->sisDiffRepository = $sisDiffRepository;
        parent::__construct();
    }

    public function index()
    {
        $modules = config('ccps.modules');

        $heldBatches = Batch::select('id')
            ->where('status', 'held')
            ->get();

        $heldDiffs = UserDataDiff::select('id')
            ->where('status', 'held')
            ->get();

        $heldDataExists = ($heldBatches->count() > 0) || ($heldDiffs->count() > 0);

        $sisHeldZipExists = $this->sisZipRepository->getHeld()->isNotEmpty();
        $sisHeldDiffExists = $this->sisDiffRepository->getHeld()->isNotEmpty();

        return view('index')->with([
            'modules'           => $modules,
            'heldDataExists'    => $heldDataExists,
            'sisHeldZipExists'  => $sisHeldZipExists,
            'sisHeldDiffExists' => $sisHeldDiffExists,
        ]);
    }

    public function horizon()
    {
        $horizonScriptVariables = Horizon::scriptVariables();
        $horizonScriptVariables['path'] = config('horizon.subfolder') . $horizonScriptVariables['path'];

        return view('horizon::layout', [
            'cssFile'                => Horizon::$useDarkTheme ? 'app-dark.css' : 'app.css',
            'horizonScriptVariables' => $horizonScriptVariables,
            'assetsAreCurrent'       => Horizon::assetsAreCurrent(),
            'isDownForMaintenance'   => \App::isDownForMaintenance(),
        ]);
    }
}
