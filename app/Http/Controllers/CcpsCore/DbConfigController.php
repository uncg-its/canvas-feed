<?php

namespace App\Http\Controllers\CcpsCore;

use Uncgits\Ccps\Controllers\DbConfigController as BaseController;

class DbConfigController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        // sis zip timing
        $timeOptions = [
            ''       => ' --- Select ---',
            '60'     => '1 Minute',
            '180'    => '3 Minutes',
            '300'    => '5 Minutes',
            '600'    => '10 Minutes',
            '900'    => '15 Minutes',
            '1800'   => '30 Minutes',
            '3600'   => '60 Minutes',
            '5400'   => '90 Minutes',
            '7200'   => '2 Hours',
            '10800'  => '3 Hours',
            '14400'  => '4 Hours',
            '18000'  => '5 Hours',
            '21600'  => '6 Hours',
            '28800'  => '8 Hours',
            '36000'  => '10 Hours',
            '43200'  => '12 Hours',
            '86400'  => '24 Hours',
            '129600' => '36 Hours',
            '172800' => '48 Hours',
            '259200' => '72 Hours',
        ];

        // max change threshold
        $sisThresholdPercentageOptions = [
            ''       => ' --- Select ---',
            '0.01'   => '1',
            '0.02'   => '2',
            '0.03'   => '3',
            '0.04'   => '4',
            '0.05'   => '5',
            '0.1'    => '10',
            '0.25'   => '25',
            '0.50'   => '50',
            '0.75'   => '75',
        ];

        // db retention options
        $sisZipDbRetentionOptions = [
            ''        => ' --- Select ---',
            '1'       => '1 day',
            '2'       => '2 days',
            '3'       => '3 days',
            '4'       => '4 days',
            '5'       => '5 days',
        ];

        $sisDiffDbRetentionOptions = [
            ''        => ' --- Select ---',
            'forever' => 'Keep Forever',
            '1'       => '1 day',
            '2'       => '2 days',
            '3'       => '3 days',
            '4'       => '4 days',
            '5'       => '5 days',
            '6'       => '6 days',
            '7'       => '7 days',
            '14'      => '2 weeks',
            '21'      => '3 weeks',
            '28'      => '4 weeks',
            '35'      => '5 weeks',
            '42'      => '6 weeks',
            '49'      => '7 weeks',
            '56'      => '8 weeks',
            '63'      => '9 weeks',
            '70'      => '10 weeks',
            '77'      => '11 weeks',
            '84'      => '12 weeks',
            '112'     => '16 weeks',
            '140'     => '20 weeks',
            '182'     => '26 weeks',
            '364'     => '52 weeks (~1 yr)',
            '728'     => '104 weeks (~2 yrs)',
            '1092'    => '208 weeks (~3 yrs)',
        ];

        // disk retention options
        $sisDiskRetentionOptions = [
            ''        => ' --- Select ---',
            'forever' => 'Keep Forever',
            '1'       => '1 day',
            '2'       => '2 days',
            '3'       => '3 days',
            '4'       => '4 days',
            '5'       => '5 days',
            '6'       => '6 days',
            '7'       => '7 days',
            '14'      => '2 weeks',
            '21'      => '3 weeks',
            '28'      => '4 weeks',
            '35'      => '5 weeks',
            '42'      => '6 weeks',
            '49'      => '7 weeks',
            '56'      => '8 weeks',
            '63'      => '9 weeks',
            '70'      => '10 weeks',
            '77'      => '11 weeks',
            '84'      => '12 weeks',
            '112'     => '16 weeks',
            '140'     => '20 weeks',
            '182'     => '26 weeks',
            '364'     => '52 weeks (~1 yr)',
            '728'     => '104 weeks (~2 yrs)',
            '1092'    => '208 weeks (~3 yrs)',
        ];


        $diffThresholdMaxValueOptions = [
            ''        => ' --- Select ---',
            '1'       => '1',
            '10'      => '10',
            '25'      => '25',
            '50'      => '50',
            '100'     => '100',
            '250'     => '250',
            '500'     => '500',
            '750'     => '750',
            '1000'    => '1000',
            '1500'    => '1500',
            '2000'    => '2000',
            '2500'    => '2500',
            '5000'    => '5000',
            '7500'    => '7500',
            '10000'   => '10000',
            '15000'   => '15000',
            '25000'   => '25000',
            '50000'   => '50000',
        ];

        // sis zip required fields
        $requiredZipCsvFiles = json_decode(data_get(app('dbConfig'), 'required_zip_csv_files'), true);
        if (is_null($requiredZipCsvFiles)) {
            $requiredZipCsvFiles = [];
        }

        return view($this->viewPath . 'config.index')->with([
            'dbConfig'                      => app('dbConfig'),
            'requiredZipCsvFiles'           => $requiredZipCsvFiles,
            'timeOptions'                   => $timeOptions,
            'sisThresholdPercentageOptions' => $sisThresholdPercentageOptions,
            'sisZipDbRetentionOptions'      => $sisZipDbRetentionOptions,
            'sisDiffDbRetentionOptions'     => $sisDiffDbRetentionOptions,
            'sisDiskRetentionOptions'       => $sisDiskRetentionOptions,
            'diffThresholdMaxValueOptions'  => $diffThresholdMaxValueOptions,
        ]);
    }
}
