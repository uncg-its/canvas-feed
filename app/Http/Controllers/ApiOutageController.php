<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ApiOutage;
use Uncgits\Ccps\Support\CcpsPaginator;

class ApiOutageController extends Controller
{
    public function index(Request $request)
    {
        $outages = ApiOutage::orderBy('created_at', 'desc')->get();
        $outages = new CcpsPaginator($outages, null, $request->page ?? 1);
        return view('api-outages.index')->with([
            'outages' => $outages
        ]);
    }

    public function show(ApiOutage $outage)
    {
        $outage->load('api_status_checks');

        return view('api-outages.show')->with([
            'outage' => $outage
        ]);
    }
}
