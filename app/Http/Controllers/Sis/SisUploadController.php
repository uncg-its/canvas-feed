<?php

namespace App\Http\Controllers\Sis;

use App\Http\Controllers\Controller;
use Uncgits\Ccps\Support\CcpsPaginator;
use App\Repositories\Sis\SisDiffRepository;

class SisUploadController extends Controller
{
    protected $sisDiffRepository;

    public function __construct()
    {
        $this->sisDiffRepository = new SisDiffRepository;
    }

    public function index()
    {
        $diffsToCheck = $this->sisDiffRepository->currentlyImporting();

        $diffsToCheck->each(function (&$diff) {
            $statusResult = \CanvasApi::using('SisImports')->getSisImportStatus(config('canvas-feed.canvas.root_account_id'), $diff->canvas_id);

            if ($statusResult->getStatus() !== 'success') {
                \Log::channel('sis-feed')->warning('Error while retrieving SIS Diff status from Canvas API', [
                    'category'  => 'api',
                    'operation' => 'upload-status',
                    'result'    => 'error',
                    'data'      => [
                        'diff'   => $diff,
                        'result' => $statusResult->getLastResult(),
                        'body'   => $statusResult->getContent(),
                    ]
                ]);

                $diff->setAttribute('progress', 'error');
                return;
            }

            // get the status
            $status = $statusResult->getContent();
            $diff->setAttribute('progress', $status->progress);
        });

        return view('sis.uploads.index')->with([
            'uploads' => new CcpsPaginator($diffsToCheck)
        ]);
    }
}
