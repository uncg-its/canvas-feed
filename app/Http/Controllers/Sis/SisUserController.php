<?php

namespace App\Http\Controllers\Sis;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Sis\SisUserRepository;

class SisUserController extends Controller
{
    protected $repository;

    public function __construct()
    {
        $this->repository = new SisUserRepository;
    }

    public function index(Request $request)
    {
        if ($request->anyFilled(array_keys($this->repository->searchable))) {
            $users = $this->repository
                ->paginated()
                ->sorted('created_at', 'desc')
                ->search($request->all());
        } else {
            $users = $this->repository
                ->paginated()
                ->sorted('created_at', 'desc')
                ->all();
        }

        return view('sis.zip-logs.users')->with([
            'users'      => $users,
            'repository' => $this->repository,
        ]);
    }
}
