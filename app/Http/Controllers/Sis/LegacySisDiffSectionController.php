<?php

namespace App\Http\Controllers\Sis;

use App\Repositories\Sis\LegacySisDiffSectionRepository;

class LegacySisDiffSectionController extends SisDiffSectionController
{
    protected $isLegacy = true;
    protected $createdAtColumn = 'timestamp';

    public function __construct()
    {
        $this->repository = new LegacySisDiffSectionRepository;
    }
}
