<?php

namespace App\Http\Controllers\Sis;

use App\Repositories\Sis\LegacySisDiffEnrollmentRepository;

class LegacySisDiffEnrollmentController extends SisDiffEnrollmentController
{
    protected $isLegacy = true;
    protected $createdAtColumn = 'timestamp';

    public function __construct()
    {
        $this->repository = new LegacySisDiffEnrollmentRepository;
    }
}
