<?php

namespace App\Http\Controllers\Sis;

use App\Repositories\Sis\LegacySisDiffUserRepository;

class LegacySisDiffUserController extends SisDiffUserController
{
    protected $isLegacy = true;
    protected $createdAtColumn = 'timestamp';

    public function __construct()
    {
        $this->repository = new LegacySisDiffUserRepository;
    }
}
