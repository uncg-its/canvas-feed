<?php

namespace App\Http\Controllers\Sis;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Sis\SisCsvFileValidationService;
use App\Repositories\Sis\SisCsvFileValidationRepository;
use App\Repositories\Sis\SisZipRepository;

class SisCsvFileValidationController extends Controller
{
    protected $repository;
    protected $service;

    public function __construct()
    {
        $this->repository = new SisCsvFileValidationRepository;
        $this->service = new SisCsvFileValidationService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->has('zipId')) {
            $sisZipRepository = new SisZipRepository;

            return view('sis.csv-file-validations.index')->with([
                'validations' => $this->repository
                    ->sorted('created_at', 'desc')
                    ->paginated()
                    ->getValidationsForZip($request->zipId),
                'zip'         => $sisZipRepository->get($request->zipId)
            ]);
        }

        return view('sis.csv-file-validations.index')->with([
            'validations' => $this->repository
                ->sorted('created_at', 'desc')
                ->paginated()
                ->all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('sis.csv-file-validations.show')->with([
            'validation' => $this->repository
                ->get($id)
                ->load('sis_csv_file')
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
