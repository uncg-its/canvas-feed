<?php

namespace App\Http\Controllers\Sis;

use App\Http\Controllers\Controller;

class SisDiffCourseController extends Controller
{
    protected $isLegacy = false;

    public function index()
    {
        return view('sis.diff-logs.courses')->with([
            'isLegacy' => $this->isLegacy,
        ]);
    }
}
