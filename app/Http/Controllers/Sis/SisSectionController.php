<?php

namespace App\Http\Controllers\Sis;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Sis\SisSectionRepository;

class SisSectionController extends Controller
{
    protected $repository;

    public function __construct()
    {
        $this->repository = new SisSectionRepository;
    }

    public function index(Request $request)
    {
        if ($request->anyFilled(array_keys($this->repository->searchable))) {
            $sections = $this->repository
                ->paginated()
                ->sorted('created_at', 'desc')
                ->search($request->all());
        } else {
            $sections = $this->repository
                ->paginated()
                ->sorted('created_at', 'desc')
                ->all();
        }

        return view('sis.zip-logs.sections')->with([
            'sections'   => $sections,
            'repository' => $this->repository,
        ]);
    }
}
