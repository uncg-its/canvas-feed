<?php

namespace App\Http\Controllers\Sis;

use Illuminate\Http\Request;
use App\Services\CronjobService;
use App\Services\Sis\SisZipService;
use App\Http\Controllers\Controller;
use App\Repositories\Sis\SisZipRepository;
use App\Http\Requests\Sis\UpdateSisZipRequest;

class SisZipController extends Controller
{
    protected $repository;
    protected $service;

    public function __construct()
    {
        $this->repository = new SisZipRepository;
        $this->service = new SisZipService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->anyFilled(array_keys($this->repository->searchable))) {
            $zips = $this->repository
                ->sorted('id', 'desc')
                ->paginated()
                ->search($request->all());
        } else {
            $zips = $this->repository
                ->sorted('id', 'desc')
                ->paginated()
                ->all();
        }

        return view('sis.zips.index')->with([
            'zips' => $zips
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('sis.zips.show')->with([
            'zip' => $this->repository->get($id),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSisZipRequest $request, $id)
    {
        try {
            $zip = $this->repository->{$request->operation}($id);
            flash('ZIP status updated successfully.')->success();
        } catch (\Exception $e) {
            flash('There was an error updating the ZIP status.')->danger();
            return redirect()->back();
        }

        if ($request->get('restartCrons', false)) {
            try {
                \DB::beginTransaction();
                $cronjobService = new CronjobService;
                $result = $cronjobService->restartAllSisCrons();
                \DB::commit();
                flash('SIS Cronjobs re-enabled successfully.')->success();
            } catch (\Exception $e) {
                \DB::rollBack();
                flash('There was an error re-enabling the SIS Cronjobs.')->danger();
            }
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
