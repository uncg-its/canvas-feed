<?php

namespace App\Http\Controllers\Sis;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Sis\SisDiffSectionRepository;

class SisDiffSectionController extends Controller
{
    protected $repository;
    protected $isLegacy = false;
    protected $createdAtColumn = 'created_at';

    public function __construct()
    {
        $this->repository = new SisDiffSectionRepository;
    }

    public function index(Request $request)
    {
        if ($request->anyFilled(array_keys($this->repository->searchable))) {
            $sections = $this->repository
                ->paginated()
                ->sorted($this->createdAtColumn, 'desc')
                ->search($request->all());
        } else {
            // no search params
            $sections = $this->repository
                ->paginated()
                ->sorted($this->createdAtColumn, 'desc')
                ->all();
        }

        return view('sis.diff-logs.sections')->with([
            'sections'   => $sections,
            'repository' => $this->repository,
            'isLegacy'   => $this->isLegacy,
        ]);
    }
}
