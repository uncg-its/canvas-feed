<?php

namespace App\Http\Controllers\Sis;

use Illuminate\Http\Request;
use App\Services\CronjobService;
use App\Http\Controllers\Controller;
use App\Services\Sis\SisDiffService;
use App\Repositories\Sis\SisDiffRepository;
use App\Http\Requests\Sis\UpdateSisDiffRequest;

class SisDiffController extends Controller
{
    protected $repository;
    protected $service;

    public function __construct()
    {
        $this->repository = new SisDiffRepository;
        $this->service = new SisDiffService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('sis.diffs.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('sis.diffs.show')->with([
            'diff' => $this->repository->get($id)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSisDiffRequest $request, $id)
    {
        try {
            $diff = $this->repository->{$request->operation}($id);
            flash('Diff status updated successfully.')->success();
        } catch (\Exception $e) {
            flash('There was an error updating the Diff status.')->danger();
            return redirect()->back();
        }

        if ($request->get('restartCrons', false)) {
            try {
                \DB::beginTransaction();
                $cronjobService = new CronjobService;
                $result = $cronjobService->restartAllSisCrons();
                \DB::commit();
                flash('SIS Cronjobs re-enabled successfully.')->success();
            } catch (\Exception $e) {
                \DB::rollBack();
                flash('There was an error re-enabling the SIS Cronjobs.')->danger();
            }
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
