<?php

namespace App\Http\Controllers\Sis;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Sis\SisEnrollmentRepository;

class SisEnrollmentController extends Controller
{
    protected $repository;

    public function __construct()
    {
        $this->repository = new SisEnrollmentRepository;
    }

    public function index(Request $request)
    {
        if ($request->anyFilled(array_keys($this->repository->searchable))) {
            $enrollments = $this->repository
                ->paginated()
                ->sorted('created_at', 'desc')
                ->search($request->all());
        } else {
            $enrollments = $this->repository
                ->paginated()
                ->sorted('created_at', 'desc')
                ->all();
        }

        return view('sis.zip-logs.enrollments')->with([
            'enrollments' => $enrollments,
            'repository'  => $this->repository,
        ]);
    }
}
