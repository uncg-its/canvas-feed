<?php

namespace App\Http\Controllers\Sis;

use App\Http\Controllers\Controller;
use App\Repositories\Sis\SisDiffUserRepository;
use Illuminate\Http\Request;

class SisDiffUserController extends Controller
{
    protected $repository;
    protected $isLegacy = false;
    protected $createdAtColumn = 'created_at';

    public function __construct()
    {
        $this->repository = new SisDiffUserRepository;
    }

    public function index(Request $request)
    {
        if ($request->anyFilled(array_keys($this->repository->searchable))) {
            $users = $this->repository
                ->paginated()
                ->sorted($this->createdAtColumn, 'desc')
                ->search($request->all());
        } else {
            // no search params
            $users = $this->repository
                ->paginated()
                ->sorted($this->createdAtColumn, 'desc')
                ->all();
        }

        return view('sis.diff-logs.users')->with([
            'users'      => $users,
            'repository' => $this->repository,
            'isLegacy'   => $this->isLegacy,
        ]);
    }
}
