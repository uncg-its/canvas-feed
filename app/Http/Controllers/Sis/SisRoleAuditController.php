<?php

namespace App\Http\Controllers\Sis;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Uncgits\Ccps\Support\CcpsPaginator;
use App\Repositories\Sis\SisZipRepository;
use App\Repositories\Sis\SisUserRepository;
use App\Repositories\Sis\SisEnrollmentRepository;

class SisRoleAuditController extends Controller
{
    protected $sisUserRepository;
    protected $sisEnrollmentRepository;
    protected $sisZipRepository;

    public function __construct()
    {
        $this->sisUserRepository = new SisUserRepository;
        $this->sisEnrollmentRepository = new SisEnrollmentRepository;
        $this->sisZipRepository = new SisZipRepository;
    }

    public function index(Request $request)
    {
        // generate roles array
        $rolesArray = collect(config('canvas-feed.sis.expected_roles'))->mapWithKeys(function ($role) {
            return [$role => strtolower($role)];
        })->prepend('--- Select ---', '')->toArray();

        $users = collect();

        if ($request->filled('role')) {
            // which zip to search?
            $zip = $request->zip_type === 'diffed' ?
                $this->sisZipRepository->lastDiffed() :
                $this->sisZipRepository->nextForDiffing();

            $users = $this->sisUserRepository
                ->fromZip($zip->id, $request->role)
                ->keyBy('user_id');

            $enrollments = $this->sisEnrollmentRepository
                ->fromZipWithRole($zip->id, $request->role)
                ->keyBy('user_id'); // this will take care of duplicates for us!

            $users = $users->intersectByKeys($enrollments);
        }

        return view('sis.role-audit.index')->with([
            'users'       => new CcpsPaginator($users),
            'rolesArray'  => $rolesArray,
            'zip'         => $zip ?? null
        ]);
    }
}
