<?php

namespace App\Http\Controllers\Sis;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Sis\SisCourseRepository;

class SisCourseController extends Controller
{
    protected $repository;

    public function __construct()
    {
        $this->repository = new SisCourseRepository;
    }

    public function index(Request $request)
    {
        if ($request->anyFilled(array_keys($this->repository->searchable))) {
            $courses = $this->repository
                ->paginated()
                ->sorted('created_at', 'desc')
                ->search($request->all());
        } else {
            $courses = $this->repository
                ->paginated()
                ->sorted('created_at', 'desc')
                ->all();
        }

        return view('sis.zip-logs.courses')->with([
            'courses'    => $courses,
            'repository' => $this->repository,
        ]);
    }
}
