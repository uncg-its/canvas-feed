<?php

namespace App\Http\Controllers\Sis;

use App\Repositories\Sis\LegacySisDiffCourseRepository;

class LegacySisDiffCourseController extends SisDiffCourseController
{
    protected $isLegacy = true;
    protected $createdAtColumn = 'timestamp';

    public function __construct()
    {
        $this->repository = new LegacySisDiffCourseRepository;
    }
}
