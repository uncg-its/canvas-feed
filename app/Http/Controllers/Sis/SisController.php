<?php

namespace App\Http\Controllers\Sis;

use App\Http\Controllers\Controller;
use App\Repositories\Sis\SisZipRepository;
use App\Repositories\Sis\SisDiffRepository;

class SisController extends Controller
{
    protected $sisZipRepository;
    protected $sisDiffRepository;

    public function __construct(SisZipRepository $sisZipRepository, SisDiffRepository $sisDiffRepository)
    {
        $this->sisZipRepository = $sisZipRepository;
        $this->sisDiffRepository = $sisDiffRepository;
    }

    public function index()
    {
        return view('sis.index')->with([
            'sisHeldZipExists'  => $this->sisZipRepository->getHeld()->isNotEmpty(),
            'sisHeldDiffExists' => $this->sisDiffRepository->getHeld()->isNotEmpty(),
        ]);
    }
}
