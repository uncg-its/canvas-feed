<?php

namespace App\Http\Controllers\Sis;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Sis\SisDiffService;
use App\Repositories\Sis\SisDiffRepository;

class SisDiffEntityController extends Controller
{
    protected $repository;
    protected $service;

    public function __construct()
    {
        $this->repository = new SisDiffRepository;
        $this->service = new SisDiffService;
    }

    public function index(Request $request, $diff, $type)
    {
        $diff = $this->repository->get($diff);

        return view('sis.diff-entities.index')->with([
            'diff'     => $diff,
            'type'     => $type,
            'entities' => $this->repository->getEntitiesForDiff($diff, $type)
        ]);
    }
}
