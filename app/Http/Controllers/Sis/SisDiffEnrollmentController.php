<?php

namespace App\Http\Controllers\Sis;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Sis\SisDiffEnrollmentRepository;

class SisDiffEnrollmentController extends Controller
{
    protected $repository;
    protected $isLegacy = false;
    protected $createdAtColumn = 'created_at';

    public function __construct()
    {
        $this->repository = new SisDiffEnrollmentRepository;
    }

    public function index(Request $request)
    {
        if ($request->anyFilled(array_keys($this->repository->searchable))) {
            $enrollments = $this->repository
                ->paginated()
                ->sorted($this->createdAtColumn, 'desc')
                ->search($request->all());
        } else {
            // no search params
            $enrollments = $this->repository
                ->paginated()
                ->sorted($this->createdAtColumn, 'desc')
                ->all();
        }

        return view('sis.diff-logs.enrollments')->with([
            'enrollments' => $enrollments,
            'repository'  => $this->repository,
            'isLegacy'    => $this->isLegacy,
        ]);
    }
}
