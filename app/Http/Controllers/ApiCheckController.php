<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ApiStatusCheck;
use Uncgits\Ccps\Support\CcpsPaginator;

class ApiCheckController extends Controller
{
    public function index(Request $request)
    {
        $checks = ApiStatusCheck::orderBy('created_at', 'desc')->with('api_outage');

        if (!$request->has('all')) {
            $checks->take(250); // limit for efficiency
        }

        $checks = $checks->get();

        $checks = new CcpsPaginator($checks, null, $request->page ?? 1);
        return view('api-checks.index')->with(compact('checks'));
    }
}
