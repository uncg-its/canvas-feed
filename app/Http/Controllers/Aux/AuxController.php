<?php

namespace App\Http\Controllers\Aux;

use App\Models\Aux\Run;
use App\Models\ApiOutage;
use App\Models\Aux\Override;
use App\CcpsCore\CronjobMeta;
use App\Models\ApiStatusCheck;
use App\Models\Aux\UserDataDiff;
use Uncgits\Ccps\UserFeed\Batch;
use App\Http\Controllers\Controller;

class AuxController extends Controller
{
    private function getApiHealth($lastApiCheck)
    {
        $outage = ApiOutage::inEffect()->first();

        if (is_null($lastApiCheck)) {
            return null;
        }

        if ($lastApiCheck->up) {
            if (is_null($outage)) {
                return [
                    'health'  => 'nominal',
                    'icon'    => 'thumbs-up',
                    'class'   => 'success',
                    'message' => 'API is reachable.'
                ];
            }

            return [
                'health'  => 'recovering',
                'icon'    => 'first-aid',
                'class'   => 'info',
                'message' => 'API is reachable. Awaiting further checks to restore app function.'
            ];
        }

        if (is_null($outage)) {
            return [
                'health'  => 'degrading',
                'icon'    => 'bell',
                'class'   => 'warning',
                'message' => 'API not reachable. Feed will shut down unless contact is restored soon.'
            ];
        }

        return [
            'health'  => 'outage',
            'icon'    => 'exclamation-triangle',
            'class'   => 'danger',
            'message' => 'API not reachable. Feed has been disabled.'
        ];
    }

    public function index()
    {
        $modules = config('ccps.modules');

        $lastRun = Run::orderBy('created_at', 'desc')
            ->select('id', 'created_at')
            // ->limit(1)
            // ->get()
            // ->whereIn('status', ['success', 'failure', 'error'])
            ->first();

        $activeOverrides = Override::active()->count();

        $apiOutage = ApiOutage::inEffect()->first();
        $lastApiCheck = ApiStatusCheck::orderBy('created_at', 'desc')->first();

        $heldBatches = Batch::select('id')
            ->where('status', 'held')
            ->get();

        $heldDiffs = UserDataDiff::select('id')
            ->where('status', 'held')
            ->get();

        $heldDataExists = ($heldBatches->count() > 0) || ($heldDiffs->count() > 0);

        return view('aux.index')->with([
            'modules'                   => $modules,
            'apiOutage'                 => $apiOutage,
            'lastRun'                   => $lastRun,
            'activeOverrides'           => $activeOverrides,
            'lastApiCheck'              => $lastApiCheck,
            'apiHealth'                 => $this->getApiHealth($lastApiCheck),
            'heldDataExists'            => $heldDataExists
        ]);
    }

    public function restartCrons()
    {
        try {
            \DB::beginTransaction();
            // start crons
            CronjobMeta::all()->each(function ($cronjob) {
                $cron = new $cronjob->class;
                if (in_array('core', $cron->getTags())) {
                    $cronjob->status = 'enabled';
                    $cronjob->save();
                    \Log::channel('aux-feed')->info('Cronjob ' . $cronjob->class . ' enabled!', [
                        'category'  => 'cron',
                        'operation' => 'start',
                        'result'    => 'success',
                        'data'      => [
                            'cronjob_meta' => $cronjob
                        ]
                    ]);
                }
            });
            \DB::commit();

            flash('User Feed crons restarted', 'success');
        } catch (\Exception $e) {
            \DB::rollBack();
            \Log::channel('aux-feed')->error('Error trying to enable User Feed cronjobs!', [
                'category'  => 'cron',
                'operation' => 'start',
                'result'    => 'error',
                'data'      => [
                    'message' => $e->getMessage()
                ]
            ]);

            flash('Error restarting User Feed crons. Please contact an administrator', 'danger');
        }
        return redirect()->route('cronjobs');
    }
}
