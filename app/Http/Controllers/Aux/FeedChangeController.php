<?php

namespace App\Http\Controllers\Aux;

use App\Models\Aux\FeedChange;
use App\Http\Controllers\Controller;

class FeedChangeController extends Controller
{
    public function show(FeedChange $feedChange)
    {
        $feedChange->load(['action', 'run', 'canvas_user']);

        return view('aux.feed-changes.show')->with([
            'feedChange' => $feedChange
        ]);
    }
}
