<?php

namespace App\Http\Controllers\Aux;

use App\Models\Aux\UserDataChange;
use App\Http\Controllers\Controller;
use Uncgits\Ccps\Support\CcpsPaginator;

class InvalidChangeController extends Controller
{
    public function index()
    {
        return view('aux.invalid-changes.index')->with([
            'invalidChanges' => new CcpsPaginator(
                UserDataChange::where('invalid', true)
                    ->orderByDesc('created_at')
                    ->get()
                )
        ]);
    }

    public function ignore(UserDataChange $change)
    {
        try {
            \DB::beginTransaction();

            $change->ignored_at = now();
            $change->save();

            \Log::channel('aux-feed')->info('User Data Change ignored.', [
                'category'  => 'model',
                'operation' => 'update',
                'result'    => 'success',
                'data'      => [
                    'user_data_change' => $change,
                    'status'           => 'ignored',
                ]
            ]);

            \DB::commit();

            flash('User Data Change ignored. Restart crons to continue processing this Run.', 'success');
        } catch (\Exception $e) {
            \DB::rollBack();
            flash('Error ignoring User Data Change. Please contact an administrator.', 'danger');
            \Log::channel('aux-feed')->info('Error while ignoring User Data Change', [
                'category'  => 'model',
                'operation' => 'update',
                'result'    => 'error',
                'data'      => [
                    'user_data_change' => $change,
                    'message'          => $e->getMessage(),
                ]
            ]);
        }

        return redirect()->route('aux.invalid-changes.index');
    }
}
