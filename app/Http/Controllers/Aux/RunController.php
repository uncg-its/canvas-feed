<?php

namespace App\Http\Controllers\Aux;

use App\Models\Aux\Run;
use Illuminate\Http\Request;
use App\Constants\SuccessStatus;
use App\Http\Controllers\Controller;
use Uncgits\Ccps\Support\CcpsPaginator;

class RunController extends Controller
{
    public function index()
    {
        $runs = Run::orderByDesc('created_at')->paginate(config('canvas-feed.aux.run_pagination'));
        return view('aux.runs.index')->with(compact('runs'));
    }

    public function show(Run $run, Request $request)
    {
        $actionStatuses = [
            ''                           => '--- Select ---',
            SuccessStatus::PENDING       => 'pending',
            SuccessStatus::IN_PROGRESS   => 'in-progress',
            SuccessStatus::SUCCESS       => 'success',
            SuccessStatus::FAILURE       => 'failure',
            SuccessStatus::ERROR         => 'error',
            SuccessStatus::IGNORED       => 'ignored',
        ];

        $actions = $run->actions->filter(function ($action) use ($request) {
            if ($request->has('username') && $request->username != '') {
                $username = optional($action->canvas_user)->username ?? $action->userTemplate->getUsername();
                if (strpos($username, $request->username) === false) {
                    return false;
                }
            }

            if ($request->has('type') && $request->type != '') {
                if ($action->actionable->type != $request->type) {
                    return false;
                };
            }

            if ($request->has('status') && $request->status != '') {
                if ($action->status_id != $request->status) {
                    return false;
                };
            }

            return true;
        });

        $actions = new CcpsPaginator($actions);

        return view('aux.runs.show')->with([
            'run'            => $run,
            'actions'        => $actions,
            'actionStatuses' => $actionStatuses
        ]);
    }
}
