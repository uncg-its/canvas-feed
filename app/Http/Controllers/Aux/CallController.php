<?php

namespace App\Http\Controllers\Aux;

use App\Models\Aux\Call;
use App\Models\Aux\Action;
use App\Http\Controllers\Controller;

class CallController extends Controller
{
    public function index(Action $action)
    {
        return view('aux.calls.index')->with(compact('action'));
    }

    public function show(Call $call)
    {
        return view('aux.calls.show')->with(compact('call'));
    }
}
