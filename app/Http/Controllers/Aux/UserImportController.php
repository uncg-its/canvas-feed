<?php

namespace App\Http\Controllers\Aux;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Aux\UserImport;
use Uncgits\Ccps\Support\CcpsPaginator;

class UserImportController extends Controller
{
    public function index()
    {
        return view('aux.user-imports.index')->with([
            'imports'  => new CcpsPaginator(UserImport::with('created_by_user')->orderByDesc('created_at')->get()),
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'sis_login_ids' => 'required',
        ]);

        $ids = preg_split("/(,|;|\\r\\n)/", $request->sis_login_ids);

        try {
            \DB::beginTransaction();

            $import = new UserImport;
            $import->sis_login_ids = json_encode($ids);
            $import->created_by_user_id = auth()->user()->id;
            $import->created_at = now();

            $import->save();

            \DB::commit();

            \Log::channel('aux-feed')->info('Initial Users Import created successfully.', [
                'category'  => 'model',
                'operation' => 'create',
                'result'    => 'success',
                'data'      => [
                    'import'       => $import,
                    'current_user' => auth()->user(),
                ]
            ]);
            flash('Import created successfully. Processing job is queued.', 'success');
        } catch (\Throwable $th) {
            flash('Error creating/queuing import. Check logs for more information.', 'danger');
            \Log::channel('aux-feed')->error('Error creating/queuing initial users import.', [
                'category'  => 'model',
                'operation' => 'create',
                'result'    => 'failure',
                'data'      => [
                    'import'       => $import,
                    'current_user' => auth()->user(),
                    'message'      => $th->getMessage()
                ]
            ]);
        }

        return redirect()->back();
    }
}
