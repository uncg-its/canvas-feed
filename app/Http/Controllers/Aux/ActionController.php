<?php

namespace App\Http\Controllers\Aux;

use App\Models\Aux\Run;
use App\Models\Aux\Action;
use App\Constants\SuccessStatus;
use App\Http\Controllers\Controller;

class ActionController extends Controller
{
    public function index()
    {
        $actions = Action::with('actions')->all();
        return view('aux.actions.index')->with(compact('actions'));
    }

    public function show(Action $action)
    {
    }

    public function retry(Action $action)
    {
        $class = '\\App\\Jobs\\Aux\\' . ucwords($action->type) . 'CanvasUser';
        $action->status_id = SuccessStatus::PENDING;
        $action->save();

        dispatch(new $class($action));

        flash('Action requeued.', 'success');

        return redirect()->back();
    }

    public function retryAll(Run $run)
    {
        $requeued = $run->actions->whereIn('status_id', [SuccessStatus::FAILURE, SuccessStatus::ERROR])->map(function ($action) {
            $class = '\\App\\Jobs\\Aux\\' . ucwords($action->type) . 'CanvasUser';
            $action->status_id = SuccessStatus::PENDING;
            $action->save();

            dispatch(new $class($action));
        });

        flash($requeued->count() . ' Action(s) requeued.', 'success');

        return redirect()->back();
    }

    public function ignore(Action $action)
    {
        $action->status_id = SuccessStatus::IGNORED;
        $action->save();

        flash('Action ignored.', 'success');

        return redirect()->back();
    }
}
