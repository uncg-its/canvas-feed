<?php

namespace App\Http\Controllers\Aux;

use Illuminate\Http\Request;
use App\Models\Aux\CanvasUser;
use App\Http\Controllers\Controller;
use Uncgits\Ccps\Support\CcpsPaginator;

class CanvasUserController extends Controller
{
    public function index(Request $request)
    {
        if ($request->has('status') && !is_null($request->status)) {
            if ($request->status == 'active') {
                $users = CanvasUser::get();
            } else {
                $users = CanvasUser::onlyTrashed()->get();
            }
        } else {
            $users = CanvasUser::withTrashed()->get();
        }

        if ($request->has('email') && !is_null($request->email)) {
            $users = $users->filter(function ($user) use ($request) {
                return stripos($user->email, $request->email) !== false;
            });
        }

        if ($request->has('first_name') && !is_null($request->first_name)) {
            $users = $users->filter(function ($user) use ($request) {
                return stripos($user->firstName, $request->first_name) !== false;
            });
        }

        if ($request->has('last_name') && !is_null($request->last_name)) {
            $users = $users->filter(function ($user) use ($request) {
                return stripos($user->lastName, $request->last_name) !== false;
            });
        }

        $users = new CcpsPaginator($users->sortBy('canvasId'), null, $request->page ?? 1);

        return view('aux.users.index')->with([
            'users' => $users
        ]);
    }

    public function show($id)
    {
        $user = CanvasUser::withTrashed()->findOrFail($id);

        return view('aux.users.show')->with([
            'canvasUser' => $user
        ]);
    }

    public function history($id, Request $request)
    {
        $user = CanvasUser::withTrashed()->findOrFail($id);

        $events = new CcpsPaginator($user->events, null, $request->page ?? 1);

        return view('aux.users.history')->with([
            'events'       => $events,
            'canvasUser'   => $user
        ]);
    }
}
