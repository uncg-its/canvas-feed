<?php

namespace App\Http\Controllers\Aux;

use Carbon\Carbon;
use App\Models\Aux\Override;
use App\Handlers\OverrideHandler;
use App\Http\Controllers\Controller;
use App\Handlers\NotificationHandler;
use App\Http\Requests\CreateOverrideRequest;
use App\Http\Requests\ChangeOverrideTimeRequest;

class OverrideController extends Controller
{    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $overrides = Override::orderBy('id', 'desc')->paginate(config('ccps.paginator_per_page'));
        return view('aux.overrides.index')->with([
            'overrides' => $overrides
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $earliestExpiration = Carbon::now()->addHours(config('canvas-feed.aux.overrides.min_length_hours'));

        return view('aux.overrides.create')->with([
            'earliestExpiration' => $earliestExpiration,
            'directionLower'     => Carbon::now()->diffInDays($earliestExpiration)
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateOverrideRequest $request)
    {
        $request->persist();

        return redirect()->route('aux.overrides.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  Override $override
     * @return \Illuminate\Http\Response
     */
    public function show(Override $override)
    {
        $override->load('notifications');
        return view('aux.overrides.show')->with(compact('override'));
    }

    public function cancel(Override $override)
    {
        $handler = new OverrideHandler($override);
        $handler->cancel();
        return redirect()->back();
    }

    public function changeTime(Override $override)
    {
        $earliestExpiration = Carbon::now()->addHours(config('canvas-feed.aux.overrides.min_length_hours'));

        return view('aux.overrides.change-time')->with([
            'earliestExpiration' => $earliestExpiration,
            'directionLower'     => Carbon::now()->diffInDays($earliestExpiration),
            'override'           => $override
        ]);
    }

    public function changeTimeUpdate(ChangeOverrideTimeRequest $request, Override $override)
    {
        $request->persist($override);

        return redirect()->route('aux.overrides.index');
    }

    public function notify(Override $override)
    {
        $handler = new NotificationHandler($override);
        $interval = Carbon::now()->diffInDays($override->expires_at) + 1;
        $success = $handler->send($interval, true);

        if ($success) {
            flash('Notification queued successfully.', 'success');
        } else {
            flash('Error queueing notification. Please contact an administrator.', 'danger');
        }

        return redirect()->back();
    }
}
