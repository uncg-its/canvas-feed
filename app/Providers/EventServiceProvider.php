<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use App\Events\Sis\SisZipMarkedAsHeld;
use App\Events\Sis\SisDiffMarkedAsHeld;
use App\Listeners\Sis\SisDiffHandbrake;
use App\Events\Aux\FailedToFetchNewBatch;
use App\Events\Sis\AbnormalSisDiffImport;
use App\Events\Sis\SisZipMarkedAsIgnored;
use App\Listeners\Sis\SisImportHandbrake;
use App\Listeners\Sis\SisUploadHandbrake;
use App\Events\Sis\SisDiffMarkedAsIgnored;
use App\Events\Sis\SisZipMarkedAsApproved;
use App\Events\Sis\SisDiffMarkedAsApproved;
use App\Events\Sis\SisZipThresholdBreached;
use App\Events\Sis\SisDiffThresholdBreached;
use App\Listeners\Sis\LogSisZipStatusChange;
use App\Listeners\Sis\LogSisDiffStatusChange;
use App\Events\Sis\SisCsvValidationErrorsFound;
use App\Events\Sis\SisZipStatusWasReset;
use App\Listeners\CcpsCore\NotificationSubscriber;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $subscribe = [
        NotificationSubscriber::class,
    ];

    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [

        // Database errors
        'App\Events\CcpsCore\UncaughtQueryException'    => [
            'App\Listeners\CcpsCore\LogUncaughtQueryException',
        ],

        // Email
        'Illuminate\Mail\Events\MessageSent'            => [
            'App\Listeners\CcpsCore\LogSentEmail',
        ],

        // Cron Jobs
        'App\Events\CcpsCore\CronjobStarted'            => [
            'App\Listeners\CcpsCore\PreCronjob',
        ],
        'App\Events\CcpsCore\CronjobFinished'           => [
            'App\Listeners\CcpsCore\PostCronjob',
        ],
        'App\Events\CcpsCore\CronjobFailed'             => [
            'App\Listeners\CcpsCore\LogFailedCronjob'
        ],
        'App\Events\CcpsCore\CronjobFailed'             => [
            'App\Listeners\CcpsCore\LogFailedCronjob'
        ],

        // Login / Logout
        'Illuminate\Auth\Events\Login'                  => [
            'App\Listeners\CcpsCore\LogLoginData',
            'App\Listeners\CcpsCore\RecordLoginTimestampInUsersTable'
        ],
        'Illuminate\Auth\Events\Logout'                 => [
            'App\Listeners\CcpsCore\LogLogoutData',
        ],
        'Illuminate\Auth\Events\Failed'                 => [
            'App\Listeners\CcpsCore\LogFailedLoginData',
        ],

        // ACL
        'App\Events\CcpsCore\AclChanged'                => [
            '\App\Listeners\CcpsCore\LogAclChange'
        ],

        // Cache
        'App\Events\CcpsCore\CacheCleared'              => [
            'App\Listeners\CcpsCore\LogCacheClear',
        ],

        // Config
        'App\Events\CcpsCore\ConfigUpdated'             => [
            'App\Listeners\CcpsCore\LogConfigUpdate',
        ],

        // Backups
        'Spatie\Backup\Events\BackupManifestWasCreated' => [
            'App\Listeners\CcpsCore\LogBackupManifestInfo',
        ],

        'Spatie\Backup\Events\BackupZipWasCreated' => [
            'App\Listeners\CcpsCore\LogBackupZipCreationInfo',
        ],

        'Spatie\Backup\Events\BackupWasSuccessful' => [
            'App\Listeners\CcpsCore\LogSuccessfulBackup',
        ],

        'Spatie\Backup\Events\BackupHasFailed' => [
            'App\Listeners\CcpsCore\LogFailedBackup',
        ],

        'Spatie\Backup\Events\CleanupWasSuccessful' => [
            'App\Listeners\CcpsCore\LogSuccessfulCleanup'
        ],

        'Spatie\Backup\Events\CleanupHasFailed' => [
            'App\Listeners\CcpsCore\LogFailedCleanup'
        ],

        // Socialite (Azure)
        \SocialiteProviders\Manager\SocialiteWasCalled::class => [
            'SocialiteProviders\Azure\AzureExtendSocialite@handle',
        ],

        // Notification Logging
        'Illuminate\Notifications\Events\NotificationSent' => [
            'App\Listeners\CcpsCore\LogNotificationSent',
        ],

        // SIS Feed Listeners

        SisDiffThresholdBreached::class => [
            SisUploadHandbrake::class
        ],

        AbnormalSisDiffImport::class => [
            SisImportHandbrake::class
        ],

        SisZipThresholdBreached::class => [
            SisDiffHandbrake::class
        ],

        SisCsvValidationErrorsFound::class => [
            SisDiffHandbrake::class
        ],

        SisZipMarkedAsHeld::class => [
            SisDiffHandbrake::class,
            LogSisZipStatusChange::class
        ],
        SisZipMarkedAsApproved::class => [
            LogSisZipStatusChange::class
        ],
        SisZipMarkedAsIgnored::class => [
            LogSisZipStatusChange::class
        ],
        SisZipStatusWasReset::class => [
            LogSisZipStatusChange::class
        ],

        SisDiffMarkedAsHeld::class => [
            SisUploadHandbrake::class,
            LogSisDiffStatusChange::class,
        ],
        SisDiffMarkedAsApproved::class => [
            LogSisDiffStatusChange::class,
        ],
        SisDiffMarkedAsIgnored::class => [
            LogSisDiffStatusChange::class,
        ],

    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        \Event::listen('cron.jobError', function ($name, $return, $runtime, $rundate) {
            \Log::channel('cron')->error('Error during cronjob', [
                'category'  => 'cron',
                'operation' => 'run',
                'result'    => 'error',
                'data'      => [
                    'job_name' => $name,
                    'message'  => $return,
                ]
            ]);
            if ($name == 'FetchNewUserDataBatch') {
                // toss another of our Events so that we can notify
                event(new FailedToFetchNewBatch($return));
            }
        });
    }
}
