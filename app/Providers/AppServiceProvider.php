<?php

namespace App\Providers;

use App\Models\Aux\Action;
use App\Models\Aux\Override;
use App\Models\Aux\FeedChange;
use App\Observers\ActionObserver;
use App\Models\ApiStatusCheck;
use App\Models\Aux\UserImport;
use App\Observers\OverrideObserver;
use App\Observers\FeedChangeObserver;
use Illuminate\Support\ServiceProvider;
use App\Observers\ApiStatusCheckObserver;
use App\Observers\UserImportObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // bindings

        // observers

        ApiStatusCheck::observe(ApiStatusCheckObserver::class);
        FeedChange::observe(FeedChangeObserver::class);
        Override::observe(OverrideObserver::class);
        Action::observe(ActionObserver::class);
        UserImport::observe(UserImportObserver::class);
    }
}
