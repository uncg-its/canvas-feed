<?php

namespace App\Repositories\Traits;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

trait EloquentCrud
{
    public $paginated = false;
    public $sortField = 'id';
    public $sortDirection = 'asc';
    public $request = null;

    public function all()
    {
        $sort = $this->getSortMethod();

        return $this->paginated ?
            $this->modelName::$sort($this->sortField)->paginate(25) :
            $this->modelName::$sort($this->sortField)->get();
    }

    public function search(array $terms = null)
    {
        $sort = $this->getSortMethod();

        $query = $this->modelName::$sort($this->sortField);
        foreach ($this->searchable ?? [] as $key => $searchParams) {
            if (isset($terms[$key])) {
                $value = $terms[$key];

                if ($searchParams['cast'] ?? '' === 'datetime') {
                    $value = Carbon::parse($value)->toDateTimeString();
                }

                $column = $searchParams['column'] ?? $key;

                $term = ($searchParams['operand'] === 'like') ? '%' . $value . '%' : $value;
                $query->where($column, $searchParams['operand'], $term);
            }
        }

        return $this->paginated ?
            $query->paginate(25) :
            $query->get();
    }

    public function get($id) : Model
    {
        return $this->modelName::findOrFail($id);
    }

    public function create(array $createData) : Model
    {
        return $this->modelName::create($createData);
    }

    public function update($id, array $updateData) : Model
    {
        $model = $this->get($id);
        $model->update($updateData);

        return $model;
    }

    public function delete($id) : bool
    {
        return $this->modelName::delete($id);
    }

    public function paginated()
    {
        $this->paginated = true;
        return $this;
    }

    public function sorted($field, $direction)
    {
        $this->sortField = $field;
        $this->sortDirection = $direction;

        return $this;
    }

    public function withRequest(Request $request)
    {
        $this->request = $request;
        return $this;
    }

    private function getSortMethod()
    {
        return strtolower($this->sortDirection === 'desc') ? 'orderByDesc' : 'orderBy';
    }
}
