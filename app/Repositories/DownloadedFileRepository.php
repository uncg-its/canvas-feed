<?php

namespace App\Repositories;

use App\CcpsCore\User;
use App\Models\DownloadedFile;
use App\Repositories\Traits\EloquentCrud;
use App\Repositories\Interfaces\EloquentRepositoryInterface;

class DownloadedFileRepository implements EloquentRepositoryInterface
{
    use EloquentCrud;

    public $modelName = DownloadedFile::class;

    public function saveSuccessfulAttempt($disk, $filename, User $user)
    {
        \Log::channel('general')->info('New file download', [
                'category'  => 'download',
                'operation' => 'download',
                'result'    => 'success',
                'data'      => [
                    'disk'     => $disk,
                    'filename' => $filename,
                    'user'     => auth()->user()
                ]
            ]);

        return $this->saveAttempt($disk, $filename, $user, 'success');
    }

    public function saveFailedAttempt($disk, $filename, User $user)
    {
        \Log::channel('general')->info('File download request failed', [
                'category'  => 'download',
                'operation' => 'download',
                'result'    => 'failure',
                'data'      => [
                    'disk'     => $disk,
                    'filename' => $filename,
                    'user'     => auth()->user()
                ]
            ]);

        return $this->saveAttempt($disk, $filename, $user, 'failure');
    }

    private function saveAttempt($disk, $filename, User $user, $result)
    {
        return $this->create([
            'user_id'    => $user->id,
            'disk'       => $disk,
            'filename'   => $filename,
            'result'     => $result,
            'created_at' => now()
        ]);
    }
}
