<?php

namespace App\Repositories\Sis;

use App\Models\Sis\SisZip;
use App\Models\Sis\SisCsvFile;
use App\Repositories\Traits\EloquentCrud;
use App\Repositories\Interfaces\EloquentRepositoryInterface;
use Uncgits\Ccps\Support\CcpsPaginator;

class SisCsvFileRepository implements EloquentRepositoryInterface
{
    use EloquentCrud;

    public $modelName = SisCsvFile::class;

    public function createAfterExtraction(array $csv, SisZip $sisZip, string $folder)
    {
        return $this->create([
            'created_at'        => now(),
            'sis_zip_id'        => $sisZip->id,
            'extraction_folder' => $folder,
            'format'            => $csv['format'],
            'filename'          => $csv['format'] . '.csv',
            'row_count'         => $csv['row_count'],
        ]);
    }

    public function markValidated(SisCsvFile $csv)
    {
        return $this->update($csv->id, [
            'validated_at' => now()
        ]);
    }

    public function markImported(SisCsvFile $csv)
    {
        return $this->update($csv->id, [
            'imported_at' => now()
        ]);
    }

    public function getEntities(SisCsvFile $csv)
    {
        $entities = $this->get($csv->id)->sis_entities;
        return $this->paginated ?
            new CcpsPaginator($entities) :
            $entities;
    }
}
