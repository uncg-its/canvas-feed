<?php

namespace App\Repositories\Sis;

use App\Models\Sis\SisDiff;
use App\Models\Sis\SisDiffCourse;
use App\Repositories\Traits\EloquentCrud;
use App\Repositories\Sis\Interfaces\SisDiffEntityRepositoryInterface;

class SisDiffCourseRepository implements SisDiffEntityRepositoryInterface
{
    use EloquentCrud;

    protected $modelName = SisDiffCourse::class;

    public $searchable = [
        'course_id' => [
            'operand' => 'like',
        ],
        'short_name' => [
            'operand' => 'like'
        ],
        'long_name' => [
            'operand' => 'like'
        ],
        'account_id' => [
            'operand' => 'like'
        ],
        'term_id' => [
            'operand' => '='
        ],
        'status' => [
            'operand' => '='
        ]
    ];

    public function createRecord(array $insertArray)
    {
        return SisDiffCourse::create($insertArray);
    }

    public function updateOrCreateRecord(array $insertArray, SisDiff $sisDiff)
    {
        return SisDiffCourse::updateOrCreate(
            [
                'diff_id'   => $sisDiff->id,
                'course_id' => $insertArray['course_id']
            ],
            $insertArray
        );
    }
}
