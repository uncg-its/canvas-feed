<?php

namespace App\Repositories\Sis;

use App\Repositories\Traits\EloquentCrud;
use App\Models\Sis\LegacySisDiffSection;

class LegacySisDiffSectionRepository extends SisDiffSectionRepository
{
    use EloquentCrud;

    protected $modelName = LegacySisDiffSection::class;
}
