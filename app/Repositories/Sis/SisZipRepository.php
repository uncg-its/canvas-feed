<?php

namespace App\Repositories\Sis;

use App\Models\Sis\SisZip;
use Illuminate\Support\Collection;
use App\Traits\ParsesIntoCarbonFormat;
use App\Events\Sis\SisZipMarkedAsHeld;
use App\Events\Sis\SisZipMarkedAsIgnored;
use App\Repositories\Traits\EloquentCrud;
use App\Events\Sis\SisZipMarkedAsApproved;
use App\Events\Sis\SisZipStatusWasReset;
use App\Repositories\Interfaces\EloquentRepositoryInterface;

class SisZipRepository implements EloquentRepositoryInterface
{
    use EloquentCrud, ParsesIntoCarbonFormat;

    public $modelName = SisZip::class;

    public $searchable = [
        'filename'       => [
            'operand' => 'like',
        ],
        'created_before' => [
            'operand'  => '<',
            'column'   => 'created_at',
            'cast'     => 'datetime',
        ],
        'created_after'  => [
            'operand'  => '>',
            'column'   => 'created_at',
            'cast'     => 'datetime',
        ],
        'status'         => [
            'operand' => '=',
        ]
    ];

    public function createAfterImport(string $filename, Collection $validCsvs)
    {
        $csvContents = $validCsvs->mapWithKeys(function ($csv) {
            return [$csv['format'] => $csv['row_count']];
        });

        return $this->create([
            'filename'     => $filename,
            'csv_contents' => $csvContents,
            'created_at'   => now()
        ]);
    }

    public function changeStatus($sisZipId, string $status)
    {
        if (is_a($sisZipId, SisZip::class)) {
            $sisZipId = $sisZipId->id;
        }

        return $this->update($sisZipId, ['status' => $status]);
    }

    private function nextWithStatus($status, $offset = 0, $idOrder = 'asc')
    {
        $next = SisZip::where('status', $status)
            ->where('type', 'auto')
            ->orderBy('created_at');

        if ($idOrder === 'desc') {
            $next->orderByDesc('id');
        } else {
            $next->orderBy('id');
        }

        $next->offset($offset)
            ->limit(1);

        return $next->first();
    }

    public function nextForImporting()
    {
        return $this->nextWithStatus('new');
    }

    public function nextForValidating()
    {
        return $this->nextWithStatus('imported');
    }

    public function nextForDiffing($offset = 0)
    {
        return $this->nextWithStatus('validated', $offset, 'desc');
    }

    public function lastDiffed()
    {
        return SisZip::where('status', 'diffed')
            ->orderByDesc('created_at')
            ->orderByDesc('id')
            ->limit(1)
            ->first();
    }

    public function latest()
    {
        return SisZip::orderByDesc('created_at')
            ->orderByDesc('id')
            ->limit(1)
            ->first();
    }

    public function getHeld()
    {
        return SisZip::where('status', 'held')
            ->get();
    }

    public function hold($id)
    {
        $zip = $this->changeStatus($id, 'held');
        event(new SisZipMarkedAsHeld($zip, auth()->user()));

        return $zip;
    }

    public function approve($id)
    {
        $zip = $this->changeStatus($id, 'validated');
        event(new SisZipMarkedAsApproved($zip, auth()->user()));

        return $zip;
    }

    public function ignore($id)
    {
        $zip = $this->changeStatus($id, 'ignored');
        event(new SisZipMarkedAsIgnored($zip, auth()->user()));

        return $zip;
    }

    public function reset($id)
    {
        $zip = $this->changeStatus($id, 'validated');
        event(new SisZipStatusWasReset($zip, auth()->user()));

        return $zip;
    }

    public function purgeable()
    {
        $daysToKeep = app('dbConfig')->get('sis_retention_db');

        if ($daysToKeep === 'forever') {
            return collect(); // we won't purge anything.
        }

        $beforeDate = now()->subDays($daysToKeep);

        $zipsToKeep = SisZip::orderByDesc('created_at')
            ->limit(config('canvas-feed.sis.purge_padding.zips'))
            ->get()
            ->keyBy('id');

        $zipsToPurge = SisZip::where('created_at', '<', $beforeDate)
            ->whereIn('status', ['diffed', 'ignored'])
            ->whereNull('purged_at')
            ->orderByDesc('created_at')
            ->get()
            ->keyBy('id');

        return $zipsToPurge->diffKeys($zipsToKeep);
    }

    public function purgeEntities(SisZip $zip, string $type)
    {
        $relationship = 'sis_' . $type;
        return $zip->$relationship()->delete();
    }

    public function markPurged(SisZip $zip)
    {
        return $zip->update(['purged_at' => now()]);
    }

    public function withinLastSeconds(int $seconds)
    {
        return SisZip::where('created_at', '>=', now()->subSeconds($seconds))
            ->get();
    }

    public function createdBetween($start, $end)
    {
        $start = $this->parseIntoCarbon($start);
        $end = $this->parseIntoCarbon($end);

        return SisZip::whereBetween('created_at', [$start, $end])
            ->get();
    }

    public function onDay($day)
    {
        $startDay = $this->parseIntoCarbon($day)->startOfDay();
        $endDay = $startDay->clone()->endOfDay();
        return $this->createdBetween($startDay, $endDay);
    }
}
