<?php

namespace App\Repositories\Sis;

use App\Repositories\Traits\EloquentCrud;
use App\Models\Sis\LegacySisDiffUser;

class LegacySisDiffUserRepository extends SisDiffUserRepository
{
    use EloquentCrud;

    protected $modelName = LegacySisDiffUser::class;
}
