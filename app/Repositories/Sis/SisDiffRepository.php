<?php

namespace App\Repositories\Sis;

use App\Events\Sis\SisDiffReset;
use App\Models\Sis\SisZip;
use App\Models\Sis\SisDiff;
use App\Traits\ParsesIntoCarbonFormat;
use App\Events\Sis\SisDiffMarkedAsHeld;
use Uncgits\Ccps\Support\CcpsPaginator;
use App\Repositories\Traits\EloquentCrud;
use App\Events\Sis\SisDiffMarkedAsIgnored;
use App\Events\Sis\SisDiffMarkedAsApproved;
use App\Repositories\Interfaces\EloquentRepositoryInterface;

class SisDiffRepository implements EloquentRepositoryInterface
{
    use EloquentCrud, ParsesIntoCarbonFormat;

    public $modelName = SisDiff::class;

    public $searchable = [
        'filename'       => [
            'operand' => 'like',
        ],
        'created_before' => [
            'operand' => '<',
            'column'  => 'created_at',
            'cast'    => 'datetime',
        ],
        'created_after'  => [
            'operand' => '>',
            'column'  => 'created_at',
            'cast'    => 'datetime',
        ],
        'status'         => [
            'operand' => '=',
        ]
    ];

    public function nextForUploading()
    {
        return SisDiff::where('status', 'validated')
            ->where('created_at', '<', now()->subSeconds(app('dbConfig')->get('time_between_diff_creation_and_upload')))
            ->orderBy('created_at')
            ->orderBy('id')
            ->limit(1)
            ->first();
    }

    public function lastUploaded()
    {
        return SisDiff::where('status', 'uploaded')
            ->orderByDesc('created_at')
            ->orderByDesc('id')
            ->limit(1)
            ->first();
    }

    public function existingBetweenZips($old, $new)
    {
        if (is_a(SisZip::class, $old)) {
            $old = $old->id;
        }

        if (is_a(SisZip::class, $new)) {
            $new = $new->id;
        }

        return SisDiff::where('old_zip_id', $old)
            ->where('new_zip_id', $new)
            ->where('status', '!=', 'ignored')
            ->first();
    }

    public function pendingImport()
    {
        return SisDiff::whereIn('status', ['validated', 'uploading', 'importing'])
            ->get();
    }

    public function unfinishedImports()
    {
        return SisDiff::whereIn('status', ['uploading', 'importing'])
            ->get();
    }

    public function currentlyImporting()
    {
        return SisDiff::where('status', 'importing')
            ->get();
    }

    public function getEntitiesForDiff(SisDiff $diff, $type)
    {
        $type = 'sis_diff_' . $type;
        return $this->paginated() ?
            new CcpsPaginator($diff->$type) :
            $diff->$type;
    }

    public function changeStatus($sisDiffId, string $status)
    {
        if (is_a($sisDiffId, SisDiff::class)) {
            $sisDiffId = $sisDiffId->id;
        }

        return $this->update($sisDiffId, ['status' => $status]);
    }

    public function getHeld()
    {
        return SisDiff::where('status', 'held')
            ->get();
    }

    public function hold($id)
    {
        $diff = $this->changeStatus($id, 'held');
        event(new SisDiffMarkedAsHeld($diff, auth()->user()));

        return $diff;
    }

    public function approve($id)
    {
        $diff = $this->changeStatus($id, 'validated');
        event(new SisDiffMarkedAsApproved($diff, auth()->user()));

        return $diff;
    }

    public function ignore($id)
    {
        $diff = $this->changeStatus($id, 'ignored');
        event(new SisDiffMarkedAsIgnored($diff, auth()->user()));

        // ignore the new ZIP so that it does not get factored into the next Diff
        $sisZipRepository = new SisZipRepository;
        $zip = $sisZipRepository->ignore($diff->new_zip_id);

        return $diff;
    }

    public function reset($id)
    {
        $diff = $this->changeStatus($id, 'validated');
        event(new SisDiffReset($diff, auth()->user()));

        return $diff;
    }

    public function purgeable()
    {
        $daysToKeep = app('dbConfig')->get('diff_retention_db');

        if ($daysToKeep === 'forever') {
            return collect(); // we won't purge anything.
        }

        $beforeDate = now()->subDays($daysToKeep);

        $zipsToKeep = SisDiff::orderByDesc('created_at')
            ->limit(config('canvas-feed.sis.purge_padding.zips'))
            ->get()
            ->keyBy('id');

        $zipsToPurge = SisDiff::where('created_at', '<', $beforeDate)
            ->whereIn('status', ['imported', 'ignored'])
            ->whereNull('purged_at')
            ->orderByDesc('created_at')
            ->get()
            ->keyBy('id');

        return $zipsToPurge->diffKeys($zipsToKeep);
    }

    public function purgeEntities(SisDiff $diff, string $type)
    {
        $relationship = 'sis_diff_' . $type;
        return $diff->$relationship()->delete();
    }

    public function markPurged(SisDiff $diff)
    {
        return $diff->update(['purged_at' => now()]);
    }

    public function withinLastSeconds(int $seconds)
    {
        return SisDiff::where('created_at', '>=', now()->subSeconds($seconds))
            ->get();
    }

    public function uploadedWithinLastSeconds(int $seconds)
    {
        return SisDiff::where('uploaded_at', '>=', now()->subSeconds($seconds))
            ->get();
    }

    public function latest()
    {
        return SisZip::orderByDesc('created_at')
            ->orderByDesc('id')
            ->limit(1)
            ->first();
    }

    public function createdBetween($start, $end)
    {
        $start = $this->parseIntoCarbon($start);
        $end = $this->parseIntoCarbon($end);

        return SisDiff::whereBetween('created_at', [$start, $end])
            ->get();
    }

    public function onDay($day)
    {
        $startDay = $this->parseIntoCarbon($day)->startOfDay();
        $endDay = $startDay->clone()->endOfDay();
        return $this->createdBetween($startDay, $endDay);
    }

    public function latestImported()
    {
        return SisDiff::whereNotNull('uploaded_at')
            ->where('status', 'imported')
            ->orderByDesc('created_at')
            ->limit(1)
            ->first();
    }

    public function latestImportedOnDay($day = null)
    {
        if (is_null($day)) {
            $day = now();
        }

        $startDay = $this->parseIntoCarbon($day)->startOfDay();
        $endDay = $startDay->clone()->endOfDay();

        return SisDiff::whereNotNull('uploaded_at')
            ->where('status', 'imported')
            ->whereBetween('uploaded_at', [$startDay, $endDay])
            ->orderByDesc('created_at')
            ->limit(1)
            ->first();
    }
}
