<?php

namespace App\Repositories\Sis;

use App\Models\Sis\SisUser;
use App\Repositories\Traits\EloquentCrud;

class SisUserRepository
{
    use EloquentCrud;

    protected $modelName = SisUser::class;

    public $searchable = [
        'user_id' => [
            'operand' => '='
        ],
        'login_id_exact' => [
            'operand'  => '=',
            'column'   => 'login_id'
        ],
        'login_id_fuzzy' => [
            'operand' => 'like',
            'column'  => 'login_id'
        ],
        'first_name' => [
            'operand' => 'like'
        ],
        'last_name' => [
            'operand' => 'like'
        ],
        'sis_zip_id' => [
            'operand' => '='
        ],
    ];

    public function fromZip($zipId)
    {
        return $this->search(['sis_zip_id' => $zipId]);
    }
}
