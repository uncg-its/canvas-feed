<?php

namespace App\Repositories\Sis;

use App\Models\Sis\SisDiff;
use App\Models\Sis\SisDiffUser;
use App\Repositories\Traits\EloquentCrud;
use App\Repositories\Sis\Interfaces\SisDiffEntityRepositoryInterface;

class SisDiffUserRepository implements SisDiffEntityRepositoryInterface
{
    use EloquentCrud;

    protected $modelName = SisDiffUser::class;

    public $searchable = [
        'user_id' => [
            'operand' => '='
        ],
        'login_id_exact' => [
            'operand'  => '=',
            'column'   => 'login_id'
        ],
        'login_id_fuzzy' => [
            'operand' => 'like',
            'column'  => 'login_id'
        ],
        'first_name' => [
            'operand' => 'like'
        ],
        'last_name' => [
            'operand' => 'like'
        ],
    ];

    public function createRecord(array $insertArray)
    {
        return SisDiffUser::create($insertArray);
    }

    public function updateOrCreateRecord(array $insertArray, SisDiff $sisDiff)
    {
        return SisDiffUser::updateOrCreate(
            [
                'diff_id' => $sisDiff->id,
                'user_id' => $insertArray['user_id']
            ],
            $insertArray
        );
    }
}
