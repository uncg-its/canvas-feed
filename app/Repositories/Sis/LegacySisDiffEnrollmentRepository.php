<?php

namespace App\Repositories\Sis;

use App\Repositories\Traits\EloquentCrud;
use App\Models\Sis\LegacySisDiffEnrollment;

class LegacySisDiffEnrollmentRepository extends SisDiffEnrollmentRepository
{
    use EloquentCrud;

    protected $modelName = LegacySisDiffEnrollment::class;
}
