<?php

namespace App\Repositories\Sis\Interfaces;

use App\Models\Sis\SisDiff;

interface SisDiffEntityRepositoryInterface
{
    public function createRecord(array $insertArray);
    public function updateOrCreateRecord(array $insertArray, SisDiff $sisDiff);
}
