<?php

namespace App\Repositories\Sis;

use App\Models\Sis\SisCsvFile;
use Illuminate\Support\Collection;
use App\Models\Sis\SisCsvFileValidation;
use App\Repositories\Traits\EloquentCrud;
use App\Repositories\Interfaces\EloquentRepositoryInterface;
use Uncgits\Ccps\Support\CcpsPaginator;

class SisCsvFileValidationRepository implements EloquentRepositoryInterface
{
    use EloquentCrud;

    public $modelName = SisCsvFileValidation::class;

    public function createAfterValidation(Collection $validationErrors, SisCsvFile $sisCsvFile)
    {
        return $this->create([
            'created_at'      => now(),
            'sis_csv_file_id' => $sisCsvFile->id,
            'success'         => $validationErrors->isEmpty(),
            'messages'        => $validationErrors->isNotEmpty() ? json_encode($validationErrors) : null,
        ]);
    }

    public function getValidationsForZip($zipId)
    {
        $sisZipRepository = new SisZipRepository;
        $zip = $sisZipRepository->get($zipId);

        $csvFileIds = $zip->csvs
            ->pluck('id')
            ->toArray();

        $validations = SisCsvFileValidation::whereIn('sis_csv_file_id', $csvFileIds)
            ->with('sis_csv_file.sis_zip')
            ->get();

        return $this->paginated ?
            new CcpsPaginator($validations) :
            $validations;
    }
}
