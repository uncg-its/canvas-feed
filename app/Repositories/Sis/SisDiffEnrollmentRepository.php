<?php

namespace App\Repositories\Sis;

use App\Models\Sis\SisDiff;
use App\Models\Sis\SisDiffEnrollment;
use App\Repositories\Traits\EloquentCrud;
use App\Repositories\Sis\Interfaces\SisDiffEntityRepositoryInterface;

class SisDiffEnrollmentRepository implements SisDiffEntityRepositoryInterface
{
    use EloquentCrud;

    protected $modelName = SisDiffEnrollment::class;

    public $searchable = [
        'user_id_exact' => [
            'operand' => '=',
            'column'  => 'user_id',
        ],
        'user_id_fuzzy' => [
            'operand' => 'like',
            'column'  => 'user_id',
        ],
        'role' => [
            'operand'  => '=',
        ],
        'section_id_fuzzy' => [
            'operand' => 'like',
            'column'  => 'section_id'
        ],
        'section_id_exact' => [
            'operand' => '=',
            'column'  => 'section_id'
        ],
        'status' => [
            'operand' => '='
        ],
        'sis_zip_id' => [
            'operand' => '='
        ],
    ];

    public function createRecord(array $insertArray)
    {
        return SisDiffEnrollment::create($insertArray);
    }

    public function updateOrCreateRecord(array $insertArray, SisDiff $sisDiff)
    {
        return SisDiffEnrollment::updateOrCreate(
            [
                'diff_id'    => $sisDiff->id,
                'user_id'    => $insertArray['user_id'],
                'section_id' => $insertArray['section_id'],
                'role'       => $insertArray['role'],
            ],
            $insertArray
        );
    }
}
