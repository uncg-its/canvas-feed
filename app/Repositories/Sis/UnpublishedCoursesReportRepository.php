<?php

namespace App\Repositories\Sis;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Uncgits\CanvasApi\CanvasApiResult;
use App\Models\Sis\UnpublishedCoursesReport;

class UnpublishedCoursesReportRepository
{
    public function unfinished()
    {
        return UnpublishedCoursesReport::whereNull('completed_at')
            ->get();
    }

    public function markCompleted(UnpublishedCoursesReport $report, CanvasApiResult $result)
    {
        // get the file and parse it.
        $fileUrl = $result->getContent()->attachment->url;

        // need proxy support
        $client = new Client();
        $options = [];
        if (config('ccps.http_proxy.enabled')) {
            $options['proxy'] = 'http://' . config('ccps.http_proxy.host') . ':' . config('ccps.http_proxy.port');
        }

        $result = $client->get($fileUrl, $options);
        $contents = $result->getBody()->getContents();
        $lines = preg_split("/\\r\\n|\\r|\\n/", $contents, -1, PREG_SPLIT_NO_EMPTY);

        $report->unpublished_courses_count = count($lines) - 1; // ignore header row
        $report->completed_at = now();
        $report->save();

        return $report;
    }

    public function latestForTerm($termId)
    {
        return UnpublishedCoursesReport::latest()
            ->where('term_id', $termId)
            ->first();
    }

    public function forTermAndDate($termId, Carbon $date)
    {
        return UnpublishedCoursesReport::latest()
            ->where('term_id', $termId)
            ->whereBetween(
                'created_at',
                [
                    $date->startOfDay(),
                    $date->clone()->endOfDay()
                ]
            )->first();
    }

    public function createForTerm($termId, CanvasApiResult $result)
    {
        return UnpublishedCoursesReport::create([
            'canvas_id'  => $result->getContent()->id,
            'term_id'    => $termId,
            'created_at' => now()
        ]);
    }
}
