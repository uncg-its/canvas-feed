<?php

namespace App\Repositories\Sis;

use App\Models\Sis\SisSection;
use App\Repositories\Traits\EloquentCrud;

class SisSectionRepository
{
    use EloquentCrud;

    protected $modelName = SisSection::class;

    public $searchable = [
        'section_id' => [
            'operand' => 'like',
        ],
        'course_id' => [
            'operand' => 'like'
        ],
        'name' => [
            'operand' => 'like'
        ],
        'status' => [
            'operand' => '='
        ]
    ];

    public function fromZip($zipId)
    {
        return $this->search(['sis_zip_id' => $zipId]);
    }
}
