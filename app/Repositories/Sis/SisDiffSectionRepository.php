<?php

namespace App\Repositories\Sis;

use App\Models\Sis\SisDiff;
use App\Models\Sis\SisDiffSection;
use App\Repositories\Traits\EloquentCrud;
use App\Repositories\Sis\Interfaces\SisDiffEntityRepositoryInterface;

class SisDiffSectionRepository implements SisDiffEntityRepositoryInterface
{
    use EloquentCrud;

    protected $modelName = SisDiffSection::class;

    public $searchable = [
        'section_id' => [
            'operand' => 'like',
        ],
        'course_id' => [
            'operand' => 'like'
        ],
        'name' => [
            'operand' => 'like'
        ],
        'status' => [
            'operand' => '='
        ]
    ];

    public function createRecord(array $insertArray)
    {
        return SisDiffSection::create($insertArray);
    }

    public function updateOrCreateRecord(array $insertArray, SisDiff $sisDiff)
    {
        return SisDiffSection::updateOrCreate(
            [
                'diff_id'    => $sisDiff->id,
                'section_id' => $insertArray['section_id']
            ],
            $insertArray
        );
    }
}
