<?php

namespace App\Repositories\Sis;

use App\Repositories\Traits\EloquentCrud;
use App\Models\Sis\LegacySisDiffCourse;

class LegacySisDiffCourseRepository extends SisDiffCourseRepository
{
    use EloquentCrud;

    protected $modelName = LegacySisDiffCourse::class;
}
