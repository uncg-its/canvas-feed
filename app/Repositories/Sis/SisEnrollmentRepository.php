<?php

namespace App\Repositories\Sis;

use App\Models\Sis\SisEnrollment;
use App\Repositories\Traits\EloquentCrud;

class SisEnrollmentRepository
{
    use EloquentCrud;

    protected $modelName = SisEnrollment::class;

    public $searchable = [
        'user_id_exact' => [
            'operand' => '=',
            'column'  => 'user_id',
        ],
        'user_id_fuzzy' => [
            'operand' => 'like',
            'column'  => 'user_id',
        ],
        'role' => [
            'operand'  => '=',
        ],
        'section_id_fuzzy' => [
            'operand' => 'like',
            'column'  => 'section_id'
        ],
        'section_id_exact' => [
            'operand' => '=',
            'column'  => 'section_id'
        ],
        'status' => [
            'operand' => '='
        ],
        'sis_zip_id' => [
            'operand' => '='
        ],
    ];

    public function fromZipWithRole($zipId, $role)
    {
        return $this->search([
            'sis_zip_id' => $zipId,
            'role'       => $role
        ]);
    }

    public function countForTermAndRole($zipId, $termSisId, $role)
    {
        return SisEnrollment::where('section_id', 'like', '%___' . $termSisId . '___%')
            ->where('role', $role)
            ->where('status', 'active')
            ->where('sis_zip_id', $zipId)
            ->count();
    }

    public function countForTermAndRoleGrouped($zipId, $termSisId, $role)
    {
        // groups by user id... may not use this.
        return SisEnrollment::select('user_id', \DB::raw('COUNT(user_id) AS count'))
            ->where('section_id', 'like', '%___' . $termSisId . '___%')
            ->where('role', $role)
            ->where('status', 'active')
            ->where('sis_zip_id', $zipId)
            ->groupBy('user_id')
            ->get();
    }

    public function tallyForTermAndRole($zipId, $termSisId, $role)
    {
        return SisEnrollment::select('count', \DB::raw('COUNT(count) AS number'))
            ->fromSub(function ($query) use ($zipId, $termSisId, $role) {
                $query->select('user_id', \DB::raw('COUNT(user_id) AS count'))
                ->from('sis_enrollments')
                ->where('section_id', 'like', '%___' . $termSisId . '___%')
                ->where('role', $role)
                ->where('status', 'active')
                ->where('sis_zip_id', $zipId)
                ->groupBy('user_id');
            }, 'a')
            ->groupBy('count')
            ->get();
        // ->mapWithKeys(function ($result) {
            //     return [$result->count => $result->number];
            // });
    }

    public function tallyForTermByRole($zipId, $termSisId)
    {
        return SisEnrollment::select('role', \DB::raw('COUNT(id) AS count'))
            ->where('section_id', 'like', '%___' . $termSisId . '___%')
            ->where('status', 'active')
            ->where('sis_zip_id', $zipId)
            ->groupBy('role')
            ->get();
    }

    public function tallyForTermByRoleUnique($zipId, $termSisId)
    {
        return SisEnrollment::select('role', \DB::raw('COUNT(user_id) AS count'))
            ->fromSub(function ($query) use ($zipId, $termSisId) {
                $query->select('role', 'user_id')
                    ->distinct()
                    ->from('sis_enrollments')
                    ->where('sis_zip_id', $zipId)
                    ->where('section_id', 'like', '%___' . $termSisId . '___%')
                    ->where('status', 'active')
                    ->get();
            }, 'a')
            ->groupBy('role')
            ->get();
    }
}
