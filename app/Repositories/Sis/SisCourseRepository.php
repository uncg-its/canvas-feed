<?php

namespace App\Repositories\Sis;

use App\Models\Sis\SisCourse;
use App\Repositories\Traits\EloquentCrud;

class SisCourseRepository
{
    use EloquentCrud;

    protected $modelName = SisCourse::class;

    public $searchable = [
        'course_id' => [
            'operand' => 'like',
        ],
        'short_name' => [
            'operand' => 'like'
        ],
        'long_name' => [
            'operand' => 'like'
        ],
        'account_id' => [
            'operand' => 'like'
        ],
        'term_id' => [
            'operand' => '='
        ],
        'status' => [
            'operand' => '='
        ],
        'sis_zip_id' => [
            'operand' => '='
        ],
    ];

    public function fromZip($zipId)
    {
        return $this->search(['sis_zip_id' => $zipId]);
    }

    public function fromTermAndZip($termSisId, $zipId)
    {
        return $this->search([
            'sis_zip_id' => $zipId,
            'term_id'    => $termSisId,
        ]);
    }
}
