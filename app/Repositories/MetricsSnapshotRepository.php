<?php

namespace App\Repositories;

use App\Models\MetricsSnapshot;
use App\Traits\ParsesIntoCarbonFormat;
use App\Repositories\Traits\EloquentCrud;

class MetricsSnapshotRepository
{
    use EloquentCrud, ParsesIntoCarbonFormat;

    public $modelName = MetricsSnapshot::class;

    public $searchable = [
        'date'       => [
            'operand' => '=',
        ],
    ];

    public function forDay($day)
    {
        $day = $this->parseIntoCarbon($day);

        return $this->search(['date' => $day->format('Y-m-d')])
            ->first();
    }

    public function existsForDay($day)
    {
        $day = $this->parseIntoCarbon($day);

        return $this->search(['date' => $day->format('Y-m-d')])
            ->count() > 0;
    }

    public function unpushed()
    {
        return MetricsSnapshot::whereNull('pushed_at')->get();
    }
}
