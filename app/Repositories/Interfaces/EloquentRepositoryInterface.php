<?php

namespace App\Repositories\Interfaces;

use Illuminate\Database\Eloquent\Model;

interface EloquentRepositoryInterface
{
    public function all();
    public function get($id) : Model;
    public function create(array $createData) : Model;
    public function update($id, array $updateData) : Model;
    public function delete($id) : bool;
}
