<?php

namespace App\Services;

use App\Repositories\DownloadedFileRepository;

class DownloadedFileService
{
    public function downloadFile($disk, $filename)
    {
        $repository = new DownloadedFileRepository;

        // does disk exist?
        $availableDisks = config('filesystems.disks');
        if (!isset($availableDisks[$disk])) {
            $repository->saveFailedAttempt($disk, $filename, auth()->user());

            flash('Error: requested disk does not exist.')->error();
            return redirect()->back();
        }

        // does file exist?
        if (\Storage::disk($disk)->exists($filename)) {
            $repository->saveSuccessfulAttempt($disk, $filename, auth()->user());

            return \Storage::disk($disk)->download($filename);
        }

        $repository->saveFailedAttempt($disk, $filename, auth()->user());

        flash('Error: requested file does not exist.')->error();
        return redirect()->back();
    }
}
