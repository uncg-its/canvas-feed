<?php

namespace App\Services;

use Carbon\Carbon;
use App\Models\MetricsSnapshot;
use App\Traits\ParsesIntoCarbonFormat;
use App\Repositories\Sis\SisZipRepository;
use App\Services\Sis\SisEnrollmentService;
use App\Repositories\Sis\SisDiffRepository;
use App\Repositories\Sis\SisCourseRepository;
use App\Repositories\MetricsSnapshotRepository;
use App\Repositories\Sis\UnpublishedCoursesReportRepository;

class MetricsSnapshotService
{
    use ParsesIntoCarbonFormat;

    public function generateForDay($day)
    {
        $day = $this->parseIntoCarbon($day);

        $rawData = [];

        $diffRepository = new SisDiffRepository;
        $diffsOnDay = $diffRepository->onDay($day);

        $rawData['diff_tally'] = $diffsOnDay->count();
        $rawData['diff_accounts'] = $diffsOnDay->sum('accounts');
        $rawData['diff_courses'] = $diffsOnDay->sum('courses');
        $rawData['diff_enrollments'] = $diffsOnDay->sum('enrollments');
        $rawData['diff_groups'] = $diffsOnDay->sum('groups');
        $rawData['diff_groupsmembership'] = $diffsOnDay->sum('groups_membership');
        $rawData['diff_sections'] = $diffsOnDay->sum('sections');
        $rawData['diff_terms'] = $diffsOnDay->sum('terms');
        $rawData['diff_users'] = $diffsOnDay->sum('users');
        $rawData['diff_xlists'] = $diffsOnDay->sum('xlists');
        $rawData['diff_total'] = $diffsOnDay->sum('total');
        $rawData['diff_largest_zip'] = $diffsOnDay->max('total');
        $rawData['diff_smallest_zip'] = $diffsOnDay->min('total');

        $zipRepository = new SisZipRepository;
        $zipsOnDay = $zipRepository->onDay($day);

        $rawData['sis_tally'] = $zipsOnDay->count();
        $rawData['sis_accounts'] = $zipsOnDay->sum('csv_contents.accounts');
        $rawData['sis_courses'] = $zipsOnDay->sum('csv_contents.courses');
        $rawData['sis_enrollments'] = $zipsOnDay->sum('csv_contents.enrollments');
        $rawData['sis_groups'] = $zipsOnDay->sum('csv_contents.groups');
        $rawData['sis_groupsmembership'] = $zipsOnDay->sum('csv_contents.groupsmembership');
        $rawData['sis_sections'] = $zipsOnDay->sum('csv_contents.sections');
        $rawData['sis_terms'] = $zipsOnDay->sum('csv_contents.terms');
        $rawData['sis_users'] = $zipsOnDay->sum('csv_contents.users');
        $rawData['sis_xlists'] = $zipsOnDay->sum('csv_contents.xlists');
        $rawData['sis_total'] = $zipsOnDay->sum('csv_contents.total');
        $rawData['sis_largest_zip'] = $zipsOnDay->max(function ($zip) {
            return array_sum($zip->csv_contents);
        });
        $rawData['sis_smallest_zip'] = $zipsOnDay->min(function ($zip) {
            return array_sum($zip->csv_contents);
        });

        // unpublished courses - which terms to report on?
        $unpublishedCoursesReportRepository = new UnpublishedCoursesReportRepository;
        $sisCourseRepository = new SisCourseRepository;

        $termsToReportOn = explode(',', app('dbConfig')->get('unpublished_courses_report_terms'));
        $latestImportedDiffOnDay = $diffRepository->latestImportedOnDay($day);

        foreach ($termsToReportOn as $termSisId) {
            // courses per term total
            if (!is_null($latestImportedDiffOnDay)) {
                $latestImportedZipId = $latestImportedDiffOnDay->new_zip_id;
                $coursesInLastImportedZip = $sisCourseRepository->fromTermAndZip($termSisId, $latestImportedZipId);
                $rawData['courses_total_for_term_' . $termSisId] = $coursesInLastImportedZip->count();

                // student/teacher metrics based on # of enrollments
                $sisEnrollmentService = new SisEnrollmentService;
                $studentEnrollments = $sisEnrollmentService->getCountMetricsArrayForTermAndRole($latestImportedZipId, $termSisId, 'student');
                $rawData = array_merge($rawData, $studentEnrollments);
                $teacherEnrollments = $sisEnrollmentService->getCountMetricsArrayForTermAndRole($latestImportedZipId, $termSisId, 'teacher');
                $rawData = array_merge($rawData, $teacherEnrollments);

                $totalEnrollments = $sisEnrollmentService->tallyTotalEnrollmentsForTermByRole($latestImportedZipId, $termSisId);
                $rawData = array_merge($rawData, $totalEnrollments);

                $uniqueEnrollments = $sisEnrollmentService->tallyUniqueEnrollmentsForTermByRole($latestImportedZipId, $termSisId);
                $rawData = array_merge($rawData, $uniqueEnrollments);

                $latestReportForTerm = $unpublishedCoursesReportRepository->forTermAndDate($termSisId, $day);
                if (!is_null($latestReportForTerm) && $latestReportForTerm->created_at->format('Y-m-d') === $day->format('Y-m-d')) {
                    $rawData['courses_unpublished_for_term_' . $termSisId] = $latestReportForTerm->unpublished_courses_count;

                    $rawData['courses_published_for_term_' . $termSisId] = $rawData['courses_total_for_term_' . $termSisId] - $rawData['courses_unpublished_for_term_' . $termSisId];

                    $rawData['courses_published_percent_for_term_' . $termSisId] = round(($rawData['courses_published_for_term_' . $termSisId] / $rawData['courses_total_for_term_' . $termSisId]) * 100, 2);
                }
            }
        }

        // create model
        return (new MetricsSnapshotRepository)->create([
            'date'             => $day->format('Y-m-d'),
            'raw_data'         => $rawData,
            'created_at'       => now(),
            'push_destination' => config('canvas-feed.metrics.push_destination')
        ]);
    }

    public function pushToSplunk(MetricsSnapshot $snapshot)
    {
        $apiSuccess = true;

        foreach ($snapshot->raw_data as $key => $value) {
            $dataToSend = [];

            $fieldsArray = [
                'metric_name'   => config('canvas-feed.metrics.prefix') . '.' . $key,
                '_value'        => $value,
            ];

            $thisArray = [
                'time'          => strtotime($snapshot->date),
                'index'         => config('canvas-feed.metrics.splunk.index'),
                'source'        => config('canvas-feed.metrics.splunk.source'),
                'host'          => config('canvas-feed.metrics.host'),
                'event'         => 'metric',
                'fields'        => $fieldsArray,
            ];

            $dataToSend[] = $thisArray;

            // dump($dataToSend);

            $metricsApiCall = \SplunkApi::sendData($dataToSend);

            // dump($metricsApiCall);

            if ($metricsApiCall['response']['httpCode'] != '200') {
                $apiSuccess = false;

                \Log::channel('metrics')->warning('API failure while pushing snapshot to Splunk', [
                    'category'  => 'metrics',
                    'operation' => 'push',
                    'result'    => 'failure',
                    'data'      => [
                        'snapshot_id' => $snapshot->id,
                        'sent_data'   => $dataToSend,
                        'response'    => $metricsApiCall
                    ]
                ]);
            }
        }

        if ($apiSuccess) {
            $snapshot->pushed_at = Carbon::now()->toDateTimeString();
            $snapshot->push_destination = 'splunk';
            $snapshot->save();

            \Log::channel('metrics')->info('Snapshot pushed to Splunk successfully', [
                'category'  => 'metrics',
                'operation' => 'push',
                'result'    => 'success',
                'data'      => [
                    'snapshot_id' => $snapshot->id,
                    'destination' => 'splunk'
                ]
            ]);

            return true;
        }

        $snapshot->pushed_at = Carbon::now()->toDateTimeString();
        $snapshot->push_destination = 'splunk';
        $snapshot->error_data = 'One or more metric pushes encountered an error.';
        $snapshot->save();

        return false;
    }
}
