<?php

namespace App\Services\Sis;

use League\Csv\Reader;
use App\Models\Sis\SisCsvFile;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use App\SisValidators\SisValidator;
use App\Repositories\Sis\SisCsvFileRepository;
use App\Repositories\Sis\SisCsvFileValidationRepository;

class SisCsvFileService
{
    protected $sisCsvFileRepository;
    protected $sisCsvFileValidationRepository;

    public function __construct()
    {
        $this->sisCsvFileRepository = new SisCsvFileRepository;
        $this->sisCsvFileValidationRepository = new SisCsvFileValidationRepository;
    }

    public function validate(SisCsvFile $csv, SisValidator $validator = null)
    {
        if (is_null($validator)) {
            $validatorClass = '\\App\\SisValidators\\' . ucwords($csv->format);
        }

        try {
            $validationErrors = new Collection;
            $csv->sis_entities()->chunk(config('canvas-feed.sis.validation.chunk_size'), function ($entities) use ($validator, $validatorClass, &$validationErrors, $csv) {
                if ($entities->isEmpty()) {
                    \Log::channel('sis-feed')->notice('No validation performed: No data in CSV file.', [
                        'category'  => 'sis-feed',
                        'operation' => 'valiation',
                        'result'    => 'notice',
                        'data'      => [
                            'sis_csv_file' => $csv,
                        ]
                    ]);
                    return;
                }

                $chunkErrors = ($validator ?? new $validatorClass)->validate($entities, $csv->zip);

                $validationErrors = $validationErrors->merge($chunkErrors);
            });
        } catch (\Throwable $th) {
            \Log::channel('sis-feed')->error('Unexpected error while validating', [
                'category'  => 'sis-feed',
                'operation' => 'validation',
                'result'    => 'error',
                'data'      => [
                    'message'      => $th->getMessage(),
                    'sis_csv_file' => $csv,
                ]
            ]);
            throw $th;
        }

        // create validation records in DB
        $validation = $this->sisCsvFileValidationRepository->createAfterValidation($validationErrors, $csv);

        // mark the CSV file as validated
        $csv = $this->sisCsvFileRepository->markValidated($csv);

        return $validation;
    }

    public function importToDatabase(SisCsvFile $csv)
    {
        // set params

        $fileContent = Reader::createFromString($csv->zip->filesystem->get($csv->extraction_folder . '/' . $csv->filename), 'r');
        $requiredFields = config('canvas-feed.sis.required_csv_fields.' . $csv->format);
        $fieldList = implode(',', $requiredFields);
        $targetTable = config('database.connections.' . config('database.default') . '.prefix') . 'sis_' . $csv->format;
        $importTime = now()->toDateTimeString();
        $lineEnding = PHP_EOL;

        \Storage::disk('temp')->put('infile.csv', $fileContent);
        $tempPath = \Storage::disk('temp')->path('infile.csv');
        \Log::channel('sis-feed')->debug('Created temporary infile for ' . $csv->filename);

        // TODO: archive table?

        $sisCsvFileRepository = new SisCsvFileRepository;

        // perform the insert with LOAD DATA INFILE
        try {
            ini_set('auto_detect_line_endings', true);

            $sql = "LOAD DATA LOCAL INFILE '$tempPath' INTO TABLE " . $targetTable . " "
            . "FIELDS TERMINATED BY ',' "
            . "OPTIONALLY ENCLOSED BY '\"' "
            . "LINES TERMINATED BY '$lineEnding' "
            . "IGNORE 1 LINES "
            . "($fieldList) "
            . "SET created_at = '$importTime', "
            . "sis_upload_id = NULL, "
            . "sis_zip_id = {$csv->zip->id}, "
            . "sis_csv_file_id = {$csv->id}, "
            . "checksum = MD5(CONCAT($fieldList)) "
            . ";";

            \DB::connection()->getpdo()->exec($sql);

            $csv = $sisCsvFileRepository->markImported($csv);
        } catch (\Throwable $th) {
            \Log::channel('sis-feed')->error('Error importing ' . $csv->filename . ' file to database', [
                'category'  => 'sis',
                'operation' => 'import',
                'result'    => 'error',
                'data'      => [
                    'sis_zip' => $csv,
                    'message' => $th->getMessage()
                ]
            ]);
            throw $th;
        } finally {
            \Storage::disk('temp')->delete('infile.csv');
            \Log::channel('sis-feed')->debug('Deleted temporary infile for ' . $csv->filename);
        }
    }
}
