<?php

namespace App\Services\Sis\Interfaces;

use Illuminate\Support\Collection;
use Illuminate\Support\Enumerable;

interface SisDiffEntityServiceInterface
{
    public function createFromDiffResults(Collection $collection);

    public function processActive(Enumerable $collection);
    public function processDeleted(Enumerable $collection);
    public function processInactive(Enumerable $collection);
}
