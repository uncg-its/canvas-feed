<?php

namespace App\Services\Sis;

class SisDiffSectionService extends BaseSisDiffEntityService
{
    protected $repositoryClass = 'App\\Repositories\\Sis\\SisDiffSectionRepository';
    protected $table = 'sis_diff_sections';
}
