<?php

namespace App\Services\Sis;

class SisDiffEnrollmentService extends BaseSisDiffEntityService
{
    protected $repositoryClass = 'App\\Repositories\\Sis\\SisDiffEnrollmentRepository';
    protected $table = 'sis_diff_enrollments';
}
