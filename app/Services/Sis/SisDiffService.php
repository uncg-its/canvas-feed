<?php

namespace App\Services\Sis;

use ZipArchive;
use App\CcpsCore\DbConfig;
use App\Models\Sis\SisZip;
use App\Models\Sis\SisDiff;
use Illuminate\Support\Enumerable;
use Symfony\Component\Finder\Finder;
use App\Traits\ClearsDirectoriesFromDisk;
use App\Repositories\Sis\SisDiffRepository;

class SisDiffService
{
    use ClearsDirectoriesFromDisk;

    public function createDiffFromSisZips(SisZip $oldZip, SisZip $newZip)
    {
        $formatsToDiff = config('canvas-feed.sis.formats_to_diff');

        $results = [];

        foreach ($formatsToDiff as $format) {
            $className = 'App\\SisDiffers\\' . ucwords($format) . 'Differ';
            $differ = new $className($oldZip, $newZip);
            $results[$format] = $differ->diff();
        }

        // get next auto_increment ID
        $connection = config('database.default');
        $prefix = config('database.connections.' . $connection . '.prefix');
        $tableInfo = \DB::select('SHOW TABLE STATUS LIKE \'' . $prefix . 'sis_diffs\'');
        $nextId = $tableInfo[0]->Auto_increment;

        $createArray = array_merge([
            'old_zip_id'    => $oldZip->id,
            'new_zip_id'    => $newZip->id,
            'filename'      => $nextId . '___' . now()->format('YmdHisA') . '.zip',
            'status'        => 'new',
            'created_at'    => now()
        ]);

        $diff = SisDiff::create($createArray);

        // prep diff CSVs - create folder
        \Storage::disk('sis-diffs')->makeDirectory($diff->folderName);

        $diffChanges = collect();
        // extrapolate results into individual tables
        foreach ($results as $format => $result) {
            // generate new CSV and pass the path
            $generatedCsvPath = \Storage::disk('sis-diffs')->path($diff->folderName . '/' . $format . '.csv');
            touch($generatedCsvPath);

            $className = 'App\\Services\\Sis\\SisDiff' . \Str::singular(ucwords($format)) . 'Service';
            $service = new $className($diff, $generatedCsvPath);
            $diffChanges[$format] = $service->createFromDiffResults($result);
        }

        $this->udpateDiffWithMetricsFromResults($diff, $diffChanges);

        return $diff;
    }

    public function udpateDiffWithMetricsFromResults(SisDiff $diff, Enumerable $metrics)
    {
        $metrics->put('total', $metrics->sum());
        $diff->update($metrics->toArray());
    }

    public function createDiffZipFromDiff(SisDiff $diff)
    {
        $zipPath = \Storage::disk('sis-diffs')->path($diff->filename);
        $csvFolder = str_replace('.zip', '', $zipPath);

        // CSVs are already created
        $csvs = new Finder;
        $csvs->files()->in($csvFolder);

        // create ZIP from folder
        $zip = new ZipArchive;
        $zipPath = \Storage::disk('sis-diffs')->path($diff->filename);
        $zip->open($zipPath, ZipArchive::CREATE);

        foreach ($csvs as $csv) {
            $zip->addFile($csv->getRealPath(), $csv->getFilename());
        }

        $zip->close();

        // record number of CSVs in this ZIP
        $repository = new SisDiffRepository;
        $diff = $repository->update($diff->id, [
            'zip_csv_count' => iterator_count($csvs),
        ]);

        // get rid of folder
        \Storage::disk('sis-diffs')->deleteDirectory($diff->folderName);

        return $zipPath;
    }

    public function validateThresholds(SisDiff $sisDiff)
    {
        // max changes per file threshold
        // TODO: if we ever add more here, abstract them into their own classes and do all the SRP things
        $maxChangesPerFile = DbConfig::getConfig('diff_threshold_max_value');
        $expectedCsvs = json_decode(DbConfig::getConfig('required_zip_csv_files'), true);

        return collect($expectedCsvs)->mapWithKeys(function ($csv) use ($sisDiff, $maxChangesPerFile) {
            $property = 'sis_diff_' . $csv;
            $changes = $sisDiff->$property()->count();
            return [
                $csv => [
                    'changes'   => $changes,
                    'threshold' => $maxChangesPerFile,
                    'breached'  => $changes > $maxChangesPerFile
                ]
            ];
        });
    }

    public function recordSuccessfulUpload(SisDiff $diff, $canvasUploadId)
    {
        $sisDiffRepository = new SisDiffRepository;

        return $sisDiffRepository->update($diff->id, [
            'status'      => 'importing',
            'canvas_id'   => $canvasUploadId,
            'uploaded_at' => now(),
        ]);
    }

    public function recordImportResult(SisDiff $diff, $result)
    {
        $sisDiffRepository = new SisDiffRepository;

        return $sisDiffRepository->update($diff->id, [
            'status' => 'imported',
            'result' => $result,
        ]);
    }

    public function diffImportedAbnormally(SisDiff $diff)
    {
        return in_array($diff->status, [
            'imported_with_messages',
            'imported-with-messages',
            'failed',
            'failed_with_messages',
            'failed-with-messages'
        ]);
    }

    public function purgeDatabase(Enumerable $diffsToPurge)
    {
        $tables = config('canvas-feed.sis.formats_to_diff');
        $repository = new SisDiffRepository;

        return $diffsToPurge->filter(function ($zip) use ($tables, $repository) {
            try {
                \DB::beginTransaction();

                foreach ($tables as $table) {
                    $repository->purgeEntities($zip, $table);
                    $repository->markPurged($zip);
                }

                \DB::commit();
                return true;
            } catch (\Exception $e) {
                \DB::rollBack();
                throw $e;
            }
        });
    }
}
