<?php

namespace App\Services\Sis;

use App\Repositories\Sis\SisCsvFileValidationRepository;

class SisCsvFileValidationService
{
    protected $sisCsvFileValidationRepository;

    public function __construct()
    {
        $this->sisCsvFileValidationRepository = new SisCsvFileValidationRepository;
    }
}
