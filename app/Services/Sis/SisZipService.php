<?php

namespace App\Services\Sis;

use ZipArchive;
use League\Csv\Reader;
use App\CcpsCore\DbConfig;
use App\Models\Sis\SisZip;
use Illuminate\Support\Enumerable;
use App\Exceptions\SisFeedException;
use Symfony\Component\Finder\Finder;
use App\Traits\ClearsDirectoriesFromDisk;
use App\Events\Sis\SisZipExtractionFailed;
use App\Repositories\Sis\SisZipRepository;
use App\Events\Sis\DetectedInvalidSisCsvFile;
use App\Events\Sis\NonZipFoundInSisOriginalPath;
use App\Events\Sis\RequiredCsvFileMissingFromSisZip;
use App\Events\Sis\UnexpectedCsvFileContainedInSisZip;

class SisZipService
{
    use ClearsDirectoriesFromDisk;

    public function moveIntoStorage(\SplFileInfo $file)
    {
        // instance of SplFileInfo
        if (!($file->getExtension() == 'zip')) {
            event(new NonZipFoundInSisOriginalPath($file->getRelativePathName()));
            throw new SisFeedException('Non-ZIP found in SIS originals path: ' . $file->getRelativePathName());
        }

        // move to storage
        $destinationName = date('YmdHisA', $file->getMTime()) . '___' . $file->getRelativePathName();
        \Storage::disk('sis-zips')->put($destinationName, $file->getContents());
        \Log::channel('sis-feed')->debug('Moved ' . $file->getRelativePathName() . ' to ' . $destinationName);

        return $destinationName;
    }

    public function extractCsvsToFolder(\SplFileInfo $file, string $extractionFolder)
    {
        \Storage::disk('sis-zips')->makeDirectory($extractionFolder);
        \Log::channel('sis-feed')->debug('Folder for extraction \'' . $extractionFolder . '\' has been created.');

        $zip = new ZipArchive;
        $unzipResult = $zip->open($file, ZipArchive::CREATE);

        if ($unzipResult !== true) {
            event(new SisZipExtractionFailed($file->getFilename()));
            throw new SisFeedException($file->getFilename() . ' failed extraction. Error code ' . $unzipResult);
        }

        $zip->extractTo(\Storage::disk('sis-zips')->path($extractionFolder));
        \Log::channel('sis-feed')->debug('Found ' . $zip->count() . ' extracted file(s).');
        $zip->close();

        $sisCsvOriginals = new Finder;
        $sisCsvOriginals->files()->in(\Storage::disk('sis-zips')->path($extractionFolder));

        return $sisCsvOriginals;
    }

    public function validateExtractedCsvs(Finder $sisCsvOriginals, $extractionFolder)
    {
        // parse sis CSV files to make sure they are what we expect them to be
        $validSisZipCsvs = collect([]);
        foreach ($sisCsvOriginals as $sisCsvOriginal) {
            try {
                $validSisZipCsvs->push($this->checkCsvContents($sisCsvOriginal));
            } catch (SisFeedException $e) {
                \Storage::disk('sis-zips')->delete($extractionFolder . '/' . $sisCsvOriginal->getFilename());
                throw $e; // bubble up
            }
        }

        // check to be sure ZIP contained everything we need
        $requiredSisZipContents = collect(json_decode(app('dbConfig')->get('required_zip_csv_files'), true));
        $actualSisZipContents = $validSisZipCsvs->map(function ($csv) {
            return $csv['format'];
        });
        $missingCsvs = $requiredSisZipContents->diff($actualSisZipContents);

        if ($missingCsvs->isNotEmpty()) {
            event(new RequiredCsvFileMissingFromSisZip($missingCsvs));
            throw new SisFeedException('Required CSV file(s) ' . $missingCsvs->implode(', ') . ' not found in SIS Zip. (FAILED)');
        }

        return $validSisZipCsvs;
    }

    protected function checkCsvContents(\SplFileInfo $file)
    {
        $result = [];

        $csv = Reader::createFromPath($file->getRealPath(), 'r');
        $csv->setHeaderOffset(0);
        $result['row_count'] = count($csv);

        $sisFormats = config('canvas-feed.sis.acceptable_csv_fields');

        $result['format'] = false;
        foreach ($sisFormats as $format => $requiredFields) {
            if (count(array_diff($csv->getHeader(), $requiredFields)) == 0) {
                $result['format'] = $format;
                break;
            }
        }

        if ($result['format'] === false) {
            // not found
            event(new DetectedInvalidSisCsvFile($file->getRelativePathName()));
            throw new SisFeedException('Type of SIS File not found! Path: ' . $file->getRelativePathName() . ' not found to be of any known format.');
        }

        if ($result['row_count'] == 0) {
            \Log::channel('sis-feed')->info('SIS Type: ' . $result['format'] . ' contains zero rows (no data).');
        }

        if (! $this->expectsCsvFile($result['format'])) {
            event(new UnexpectedCsvFileContainedInSisZip($result['format']));
            throw new SisFeedException($result['format'] . ' CSV file found in SIS ZIP, but is not expected. (FAILED)');
        }

        return $result;
    }

    protected function expectsCsvFile(string $format)
    {
        return in_array($format, json_decode(app('dbConfig')->get('required_zip_csv_files'), true));
    }

    public function importToDatabase(SisZip $sisZip)
    {
        $sisCsvFileService = new SisCsvFileService;

        \DB::transaction(function () use ($sisZip, $sisCsvFileService) {
            $sisZip->status = 'importing';
            $sisZip->save();

            $sisZip->csvs->each(function ($csv) use ($sisCsvFileService) {
                $sisCsvFileService->importToDatabase($csv);
            });

            $sisZip->status = 'imported';
            $sisZip->save();
        });
    }

    public function getThresholdCheckResults(SisZip $sisZip)
    {
        $sisZipRepository = new SisZipRepository;

        $previousZip = $sisZipRepository->lastDiffed();

        $maxPercentChange = DbConfig::getConfig('sis_zip_max_change') * 100;

        $newContents = $sisZip->csv_contents;
        $oldContents = optional($previousZip)->csv_contents ?? '';

        return collect($newContents)->mapWithKeys(function ($number, $type) use ($oldContents, $maxPercentChange) {
            $oldNumber = $oldContents[$type] ?? 0;
            $difference = abs($oldNumber - $number);
            $percentChange = $oldNumber > 0 ? round($difference / $oldNumber * 100, 2) : 100;

            return [
                $type => [
                    'old_number'         => $oldNumber,
                    'current_number'     => $number,
                    'percent_change'     => $percentChange,
                    'max_percent_change' => $maxPercentChange,
                    'breached'           => $percentChange > $maxPercentChange
                ]
            ];
        });
    }

    public function purgeDatabase(Enumerable $zipsToPurge)
    {
        $tables = config('canvas-feed.sis.formats_to_diff');
        $repository = new SisZipRepository;

        return $zipsToPurge->filter(function ($zip) use ($tables, $repository) {
            try {
                \DB::beginTransaction();

                foreach ($tables as $table) {
                    $repository->purgeEntities($zip, $table);
                }
                $repository->markPurged($zip);

                \DB::commit();
                return true;
            } catch (\Exception $e) {
                \DB::rollBack();
                throw $e;
            }
        });
    }
}
