<?php

namespace App\Services\Sis;

use App\Repositories\Sis\SisEnrollmentRepository;

class SisEnrollmentService
{
    protected $repository;

    public function __construct()
    {
        $this->repository = new SisEnrollmentRepository;
    }

    public function getCountMetricsArrayForTermAndRole($zipId, $termSisId, $role)
    {
        $tally = $this->repository->tallyForTermAndRole($zipId, $termSisId, $role);

        $thresholds = [
            ['min' => 0, 'max' => 6],
            ['min' => 7, 'max' => 13],
            ['min' => 14, 'max' => 20],
            ['min' => 21, 'max' => 27],
            ['min' => 28, 'max' => 34],
            ['min' => 35, 'max' => 41],
            ['min' => 42, 'max' => 48],
            ['min' => 49, 'max' => 55],
            ['min' => 56, 'max' => null],
        ];

        $tallyGrouped = collect($thresholds)->mapWithKeys(function ($threshold) use ($termSisId, $role, $tally) {
            $thresholdString = (is_null($threshold['max'])) ?
                $threshold['min'] . '+' :
                $threshold['min'] . '-' . $threshold['max'];

            $key = 'enrollment_' . $role . '_' . $thresholdString . '_courses_' . $termSisId;

            $summable = (is_null($threshold['max'])) ?
                $tally->where('count', '>', $threshold['min']) :
                $tally->whereBetween('count', $threshold);

            return [$key => $summable->sum('number')];
        });

        return $tallyGrouped->toArray();
    }

    public function tallyTotalEnrollmentsForTermByRole($zipId, $termSisId)
    {
        $tally = $this->repository->tallyForTermByRole($zipId, $termSisId);
        return $tally->mapWithKeys(function ($group) use ($termSisId) {
            $key = 'enrollment_' . $group->role . '_total_' . $termSisId;
            return [$key => $group->count];
        })->toArray();
    }

    public function tallyUniqueEnrollmentsForTermByRole($zipId, $termSisId)
    {
        $tally = $this->repository->tallyForTermByRoleUnique($zipId, $termSisId);
        return $tally->mapWithKeys(function ($group) use ($termSisId) {
            $key = 'enrollment_' . $group->role . '_unique_' . $termSisId;
            return [$key => $group->count];
        })->toArray();
    }
}
