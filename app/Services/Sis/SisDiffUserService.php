<?php

namespace App\Services\Sis;

class SisDiffUserService extends BaseSisDiffEntityService
{
    protected $repositoryClass = 'App\\Repositories\\Sis\\SisDiffUserRepository';
    protected $table = 'sis_diff_users';
}
