<?php

namespace App\Services\Sis;

class SisDiffCourseService extends BaseSisDiffEntityService
{
    protected $repositoryClass = 'App\\Repositories\\Sis\\SisDiffCourseRepository';
    protected $table = 'sis_diff_courses';
}
