<?php

namespace App\Services\Sis;

use League\Csv\Writer;
use App\Models\Sis\SisDiff;
use Illuminate\Support\Collection;
use Illuminate\Support\Enumerable;
use App\Services\Sis\Interfaces\SisDiffEntityServiceInterface;

abstract class BaseSisDiffEntityService implements SisDiffEntityServiceInterface
{
    protected $sisDiff;
    protected $repository;
    protected $repositoryClass;
    protected $csvPath;
    protected $writer;
    protected $header = [];

    public $fieldsNotIncludedInDiff = [
        'id',
        'checksum',
        'sis_upload_id',
        'sis_zip_id',
        'sis_csv_file_id',
        'created_at'
    ];

    public function __construct(SisDiff $sisDiff, string $csvPath)
    {
        $this->sisDiff = $sisDiff;
        $this->repository = new $this->repositoryClass;

        $this->csvPath = $csvPath;
        $this->writer = Writer::createFromPath($csvPath);
    }

    public function createFromDiffResults(Collection $results)
    {
        $deleted = $this->processDeleted($results['deleted']);
        $active = $this->processActive($results['active']);
        $inactive = $this->processInactive($results['inactive']);

        if (count($this->header) === 0) {
            // there was nothing to read in
            return 0;
        }

        // read the CSV with LOAD DATA INFILE

        // set params
        $fieldList = implode(',', $this->header);
        $targetTable = config('database.connections.' . config('database.default') . '.prefix') . $this->table;
        $importTime = now()->toDateTimeString();
        $lineEnding = PHP_EOL;

        // perform the insert with LOAD DATA INFILE
        try {
            ini_set('auto_detect_line_endings', true);

            $sql = "LOAD DATA LOCAL INFILE '{$this->csvPath}' INTO TABLE " . $targetTable . " "
            . "FIELDS TERMINATED BY ',' "
            . "OPTIONALLY ENCLOSED BY '\"' "
            . "LINES TERMINATED BY '$lineEnding' "
            . "IGNORE 1 LINES "
            . "($fieldList) "
            . "SET created_at = '{$importTime}', "
            . "diff_id = {$this->sisDiff->id} "
            . ";";

            \DB::connection()->getpdo()->exec($sql);
        } catch (\Throwable $th) {
            \Log::channel('sis-feed')->error('Error importing diff CSV file to database', [
                'category'  => 'sis-feed',
                'operation' => 'diff',
                'result'    => 'error',
                'data'      => [
                    'csv_path' => $this->csvPath,
                    'message'  => $th->getMessage()
                ]
            ]);
            throw $th;
        }


        return $active + $deleted + $inactive;
    }

    // basic, can be overridden
    public function processDeleted(Enumerable $collection)
    {
        return $this->process($collection, 'deleted');
    }

    public function processActive(Enumerable $collection)
    {
        return $this->process($collection);
    }

    public function processInactive(Enumerable $collection)
    {
        return $this->process($collection);
    }

    // main handler
    public function process(Enumerable $collection, $status = null)
    {
        if ($collection->isEmpty()) {
            return 0;
        }

        // write header
        if (count($this->header) === 0) {
            $this->header = $collection->first()->toCsvHeader();
            $this->writer->insertOne($this->header);
        }

        // write rows
        $this->writer->insertAll($collection->map(function ($entity) use ($status) {
            if (!is_null($status)) {
                $entity->status = $status;
            }
            return $entity->toCsvRow();
        }));

        return $collection->count();
    }
}
