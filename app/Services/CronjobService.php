<?php

namespace App\Services;

use App\CcpsCore\CronjobMeta;

class CronjobService
{
    public function restartAllSisCrons()
    {
        // restart the crons
        $sisCrons = CronjobMeta::all()->filter(function ($meta) {
            $cron = new $meta->class;
            return in_array('sis-feed', $cron->getTags()) && in_array('core', $cron->getTags());
        });

        $sisCrons->each(function ($meta) {
            $meta->status = 'enabled';
            $meta->save();
        });
    }
}
