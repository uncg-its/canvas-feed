<?php

namespace App\Handlers;

use Carbon\Carbon;
use App\Models\Aux\Override;
use Uncgits\GrouperApi\Exceptions\ApiResultException;

class OverrideHandler
{
    protected $override;

    public function __construct(Override $override)
    {
        $this->override = $override;
    }

    public function cancel()
    {
        $success = true;

        try {
            \DB::beginTransaction();

            $this->override->canceled_at = Carbon::now()->toDateTimeString();
            $this->override->canceled_by_id = auth()->user()->id;
            $this->override->save();

            // take action in Grouper
            $targetGroupConfigKey = ($this->override->type == 'add')
                ? 'canvas-feed.aux.grouper.allow_group'
                : 'canvas-feed.aux.grouper.deny_group';

            $targetGroup = explode(':', config($targetGroupConfigKey));
            $targetGroupGroup = array_pop($targetGroup);
            $targetGroupStem = implode(':', $targetGroup);

            $result = \Grouper::prefetchSubjectIds([$this->override->canvas_user->uncgUsername])
                ->deleteMembers([$this->override->canvas_user->uncgUsername], $targetGroupStem, $targetGroupGroup);

            if ($result['response']['result'] === 'FAILURE') {
                throw new ApiResultException('Grouper API failure: ' . $result['response']['httpCode'] . ' - ' . $result['response']['resultCode'] . '; ' . $result['body']->WsAddMemberResults->results[0]->resultMetadata->resultCode);
            }


            \Log::channel('aux-feed')->info('Override canceled', [
                'category'  => 'model',
                'operation' => 'update',
                'result'    => 'success',
                'data'      => [
                    'override'     => $this->override,
                    'current_user' => auth()->user()
                ]
            ]);


            flash('Override canceled successfully. The user\'s status will be updated when the feed runs next.', 'success');
            \DB::commit();
        } catch (\Exception $e) {
            $success = false;
            \DB::rollBack();
            \Log::channel('aux-feed')->error('Error trying to cancel Override', [
                'category'  => 'model',
                'operation' => 'update',
                'result'    => 'error',
                'data'      => [
                    'override'     => $this->override,
                    'current_user' => auth()->user(),
                    'message'      => $e->getMessage()
                ]
            ]);
            flash('Error canceling override. Please contact an administrator.', 'danger');
        }

        return $success;
    }
}
