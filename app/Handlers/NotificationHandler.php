<?php

namespace App\Handlers;

use Carbon\Carbon;
use App\Models\Aux\Override;
use App\Mail\OverrideNotificationMail;
use App\Models\Aux\OverrideNotification;

class NotificationHandler
{
    protected $override;

    public function __construct(Override $override)
    {
        $this->override = $override;
    }

    public function send(int $interval, bool $manual = false)
    {
        $success = true;
        try {
            \DB::beginTransaction();

            // send notification. log that it was sent.
            $notification = new OverrideNotification;
            $notification->override_id = $this->override->id;
            $notification->interval_days = $interval;
            $notification->manual = $manual;
            $notification->created_at = Carbon::now()->toDateTimeString();
            $notification->save();

            $email = new OverrideNotificationMail($this->override, $interval);

            \Mail::queue($email);

            \Log::channel('aux-feed')->info('Queued override notification'. $this->override->id, [
                'category'  => 'email',
                'operation' => 'queue',
                'result'    => 'success',
                'data'      => [
                    'email'    => $email,
                    'interval' => $interval,
                    'override' => $this->override
                ]
            ]);
            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollBack();
            $success = false;
            \Log::channel('aux-feed')->error('Error queuing override notification', [
                'category'  => 'email',
                'operation' => 'queue',
                'result'    => 'error',
                'data'      => [
                    'email'    => $email ?? null,
                    'interval' => $interval,
                    'override' => $this->override,
                    'message'  => $e->getMessage()
                ]
            ]);
        }

        return $success;
    }
}
