<?php

namespace App\Cronjobs;

use App\Repositories\MetricsSnapshotRepository;
use App\Services\MetricsSnapshotService;
use Carbon\Carbon;
use Uncgits\Ccps\Helpers\CronjobResult;
use Uncgits\Ccps\Models\Cronjob;

class GenerateDailyMetricsSnapshot extends Cronjob
{
    protected $schedule = '0 1 * * *'; // default schedule (overridable in database)
    protected $display_name = 'Generate Daily Metrics Snapshot'; // default display name (overridable in database)
    protected $description = 'Generates a metrics snapshot for a given day (in the past, to ensure all reporting has been caught up).'; // default description name (overridable in database)
    protected $tags = ['metrics']; // default tags (overridable in database)

    protected function execute()
    {
        $repository = new MetricsSnapshotRepository;
        $service = new MetricsSnapshotService;

        // add one because we're executing this job at 1am
        $day = Carbon::now()->subDays(config('canvas-feed.metrics.generation_gap_days') + 1)->format('Y-m-d');
        \Log::channel('metrics')->info('Starting metrics generation for ' . $day, [
            'category'  => 'metrics',
            'operation' => 'generate',
            'result'    => 'starting',
            'data'      => [
                'day' => $day,
            ]
        ]);

        // does a snapshot already exist for that day?
        if ($repository->existsForDay($day)) {
            \Log::channel('metrics')->warning('Metrics already exist for this day', [
                'category'  => 'metrics',
                'operation' => 'generate',
                'result'    => 'skipped',
                'data'      => [
                    'day' => $day
                ]
            ]);
            return new CronjobResult(true);
        }

        $snapshot = $service->generateForDay($day);

        \Log::channel('metrics')->info('Metrics snapshot generated', [
            'category'  => 'metrics',
            'operation' => 'generate',
            'result'    => 'success',
            'data'      => [
                'day'         => $day,
                'snapshot_id' => $snapshot->id
            ]
        ]);

        return new CronjobResult(true);
    }
}
