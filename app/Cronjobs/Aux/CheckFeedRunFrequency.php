<?php

namespace App\Cronjobs\Aux;

use Carbon\Carbon;
use App\Models\Aux\Run;
use Uncgits\Ccps\Models\Cronjob;
use App\Events\FeedRunFrequencyBreach;
use Uncgits\Ccps\Helpers\CronjobResult;

class CheckFeedRunFrequency extends Cronjob
{
    protected $schedule = '30 0 * * *'; // default schedule (overridable in database)
    protected $display_name = 'CheckFeedRunFrequency'; // default display name (overridable in database)
    protected $description = 'Checks the date/time of the last Feed Run to be sure it is within threshold allowance.'; // default description name (overridable in database)
    protected $tags = ['aux-feed']; // default tags (overridable in database)

    protected function execute()
    {
        $runFrequencyThreshold = app('dbConfig')->get('max_hours_since_last_feed_run');

        $lastFeedRun = Run::orderBy('created_at', 'desc')->first();

        if (is_null($lastFeedRun)) {
            \Log::channel('aux-feed')->info('Canvas Aux Feed has never run. Skipping Run Frequency Check.', [
                'category'  => 'aux-feed',
                'operation' => 'frequency_check',
                'result'    => 'skipped',
                'data'      => []
            ]);
            return new CronjobResult(true);
        }

        $hoursAgo = Carbon::now()->diffInHours($lastFeedRun->created_at);

        if ($hoursAgo > $runFrequencyThreshold) {
            $message = 'WARNING: Canvas Aux Feed was last run ' . $hoursAgo
                . ' hour(s) ago, exceeding the threshold of ' . $runFrequencyThreshold;
            event(new FeedRunFrequencyBreach($message));
            \Log::channel('aux-feed')->warning($message, [
                'category'  => 'aux-feed',
                'operation' => 'frequency_check',
                'result'    => 'failure',
                'data'      => []
            ]);
            return new CronjobResult(true);
        }

        \Log::channel('aux-feed')->info('Canvas Aux Feed was last run ' . $hoursAgo
            . ' hour(s) ago, within threshold of ' . $runFrequencyThreshold, [
                'category'  => 'aux-feed',
                'operation' => 'frequency_check',
                'result'    => 'success',
                'data'      => []
            ]);
        return new CronjobResult(true);
    }
}
