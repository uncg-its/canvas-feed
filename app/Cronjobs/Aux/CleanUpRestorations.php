<?php

namespace App\Cronjobs\Aux;

use App\Models\Aux\LastEnrollment;
use App\Models\Aux\LastGroup;
use Uncgits\Ccps\Helpers\CronjobResult;
use Uncgits\Ccps\Models\Cronjob;

class CleanUpRestorations extends Cronjob
{
    protected $schedule = '0 * * * *'; // default schedule (overridable in database)
    protected $display_name = 'Clean up Restorations'; // default display name (overridable in database)
    protected $description = 'Purge unneeded data from the database regarding restored enrollments / groups for re-added users'; // default description name (overridable in database)
    protected $tags = ['aux-feed']; // default tags (overridable in database)

    protected function execute()
    {
        if (app('dbConfig')->get('aux_restore_data')) {
            $daysBack = app('dbConfig')->get('aux_restoration_cleanup_days', 7);
            $lowerBound = now()->subDays($daysBack);

            try {
                \DB::beginTransaction();
                $enrollmentsToDelete = LastEnrollment::where('restored_at', '<', $lowerBound)->get();
                $groupsToDelete = LastGroup::where('restored_at', '<', $lowerBound)->get();

                if ($enrollmentsToDelete->count() + $groupsToDelete->count() == 0) {
                    \Log::channel('aux-feed')->info('No old restoration data to delete', [
                        'category'  => 'aux-feed',
                        'operation' => 'cleanup',
                        'result'    => 'success',
                        'data'      => []
                    ]);

                    \DB::commit();
                    return new CronjobResult(true);
                }

                $enrollmentsToDelete->each(function ($enrollment) {
                    $enrollment->delete();
                });

                $groupsToDelete->each(function ($group) {
                    $group->delete();
                });

                \Log::channel('aux-feed')->info('Deleted old restoration data', [
                    'category'  => 'aux-feed',
                    'operation' => 'cleanup',
                    'result'    => 'success',
                    'data'      => [
                        'enrollment_ids' => $enrollmentsToDelete->pluck('id')->implode(','),
                        'group_ids'      => $groupsToDelete->pluck('id')->implode(','),
                    ]
                ]);

                \DB::commit();
            } catch (\Throwable $th) {
                \DB::rollBack();
                throw $th;
            }
            return new CronjobResult(true);
        }

        \Log::channel('aux-feed')->notice('Enrollment restoration is not enabled. Skipping database purge.', [
            'category'  => 'aux-feed',
            'operation' => 'cleanup',
            'result'    => 'skipped',
            'data'      => []
        ]);

        return new CronjobResult(true);
    }
}
