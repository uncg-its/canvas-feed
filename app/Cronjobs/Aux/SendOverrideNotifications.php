<?php

namespace App\Cronjobs\Aux;

use Carbon\Carbon;
use App\Models\Aux\Override;
use Uncgits\Ccps\Models\Cronjob;
use App\Handlers\NotificationHandler;
use Uncgits\Ccps\Helpers\CronjobResult;

class SendOverrideNotifications extends Cronjob
{
    protected $schedule = '0 23 * * *'; // default schedule (overridable in database)
    protected $display_name = 'SendOverrideNotifications'; // default display name (overridable in database)
    protected $description = 'Send notifications for active notifications to responsible parties'; // default description name (overridable in database)
    protected $tags = ['aux-feed']; // default tags (overridable in database)

    protected function execute()
    {
        $overrideIntervals = collect(explode(',', app('dbConfig')->get('notification_intervals')));
        if ($overrideIntervals->isEmpty()) {
            $message = 'Notification intervals have not been set in the app config.';
            \Log::channel('aux-feed')->error($message, [
                'category'  => 'aux-feed',
                'operation' => 'override_notification',
                'result'    => 'failure',
                'data'      => []
            ]);
            return new CronjobResult(false, $message);
        }

        $activeOverrides = Override::active()->with('notifications')->get();
        if ($activeOverrides->isEmpty()) {
            \Log::channel('aux-feed')->info('No active overrides. No notifications to send.', [
                'category'  => 'aux-feed',
                'operation' => 'override_notification',
                'result'    => 'skipped',
                'data'      => []
            ]);
            return new CronjobResult(true);
        }


        $today = Carbon::now();
        $activeOverrides->each(function ($override) use ($overrideIntervals, $today) {
            // check to see what notifications need to be sent, and if today is that day, then send it.
            $override->unsent_notification_intervals->each(function ($interval) use ($override, $today) {
                if ($today->format('Y-m-d') == $override->expires_at->subDays($interval)->format('Y-m-d')) {
                    $handler = new NotificationHandler($override);
                    $success = $handler->send($interval);
                    \Log::channel('aux-feed')->info('Sent override notification', [
                        'category'  => 'aux-feed',
                        'operation' => 'override_notification',
                        'result'    => 'success',
                        'data'      => [
                            'override' => $override
                        ]
                    ]);
                }
            });
        });

        return new CronjobResult(true);
    }
}
