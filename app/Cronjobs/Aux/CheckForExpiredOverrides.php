<?php

namespace App\Cronjobs\Aux;

use Carbon\Carbon;
use App\Models\Aux\Override;
use Uncgits\Ccps\Helpers\CronjobResult;
use Uncgits\Ccps\Models\Cronjob;
use Uncgits\Ccps\UserFeed\Batch;
use Uncgits\GrouperApiLaravel\Facades\Grouper;

class CheckForExpiredOverrides extends Cronjob
{
    protected $schedule = '*/30 * * * *'; // default schedule (overridable in database)
    protected $display_name = 'CheckForExpiredOverrides'; // default display name (overridable in database)
    protected $description = 'Checks for expired overrides and takes appropriate action'; // default description name (overridable in database)
    protected $tags = ['aux-feed']; // default tags (overridable in database)

    protected function execute()
    {
        $overrides = Override::expired()->unresolved()->get();

        if ($overrides->count() == 0) {
            \Log::channel('aux-feed')->info('No unresolved overrides found. Nothing to do.', [
                'category'  => 'aux-feed',
                'operation' => 'override_resolution',
                'result'    => 'skipped',
                'data'      => [
                    'unresolved_overrides' => 0
                ]
            ]);
            return new CronjobResult(true);
        }

        \Log::channel('aux-feed')->debug('Overrides to resolve: ' . $overrides->count());
        $overrides->each(function ($override) {
            try {
                \DB::beginTransaction();

                // prepare database updates
                $override->resolved_at = Carbon::now()->toDateTimeString();
                $override->save();

                // take action in Grouper
                $targetGroupConfigKey = ($override->type == 'add')
                    ? 'auf.grouper.allow_group'
                    : 'auf.grouper.deny_group';

                $targetGroup = explode(':', config($targetGroupConfigKey));
                $targetGroupGroup = array_pop($targetGroup);
                $targetGroupStem = implode(':', $targetGroup);
                Grouper::prefetchSubjectIds([$override->canvas_user->uncgUsername])
                    ->deleteMembers([$override->canvas_user->uncgUsername], $targetGroupStem, $targetGroupGroup);

                // check user's current status as of the last feed run - if they are not eligible any longer, then
                // we need to remove them now.

                // PUNTING this for now. Addresses edge cases only, where override expires without another feed run in between.
                // $latestData = Batch::latestDiffed()->user_data;
                // $userInfo = $latestData->filter(function ($item) use ($override) {
                //     return $item->data[config('auf.canvas.user_field_map.canvasId')] == strtoupper($override->canvas_user->uncgUsername);
                // });

                // if ($userInfo->isEmpty()) {
                //     // remove them from Canvas now
                //     $override->spawnAction('delete');
                // }

                \DB::commit();
                \Log::channel('aux-feed')->info('Resolved Override ' . $override->id, [
                    'category'  => 'aux-feed',
                    'operation' => 'override_resolution',
                    'result'    => 'success',
                    'data'      => [
                        'override'    => $override,
                        'canvas_user' => $override->canvas_user
                    ]
                ]);
                return new CronjobResult(true);
            } catch (\Throwable $th) {
                \DB::rollBack();
                // TODO: any expected failures we can catch?
                throw $th;
            }
        });

        return new CronjobResult(true);
    }
}
