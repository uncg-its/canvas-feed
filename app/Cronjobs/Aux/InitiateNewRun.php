<?php

namespace App\Cronjobs\Aux;

use App\Models\Aux\Run;
use App\CcpsCore\CronjobMeta;
use App\Constants\SuccessStatus;
use App\Models\Aux\UserDataDiff;
use Uncgits\Ccps\Models\Cronjob;
use App\Jobs\Aux\ParseDiffIntoActions;
use App\Events\Aux\DiffFailedValidation;
use Uncgits\Ccps\Helpers\CronjobResult;

class InitiateNewRun extends Cronjob
{
    protected $schedule = '* * * * *'; // default schedule (overridable in database)
    protected $display_name = 'InitiateNewRun'; // default display name (overridable in database)
    protected $description = 'Initates a new run of the Canvas Auxiliary Feed based on latest diff.'; // default description name (overridable in database)
    protected $tags = ['aux-feed', 'core']; // default tags (overridable in database)

    protected function execute()
    {
        // PHP memory limit and exec time overrides per User Feed
        if (!is_null($phpTempMemoryLimit = config('ccps-user-feed.php.temp_memory_limit'))) {
            ini_set('memory_limit', $phpTempMemoryLimit);
            \Log::channel('aux-feed')->debug('ParseDiffIntoActions: PHP Memory Limit overridden to: ' . $phpTempMemoryLimit);
        }

        if (!is_null($phpTempMaxExecutionTime = config('ccps-user-feed.php.temp_max_execution_time'))) {
            ini_set('max_execution_time', $phpTempMaxExecutionTime);
            \Log::channel('aux-feed')->debug('ParseDiffIntoActions: PHP Execution Time overridden to: ' . $phpTempMaxExecutionTime);
        }


        // if there is already a Run that is still in progress, abort.
        \Log::channel('aux-feed')->debug('Checking for Runs that are still processing or in-progress.');
        // $runsStillProcessing = Run::all()->whereIn('status', [SuccessStatus::PENDING, SuccessStatus::IN_PROGRESS]);
        // all() is too demanding on the system. theoretically we could use only the last Run as a gauge but this will be a good safety.
        $runsStillProcessing = Run::latest()
            ->limit(5)
            ->get()
            ->whereIn('status', [SuccessStatus::PENDING, SuccessStatus::IN_PROGRESS]);

        if ($runsStillProcessing->isNotEmpty()) {
            \Log::channel('aux-feed')->warning('One or more Runs are still pending or in-progress.', [
                'category'  => 'aux-feed',
                'operation' => 'initiate_run',
                'result'    => 'failure',
                'data'      => [
                    'runs_in_progress' => $runsStillProcessing->implode('id', ', ')
                ]
            ]);
            return new CronjobResult(true);
        }

        // check for earliest expanded diff in the database that was not part of a run.
        \Log::channel('aux-feed')->debug('Checking for earliest Diff that has not already been processed.');
        $diffToProcess = UserDataDiff::where('status', 'expanded')->doesntHave('runs')->oldest()->first();

        if (is_null($diffToProcess)) {
            \Log::channel('aux-feed')->info('No User Data Diffs to process. Exiting.', [
                'category'  => 'aux-feed',
                'operation' => 'initiate_run',
                'result'    => 'skipped',
                'data'      => []
            ]);
            return new CronjobResult(true);
        }

        // if found, proceed to validate data and kick off a new job run
        // validate the diff
        $invalidEntries = $diffToProcess->user_data_changes->where('ignored_at', null)->filter(function ($change) {
            return $change->data[config('canvas-feed.aux.canvas.primary_id')] == '' || $change->data[config('canvas-feed.aux.canvas.user_field_map.username')] == '';
        });

        // if validated then dispatch a new parse action
        if ($invalidEntries->isNotEmpty()) {
            \Log::channel('aux-feed')->error(
                'Invalid User Data Change(s) found in Diff! Cannot create new Run.',
                [
                    'category'  => 'aux-feed',
                    'operation' => 'initiate_run',
                    'result'    => 'failure',
                    'data'      => [
                        'invalid_changes' => $invalidEntries
                    ]
                ]
            );

            // record record as invalid
            $invalidEntries->each(function ($change) {
                $change->invalid = true;
                $change->save();
            });

            // stop crons
            CronjobMeta::all()->each(function ($cronjob) {
                $cron = new $cronjob->class;
                if (in_array('core', $cron->getTags())) {
                    $cronjob->status = 'disabled';
                    $cronjob->save();
                    \Log::channel('aux-feed')->info('Cronjob ' . $cronjob->class . ' stopped!', [
                        'category'  => 'cron',
                        'operation' => 'stop',
                        'result'    => 'success',
                        'data'      => [
                            'cronjob_meta' => $cronjob
                        ]
                    ]);
                }
            });

            event(new DiffFailedValidation($diffToProcess, $invalidEntries));

            return new CronjobResult(true);
        }

        dispatch(new ParseDiffIntoActions($diffToProcess));

        return new CronjobResult(true);
    }
}
