<?php

namespace App\Cronjobs;

use Uncgits\Ccps\Models\Cronjob;
use App\Services\MetricsSnapshotService;
use App\Repositories\MetricsSnapshotRepository;
use Uncgits\Ccps\Helpers\CronjobResult;

class PushDailyMetricsSnapshots extends Cronjob
{
    protected $schedule = '0 2 * * *'; // default schedule (overridable in database)
    protected $display_name = 'Push Daily Metrics Snapshots'; // default display name (overridable in database)
    protected $description = 'Pushes daily metrics snapshots to external service(s) as configured'; // default description name (overridable in database)
    protected $tags = ['metrics']; // default tags (overridable in database)

    protected function execute()
    {
        $repository = new MetricsSnapshotRepository;
        $service = new MetricsSnapshotService;

        // push anything not pushed yet
        $snapshotsToPush = $repository->unpushed();

        if ($snapshotsToPush->count() === 0) {
            \Log::channel('metrics')->info('No un-pushed metric snapshots exist.', [
                'category'  => 'metrics',
                'operation' => 'push',
                'result'    => 'skipped',
                'data'      => []
            ]);
            return new CronjobResult(true);
        }

        list($pushed, $failed) = $snapshotsToPush->partition(function ($snapshot) use ($service) {
            $method = 'pushTo' . \Str::studly($snapshot->push_destination);

            if (!method_exists($service, $method)) {
                $error = 'Unsupported metrics destination given in application configuration. Cannot proceed with push.';
                \Log::channel('metrics')->warning($error, [
                'category'  => 'metrics',
                'operation' => 'push',
                'result'    => 'failure',
                'data'      => [
                    'destination' => $snapshot->push_destination
                ]
            ]);

                return new CronjobResult(false, $error);
            }

            return $service->$method($snapshot);
        });

        if ($failed->isNotEmpty()) {
            $message = 'One or more snapshots failed to push properly';
            \Log::channel('metrics')->warning($message, [
                'category'  => 'metrics',
                'operation' => 'push',
                'result'    => 'failure',
                'data'      => [
                    'failed_snapshots' => $failed->pluck('id')
                ]
            ]);

            return new CronjobResult(false, $message);
        }

        \Log::channel('metrics')->info('Metrics snapshot push completed successfully.', [
            'category'  => 'metrics',
            'operation' => 'push',
            'result'    => 'success',
            'data'      => [
                'pushed_snapshots' => $pushed->pluck('id')
            ]
        ]);

        return new CronjobResult(true);
    }
}
