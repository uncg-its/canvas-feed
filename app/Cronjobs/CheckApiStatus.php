<?php

namespace App\Cronjobs;

use Carbon\Carbon;
use App\Models\ApiStatusCheck;
use Uncgits\Ccps\Helpers\CronjobResult;
use Uncgits\Ccps\Models\Cronjob;

class CheckApiStatus extends Cronjob
{
    protected $schedule = '*/5 * * * *'; // default schedule (overridable in database)
    protected $display_name = 'CheckApiStatus'; // default display name (overridable in database)
    protected $description = 'Checks Canvas API Status'; // default description name (overridable in database)
    protected $tags = ['canvas-feed']; // default tags (overridable in database)

    private $check;

    protected function execute()
    {
        try {
            \DB::beginTransaction();

            $this->check = new ApiStatusCheck;
            $this->check->created_at = Carbon::now();
            $this->check->up = true;

            $result = \CanvasApi::using('accounts')->getAccount(1);

            if ($result->getStatus() === 'success') {
                $this->check->save();
                \Log::channel('api')->info('Canvas API check successful.', [
                    'category'  => 'canvas-feed',
                    'operation' => 'api_check',
                    'result'    => 'success',
                    'data'      => [
                        'api_status_check' => $this->check
                    ]
                ]);

                \DB::commit();
                return new CronjobResult(true);
            }

            $this->check->up = false;
            $this->check->message = $result->getMessage();
            $this->check->save();
            \Log::channel('api')->info('Canvas API check failed.', [
                'category'  => 'canvas-feed',
                'operation' => 'api_check',
                'result'    => 'failure',
                'data'      => [
                    'api_status_check' => $this->check,
                    'message'          => $result->getMessage(),
                    'result'           => $result
                ]
            ]);

            \DB::commit();
        } catch (\Throwable $th) {
            \DB::rollBack();
            throw $th;
        }

        return new CronjobResult(true);
    }
}
