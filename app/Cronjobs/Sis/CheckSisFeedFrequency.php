<?php

namespace App\Cronjobs\Sis;

use Uncgits\Ccps\Models\Cronjob;
use App\Events\Sis\SisFeedFrequencyBreach;
use App\Repositories\Sis\SisZipRepository;
use App\Repositories\Sis\SisDiffRepository;
use Uncgits\Ccps\Helpers\CronjobResult;

class CheckSisFeedFrequency extends Cronjob
{
    protected $schedule = '0,30 0-4,8-23 * * *'; // default schedule (overridable in database)
    protected $display_name = 'Check SIS Feed Frequency'; // default display name (overridable in database)
    protected $description = 'Checks to ensure SIS Feed has been running at an acceptable frequency'; // default description name (overridable in database)
    protected $tags = ['sis-feed']; // default tags (overridable in database)

    protected function execute()
    {
        $sisZipRepository = new SisZipRepository;
        $sisDiffRepository = new SisDiffRepository;

        // CHECK 1: SIS ZIP frequency check

        $sisFileFrequencySeconds = app('dbConfig')->get('max_time_between_sis_zips');
        \Log::channel('sis-feed')->info('Checking for an imported SIS ZIP within timeframe threshold', [
            'category'  => 'sis-feed',
            'operation' => 'frequency-check',
            'result'    => 'setup',
            'data'      => [
                'threshold_seconds' => $sisFileFrequencySeconds
            ]
        ]);

        $sisZipsWithinThreshold = $sisZipRepository->withinLastSeconds($sisFileFrequencySeconds);

        if ($sisZipsWithinThreshold->isEmpty()) {
            $lastSeenZip = $sisZipRepository->latest();

            // log
            \Log::channel('sis-feed')->warning('SIS ZIP timeframe threshold breached', [
                'category'  => 'sis-feed',
                'operation' => 'frequency-check',
                'result'    => 'failure',
                'data'      => [
                    'threshold_seconds' => $sisFileFrequencySeconds,
                    'last_seen_zip'     => $lastSeenZip,
                ]
            ]);
            // alert
            event(new SisFeedFrequencyBreach('ZIP Frequency Breach: SIS ZIP expected within last ' . $sisFileFrequencySeconds . ' seconds; none found.'));
        } else {
            \Log::channel('sis-feed')->info('SIS ZIP timeframe threshold met', [
                'category'  => 'sis-feed',
                'operation' => 'frequency-check',
                'result'    => 'success',
                'data'      => [
                    'threshold_seconds'     => $sisFileFrequencySeconds,
                    'zips_within_threshold' => $sisZipsWithinThreshold->count(),
                ]
            ]);
        }

        // CHECK 2: SIS Diff Frequency check

        $sisDiffFrequencySeconds = app('dbConfig')->get('max_time_between_sis_diffs');
        \Log::channel('sis-feed')->info('Checking for a completed SIS Diff within timeframe threshold', [
            'category'  => 'sis-feed',
            'operation' => 'frequency-check',
            'result'    => 'setup',
            'data'      => [
                'threshold_seconds' => $sisDiffFrequencySeconds
            ]
        ]);

        $sisDiffsWithinThreshold = $sisDiffRepository->withinLastSeconds($sisDiffFrequencySeconds);

        if ($sisDiffsWithinThreshold->isEmpty()) {
            $latestDiff = $sisDiffRepository->latest();

            // log
            \Log::channel('sis-feed')->warning('SIS Diff creation timeframe threshold breached', [
                'category'  => 'sis-feed',
                'operation' => 'frequency-check',
                'result'    => 'failure',
                'data'      => [
                    'threshold_seconds' => $sisDiffFrequencySeconds,
                    'latest_diff'       => $latestDiff,
                ]
            ]);
            // alert
            event(new SisFeedFrequencyBreach('Diff Frequency Breach: SIS Diff expected within last ' . $sisDiffFrequencySeconds . ' seconds; none found.'));
        } else {
            \Log::channel('sis-feed')->info('SIS Diff timeframe threshold met', [
                'category'  => 'sis-feed',
                'operation' => 'frequency-check',
                'result'    => 'success',
                'data'      => [
                    'threshold_seconds'      => $sisDiffFrequencySeconds,
                    'diffs_within_threshold' => $sisDiffsWithinThreshold->count(),
                ]
            ]);
        }

        // CHECK 3: SIS Diff Upload Frequency check

        $sisDiffUploadFrequencySeconds = app('dbConfig')->get('max_time_between_sis_diff_uploads');
        \Log::channel('sis-feed')->info('Checking for an uploaded SIS Diff within timeframe threshold', [
            'category'  => 'sis-feed',
            'operation' => 'frequency-check',
            'result'    => 'setup',
            'data'      => [
                'threshold_seconds' => $sisDiffUploadFrequencySeconds
            ]
        ]);

        $sisDiffsWithinThreshold = $sisDiffRepository->uploadedWithinLastSeconds($sisDiffUploadFrequencySeconds);

        if ($sisDiffsWithinThreshold->isEmpty()) {
            $lastUploadedDiff = $sisDiffRepository->lastUploaded();

            // log
            \Log::channel('sis-feed')->warning('SIS Diff upload timeframe threshold breached', [
                'category'  => 'sis-feed',
                'operation' => 'frequency-check',
                'result'    => 'failure',
                'data'      => [
                    'threshold_seconds'  => $sisDiffUploadFrequencySeconds,
                    'last_uploaded_diff' => $lastUploadedDiff,
                ]
            ]);
            // alert
            event(new SisFeedFrequencyBreach('Diff Frequency Breach: SIS Diff upload expected within last ' . $sisDiffUploadFrequencySeconds . ' seconds; none found.'));
        } else {
            \Log::channel('sis-feed')->info('SIS Diff upload timeframe threshold met', [
                'category'  => 'sis-feed',
                'operation' => 'frequency-check',
                'result'    => 'success',
                'data'      => [
                    'threshold_seconds'      => $sisDiffUploadFrequencySeconds,
                    'diffs_within_threshold' => $sisDiffsWithinThreshold->count(),
                ]
            ]);
        }

        return new CronjobResult(true);
    }
}
