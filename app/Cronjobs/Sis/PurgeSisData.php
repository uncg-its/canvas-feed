<?php

namespace App\Cronjobs\Sis;

use Uncgits\Ccps\Models\Cronjob;
use App\Services\Sis\SisZipService;
use App\Services\Sis\SisDiffService;
use App\Repositories\Sis\SisZipRepository;
use App\Repositories\Sis\SisDiffRepository;
use Uncgits\Ccps\Helpers\CronjobResult;

class PurgeSisData extends Cronjob
{
    protected $schedule = '0 7,11,15,19 * * *'; // default schedule (overridable in database)
    protected $display_name = 'Purge SIS Data'; // default display name (overridable in database)
    protected $description = 'Purges SIS Data that is older than the threshold set in config.'; // default description name (overridable in database)
    protected $tags = ['sis-feed']; // default tags (overridable in database)

    protected $sisZipRepository;
    protected $sisDiffRepository;
    protected $sisZipService;
    protected $sisDiffService;

    protected function execute()
    {
        // setup
        $this->sisZipRepository = new SisZipRepository;
        $this->sisDiffRepository = new SisDiffRepository;
        $this->sisZipService = new SisZipService;
        $this->sisDiffService = new SisDiffService;

        // purge zip info from database
        $zipsToPurgeDb = $this->sisZipRepository->purgeable();
        if ($zipsToPurgeDb->isEmpty()) {
            \Log::channel('sis-feed')->info('No SIS ZIPs to purge.', [
                'category'  => 'sis-feed',
                'operation' => 'purge',
                'result'    => 'skipped',
                'data'      => []
            ]);
        } else {
            $purgedZips = $this->sisZipService->purgeDatabase($zipsToPurgeDb);
            \Log::channel('sis-feed')->info('SIS ZIP data purge complete.', [
                'category'  => 'sis-feed',
                'operation' => 'purge',
                'result'    => 'success',
                'data'      => [
                    'purged_zips' => $purgedZips->pluck('id')
                ]
            ]);
        }

        // purge diff info from database
        $diffsToPurgeDb = $this->sisDiffRepository->purgeable();
        if ($diffsToPurgeDb->isEmpty()) {
            \Log::channel('sis-feed')->info('No SIS Diffs to purge.', [
                'category'  => 'sis-feed',
                'operation' => 'purge',
                'result'    => 'skipped',
                'data'      => []
            ]);
        } else {
            $purgedDiffs = $this->sisDiffService->purgeDatabase($diffsToPurgeDb);
            \Log::channel('sis-feed')->info('SIS Diff data purge complete.', [
                'category'  => 'sis-feed',
                'operation' => 'purge',
                'result'    => 'success',
                'data'      => [
                    'purged_diffs' => $purgedDiffs->pluck('id')
                ]
            ]);
        }

        // purge zips from disk
        $daysOfZipsToKeep = app('dbConfig')->get('sis_retention_zip');
        if ($daysOfZipsToKeep !== 'forever') {
            $purgedZipsFromDisk = $this->sisZipService->purgeDisk('sis-zips', $daysOfZipsToKeep);
        }

        \Log::channel('sis-feed')->info('SIS ZIP disk purge complete.', [
            'category'  => 'sis-feed',
            'operation' => 'purge',
            'result'    => 'success',
            'data'      => [
                'days_to_keep' => $daysOfZipsToKeep,
                'purged_files' => $purgedZipsFromDisk ?? 'none',
            ]
        ]);


        // purge diffs from disk
        $daysOfDiffsToKeep = app('dbConfig')->get('diff_retention_zip');
        if ($daysOfDiffsToKeep !== 'forever') {
            $purgedDiffsFromDisk = $this->sisDiffService->purgeDisk('diff-zips', $daysOfDiffsToKeep);
        }

        \Log::channel('sis-feed')->info('SIS Diff disk purge complete.', [
            'category'  => 'sis-feed',
            'operation' => 'purge',
            'result'    => 'success',
            'data'      => [
                'days_to_keep' => $daysOfDiffsToKeep,
                'purged_files' => $purgedDiffsFromDisk ?? 'none',
            ]
        ]);

        return new CronjobResult(true);
    }
}
