<?php

namespace App\Cronjobs\Sis;

use Uncgits\Ccps\Models\Cronjob;
use App\Services\Sis\SisZipService;
use App\Services\Sis\SisCsvFileService;
use App\Repositories\Sis\SisZipRepository;
use App\Events\Sis\SisZipThresholdBreached;
use App\Repositories\Sis\SisDiffRepository;
use App\Events\Sis\SisCsvValidationErrorsFound;
use Uncgits\Ccps\Helpers\CronjobResult;

class ValidateImportedSisZip extends Cronjob
{
    protected $schedule = '*/5 0-4,7-23 * * *'; // default schedule (overridable in database)
    protected $display_name = 'SIS - Step 3 - Validate'; // default display name (overridable in database)
    protected $description = 'Validates imported SIS data'; // default description name (overridable in database)
    protected $tags = ['sis-feed', 'core']; // default tags (overridable in database)

    protected $sisZipRepository;
    protected $sisDiffRepository;
    protected $sisCsvFileService;
    protected $sisZipService;

    protected $zipToValidate;

    protected function execute()
    {
        // setup
        $this->sisZipRepository = new SisZipRepository;
        $this->sisDiffRepository = new SisDiffRepository;
        $this->sisCsvFileService = new SisCsvFileService;
        $this->sisZipService = new SisZipService;

        // is there another unresolved Hold?
        $heldZips = $this->sisZipRepository->getHeld();
        if ($heldZips->isNotEmpty()) {
            \Log::channel('sis-feed')->notice('One or more SIS Zips are held and need to be resolved before further validations can occur.', [
                'category'  => 'sis-feed',
                'operation' => 'validation',
                'result'    => 'canceled',
                'data'      => [
                    'held_zips' => $heldZips
                ]
            ]);

            return new CronjobResult(true);
        }

        $heldDiffs = $this->sisDiffRepository->getHeld();
        if ($heldDiffs->isNotEmpty()) {
            \Log::channel('sis-feed')->notice('One or more SIS Diffs are held and need to be resolved before further validations can occur.', [
                'category'  => 'sis-feed',
                'operation' => 'validation',
                'result'    => 'canceled',
                'data'      => [
                    'held_diffs' => $heldDiffs
                ]
            ]);

            return new CronjobResult(true);
        }


        $nextZipReadyForDiffing = $this->sisZipRepository->nextForDiffing();
        if (!is_null($nextZipReadyForDiffing) && !is_null($this->sisZipRepository->lastDiffed())) {
            \Log::channel('sis-feed')->notice('A validated SIS Zip has not been diffed yet. Diffing must finish before further validations can occur.', [
                'category'  => 'sis-feed',
                'operation' => 'validation',
                'result'    => 'canceled',
                'data'      => [
                    'next_zip_for_diffing' => $nextZipReadyForDiffing
                ]
            ]);

            return new CronjobResult(true);
        }

        // get earliest imported but not validated zip
        $this->zipToValidate = $this->sisZipRepository->nextForValidating();

        if (is_null($this->zipToValidate)) {
            \Log::channel('sis-feed')->info('No ZIP ready for validating.', [
                'category'  => 'sis-feed',
                'operation' => 'validation',
                'result'    => 'skipped',
                'data'      => []
            ]);
            return new CronjobResult(true);
        }

        \Log::channel('sis-feed')->debug('Pulled next zip for validating: ', ['zip_id' => $this->zipToValidate->id]);
        // update status to validation in progress
        $this->zipToValidate = $this->sisZipRepository->changeStatus($this->zipToValidate, 'validating');

        try {
            \DB::beginTransaction();

            \Log::channel('sis-feed')->info('Validating SIS Zip ' . $this->zipToValidate->id, [
                'category'  => 'sis-feed',
                'operation' => 'validation',
                'result'    => 'starting',
                'data'      => [
                    'sis_zip' => $this->zipToValidate
                ]
            ]);

            // now perform validation on any files that were included in the zip

            list($validCsvs, $invalidCsvs) = $this->zipToValidate->csvs->partition(function ($csv) {
                $validation = $this->sisCsvFileService->validate($csv);
                \Log::channel('sis-feed')->info('Validation of CSV file successful', [
                    'category'  => 'sis-feed',
                    'operation' => 'validation',
                    'result'    => 'success',
                    'data'      => [
                        'sis_csv_file'      => $csv,
                        'records_validated' => $csv->sis_entities()->count()
                    ]
                ]);

                return $validation->success;
            });

            if ($invalidCsvs->isNotEmpty()) {
                $invalidCsvs->each(function ($failed) {
                    event(new SisCsvValidationErrorsFound($failed->latestValidation));
                });

                \Log::channel('sis-feed')->warning('Validation of CSV file failed', [
                    'category'  => 'sis-feed',
                    'operation' => 'validation',
                    'result'    => 'failure',
                    'data'      => [
                        'validation_ids' => $invalidCsvs->pluck('latestValidation.id'),
                    ]
                ]);

                // what else?
                $this->zipToValidate = $this->sisZipRepository->changeStatus($this->zipToValidate, 'held');

                \DB::commit();
                return new CronjobResult(true);
            }

            // validate thresholds
            $failedThresholdChecks = $this->sisZipService->getThresholdCheckResults($this->zipToValidate)->filter(function ($result) {
                return $result['breached'];
            });

            if ($failedThresholdChecks->isNotEmpty()) {
                event(new SisZipThresholdBreached($this->zipToValidate, $failedThresholdChecks));

                \Log::channel('sis-feed')->warning('SIS CSV threshold check failed', [
                    'category'  => 'sis-feed',
                    'operation' => 'threshold-check',
                    'result'    => 'failure',
                    'data'      => [
                        'zip'                     => $this->zipToValidate,
                        'failed_threshold_checks' => $failedThresholdChecks
                    ]
                ]);

                $this->zipToValidate = $this->sisZipRepository->changeStatus($this->zipToValidate, 'held');

                \DB::commit();
                return new CronjobResult(true);
            }

            // we were successful in validating the contents and passing threshold checks
            $this->zipToValidate = $this->sisZipRepository->changeStatus($this->zipToValidate, 'validated');

            \Log::channel('sis-feed')->info('Successfully validated SIS Zip ' . $this->zipToValidate->id, [
                'category'  => 'sis-feed',
                'operation' => 'validation',
                'result'    => 'success',
                'data'      => [
                    'sis_zip'           => $this->zipToValidate,
                    'records_validated' => $validCsvs->mapWithKeys(function ($csv) {
                        return [$csv->format => $csv->row_count];
                    })
                ]
            ]);

            \DB::commit();
        } catch (\Throwable $th) {
            \DB::rollBack();
            $this->zipToValidate = $this->sisZipRepository->changeStatus($this->zipToValidate, 'imported');
            throw $th;
        }

        return new CronjobResult(true);
    }
}
