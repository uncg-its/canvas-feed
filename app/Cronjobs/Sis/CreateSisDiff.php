<?php

namespace App\Cronjobs\Sis;

use App\Helpers\MemoryHelper;
use Uncgits\Ccps\Models\Cronjob;
use App\Services\Sis\SisDiffService;
use App\Events\CcpsCore\CronjobStarted;
use App\Events\CcpsCore\CronjobFinished;
use App\Repositories\Sis\SisZipRepository;
use App\Repositories\Sis\SisDiffRepository;
use App\Events\Sis\SisDiffThresholdBreached;
use Uncgits\Ccps\Helpers\CronjobResult;

class CreateSisDiff extends Cronjob
{
    protected $schedule = '*/5 0-4,7-23 * * *'; // default schedule (overridable in database)
    protected $display_name = 'SIS - Step 4 - Diff'; // default display name (overridable in database)
    protected $description = 'Creates a SIS Diff from the latest imported data'; // default description name (overridable in database)
    protected $tags = ['sis-feed', 'core']; // default tags (overridable in database)

    protected $startTime;
    protected $diff;

    protected function preExecution()
    {
        // override
        $this->startTime = now();
        event(new CronjobStarted($this));
    }

    protected function execute()
    {
        ini_set('memory_limit', '1024M');
        ini_set('max_execution_time', '600');

        // setup
        $sisDiffRepository = new SisDiffRepository;
        $sisZipRepository = new SisZipRepository;

        $diffsPendingImport = $sisDiffRepository->pendingImport();
        if ($diffsPendingImport->isNotEmpty()) {
            \Log::channel('sis-feed')->notice('Cannot create new SIS diff while there is an existing diff ready for upload, or currently importing.', [
                'category'  => 'sis-feed',
                'operation' => 'diff',
                'result'    => 'canceled',
                'data'      => [
                    'diffs_pending_import' => $diffsPendingImport->pluck('id')
                ]
            ]);
            return new CronjobResult(true);
        }

        $heldDiffs = $sisDiffRepository->getHeld();
        if ($heldDiffs->isNotEmpty()) {
            \Log::channel('sis-feed')->notice('One or more SIS Diffs are held and need to be resolved before further validations can occur.', [
                'category'  => 'sis-feed',
                'operation' => 'validation',
                'result'    => 'canceled',
                'data'      => [
                    'held_diffs' => $heldDiffs
                ]
            ]);

            return new CronjobResult(true);
        }


        // are there any zips to be diffed?
        $nextZipToDiff = $sisZipRepository->nextForDiffing();
        if (is_null($nextZipToDiff)) {
            \Log::channel('sis-feed')->notice('Cannot create new SIS diff; no new valiated zip was found', [
                'category'  => 'sis-feed',
                'operation' => 'diff',
                'result'    => 'canceled',
                'data'      => []
            ]);
            return new CronjobResult(true);
        }

        $lastDiffedZip = $sisZipRepository->lastDiffed();
        if (is_null($lastDiffedZip)) {
            // there was no last diff. this must be the first one...
            $lastDiffedZip = $nextZipToDiff;
            $nextZipToDiff = $sisZipRepository->nextForDiffing(1); // get next one that is ready

            if (is_null($nextZipToDiff)) {
                \Log::channel('sis-feed')->notice('Only one validated zip exists. Cannot diff.', [
                    'category'  => 'sis-feed',
                    'operation' => 'diff',
                    'result'    => 'canceled',
                    'data'      => [
                        'old_zip_id' => $lastDiffedZip->id,
                    ]
                ]);
                return new CronjobResult(true);
            }
        }

        // is the last good zip the same as the current one? shouldn't be, but pulling this in here from old CST anyway.
        if ($lastDiffedZip->id == $nextZipToDiff->id) {
            \Log::channel('sis-feed')->notice('Latest validated zip is the same as the last successful one. No need to diff.', [
                'category'  => 'sis-feed',
                'operation' => 'diff',
                'result'    => 'canceled',
                'data'      => [
                    'old_zip_id' => $lastDiffedZip->id,
                    'new_zip_id' => $nextZipToDiff->id
                ]
            ]);
            return new CronjobResult(true);
        }

        // do we already have a diff between these two zips?
        $existingDiffWithTheseZips = $sisDiffRepository->existingBetweenZips($lastDiffedZip->id, $nextZipToDiff->id);
        if (!is_null($existingDiffWithTheseZips)) {
            \Log::channel('sis-feed')->notice('Diff already exists between these zips', [
                'category'  => 'sis-feed',
                'operation' => 'diff',
                'result'    => 'canceled',
                'data'      => [
                    'old_zip_id'    => $lastDiffedZip->id,
                    'new_zip_id'    => $nextZipToDiff->id,
                    'existing_diff' => $existingDiffWithTheseZips->id,
                ]
            ]);
            return new CronjobResult(true);
        }

        // create the diff in the database and the corresponding ZIP files.
        try {
            \DB::beginTransaction();

            $diffService = new SisDiffService;
            $this->diff = $diffService->createDiffFromSisZips($lastDiffedZip, $nextZipToDiff);

            $nextZipToDiff->status = 'diffed';
            $nextZipToDiff->save();

            // should come into play on the initial diff only.
            if ($lastDiffedZip->status !== 'diffed') {
                $lastDiffedZip->status = 'diffed';
                $lastDiffedZip->save();
            }

            // create the ZIP
            $zipPath = $diffService->createDiffZipFromDiff($this->diff);

            \Log::channel('sis-feed')->info('SIS diff and ZIP file created successfully', [
                'category'  => 'sis-feed',
                'operation' => 'diff',
                'result'    => 'success',
                'data'      => [
                    'diff'     => $this->diff,
                    'zip_path' => $zipPath
                ]
            ]);

            \DB::commit();
        } catch (\Throwable $th) {
            \DB::rollBack();
            throw $th;
        }

        // validate the contents against thresholds
        try {
            \DB::beginTransaction();

            $validationResult = $diffService->validateThresholds($this->diff);

            $breachedCsvs = $validationResult->filter(function ($result) {
                return $result['breached'];
            });

            if ($breachedCsvs->isNotEmpty()) {

                // alarm but don't abort entirely.
                event(new SisDiffThresholdBreached($this->diff, $breachedCsvs));
                \Log::channel('sis-feed')->warning('SIS diff threshold breached', [
                    'category'  => 'sis-feed',
                    'operation' => 'diff',
                    'result'    => 'breach',
                    'data'      => [
                        'diff'              => $this->diff,
                        'validation_result' => $validationResult,
                    ]
                ]);

                $this->diff->status = 'held';
                $this->diff->save();

                \DB::commit();
                return new CronjobResult(true);
            }

            // everything checks out.
            $this->diff->status = 'validated';
            $this->diff->save();

            \DB::commit();
        } catch (\Throwable $th) {
            \DB::rollBack();
            throw $th;
        }

        return new CronjobResult(true);
    }

    protected function postExecution()
    {
        // override
        event(new CronjobFinished($this));
        \Log::channel('sis-feed')->info('Diff creation statistics available', [
            'category'  => 'sis-feed',
            'operation' => 'diff',
            'result'    => 'success',
            'data'      => [
                'diff'        => $this->diff,
                'peak_memory' => MemoryHelper::getPeakMemoryUsage(),
                'time_taken'  => now()->diffInSeconds($this->startTime)
            ]
        ]);
    }
}
