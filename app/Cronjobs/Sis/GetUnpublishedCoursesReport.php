<?php

namespace App\Cronjobs\Sis;

use Uncgits\Ccps\Models\Cronjob;
use App\Repositories\Sis\UnpublishedCoursesReportRepository;
use Uncgits\Ccps\Helpers\CronjobResult;

class GetUnpublishedCoursesReport extends Cronjob
{
    protected $schedule = '0,30 0-4,8-23 * * *'; // default schedule (overridable in database)
    protected $display_name = 'Get Unpublished Courses Report'; // default display name (overridable in database)
    protected $description = 'Starts and/or check status of an existing Unpublished Courses Report'; // default description name (overridable in database)
    protected $tags = ['sis-feed','metrics']; // default tags (overridable in database)

    protected function execute()
    {
        $repository = new UnpublishedCoursesReportRepository;
        $unfinishedReports = $repository->unfinished();

        \Log::channel('sis-feed')->info('Retrieved ' . $unfinishedReports->count() . ' unfinished report(s) from DB', [
            'category'  => 'unpublished-courses-report',
            'operation' => 'get-unfinished',
            'result'    => 'success',
            'data'      => [
                'report_ids' => $unfinishedReports->pluck('id')
            ]
        ]);

        $rootAccountId = config('canvas-feed.canvas.root_account_id');

        $successfulCalls = $unfinishedReports->filter(function ($report) use ($rootAccountId, $repository) {
            $result = \CanvasApi::using('AccountReports')
                ->getStatusOfReport($rootAccountId, 'unpublished_courses_csv', $report->canvas_id);

            if ($result->getStatus() !== 'success') {
                \Log::channel('sis-feed')->warning('API call to check report status failed', [
                    'category'  => 'unpublished-courses-report',
                    'operation' => 'check-status',
                    'result'    => 'failure',
                    'data'      => [
                        'report' => $report,
                        'result' => $result,
                    ]
                ]);

                return new CronjobResult(false, 'API call to check report status failed.');
            }

            if ($result->getContent()->status === 'complete') {
                $report = $repository->markCompleted($report, $result);

                \Log::channel('sis-feed')->info('Unpublished Courses Report completed', [
                    'category'  => 'unpublished-courses-report',
                    'operation' => 'check-status',
                    'result'    => 'success',
                    'data'      => [
                        'report'      => $report,
                        'report_info' => $result->getContent(),
                    ]
                ]);
            }
        });

        $termsToReportOn = explode(',', app('dbConfig')->get('unpublished_courses_report_terms'));

        $termsResult = \CanvasApi::using('EnrollmentTerms')
            ->listEnrollmentTerms($rootAccountId);

        if ($termsResult->getStatus() !== 'success') {
            \Log::channel('sis-feed')->warning('API call to get enrollment terms failed', [
                'category'  => 'unpublished-courses-report',
                'operation' => 'get-terms',
                'result'    => 'failure',
                'data'      => [
                    'result' => $termsResult,
                ]
            ]);

            return new CronjobResult(false, 'API call to get enrollment terms failed.');
        }

        $termsBySisId = collect($termsResult->getContent())->keyBy('sis_term_id');

        foreach ($termsToReportOn as $termId) {
            // check to see if we should run another report. it is only necessary 1x daily.
            $lastReport = $repository->latestForTerm($termId);

            if (is_null($lastReport) || ! $lastReport->created_at->isToday()) {
                // we need a new report for this day and term

                if (!isset($termsBySisId[$termId])) {
                    \Log::channel('sis-feed')->warning('Invalid Enrollment Term specified in config', [
                        'category'  => 'unpublished-courses-report',
                        'operation' => 'check-status',
                        'result'    => 'failure',
                        'data'      => [
                            'term'   => $termId,
                        ]
                    ]);

                    return new CronjobResult(false, 'Invalid Enrollment Term specified in config');
                }

                $result = \CanvasApi::using('AccountReports')
                    ->addParameters([
                        'parameters' => [
                            'enrollment_term_id' => $termsBySisId[$termId]->id
                        ]
                    ])->startReport($rootAccountId, 'unpublished_courses_csv');

                if ($result->getStatus() !== 'success') {
                    \Log::channel('sis-feed')->warning('API call to create new report failed', [
                        'category'  => 'unpublished-courses-report',
                        'operation' => 'check-status',
                        'result'    => 'failure',
                        'data'      => [
                            'term'   => $termId,
                            'result' => $result,
                        ]
                    ]);

                    return new CronjobResult(false, 'API call to create new report failed.');
                }

                $report = $repository->createForTerm($termId, $result);

                \Log::channel('sis-feed')->info('Started new Unpublished Courses Report', [
                    'category'  => 'unpublished-courses-report',
                    'operation' => 'create',
                    'result'    => 'success',
                    'data'      => [
                        'term'   => $termId,
                        'report' => $report,
                        'date'   => now()->format('Y-m-d')
                    ]
                ]);
            } else {
                \Log::channel('sis-feed')->info('No new Unpublished Courses Report is necessary for today.', [
                    'category'  => 'unpublished-courses-report',
                    'operation' => 'create',
                    'result'    => 'skipped',
                    'data'      => [
                        'term'        => $termId,
                        'last_report' => $lastReport,
                        'date'        => now()->format('Y-m-d')
                    ]
                ]);
            }
        }

        return new CronjobResult(true);
    }
}
