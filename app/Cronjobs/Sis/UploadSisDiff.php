<?php

namespace App\Cronjobs\Sis;

use Uncgits\Ccps\Models\Cronjob;
use App\Events\Sis\SisUploadFailed;
use App\Exceptions\SisFeedException;
use App\Services\Sis\SisDiffService;
use App\Repositories\Sis\SisDiffRepository;
use Uncgits\Ccps\Helpers\CronjobResult;

class UploadSisDiff extends Cronjob
{
    protected $schedule = '*/5 0-4,7-23 * * *'; // default schedule (overridable in database)
    protected $display_name = 'SIS - Step 5 - Upload'; // default display name (overridable in database)
    protected $description = 'Uploads the created SIS Diff to Canvas via API'; // default description name (overridable in database)
    protected $tags = ['sis-feed', 'core']; // default tags (overridable in database)

    protected $sisDiffRepository;
    protected $sisDiffService;

    protected function execute()
    {
        // setup
        $this->sisDiffRepository = new SisDiffRepository;
        $this->sisDiffService = new SisDiffService;

        // determine which zip is next to upload
        $diffToUpload = $this->sisDiffRepository->nextForUploading();
        if (is_null($diffToUpload)) {
            \Log::channel('sis-feed')->notice('No SIS Diffs are ready for upload.', [
                'category'  => 'sis-feed',
                'operation' => 'upload',
                'result'    => 'canceled',
                'data'      => []
            ]);
            return new CronjobResult(true);
        }

        // is there an upload in progress?
        $unfinishedImports = $this->sisDiffRepository->unfinishedImports();
        if ($unfinishedImports->isNotEmpty()) {
            \Log::channel('sis-feed')->notice('An existing SIS Diff upload is still in progress.', [
                'category'  => 'sis-feed',
                'operation' => 'upload',
                'result'    => 'canceled',
                'data'      => [
                    'uploads_in_progress' => $unfinishedImports,
                ]
            ]);
            return new CronjobResult(true);
        }

        // move diff zip to temp storage
        $tempFilename = $diffToUpload->id . '___diff___temp.zip';
        $diff = \Storage::disk('temp')->put($tempFilename, \Storage::disk('sis-diffs')->get($diffToUpload->filename));
        \Log::channel('sis-feed')->debug('Created temporary diff file for ' . $diffToUpload->filename);

        try {
            $result = \CanvasApi::using('SisImports')
            ->urlEncodeParameters()
            ->addParameters(config('canvas-feed.sis.upload_config'))
            ->addMultipart([
                [
                    'name'     => 'attachment',
                    'contents' => fopen(\Storage::disk('temp')->path($tempFilename), 'r')
                ]
            ])->importSisData(config('canvas-feed.canvas.root_account_id'));

            if ($result->getStatus() !== 'success') {
                event(new SisUploadFailed($result));
                throw new SisFeedException('API error while importing SIS Diff: ' . $result->getLastResult()['reason']);
            }

            $diffToUpload = $this->sisDiffService->recordSuccessfulUpload($diffToUpload, $result->getContent()->id);

            \Log::channel('sis-feed')->info('SIS Diff uploaded to Canvas', [
                'category'  => 'sis-feed',
                'operation' => 'upload',
                'result'    => 'success',
                'data'      => [
                    'diff' => $diffToUpload
                ]
            ]);

            return new CronjobResult(true);
        } catch (SisFeedException $e) {
            return new CronjobResult(false, $e->getMessage()); // `finally` block will still be executed per https://www.php.net/manual/en/language.exceptions.php
        } catch (\Throwable $th) {
            throw $th;
        } finally {
            // remove temp file
            \Storage::disk('temp')->delete($tempFilename);
            \Log::channel('sis-feed')->debug('Deleted temporary diff file for ' . $diffToUpload->filename);
        }
    }
}
