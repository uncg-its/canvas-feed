<?php

namespace App\Cronjobs\Sis;

use App\Exceptions\SisFeedException;
use Uncgits\Ccps\Models\Cronjob;
use App\Services\Sis\SisZipService;
use Symfony\Component\Finder\Finder;
use App\Repositories\Sis\SisZipRepository;
use App\Repositories\Sis\SisCsvFileRepository;
use Uncgits\Ccps\Helpers\CronjobResult;

class MoveSisZipIntoStorage extends Cronjob
{
    protected $schedule = '*/5 0-4,7-23 * * *'; // default schedule (overridable in database)
    protected $display_name = 'SIS - Step 1 - Move'; // default display name (overridable in database)
    protected $description = 'Moves SIS files from deposit location into local storage'; // default description name (overridable in database)
    protected $tags = ['sis-feed', 'core']; // default tags (overridable in database)

    protected $sisZipRepository;
    protected $sisZipService;
    protected $sisCsvFileRepository;

    protected function execute()
    {
        // setup
        $this->sisZipRepository = new SisZipRepository;
        $this->sisZipService = new SisZipService;
        $this->sisCsvFileRepository = new SisCsvFileRepository;

        $originalSisPath = config('canvas-feed.sis.original_path');
        \Log::channel('sis-feed')->debug('Looking for SIS original files at: ' . $originalSisPath);

        $sisZipOriginals = new Finder;
        $sisZipOriginals->files()
                ->in($originalSisPath)
                ->sortByName();
        \Log::channel('sis-feed')->debug('Found ' . count($sisZipOriginals) . ' SIS original file(s) to move.');

        $processed = 0;

        foreach ($sisZipOriginals as $sisZipOriginal) {
            \DB::beginTransaction();

            try {
                try {
                    $destinationName = $this->sisZipService->moveIntoStorage($sisZipOriginal);
                } catch (SisFeedException $e) {
                    \Log::channel('sis-feed')->notice($e->getMessage(), [
                            'category'  => 'sis-feed',
                            'operation' => 'move',
                            'result'    => 'skipped',
                            'data'      => []
                        ]);
                    continue;
                }

                // extract to folder
                $extractionFolder = substr($destinationName, 0, -4);
                $extractedCsvs = $this->sisZipService->extractCsvsToFolder($sisZipOriginal, $extractionFolder);

                // validate extracted contents
                $validSisZipCsvs = $this->sisZipService->validateExtractedCsvs($extractedCsvs, $extractionFolder);

                // create SIS Zip in DB
                $sisZipModel = $this->sisZipRepository->createAfterImport($destinationName, $validSisZipCsvs);

                // create SIS CSVs in DB
                $validSisZipCsvs->each(function ($csv) use ($sisZipModel, $extractionFolder) {
                    $created = $this->sisCsvFileRepository->createAfterExtraction($csv, $sisZipModel, $extractionFolder);
                    \Log::channel('sis-feed')->debug('Created SIS CSV in database', ['csv' => $created]);
                });

                // extraction finished, clean up
                \Log::channel('sis-feed')->debug('Extraction Finished - ' . $extractionFolder . '/' . $destinationName);
                unlink($originalSisPath . '/' . $sisZipOriginal->getRelativePathName());
                \Log::channel('sis-feed')->debug('Cleaned up original file ' . $sisZipOriginal->getRelativePathName());

                \DB::commit();
                $processed++;

                if ($processed >= config('canvas-feed.sis.zip_move_limit')) {
                    \Log::channel('sis-feed')->info('Moved ' . $processed . ' ZIP files; single-transaction limit reached.', [
                        'category'  => 'sis-feed',
                        'operation' => 'move',
                        'result'    => 'info',
                        'data'      => []
                    ]);

                    return new CronjobResult(true);
                }
                // so that we don't end up with two zips created at the same time. will make sorting easier later.
                sleep(1);
            } catch (\Throwable $th) {
                \DB::rollBack();
                throw $th;
            }
        }

        return new CronjobResult(true);
    }
}
