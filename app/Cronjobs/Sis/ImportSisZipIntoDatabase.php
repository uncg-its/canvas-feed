<?php

namespace App\Cronjobs\Sis;

use Uncgits\Ccps\Models\Cronjob;
use App\Services\Sis\SisZipService;
use Uncgits\Ccps\Helpers\CronjobResult;
use App\Repositories\Sis\SisZipRepository;

class ImportSisZipIntoDatabase extends Cronjob
{
    protected $schedule = '*/5 0-4,7-23 * * *'; // default schedule (overridable in database)
    protected $display_name = 'SIS - Step 2 - Import'; // default display name (overridable in database)
    protected $description = 'Imports SIS Zip data from storage into database'; // default description name (overridable in database)
    protected $tags = ['sis-feed', 'core']; // default tags (overridable in database)

    protected $sisZipRepository;
    protected $sisZipService;

    protected function execute()
    {
        // setup
        $this->sisZipRepository = new SisZipRepository;
        $this->sisZipService = new SisZipService;

        // get next file to import
        $zipToImport = $this->sisZipRepository->nextForImporting();

        \Log::channel('sis-feed')->debug('Pulled next zip for importing: ', ['zip_id' => optional($zipToImport)->id ?? 'none']);

        if (!is_null($zipToImport)) {
            $result = $this->sisZipService->importToDatabase($zipToImport);
        }

        return new CronjobResult(true);
    }
}
