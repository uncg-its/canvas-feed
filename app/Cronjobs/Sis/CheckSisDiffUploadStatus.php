<?php

namespace App\Cronjobs\Sis;

use Uncgits\Ccps\Models\Cronjob;
use App\Services\Sis\SisDiffService;
use Uncgits\Ccps\Helpers\CronjobResult;
use App\Events\Sis\AbnormalSisDiffImport;
use App\Repositories\Sis\SisDiffRepository;

class CheckSisDiffUploadStatus extends Cronjob
{
    protected $schedule = '*/5 0-4,7-23 * * *'; // default schedule (overridable in database)
    protected $display_name = 'SIS - Step 6 - Check Upload Status'; // default display name (overridable in database)
    protected $description = 'Checks the status of a SIS Diff upload'; // default description name (overridable in database)
    protected $tags = ['sis-feed', 'core']; // default tags (overridable in database)

    protected $sisDiffRepository;
    protected $sisDiffService;

    protected function execute()
    {
        // setup
        $this->sisDiffRepository = new SisDiffRepository;
        $this->sisDiffService = new SisDiffService;

        $diffsToCheck = $this->sisDiffRepository->currentlyImporting();

        if ($diffsToCheck->isEmpty()) {
            // nothing to do
            \Log::channel('sis-feed')->info('No SIS diff uploads to check.', [
                'category'  => 'sis-feed',
                'operation' => 'upload-status',
                'result'    => 'canceled',
                'data'      => []
            ]);

            return new CronjobResult(true);
        }

        $diffsToCheck->each(function ($diff) {
            $statusResult = \CanvasApi::using('SisImports')->getSisImportStatus(config('canvas-feed.canvas.root_account_id'), $diff->canvas_id);

            if ($statusResult->getStatus() !== 'success') {
                \Log::channel('sis-feed')->warning('Error while retrieving SIS Diff status from Canvas API', [
                    'category'  => 'sis-feed',
                    'operation' => 'upload-status',
                    'result'    => 'error',
                    'data'      => [
                        'diff'   => $diff,
                        'result' => $statusResult->getLastResult(),
                        'body'   => $statusResult->getContent(),
                    ]
                ]);

                return new CronjobResult(false, 'API error. Please check logs.');
            }

            // get the status
            $status = $statusResult->getContent();
            if ($status->progress != 100 && !$this->sisDiffService->diffImportedAbnormally($diff)) {
                \Log::channel('sis-feed')->notice('SIS Diff not finished importing yet.', [
                    'category'  => 'sis-feed',
                    'operation' => 'upload-status',
                    'result'    => 'canceled',
                    'data'      => [
                        'diff'             => $diff,
                        'current_progress' => $status->progress
                    ]
                ]);
                return new CronjobResult(true);
            }

            // completed
            $diff = $this->sisDiffService->recordImportResult($diff, $status->workflow_state);

            \Log::channel('sis-feed')->info('Import status for SIS Diff ' . $diff->id . ' updated', [
                'category'  => 'sis-feed',
                'operation' => 'upload-status',
                'result'    => 'success',
                'data'      => [
                    'diff'   => $diff,
                    'status' => $status->workflow_state
                ]
            ]);


            // check the status and take action as necessary
            if ($this->sisDiffService->diffImportedAbnormally($diff)) {
                event(new AbnormalSisDiffImport($diff));
            }
        });

        return new CronjobResult(true);
    }
}
