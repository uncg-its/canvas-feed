<?php

namespace App\Notifications;

use App\ApiOutage;
use App\ApiStatusCheck;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Uncgits\Ccps\Messages\GoogleChatMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\SlackMessage;

class CanvasApiUpNotification extends Notification
{
    protected $check;
    protected $outage;

    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(ApiStatusCheck $check, ApiOutage $outage)
    {
        $this->check = $check;
        $this->outage = $outage;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return $notifiable->channel;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if ($notifiable->type == 'sms') {
            return $this->toSms($notifiable);
        }

        return (new MailMessage)
            ->markdown('emails.canvas-api-up-notification', ['outage' => $this->outage]);
    }

    public function toGoogleChat($notifiable)
    {
        return (new GoogleChatMessage)
            ->content('NOTICE: Canvas API is up. Resolved Outage ID: ' . $this->outage->id)
            ->to($notifiable->key);
    }

    public function toSms($notifiable)
    {
        return (new MailMessage)
            ->markdown('emails.plaintext.canvas-api-up-notification', ['outage' => $this->outage]);
    }

    public function toSlack($notifiable)
    {
        // is the proxy needed?
        $proxyValue = config('ccps.http_proxy.enabled')
            ? config('ccps.http_proxy.host') . ':' . config('ccps.http_proxy.port')
            : '';

        return (new SlackMessage)
            ->http(['proxy' => $proxyValue])
            ->content('NOTICE: Canvas API is up. Resolved Outage ID: ' . $this->outage->id);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
