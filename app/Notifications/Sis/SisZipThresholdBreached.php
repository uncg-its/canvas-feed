<?php

namespace App\Notifications\Sis;

use App\Models\Sis\SisZip;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Uncgits\Ccps\Messages\GoogleChatMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Support\Collection;

class SisZipThresholdBreached extends Notification
{
    use Queueable;

    protected $sisZip;
    protected $failedThresholdChecks;
    protected $shortMessage;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(SisZip $sisZip, Collection $failedThresholdChecks)
    {
        $this->sisZip = $sisZip;
        $this->failedThresholdChecks = $failedThresholdChecks;

        $stats = $failedThresholdChecks->map(function ($result, $type) {
            return $type . ': ' . $result['percent_change'] . '% change (' . $result['current_number'] . ' records vs. previous value of ' . $result['old_number'] . ')';
        })->implode('; ');

        $this->shortMessage = 'SIS Zip ' . $sisZip->id . ' failed validation threshold check. ' . $stats;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return $notifiable->channel;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if ($notifiable->type == 'sms') {
            return $this->toSms($notifiable);
        }

        return (new MailMessage)
            ->markdown('emails.sis-zip-threshold-breached', [
                'sisZip'                => $this->sisZip,
                'failedThresholdChecks' => $this->failedThresholdChecks
            ]);
    }

    public function toGoogleChat($notifiable)
    {
        return (new GoogleChatMessage)
            ->content($this->shortMessage)
            ->to($notifiable->key);
    }

    public function toSms($notifiable)
    {
        return (new MailMessage)
            ->markdown('emails.plaintext.sis-zip-threshold-breached', [
                'message' => $this->shortMessage
            ]);
    }

    public function toSlack($notifiable)
    {
        // is the proxy needed?
        $proxyValue = config('ccps.http_proxy.enabled')
            ? config('ccps.http_proxy.host') . ':' . config('ccps.http_proxy.port')
            : '';

        return (new SlackMessage)
            ->http(['proxy' => $proxyValue])
            ->content($this->shortMessage);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
