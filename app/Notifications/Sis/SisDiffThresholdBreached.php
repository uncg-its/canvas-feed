<?php

namespace App\Notifications\Sis;

use App\Models\Sis\SisDiff;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Collection;
use Illuminate\Notifications\Notification;
use Uncgits\Ccps\Messages\GoogleChatMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\SlackMessage;

class SisDiffThresholdBreached extends Notification
{
    use Queueable;

    protected $sisDiff;
    protected $breachedCsvs;
    protected $shortMessage;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(SisDiff $sisDiff, Collection $breachedCsvs)
    {
        $this->sisDiff = $sisDiff;
        $this->breachedCsvs = $breachedCsvs;

        $stats = $breachedCsvs->map(function ($result, $type) {
            return $type . ': ' . $result['changes'] . ' changes / ' . $result['threshold'] . ' allowed';
        })->implode('; ');

        $this->shortMessage = 'SIS Diff ' . $sisDiff->id . ' failed validation threshold check. ' . $stats;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return $notifiable->channel;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if ($notifiable->type == 'sms') {
            return $this->toSms($notifiable);
        }

        return (new MailMessage)
            ->markdown('emails.sis-diff-threshold-breached', [
                'diff'             => $this->sisDiff,
                'breachedCsvs'     => $this->breachedCsvs
            ]);
    }

    public function toGoogleChat($notifiable)
    {
        return (new GoogleChatMessage)
            ->content($this->shortMessage)
            ->to($notifiable->key);
    }

    public function toSms($notifiable)
    {
        return (new MailMessage)
            ->markdown('emails.plaintext.sis-diff-threshold-breached', [
                'message' => $this->shortMessage
            ]);
    }

    public function toSlack($notifiable)
    {
        // is the proxy needed?
        $proxyValue = config('ccps.http_proxy.enabled')
            ? config('ccps.http_proxy.host') . ':' . config('ccps.http_proxy.port')
            : '';

        return (new SlackMessage)
            ->http(['proxy' => $proxyValue])
            ->content($this->shortMessage);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
