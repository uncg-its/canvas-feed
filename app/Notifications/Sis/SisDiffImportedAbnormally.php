<?php

namespace App\Notifications\Sis;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Uncgits\Ccps\Messages\GoogleChatMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\SlackMessage;

class SisDiffImportedAbnormally extends Notification
{
    protected $message;

    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($message)
    {
        $this->message = $message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return $notifiable->channel;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if ($notifiable->type == 'sms') {
            return $this->toSms($notifiable);
        }

        return (new MailMessage)
            ->markdown('emails.sis-diff-imported-abnormally', ['message' => $this->message]);
    }

    public function toGoogleChat($notifiable)
    {
        return (new GoogleChatMessage)
            ->content($this->message)
            ->to($notifiable->key);
    }

    public function toSms($notifiable)
    {
        return (new MailMessage)
            ->markdown('emails.plaintext.sis-diff-imported-abnormally', ['message' => $this->message]);
    }

    public function toSlack($notifiable)
    {
        // is the proxy needed?
        $proxyValue = config('ccps.http_proxy.enabled')
            ? config('ccps.http_proxy.host') . ':' . config('ccps.http_proxy.port')
            : '';

        return (new SlackMessage)
            ->http(['proxy' => $proxyValue])
            ->content($this->message);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
