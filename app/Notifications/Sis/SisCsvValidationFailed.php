<?php

namespace App\Notifications\Sis;

use Illuminate\Bus\Queueable;
use App\Models\Sis\SisCsvFileValidation;
use Illuminate\Notifications\Notification;
use Uncgits\Ccps\Messages\GoogleChatMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\SlackMessage;

class SisCsvValidationFailed extends Notification
{
    protected $validation;
    protected $shortMessage;

    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(SisCsvFileValidation $validation)
    {
        $this->validation = $validation;
        $this->shortMessage = 'SIS CSV file validation has failed. Details: ' . url(route('sis.csv-file-validations.show', $validation));
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return $notifiable->channel;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if ($notifiable->type == 'sms') {
            return $this->toSms($notifiable);
        }

        return (new MailMessage)
            ->markdown('emails.sis-csv-validation-failed', ['validation' => $this->validation]);
    }

    public function toGoogleChat($notifiable)
    {
        return (new GoogleChatMessage)
            ->content($this->shortMessage)
            ->to($notifiable->key);
    }

    public function toSms($notifiable)
    {
        return (new MailMessage)
            ->markdown('emails.plaintext.sis-csv-validation-failed', ['message' => $this->shortMessage]);
    }

    public function toSlack($notifiable)
    {
        // is the proxy needed?
        $proxyValue = config('ccps.http_proxy.enabled')
            ? config('ccps.http_proxy.host') . ':' . config('ccps.http_proxy.port')
            : '';

        return (new SlackMessage)
            ->http(['proxy' => $proxyValue])
            ->content($this->shortMessage);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
