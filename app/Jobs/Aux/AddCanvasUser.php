<?php

namespace App\Jobs\Aux;

use Carbon\Carbon;
use App\Models\Aux\Action;
use App\Models\Aux\Override;
use App\Traits\LogsApiCalls;
use Illuminate\Bus\Queueable;
use App\Models\Aux\CanvasUser;
use App\Constants\SuccessStatus;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Events\Aux\FeedActionFailed;
use App\Exceptions\AuxFeedException;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class AddCanvasUser implements ShouldQueue
{
    protected $canvasUserTemplate;
    protected $feedAction;

    protected $callResult;
    protected $callStatus;

    public $retryAfter = 30;

    use Dispatchable, InteractsWithQueue, Queueable, LogsApiCalls;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Action $feedAction)
    {
        $this->onQueue('api');
        $this->feedAction = $feedAction;
        $this->canvasUserTemplate = $feedAction->userTemplate;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Redis::throttle('aux-feed')
            ->allow(config('canvas-feed.aux.queue.aux-feed.throttle_job_limit'))
            ->every(config('canvas-feed.aux.queue.aux-feed.throttle_timespan_seconds'))
            ->then(function () {
                // if this user has an active override in place, we should not perform the add.
                if ($this->feedAction->event != 'override') {
                    $activeOverridesForUser = Override::active()->where('canvas_user_id', $this->canvasUserTemplate->primaryId)->get();
                    if ($activeOverridesForUser->count() > 0) {
                        \Log::channel('aux-feed')->info('User ' . $this->canvasUserTemplate->getUsername() . ' has an active override - Add action canceled.', [
                            'category'  => 'aux-feed',
                            'operation' => 'add_user',
                            'result'    => 'skipped',
                            'data'      => [
                                'user_template' => $this->canvasUserTemplate,
                                'reason'        => 'active_override'
                            ]
                        ]);
                        return;
                    }
                }

                try {
                    // first transaction
                    \DB::transaction(function () {
                        // set Action to in-progress
                        if ($this->feedAction->status_id != SuccessStatus::IN_PROGRESS) {
                            $this->feedAction->status_id = SuccessStatus::IN_PROGRESS;
                            $this->feedAction->save();
                        }
                    });

                    // second transaction
                    \DB::beginTransaction();

                    $canvasUser = CanvasUser::withTrashed()->find($this->canvasUserTemplate->primaryId);
                    if (!is_null($canvasUser)) {
                        // they exist and should be un-trashed
                        $canvasUser->restore();
                    } else {
                        // create locally first so we can only do API stuff if it was successful
                        $canvasUser = $this->canvasUserTemplate->createCanvasUserLocal();
                    }



                    // set success status for Call

                    $this->callStatus = SuccessStatus::SUCCESS;
                    $this->feedAction->status_id = SuccessStatus::SUCCESS;
                    $this->feedAction->completed_at = Carbon::now()->toDateTimeString();
                    $this->feedAction->save();

                    // add user via API last, after other DB stuff - this will be the same regardless of whether the user
                    // ever existed in Canvas before (e.g. is a restore() vs a new add)
                    $this->callResult = $this->canvasUserTemplate->createCanvasUserApi();

                    // success?
                    if ($this->callResult->getStatus() !== 'success') {
                        $this->callStatus = SuccessStatus::FAILURE;

                        // set retry-after if we got a 429 from the Canvas API
                        $lastCall = $this->callResult->getLastCall();
                        $code = $lastCall['response']['code'];
                        if ($code == 403) {
                            // TODO: exponential backoff?
                            $this->retryAfter = config('canvas-feed.aux.queue.aux-feed.retry_delay');
                        }

                        \Log::channel('aux-feed')->error('Error while making API call to Canvas', [
                            'category'  => 'aux-feed',
                            'operation' => 'add_user',
                            'result'    => 'failure',
                            'data'      => [
                                'user_template' => $this->canvasUserTemplate,
                                'message'       => $lastCall['response']['reason'],
                                'retry-after'   => $this->retryAfter,
                            ]
                        ]);

                        $this->logCall();
                        throw new AuxFeedException('Unsuccessful attempt to add user. Check logs for more details'); // job should fail and be retried
                    }

                    // it was successful!
                    \DB::commit();

                    // restore enrollments - outside of transaction
                    if (app('dbConfig')->get('aux_restore_data')) {
                        \Log::channel('aux-feed')->info('Queueing job to restore enrollments for ' . $canvasUser->primaryId);
                        dispatch(new RestoreUserEnrollments($canvasUser));
                    }

                    // restore groups - outside of transaction
                    if (app('dbConfig')->get('aux_restore_data')) {
                        \Log::channel('aux-feed')->info('Queueing job to restore groups for ' . $canvasUser->primaryId);
                        dispatch(new RestoreUserGroups($canvasUser));
                    }


                    // log the call
                    $this->logCall();

                    // report to the user audit trail log
                    if ($this->feedAction->actionable_type == Override::class) {
                        \Log::channel('user-audit-trail')->info('User ' . $canvasUser->uncgUsername . ' was created in Canvas via override.', [
                            'category'  => 'aux-feed',
                            'operation' => 'add_user',
                            'result'    => 'success',
                            'data'      => [
                                'user_template' => $this->canvasUserTemplate,
                                'via'           => 'override'
                            ]
                        ]);
                    } else {
                        \Log::channel('user-audit-trail')->info('User ' . $canvasUser->uncgUsername . ' was created in Canvas.', [
                            'category'  => 'aux-feed',
                            'operation' => 'add_user',
                            'result'    => 'success',
                            'data'      => [
                                'user_template' => $this->canvasUserTemplate,
                                'via'           => 'feed'
                            ]
                        ]);
                    }
                } catch (\Exception $e) {
                    \DB::rollBack();
                    \Log::channel('aux-feed')->critical('Error during AddCanvasUser process', [
                        'category'  => 'aux-feed',
                        'operation' => 'add_user',
                        'result'    => 'error',
                        'data'      => [
                            'user_template' => $this->canvasUserTemplate,
                            'message'       => $e->getMessage(),
                        ]
                    ]);

                    if (!is_null($this->callResult)) {
                        $this->logCall();
                    }


                    throw $e; // job should fail and be retried
                }
            }, function () {
                // could not obtain lock
                return $this->release(config('canvas-feed.aux.queue.aux-feed.throttle_release_seconds'));
            });
    }

    public function failed(\Exception $exception)
    {
        $this->feedAction->status_id = SuccessStatus::FAILURE;
        $this->feedAction->save();

        \DB::commit();

        event(new FeedActionFailed($this->feedAction, $exception->getMessage()));
    }
}
