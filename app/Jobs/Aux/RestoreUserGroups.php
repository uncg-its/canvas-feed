<?php

namespace App\Jobs\Aux;

use App\Events\Aux\RestorationFailed;
use Illuminate\Bus\Queueable;
use App\Models\Aux\CanvasUser;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class RestoreUserGroups implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $canvasUser;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(CanvasUser $canvasUser)
    {
        $this->onQueue('api');
        $this->canvasUser = $canvasUser;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $failures = $this->canvasUser->groups_to_restore->filter(function ($group) {
            try {
                $this->canvasUser->restoreGroup($group);
                $group->restored_at = now();
                $group->save();

                \Log::channel('aux-feed')->info('Restored group after re-add', [
                    'category'  => 'aux-feed',
                    'operation' => 'restore_groups',
                    'result'    => 'success',
                    'data'      => [
                        'canvas_user' => $this->canvasUser,
                        'group'       => $group,
                    ]
                ]);

                return false;
            } catch (\Throwable $th) {
                \Log::channel('aux-feed')->error('Error while restoring groups for user ' . $this->canvasUser->primary_id, [
                    'category'  => 'aux-feed',
                    'operation' => 'restore_groups',
                    'result'    => 'error',
                    'data'      => [
                        'canvas_user' => $this->canvasUser,
                        'group'       => $group,
                        'message'     => $th->getMessage(),
                    ]
                ]);

                return true;
            }
        });

        if ($failures->isNotEmpty()) {
            \Log::channel('aux-feed')->notice('Restoring groups for user ' . $this->canvasUser->primary_id . ' was not successful.', [
                'category'  => 'aux-feed',
                'operation' => 'restore_groups',
                'result'    => 'failure',
                'data'      => [
                    'canvas_user'        => $this->canvasUser,
                    'failed_groups'      => $failures
                ]
            ]);

            event(RestorationFailed('groups', $this->canvasUser, $failures));
        }
    }
}
