<?php

namespace App\Jobs\Aux;

use Carbon\Carbon;
use App\Models\Aux\Run;
use Illuminate\Bus\Queueable;
use App\Models\Aux\CanvasUser;
use App\Models\Aux\FeedChange;
use App\Models\Aux\UserDataDiff;
use App\Models\Aux\CanvasUserTemplate;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ParseDiffIntoActions implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The Run to be referenced
     *
     * @var UserDataDiff
     */
    private $run;
    protected $diffToProcess;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(UserDataDiff $diffToProcess)
    {
        $this->onQueue('aux-feed');
        $this->diffToProcess = $diffToProcess;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // PHP memory limit and exec time overrides per User Feed
        if (!is_null($phpTempMemoryLimit = config('ccps-user-feed.php.temp_memory_limit'))) {
            ini_set('memory_limit', $phpTempMemoryLimit);
            \Log::channel('aux-feed')->debug('ParseDiffIntoActions: PHP Memory Limit overridden to: ' . $phpTempMemoryLimit);
        }

        if (!is_null($phpTempMaxExecutionTime = config('ccps-user-feed.php.temp_max_execution_time'))) {
            ini_set('max_execution_time', $phpTempMaxExecutionTime);
            \Log::channel('aux-feed')->debug('ParseDiffIntoActions: PHP Execution Time overridden to: ' . $phpTempMaxExecutionTime);
        }

        try {
            \DB::beginTransaction();

            $this->run = Run::create([
                'user_data_diff_id' => $this->diffToProcess->id,
                'diff_info'         => json_encode([
                    'metrics'      => $this->diffToProcess->metrics,
                    'summary'      => $this->diffToProcess->data_summary,
                    'change_count' => $this->diffToProcess->user_data_changes->count()
                ]),
                'created_at'        => Carbon::now()->toDateTimeString(),
            ]);

            $this->run->user_data_diff->user_data_changes->each(function ($item) {
                if (!is_null($item->ignored_at)) {
                    \Log::channel('aux-feed')->notice('Skipping ignored Change.', [
                        'category'  => 'aux-feed',
                        'operation' => 'parse_diff',
                        'result'    => 'skipped',
                        'data'      => [
                            'user_data_change' => $item,
                        ]
                    ]);
                    return;
                }

                // if user does not have an email address in Grouper, it is not eligible for Canvas.
                if (empty($item->data[config('canvas-feed.aux.canvas.user_field_map.email')])) {
                    \Log::channel('aux-feed')->notice('Skipping change without valid email.', [
                        'category'  => 'aux-feed',
                        'operation' => 'parse_diff',
                        'result'    => 'skipped',
                        'data'      => [
                            'user_data_change' => $item,
                        ]
                    ]);
                    return;
                }


                // parse change into Canvas User Template model
                $canvasUserTemplate = new CanvasUserTemplate($item);

                $change = new FeedChange;
                $change->run_id = $this->run->id;
                $change->user_data_change_id = $item->id;
                $change->canvas_user_id = $canvasUserTemplate->primaryId;
                $change->user_template = $canvasUserTemplate;
                $change->created_at = Carbon::now()->toDateTimeString();

                // for double-checking purposes, get the user from local DB (if they exist)
                $localUser = CanvasUser::find($canvasUserTemplate->primaryId);

                // determine action type, and save the FeedChange. Observers will handle the rest.
                if (!isset($item->changed_data['exists_in_source'])) {
                    // UPDATE the user (or add if they don't exist locally)
                    $change->type = is_null($localUser) ? 'add' : 'update';
                    $change->save();
                    return;
                }

                // we know source is set
                if (!$item->changed_data['exists_in_source']) {
                    if (is_null($localUser)) {
                        // the user doesn't exist locally... so we don't need to do anything.
                        return;
                    }

                    // DELETE the user
                    $change->type = 'delete';
                    $change->save();
                } else {
                    // UPDATE the user (or add if they don't exist locally)
                    $change->type = is_null($localUser) ? 'add' : 'update';
                    $change->save();
                }
            });
            \DB::commit();
            return;
        } catch (\Exception $e) {
            \DB::rollBack();
            \Log::channel('aux-feed')->error('Error during diff parsing', [
                'category'  => 'aux-feed',
                'operation' => 'parse_diff',
                'result'    => 'error',
                'data'      => [
                    'diff_id' => $this->diffToProcess->id,
                    'message' => $e->getMessage(),
                ]
            ]);
            throw $e;
        }
    }
}
