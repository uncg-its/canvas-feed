<?php

namespace App\Jobs\Aux;

use Illuminate\Bus\Queueable;
use App\Models\Aux\CanvasUser;
use App\Models\Aux\CanvasUserTemplate;
use App\Models\Aux\UserImport;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Uncgits\CanvasApi\Exceptions\CanvasApiException;

class ProcessUserImport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $import;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(UserImport $import)
    {
        $this->import = $import;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // for each user given, check Canvas status and create a Canvas User Template from that user. Then save it locally.
        $message = [
            'added'     => [],
            'duplicate' => [],
            'error'     => [],
        ];

        $localUsers = CanvasUser::all();

        collect((json_decode($this->import->sis_login_ids, true)))->each(function ($id) use (&$message, $localUsers) {
            if ($localUsers->has($id)) {
                $message['duplicate'][] = $id;
                return;
            }

            try {
                $user = CanvasUserTemplate::createFromApi('sis_login_id:' . $id);
                $user->createCanvasUserLocal();
                $message['added'][] = $id;
            } catch (CanvasApiException $e) {
                $message['error'][] = $id;
            }

            return;
        });

        $this->import->message = json_encode($message);
        $this->import->completed_at = now();
        $this->import->save();

        return;
    }
}
