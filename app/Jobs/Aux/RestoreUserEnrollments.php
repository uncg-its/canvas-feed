<?php

namespace App\Jobs\Aux;

use Illuminate\Bus\Queueable;
use App\Models\Aux\CanvasUser;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class RestoreUserEnrollments implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $canvasUser;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(CanvasUser $canvasUser)
    {
        $this->onQueue('api');
        $this->canvasUser = $canvasUser;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $failures = $this->canvasUser->enrollments_to_restore->filter(function ($enrollment) {
            try {
                $this->canvasUser->restoreEnrollment($enrollment);
                $enrollment->restored_at = now();
                $enrollment->save();

                \Log::channel('aux-feed')->info('Restored enrollment after re-add', [
                    'category'  => 'aux-feed',
                    'operation' => 'restore_enrollments',
                    'result'    => 'success',
                    'data'      => [
                        'canvas_user' => $this->canvasUser,
                        'enrollment'  => $enrollment,
                    ]
                ]);

                return false;
            } catch (\Throwable $th) {
                \Log::channel('aux-feed')->error('Error while restoring enrollment for user ' . $this->canvasUser->primary_id, [
                    'category'  => 'aux-feed',
                    'operation' => 'restore_enrollments',
                    'result'    => 'error',
                    'data'      => [
                        'canvas_user' => $this->canvasUser,
                        'enrollment'  => $enrollment,
                        'message'     => $th->getMessage(),
                    ]
                ]);

                return true;
            }
        });

        if ($failures->isNotEmpty()) {
            \Log::channel('aux-feed')->notice('Restoring enrollments for user ' . $this->canvasUser->primary_id . ' was not successful.', [
                'category'  => 'aux-feed',
                'operation' => 'restore_enrollments',
                'result'    => 'failure',
                'data'      => [
                    'canvas_user'        => $this->canvasUser,
                    'failed_enrollments' => $failures
                ]
            ]);

            event(RestorationFailed('enrollments', $this->canvasUser, $failures));
        }
    }
}
