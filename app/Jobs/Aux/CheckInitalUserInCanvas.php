<?php

namespace App\Jobs\Aux;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CheckInitalUserInCanvas implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $primary_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($primary_id)
    {
        $this->primary_id = $primary_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // this is a user that was given to us as a user who should be in Canvas.

        // query Grouper for the user information and create a new User Template.

        // get user's current Canvas status

        // update or create the record locally.
    }
}
