<?php

namespace App\Jobs\Aux;

use Carbon\Carbon;
use App\Models\Aux\Action;
use App\Traits\LogsApiCalls;
use Illuminate\Bus\Queueable;
use App\Models\Aux\CanvasUser;
use App\Constants\SuccessStatus;
use App\Events\Aux\FeedActionFailed;
use App\Exceptions\AuxFeedException;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class UpdateCanvasUser implements ShouldQueue
{
    protected $canvasUserTemplate;
    protected $feedAction;

    protected $callResult;
    protected $callStatus;

    public $retryAfter = 30;

    use Dispatchable, InteractsWithQueue, Queueable, LogsApiCalls;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Action $feedAction)
    {
        $this->onQueue('api');
        $this->feedAction = $feedAction;
        $this->canvasUserTemplate = $feedAction->userTemplate;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Redis::throttle('aux-feed')
            ->allow(config('canvas-feed.aux.queue.aux-feed.throttle_job_limit'))
            ->every(config('canvas-feed.aux.queue.aux-feed.throttle_timespan_seconds'))
            ->then(function () {
                // make call and update user list
                try {
                    // first transaction
                    \DB::transaction(function () {
                        // set Action to in-progress
                        if ($this->feedAction->status_id != SuccessStatus::IN_PROGRESS) {
                            $this->feedAction->status_id = SuccessStatus::IN_PROGRESS;
                            $this->feedAction->save();
                        }
                    });

                    // second transaction
                    \DB::beginTransaction();

                    $user = CanvasUser::findOrFail($this->canvasUserTemplate->primaryId);
                    $userData = json_decode($user->data, true);
                    $user->data = json_encode(array_merge($userData, $this->canvasUserTemplate->getChangedData()->toArray()));

                    $user->save();

                    // make call
                    $this->callResult = $this->canvasUserTemplate->updateCanvasUserApi();

                    // set success status for Call
                    $this->callStatus = SuccessStatus::SUCCESS;

                    // update action status
                    $this->feedAction->status_id = SuccessStatus::SUCCESS;
                    $this->feedAction->completed_at = Carbon::now()->toDateTimeString();
                    $this->feedAction->save();

                    if ($this->canvasUserTemplate->getChangedData() != $this->canvasUserTemplate->getOldData()) {
                        \Log::channel('user-audit-trail')->info('User ' . $user->uncgUsername . ' was updated in Canvas', [
                            'category'  => 'aux-feed',
                            'operation' => 'update_user',
                            'result'    => 'success',
                            'data'      => [
                                'user_template' => $this->canvasUserTemplate,
                            ]
                        ]);
                    }

                    // success?
                    if ($this->callResult->getStatus() !== 'success') {
                        \DB::rollBack();
                        $this->callStatus = SuccessStatus::FAILURE;

                        // set retry-after if we got a 429 from the Canvas API
                        $lastCall = $this->callResult->getLastCall();
                        $code = $lastCall['response']['code'];
                        if ($code == 403) {
                            // TODO: exponential backoff?
                            $this->retryAfter = config('canvas-feed.aux.queue.aux-feed.retry_delay');
                        }

                        \Log::channel('aux-feed')->error('Error while making API call to Canvas', [
                            'category'  => 'aux-feed',
                            'operation' => 'update_user',
                            'result'    => 'failure',
                            'data'      => [
                                'user_template' => $this->canvasUserTemplate,
                                'message'       => $lastCall['response']['reason'],
                                'retry-after'   => $this->retryAfter,
                            ]
                        ]);

                        $this->logCall();
                        throw new AuxFeedException('Unsuccessful attempt to update user. Check logs for more details'); // job should fail and be retried
                    }

                    // it was successful!
                    \DB::commit();
                    // log the call
                    $this->logCall();

                    \Log::channel('user-audit-trail')->info('User ' . $user->uncgUsername . ' was updated in Canvas.', [
                        'category'  => 'aux-feed',
                        'operation' => 'update_user',
                        'result'    => 'success',
                        'data'      => [
                            'user_template' => $this->canvasUserTemplate,
                            'via'           => 'feed'
                        ]
                    ]);
                } catch (\Exception $e) {
                    \Log::channel('aux-feed')->critical('Error during UpdateCanvasUser process', [
                        'category'  => 'aux-feed',
                        'operation' => 'update_user',
                        'result'    => 'error',
                        'data'      => [
                            'user_template' => $this->canvasUserTemplate,
                            'message'       => $e->getMessage(),
                        ]
                    ]);

                    \DB::commit(); // commit call info

                    throw $e; // job should fail and be retried
                }
            }, function () {
                // could not obtain lock
                return $this->release(config('canvas-feed.aux.queue.aux-feed.throttle_release_seconds'));
            });
    }

    public function failed(\Exception $exception)
    {
        $this->feedAction->status_id = SuccessStatus::FAILURE;
        $this->feedAction->save();

        \DB::commit();

        event(new FeedActionFailed($this->feedAction, $exception->getMessage()));
    }
}
