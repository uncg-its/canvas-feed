<?php

namespace App\Jobs\Aux;

use Carbon\Carbon;
use App\Models\Aux\Action;
use App\Models\Aux\Override;
use App\Traits\LogsApiCalls;
use App\Models\Aux\LastGroup;
use Illuminate\Bus\Queueable;
use App\Models\Aux\CanvasUser;
use App\Constants\SuccessStatus;
use App\Models\Aux\LastEnrollment;
use App\Events\Aux\FeedActionFailed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class DeleteCanvasUser implements ShouldQueue
{
    protected $canvasUserTemplate;
    protected $feedAction;

    protected $callResult;
    protected $callStatus;

    public $retryAfter = 30;

    use Dispatchable, InteractsWithQueue, Queueable, LogsApiCalls;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Action $feedAction)
    {
        $this->onQueue('api');
        $this->feedAction = $feedAction;
        $this->canvasUserTemplate = $feedAction->userTemplate;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Redis::throttle('aux-feed')
            ->allow(config('canvas-feed.aux.queue.aux-feed.throttle_job_limit'))
            ->every(config('canvas-feed.aux.queue.aux-feed.throttle_timespan_seconds'))
            ->then(function () {
                // if this user has an active override in place, we should not perform the remove.
                if ($this->feedAction->event != 'override') {
                    $activeOverridesForUser = Override::active()->where('canvas_user_id', $this->canvasUserTemplate->primaryId)->get();
                    if ($activeOverridesForUser->count() > 0) {
                        \Log::channel('aux-feed')->info('User ' . $this->canvasUserTemplate->getUsername() . ' has an active override - Add action canceled.', [
                            'category'  => 'aux-feed',
                            'operation' => 'delete_user',
                            'result'    => 'skipped',
                            'data'      => [
                                'user_template' => $this->canvasUserTemplate,
                                'reason'        => 'active_override'
                            ]
                        ]);
                        return;
                    }
                }

                try {
                    // first transaction
                    \DB::transaction(function () {
                        // set Action to in-progress
                        if ($this->feedAction->status_id != SuccessStatus::IN_PROGRESS) {
                            $this->feedAction->status_id = SuccessStatus::IN_PROGRESS;
                            $this->feedAction->save();
                        }
                    });

                    // second - delete user
                    \DB::beginTransaction();

                    $canvasUser = CanvasUser::findOrFail($this->canvasUserTemplate->primaryId);

                    // set success status for Call
                    $this->callStatus = SuccessStatus::SUCCESS;

                    $canvasUser->delete();


                    $this->feedAction->status_id = SuccessStatus::SUCCESS;
                    $this->feedAction->completed_at = Carbon::now()->toDateTimeString();
                    $this->feedAction->save();

                    // store last enrollments for user
                    collect($this->canvasUserTemplate->getLastEnrollments())->each(function ($enrollment) {
                        $lastEnrollment = new LastEnrollment;
                        $lastEnrollment->canvas_user_id = $this->canvasUserTemplate->primaryId;
                        $lastEnrollment->action_id = $this->feedAction->id;
                        $lastEnrollment->enrollment_id = $enrollment->id;
                        $lastEnrollment->course_id = $enrollment->course_id;
                        $lastEnrollment->type = $enrollment->type;
                        $lastEnrollment->course_section_id = $enrollment->course_section_id;
                        $lastEnrollment->root_account_id = $enrollment->root_account_id;
                        $lastEnrollment->enrollment_state = $enrollment->enrollment_state;
                        $lastEnrollment->role_id = $enrollment->role_id;
                        $lastEnrollment->limit_privileges_to_course_section = $enrollment->limit_privileges_to_course_section;
                        $lastEnrollment->created_at = now();

                        $lastEnrollment->save();
                    });

                    // store last groups for user
                    collect($this->canvasUserTemplate->getLastGroups())->each(function ($group) {
                        $lastGroup = new LastGroup;
                        $lastGroup->canvas_user_id = $this->canvasUserTemplate->primaryId;
                        $lastGroup->action_id = $this->feedAction->id;
                        $lastGroup->group_id = $group->id;
                        $lastGroup->join_level = $group->join_level;
                        $lastGroup->group_category_id = $group->group_category_id;
                        $lastGroup->course_id = $group->course_id;
                        $lastGroup->concluded = $group->concluded;
                        $lastGroup->created_at = now();

                        $lastGroup->save();
                    });


                    // do API call last, after DB stuff has succeeded
                    $this->callResult = $this->canvasUserTemplate->deleteCanvasUserApi();

                    // report to the user audit trail log
                    \Log::channel('user-audit-trail')->info('User ' . $canvasUser->uncgUsername . ' was deleted from Canvas.', [
                        'category'  => 'aux-feed',
                        'operation' => 'delete_user',
                        'result'    => 'success',
                        'data'      => [
                            'user_template' => $this->canvasUserTemplate,
                        ]
                    ]);

                    // success?
                    if ($this->callResult->getStatus() !== 'success') {
                        $this->callStatus = SuccessStatus::FAILURE;

                        // set retry-after if we got a 403 from the Canvas API
                        $lastCall = $this->callResult->getLastCall();
                        $code = $lastCall['response']['code'];
                        if ($code == 403) {
                            // TODO: exponential backoff?
                            $this->retryAfter = config('canvas-feed.aux.queue.aux-feed.retry_delay');
                        }

                        \Log::channel('aux-feed')->error('Error while making API call to Canvas', [
                            'category'  => 'aux-feed',
                            'operation' => 'delete_user',
                            'result'    => 'failure',
                            'data'      => [
                                'user_template' => $this->canvasUserTemplate,
                                'message'       => $lastCall['response']['reason'],
                                'retry-after'   => $this->retryAfter,
                            ]
                        ]);

                        throw new AuxFeedException('Unsuccessful attempt to delete user. Check logs for more details'); // job should fail and be retried
                    }

                    // it was successful!
                    \DB::commit();
                    // log the call
                    $this->logCall();

                    // report to the user audit trail log
                    if ($this->feedAction->actionable_type == Override::class) {
                        \Log::channel('user-audit-trail')->info('User ' . $canvasUser->uncgUsername . ' was deleted in Canvas via override.', [
                            'category'  => 'aux-feed',
                            'operation' => 'delete_user',
                            'result'    => 'success',
                            'data'      => [
                                'user_template' => $this->canvasUserTemplate,
                                'via'           => 'override'
                            ]
                        ]);
                    } else {
                        \Log::channel('user-audit-trail')->info('User ' . $canvasUser->uncgUsername . ' was deleted in Canvas.', [
                            'category'  => 'aux-feed',
                            'operation' => 'delete_user',
                            'result'    => 'success',
                            'data'      => [
                                'user_template' => $this->canvasUserTemplate,
                                'via'           => 'feed'
                            ]
                        ]);
                    }
                } catch (\Exception $e) {
                    \DB::rollBack();
                    \Log::channel('aux-feed')->critical('Error during DeleteCanvasUser process', [
                        'category'  => 'aux-feed',
                        'operation' => 'delete_user',
                        'result'    => 'error',
                        'data'      => [
                            'user_template' => $this->canvasUserTemplate,
                            'message'       => $e->getMessage(),
                        ]
                    ]);

                    if (!is_null($this->callResult)) {
                        $this->logCall();
                    }

                    throw $e; // job should fail and be retried
                }
            }, function () {
                // could not obtain lock
                return $this->release(config('canvas-feed.aux.queue.aux-feed.throttle_release_seconds'));
            });
    }

    public function failed(\Exception $exception)
    {
        $this->feedAction->status_id = SuccessStatus::FAILURE;
        $this->feedAction->save();

        \DB::commit();

        event(new FeedActionFailed($this->feedAction, $exception->getMessage()));
    }
}
